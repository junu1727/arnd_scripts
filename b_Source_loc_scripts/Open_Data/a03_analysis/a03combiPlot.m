%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a03combi_plot.m
% plot the effect sizes and ERPs for each component. Show scalp(ind.),
% scalp(sta.) and ear-conditions.
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% settings
tasktitle = {'(n=15)','(n=15)','(n=16)','(n=15)','(n=16)'};

%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };

MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
FIGPATH = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\';
CIPATH = 'C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)';
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

%load variables made in a01
load([MAINPATH,'quality_check.mat'],'q_check');
load([MAINPATH,'MaxVar.mat'],'maxVar');
load([MAINPATH,'MeanES.mat'],'mean_es');
load([MAINPATH,'MeanES.mat'],'hedgesG_mean');
load([MAINPATH,'MeanES.mat'],'hedgesG_std');
load([MAINPATH,'MeanES.mat'],'hedgesG_GND_AVG');

%% Plot 1: GND AVG ERPs for the standard channels
con = fieldnamesr(maxVar,1);
tasks = fieldnamesr(maxVar.cap,1);
Colors = {'cyan','blue','red','yellow','green'};
pure_green =[0.4660 0.6740 0.1880];
pure_red = [0.8500, 0.3250, 0.0980];
pure_blue = [0,0.4470,0.7410];
pure_yellow = [0.9290 0.6940 0.1250];
Shades = [pure_blue;pure_blue;pure_red;pure_yellow;pure_green];
transparency = 0.5;
%use the ci-plot function in matlab
cd('C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)');
movepic = [0.05 -0.08 -0.13 -0.18 -0.23];
moveplot = [0.05 -0.05 -0.1 -0.15 -0.2];

taskLegend1 = {'N1','att','target','target','congruent',};
taskLegend2 = {'N1','unatt','standard','standard','incongruent',};
ylims = [-7 3; -3 3; -3 2; -3 9; -5.5 7];
vars = fieldnamesr(mean_es.cap.N1,1);
figpath = dir([FIGPATH,'*.jpg']);
cd(FIGPATH)

%plot the topoplots from a02
count = 1;
bigfigure = figure('DefaultAxesFontWeight','bold','DefaultAxesFontSize',10);
for t = 1:length(tasks)
    imr = imread(figpath(t).name);
    ax0  = subplot(6,5,t);
    imshow(imr);
    title(tasktitle{t},'FontSize', 12, 'FontWeight', 'bold')
    if t ~=1
        ax0.Position(1) = ax0.Position(1) + movepic(t) + 0.024; %plot is moved for better spacing
    else
        ax0.Position(1) = ax0.Position(1) - 0.015; %plot is moved for better spacing
    end
    ax0.OuterPosition(3:4) = ax0.OuterPosition(3:4) + 0.024;
    count = count+1; %count goes on in the next loop, just fills the subplots with different kinds of plots
end

%plot the ERPs from scalp(sta), scalp(ind) and ear
cd(CIPATH);
bc = 1;
colorc = 1;
for c = [1 1 3] %once the cap (standard channel), cap again (ind. channel), then ear, (ind. channel)
    for t = 1:length(tasks)
        
        ax = subplot(6,5,count);
        
        if t == 1
            ax.Position(3) = ax.Position(3)/5; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
            timeVec =  linspace(50,240,20);
            ax.Position(1) = ax.Position(1) + moveplot(t); %plot is moved for better spacing
        else
            ax.Position(3) = ax.Position(3)/2; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
            timeVec =  linspace(-200,790,100);
            ax.Position(1) = ax.Position(1) + moveplot(t) + 0.03; %plot is moved for better spacing
        end
        
        if bc == 1
            v1 = find(strcmp(vars,'ERP_stacond1'));
            v2 = find(strcmp(vars,'ERP_stacond2'));
        elseif bc == 2 || bc == 3
            v1 = find(strcmp(vars,'ERP_cond1'));
            v2 = find(strcmp(vars,'ERP_cond2'));
        else
        end
        
        %cond1 (attended, inconcgr., etc.) - spread
        ci1 = ciplot(mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1) - std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),...
            mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1) + std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),...
            timeVec,Shades(4,:),transparency);
        ci1.EdgeColor = 'none';
        
        hold on
        
        if t ~= 1
            %cond2 (unattended, concgr., etc.) - spread
            ci2 = ciplot(mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),...
                mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) + std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),...
                timeVec,Shades(5,:),transparency);
            ci2.EdgeColor = 'none';
            
            %difference wave - spread
            ci3 = ciplot((mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)) ...
                        - (std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) -  std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)),...
                              (mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)) ...
                        + (std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) -  std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)),...
                timeVec,Shades(c,:),transparency);
            ci3.EdgeColor = 'none';
            
            
            %cond2 (unattended, concgr., etc.)  - mean
            plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),'color',Colors{5},'LineWidth',3)
        end
        %cond1 (attended, inconcgr., etc.)  - mean
        plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),'color',Colors{4},'LineWidth',3)
        
        if t ~= 1
            %difference wave - mean
            plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),'color',Colors{bc},'LineWidth',3)
        end
        
        ylim(ylims(t,:))
        if t == 1
            xlim([50 250])
        else
            xlim([-200 800])
        end
        
        if t == 1
            ylabel('Amplitude [�V]','FontSize', 12, 'FontWeight', 'bold')
        end
        
        box off
        pos(count,:) = get(gca,'Position');
        
        count = count +1;
        
    end
    bc = bc+1;
end

%plot the effect sizes of the three EEG-conditions
LineStyles = {'-','--'};
Colors2 = {'cyan','blue','red','red'};
Shades2 = [pure_blue;pure_blue;pure_red;pure_yellow;pure_green];
for xy = 1:2
    for t = 1:length(tasks)
        
        ax2 = subplot(6,5,count);
        if t == 1
            ax2.Position(3) = ax2.Position(3)/5; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
            timeVec =  linspace(50,240,20);
            ax2.Position(1) = ax2.Position(1) + moveplot(t); %plot is moved for better spacing
            ax2.Position(2) = ax2.Position(2) - 0.03; %plot is moved for better spacing
        else
            ax2.Position(3) = ax2.Position(3)/2; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
            timeVec =  linspace(-200,790,100);
            ax2.Position(1) = ax2.Position(1) + moveplot(t) + 0.03; %plot is moved for better spacing
            ax2.Position(2) = ax2.Position(2) - 0.03; %plot is moved for better spacing
        end
        
        for c = [1 2 4]
            if xy == 1
                ci1 = ciplot(hedgesG_mean{c,t} - hedgesG_std{c,t},hedgesG_mean{c,t} + hedgesG_std{c,t},timeVec,Shades2(c,:),transparency);
                ci1.EdgeColor = 'none';
                hold on
                plot(timeVec,hedgesG_mean{c,t},'color',Colors2{c},'LineWidth',3,'LineStyle',LineStyles{1})
            else
                plot(timeVec,hedgesG_GND_AVG{c,t},'color',Colors2{c},'LineWidth',3,'LineStyle',LineStyles{1})
                hold on
            end
        end
        
        %change the order of the lines in the plot, so that the actual
        %lines are always in front of the shadings
        if xy == 1
            h = get(gca,'Children');
            set(gca,'Children',[h(1) h(3) h(5) h(6) h(4) h(2)])
        end
        
        
        if t ==1 && xy == 2
            ylabel('Hedges� g [abs.]','FontSize', 12, 'FontWeight', 'bold')
            xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
        elseif t == 1
            ylabel('Hedges� g [abs.]','FontSize', 12, 'FontWeight', 'bold')
            xlim([50 250])
        else
            xlim([-200 800])
        end
        box off
        pos(count,:) = get(gca,'Position');
        count = count +1;
        
    end
end

%set the A-F labels on a specified position
set(gcf,'Color','w')
set(gcf, 'Position', get(0, 'Screensize'));
annotation('textbox',[0.11708,0.9224-0.015,0.017708,0.045283],'String','A','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
annotation('textbox',[0.11708,0.7799-0.015,0.017708,0.045283],'String','B','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
annotation('textbox',[0.11708,0.6374-0.015,0.017708,0.045283],'String','C','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
annotation('textbox',[0.11708,0.4949-0.015,0.017708,0.045283],'String','D','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
annotation('textbox',[0.11708,0.3524-0.045,0.017708,0.045283],'String','E','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
annotation('textbox',[0.11708,0.2100-0.045,0.017708,0.045283],'String','F','FontSize', 22, 'FontWeight', 'bold','LineStyle','none')

cd('O:\arm_testing\Experimente\cEEGrid_vs_Cap\')
print(bigfigure,'C:\Users\arnd\Pictures\Bigfigure','-dpng') %save figure

%end of script