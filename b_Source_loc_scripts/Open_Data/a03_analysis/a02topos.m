%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a02topos.m
% We get the mean ERP over subjects for each ERP-component (scalp(sta.) 
% only). We then get the peak of these ERPs and save the topographies at
% those time points.
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% settings
taskLetter    = {'N1';'a';'b';'c';'d'};
numChans = [96 12 12];
iwindow = [150 180 180 380 380];
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
eeglab

load([MAINPATH,'MeanES.mat']);
load([MAINPATH,'MaxVar.mat']);

%prepare place holder variable for plotting in EEGLAB. Use any EEG-file
temp_cap = pop_loadset('filename', 'zilu79_b_dev.set', 'filepath','O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\zilu79\04_clean\b_passive_oddball\');
temp_cap.trials = 1;
temp_ear = pop_loadset('filename', 'zilu79_b_dev_ear.set', 'filepath','O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\zilu79\04_clean\b_passive_oddball\');
temp_ear.trials = 1;
temp_capN1 = pop_loadset('filename', 'zilu79_a_ica_cleaned_N1_all.set', 'filepath','O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\zilu79\04_clean\a_attended_speaker\');
temp_capN1.trials = 1;
temp_earN1 = pop_loadset('filename', 'zilu79_a_ica_ear_N1_all_ear.set', 'filepath','O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\zilu79\04_clean\a_attended_speaker\');
temp_earN1.trials = 1;

load([MAINPATH,'quality_check.mat'],'q_check');
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for t = 1:length(taskLetter)
    q_task = t; %counter for the tasks. there are 4 tasks, but task a has two runs, one for N1 from tone onset, one from all later tones. Therefore, we need an additional counter
    if t > 1
        q_task = q_task - 1;
        special_N1 = 0;
        getTaskNum = q_check([q_check.badEarChan] == 0);
        getTaskNum = sum(contains({getTaskNum.SBJ},['_',taskLetter{t}]));
    else
        special_N1 = 1;
        getTaskNum = q_check([q_check.badEarChan] == 0);
        getTaskNum = sum(contains({getTaskNum.SBJ},['_',taskLetter{t+1}]));
    end
        count = 0; %don't allow empty lines in the final variable. If a task is empty, skip that line instead of writing a zero line
        savetemp1 = zeros(numChans(c),100,getTaskNum); %pre-allocate the matrix saving all ERPs for condition1...
    
        for s = 1:length(SBJ)
            
            temp_q = q_check;
            getSBJ = contains({temp_q.SBJ},SBJ{s});
            temp_q = temp_q(getSBJ);
            
            if temp_q(q_task).badEarChan == 0
                
                if taskLetter{t} == 'N1'
                    naming1st = [];
                elseif taskLetter{t} == 'a'
                    naming1st = ['_att'];
                elseif taskLetter{t} == 'b' || taskLetter{t} == 'c'
                    naming1st = ['_dev'];
                else taskLetter{t} == 'd'
                    naming1st = ['_incongr'];
                end
                
                %get scalp-data
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{t},'*',naming1st,'.set']); %set 1 for the cleaned cap data
                con1path = con1path(1);
                
                % get folder names                
                if taskLetter{t} == 'N1'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{t} == 'a'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{t} == 'b'
                    folderName = 'b_passive_oddball';
                elseif taskLetter{t} == 'c'
                    folderName = 'c_active_oddball';
                elseif taskLetter{t} == 'd'
                    folderName = 'd_conguency';
                else
                    error('please enter a valid taskLetter (a-d)')
                end
                
                PATHIN  = con1path.folder;
                PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
                if ~exist (PATHOUT)
                    mkdir(PATHOUT)
                end
                
                % load data: 1 condition
                condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                savetemp1(:,:,s-count) = mean(condition1.data,3);
                
            else
                count = count + 1;
            end
        end
        
        savetopo1.(taskLetter{t}) = mean(savetemp1,3);
        temp_cap.data = savetopo1.(taskLetter{t}); %load data into the template EEGLAB-variable for plotting
        figCap(t) = figure;
        pop_topoplot(temp_cap, 1, iwindow(t),'',[1 1] ,0,'electrodes','on','whitebk','on');
        set(gcf,'Color','w')
        cbar = get(figCap(t),'Children');
        delete(cbar(1))
        
        %save the plot to later integrate it into the plot in a03
        print(figCap(t),['O:\arm_testing\Experimente\cEEGrid_vs_Cap\Fig',num2str(t)],'-djpeg') 
        set(gcf,'Color','w')       
end


