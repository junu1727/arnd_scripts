MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
load([MAINPATH,'MeanES.mat'],'mean_es');
load([MAINPATH,'MaxVar.mat'],'maxVar');

timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
window_start  = [ 50  50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
con = fieldnamesr(mean_es,1);
tasks = fieldnamesr(mean_es.cap,1);

for c = 1:length(con)
    for t = 1:length(tasks)
        if t ~= 1
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        else
            timeWin = find(timeVecN1 == window_start(t)) : find(timeVecN1 == 240);
        end
        for s = 1:size(mean_es.(con{c}).(tasks{t}).ERP_cond1,1)
            if t ~= 1 && t ~= 4
                diffmax = mean_es.(con{c}).(tasks{t}).ERP_cond2(s,:) - mean_es.(con{c}).(tasks{t}).ERP_cond1(s,:);
                mean_es.(con{c}).(tasks{t}).Diff_max(s) = min(diffmax(:,timeWin));
            elseif t == 1
                diffmax = mean_es.(con{c}).(tasks{t}).ERP_cond1(s,:);
                mean_es.(con{c}).(tasks{t}).Diff_max(s) = min(diffmax(:,timeWin));
            else
                diffmax = mean_es.(con{c}).(tasks{t}).ERP_cond2(s,:) - mean_es.(con{c}).(tasks{t}).ERP_cond1(s,:);
                mean_es.(con{c}).(tasks{t}).Diff_max(s) = max(diffmax(:,timeWin));
            end
        end
    end
end

for c = 1:length(con)
    for t = 1:length(tasks)
        if t ~= 1
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        else
            timeWin = find(timeVecN1 == window_start(t)) : find(timeVecN1 == 240);
        end
        for s = 1:size(mean_es.(con{c}).(tasks{t}).ERP_cond1,1)
            if t ~= 1 && t ~= 4
                diffmax = mean_es.(con{c}).(tasks{t}).ERP_stacond2(s,:) - mean_es.(con{c}).(tasks{t}).ERP_stacond1(s,:);
                mean_es.(con{c}).(tasks{t}).Diff_max_sta(s) = min(diffmax(:,timeWin));
            elseif t == 1
                diffmax = mean_es.(con{c}).(tasks{t}).ERP_stacond1(s,:);
                mean_es.(con{c}).(tasks{t}).Diff_max_sta(s) = min(diffmax(:,timeWin));
            else
                diffmax = mean_es.(con{c}).(tasks{t}).ERP_stacond2(s,:) - mean_es.(con{c}).(tasks{t}).ERP_stacond1(s,:);
                mean_es.(con{c}).(tasks{t}).Diff_max_sta(s) = max(diffmax(:,timeWin));
            end
        end
    end
end


%% unilateral left/right
uniR = {'E18 - E24','E18 - E71','E18 - E72','E18 - E85','E18 - E92','E24 - E71','E24 - E72','E24 - E85',...
    'E24 - E92','E71 - E72','E71 - E85','E71 - E92','E72 - E85','E72 - E92','E85 - E92'};
uniL = {'E22 - E28','E22 - E80','E22 - E81','E18 - E90','E22 - E95','E28 - E80','E28 - E81','E28 - E90',...
    'E28 - E95','E80 - E81','E80 - E90','E80 - E95','E81 - E90','E81 - E95','E90 - E95'};

for t = 1:length(tasks)
    if t ~= 1
        timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
    else
        timeWin = find(timeVecN1 == window_start(t)) : find(timeVecN1 == 240);
    end
    count = 0;
    for s = 1:size(maxVar.ear.(tasks{t}),2)
        if ~isempty(maxVar.ear.(tasks{t})(s).bestchans)
            for x = 1:length(uniL) %loop through unilateral channels (left ear) to get their indices in the channel lsit
                bestL(x) = find(strcmp(uniL(x),maxVar.ear.(tasks{t})(s).bestchans));
            end
            for x = 1:length(uniR) %loop through unilateral channels (right ear) to get their indices in the channel lsit
                bestR(x) = find(strcmp(uniR(x),maxVar.ear.(tasks{t})(s).bestchans));
            end
            
            if t ~= 1 && t ~= 4
                %left
                m1 = maxVar.ear.(tasks{t})(s).ERP_cond1(maxVar.ear.(tasks{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                m2 = maxVar.ear.(tasks{t})(s).ERP_cond2(maxVar.ear.(tasks{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                diffmax = m2- m1;
                mean_es.ear.(tasks{t}).Diff_max_left(s-count) = min(diffmax(:,timeWin));
                %right
                m1 = maxVar.ear.(tasks{t})(s).ERP_cond1(maxVar.ear.(tasks{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                m2 = maxVar.ear.(tasks{t})(s).ERP_cond2(maxVar.ear.(tasks{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                diffmax = m2- m1;
                mean_es.ear.(tasks{t}).Diff_max_right(s-count) = min(diffmax(:,timeWin));
            elseif t == 1
                %left
                m1 = maxVar.ear.(tasks{t})(s).ERP_cond1(maxVar.ear.(tasks{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                diffmax = m1;
                mean_es.ear.(tasks{t}).Diff_max_left(s-count) = min(diffmax(:,timeWin));
                %right
                m1 = maxVar.ear.(tasks{t})(s).ERP_cond1(maxVar.ear.(tasks{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                diffmax = m1;
                mean_es.ear.(tasks{t}).Diff_max_right(s-count) = min(diffmax(:,timeWin));
            else
                %left
                m1 = maxVar.ear.(tasks{t})(s).ERP_cond1(maxVar.ear.(tasks{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                m2 = maxVar.ear.(tasks{t})(s).ERP_cond2(maxVar.ear.(tasks{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                diffmax = m2- m1;
                mean_es.ear.(tasks{t}).Diff_max_left(s-count) = max(diffmax(:,timeWin));
                %right
                m1 = maxVar.ear.(tasks{t})(s).ERP_cond1(maxVar.ear.(tasks{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                m2 = maxVar.ear.(tasks{t})(s).ERP_cond2(maxVar.ear.(tasks{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                diffmax = m2- m1;
                mean_es.ear.(tasks{t}).Diff_max_right(s-count) = max(diffmax(:,timeWin));
            end
else
count = count+1;
        end
    end
end

save([MAINPATH,'MeanES.mat'],'mean_es','hedgesG_mean','hedgesG_std','hedgesG_GND_AVG');

for t = 1:length(tasks)
table2go(t,1) = median(mean_es.cap.(tasks{t}).Diff_max_sta);
table2go(t,2) =   mean(mean_es.cap.(tasks{t}).Diff_max_sta);
table2go(t,3) =    std(mean_es.cap.(tasks{t}).Diff_max_sta);
table2go(t,4) = median(mean_es.cap.(tasks{t}).Diff_max);
table2go(t,5) =   mean(mean_es.cap.(tasks{t}).Diff_max);
table2go(t,6) =    std(mean_es.cap.(tasks{t}).Diff_max);
table2go(t,7) = median(mean_es.ear.(tasks{t}).Diff_max);
table2go(t,8) =   mean(mean_es.ear.(tasks{t}).Diff_max);
table2go(t,9) =    std(mean_es.ear.(tasks{t}).Diff_max);
table2go(t,10) = median(mean_es.ear.(tasks{t}).Diff_max_right);
table2go(t,11) =   mean(mean_es.ear.(tasks{t}).Diff_max_right);
table2go(t,12) =    std(mean_es.ear.(tasks{t}).Diff_max_right);
table2go(t,13) = median(mean_es.ear.(tasks{t}).Diff_max_left);
table2go(t,14) =   mean(mean_es.ear.(tasks{t}).Diff_max_left);
table2go(t,15) =    std(mean_es.ear.(tasks{t}).Diff_max_left);
end

table2go = array2table(table2go);

writetable(table2go,'table_ERP.xlsx','Sheet','MyNewSheet','WriteVariableNames',false);
