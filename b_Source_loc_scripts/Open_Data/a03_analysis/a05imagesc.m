%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a05imagesc.m
% plot the amplitudes/effect sizes of each bipolar channel pair for all 
% subjects for unilateral ear-EEG, both left and right.
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set some variables
taskLetter    = {'N1';'a';'b';'c';'d'};
window_start  = [50 50 130 250 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 250 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
tnames = {'N1 (n=15)','N1(att)(n=15)','MMN (n=16)','P300 (n=14)','N400 (n=16)'};
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

% load([MAINPATH,'leftChans.mat']);
% load([MAINPATH,'rightChans.mat']);
% load([MAINPATH,'biChans.mat']);
load([MAINPATH,'MaxVar.mat']);
load([MAINPATH,'MeanES.mat'],'mean_es');

con = fieldnamesr(maxVar,1);%get fieldnames for looping through each layer

%% calc distances and orientations of the bipolar channels
eeglab
chanPath = dir([MAINPATH,SBJ{1},'\04_clean\*\*_a','*','ear.set']); %set 1 for the cleaned cap data
EEG = pop_loadset('filename', chanPath(1).name, 'filepath',chanPath(1).folder);

%get indices of channels from left/right ear and bilateral
posR = [1 3 5 6 9 11];
posL = [2 4 7 8 10 12];
chanR = [EEG.chanlocs(posR).X; EEG.chanlocs(posR).Y; EEG.chanlocs(posR).Z]';
chanL = [EEG.chanlocs(posL).X; EEG.chanlocs(posL).Y; EEG.chanlocs(posL).Z]';

%loop through channels to get their distance and names of the bipolar channel
collectR=[];
collectL=[];
index=1;
for ch = 1:length(posR)-1
    for b = ch + 1:length(posR)
        collectR(index) = norm(chanR(ch,:)  - chanR(b,:));
        namesR{index} = [EEG.chanlocs(posR(ch)).labels,' - ', EEG.chanlocs(posR(b)).labels];
        collectL(index) = norm(chanL(ch,:)  - chanL(b,:));
        namesL{index} = [EEG.chanlocs(posL(ch)).labels,' - ', EEG.chanlocs(posL(b)).labels];
        index=index+1;
    end
end

% do the same for all bilateral bipolar channels
collectB=[];
index = 1;
for ch = 1:length(posR)
    for b = 1:length(posL)
        collectB(index) = norm(chanR(ch,:)  - chanL(b,:));
        namesB{index} = [EEG.chanlocs(posR(ch)).labels,' - ', EEG.chanlocs(posL(b)).labels];
        index=index+1;
    end
end

%% change the naming and order of electrodes

% horR = namesR([4 9 10]);
% verR = namesR([3 14 5 11 6 8]);
% qpaR  = namesR([2 12 15]); %from posterior to anterior
% qapR  = namesR([1 7 13]); %from anterior to posterior

% horL = namesL([10 4 9]);
% verL = namesL([13 7 8 2 12 5]);
% qpaL  = namesL([14 15 3]); %from posterior to anterior
% qapL  = namesL([1 6 11]); %from anterior to posterior

% sort after orientation, horizontal, vertical, right diagonal, left diagonal
sortingr = [4 9 10 3 14 5 11 6 8 2 12 15 1 7 13];
[pholder indR] = sort(collectR,'desc');
newsortR = [4 10 9 6 14 8 3 5 11 12 15 2 7 13 1]; %sort after distance within a category

sortingL = [10 4 9 13 7 8 2 12 5 14 15 3 1 6 11];
[pholder indL] = sort(collectR,'desc');
newsortL = [4 10 9 12 7 2 13 8 5 14 15 3 6 1 11];%sort after distance within a category

collectR = collectR(newsortR);
collectL = collectL(newsortL);
namesR = namesR(newsortR);
namesL = namesL(newsortL);

%sort all bilateral channels from highest to lowest distance (does not make sense to include angles here)
[collectB indB] = sort(collectB,'desc');
namesB = namesB(indB);

%change the names so that the lower electrode number always comes first
for c = 1:length(namesR)
    if str2num(namesR{1,c}(2:3)) >  str2num(namesR{1,c}(end-1:end))
        str1 = namesR{1,c}(2:3);
        str2 = namesR{1,c}(end-1:end);
        namesR{1,c}(2:3) = str2;
        namesR{1,c}(end-1:end) = str1;
    end
end

for c = 1:length(namesL)
    if str2num(namesL{1,c}(2:3)) >  str2num(namesL{1,c}(end-1:end))
        str1 = namesL{1,c}(2:3);
        str2 = namesL{1,c}(end-1:end);
        namesL{1,c}(2:3) = str2;
        namesL{1,c}(end-1:end) = str1;
    end
end

for c = 1:length(namesB)
    if str2num(namesB{1,c}(2:3)) >  str2num(namesB{1,c}(end-1:end))
        str1 = namesB{1,c}(2:3);
        str2 = namesB{1,c}(end-1:end);
        namesB{1,c}(2:3) = str2;
        namesB{1,c}(end-1:end) = str1;
    end
end

%% loop through task, conditions and subjects
for s = 1:length(SBJ)-1 %loop through subjects (without the last, it was excluded completely)
    
    for t = 1:length(taskLetter) %loop through tasks
        
        load([MAINPATH,'quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        q_check(5)= q_check(1);
        q_check = circshift(q_check,1);
        
        if t ~= 1
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        else
            timeWin = find(timeVecN1 == window_start(t)) : find(timeVecN1 == 240) -1;
        end
        
        if q_check(t).badEarChan == 0
            
            %get the indices of the channels (same channels in the same order every time)
            for x = 1:length(namesR)
                numsright_AMP(x) = find(strcmp(namesR(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            for x = 1:length(namesL)
                numsleft_AMP(x) = find(strcmp(namesL(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            
            %now get the peaks from each of these ERPs from every right, left and bilateral channel channel
            pksRight_AMP(s,t,1:length(numsright_AMP)) = max(abs(maxVar.ear.(taskLetter{t})(s).ERP_cond2(numsright_AMP,timeWin) - maxVar.ear.(taskLetter{t})(s).ERP_cond1(numsright_AMP,timeWin)),[],2);
            pksLeft_AMP(s,t,1:length(numsleft_AMP))   = max(abs(maxVar.ear.(taskLetter{t})(s).ERP_cond2(numsleft_AMP,timeWin) - maxVar.ear.(taskLetter{t})(s).ERP_cond1(numsleft_AMP,timeWin)),[],2);
            
            %get the indices of the channels (same channels in the same order every time)
            for x = 1:length(namesR)
                numsright_ES(x) = find(strcmp(namesR(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            for x = 1:length(namesL)
                numsleft_ES(x) = find(strcmp(namesL(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            
            %now get the peaks from each of these effect sizes over time
            l=1;
            for loc = numsright_ES
                pksRight_ES(s,t,l)=  maxVar.ear.(taskLetter{t})(s).maxVal(loc); % store the peaks of each subject*task*channel
                l = l+1;
            end
            
            l=1;
            for loc = numsleft_ES
                pksLeft_ES(s,t,l)= maxVar.ear.(taskLetter{t})(s).maxVal(loc); % store the peaks of each subject*task*channel
                l = l+1;
            end
        end
    end
end
%% change the names of the electrodes for easier reading
tempNames = {'71','72','85','18','24','92','81','80','90','22','28','95',};
newNames = {'R1','R2','R3','R4','R5','R6','L1','L2','L3','L4','L5','L6',};

for st = 1:length(tempNames)
    namesR = strrep(namesR,tempNames{st},newNames{st});
    namesL = strrep(namesL,tempNames{st},newNames{st});
    namesB = strrep(namesB,tempNames{st},newNames{st});
end

namesR = strrep(namesR,'E','');
namesL = strrep(namesL,'E','');
namesB = strrep(namesB,'E','');
names = {namesR; namesL};

% add line 20 for the rejected subject 20 plus 21 for the mean over all subjects
pksRight_AMP(20:21,:,:) = 0;
pksLeft_AMP(20:21,:,:) = 0;

pksRight_ES(20:21,:,:) = 0;
pksLeft_ES(20:21,:,:) = 0;

%read in the images of the ear-electrodes (custom-made).
cd('C:\Users\arnd\Pictures\')
imr = imread('earR.jpg');
iml = imread('earL.jpg');
imb = imread('earB.jpg');
imrcolor = imread('earRcolor.jpg');
imlcolor = imread('earLcolor.jpg');
im = {imrcolor; imlcolor};
colors = {'red' 'red' 'red' 'blue' 'blue' 'blue' 'blue' 'blue' 'blue' 'magenta' 'magenta' 'magenta' 'green' 'green' 'green'};

%% create the figure
for runs = 1:2
    impic(runs) = figure;
    subp = 1;
    for rl = 1:length(names)
        for i = 1:length(taskLetter)
            
            subplot(2,5,subp)
            
            %get the absolute peak values
            if rl == 1
                if runs == 1
                    pks1(:,:,i) = abs(squeeze(pksRight_AMP(:,i,:)));
                    pks2(:,:,i) = abs(squeeze(pksRight_ES (:,i,:)));
                else
                    pks1(:,:,i) = abs(squeeze(pksLeft_AMP(:,i,:)));
                    pks2(:,:,i) = abs(squeeze(pksLeft_ES (:,i,:)));
                end
            else
                if runs == 1
                    pks1(:,:,i) = abs(squeeze(pksRight_AMP(:,i,:)));
                    pks2(:,:,i) = abs(squeeze(pksRight_ES (:,i,:)));
                else
                    pks1(:,:,i) = abs(squeeze(pksLeft_ES(:,i,:)));
                    pks2(:,:,i) = abs(squeeze(pksLeft_ES (:,i,:)));
                end
            end
            
            %rescale peaks from 0 to 1 to compare between subjects
            scaleR = max(pks1(:,:,i),[],2);
            scaleL = max(pks2(:,:,i),[],2);
            
            pks1(:,:,i) = rescale(pks1(:,:,i),'InputMin',0,'InputMax',scaleR);
            pks2(:,:,i) = rescale(pks2(:,:,i),'InputMin',0,'InputMax',scaleL);
            
            pks1(21,:,i) = mean(pks1(find(pks1(:,1)~=0),:,i),1);
            pks2(21,:,i) = mean(pks2(find(pks2(:,1)~=0),:,i),1);
            
            pks1(21,:,i) = rescale(pks1(21,:,i),'InputMin',0,'InputMax',max(pks1(21,:,i),[],2));
            pks2(21,:,i) = rescale(pks2(21,:,i),'InputMin',0,'InputMax',max(pks2(21,:,i),[],2));
            
            [highR(i,:) indR(i,:)] = sort(pks1(21,:,i),'desc');
            highnameR{i} = namesR(indR(i,:));
            [highL(i,:) indL(i,:)] = sort(pks2(21,:,i),'desc');
            highnameL{i} = namesL(indL(i,:));
            
            pks = {squeeze(pks1(:,:,i)) squeeze(pks2(:,:,i))};
            
            pic = imagesc(pks{1,rl});
            if i == 1
                yticks(1:21)
                yticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','Mean',})
                ylabel('Subject')
                set(gca,'fontweight','bold','fontsize',8);
            else
                set(gca,'YTickLabel',[]);
            end
            if subp > 5
                xticks(1:size(pks{1,runs},2))
                xticklabels({names{runs,1}{1,1:size(pks{1,runs},2)}});
                xtickangle(45)
                ax=gca;
                for w = 1:length(colors)
                    ax.XTickLabel{w} = [['\color{',colors{w},'}'] ax.XTickLabel{w}];
                end
                set(ax,'fontweight','bold','fontsize',8);
                ax.Position = ax.Position + [0 0.21 0.02 0];
            else
                ax=gca;
                set(ax,'XTickLabel',[]);
                set(ax,'fontweight','bold','fontsize',8);
                ax.Position = ax.Position + [0 0 0.0197 0];
            end
            axis square;
            box 'off'
            
            if subp == 6
                xlabel('Channel')
            end
            
            if subp < 6
                title(tnames{i},'FontSize',14,'FontWeight','bold')
            end
            subp = subp + 1;
        end
        set(gcf,'Color','w')
        set(gcf, 'Position', get(0, 'Screensize'));
    end
end
 print(impic(1),'C:\Users\arnd\Pictures\\Right_AMP_ES','-dpng') %store plots
 print(impic(2),'C:\Users\arnd\Pictures\\Left_AMP_ES','-dpng')

 %end of script