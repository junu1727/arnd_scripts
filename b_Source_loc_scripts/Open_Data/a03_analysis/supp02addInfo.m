%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% supp02addInfo.m
% get some additional information about the data, like behavioral data and
% infos about the results of pre-processing (number of epochs removed, etc.)
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%set some paths and variables
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };

%get reaction times from N400-task
for s = 1:length(SBJ)
    load([MAINPATH,'quality_check.mat'],'q_check');
    getSBJ = contains({q_check.SBJ},SBJ{s});
    q_check = q_check(getSBJ);
    if q_check(4).badEarChan == 0
        
        load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\log\CW_onsets.mat']);
        R(s,:) = rTime;
    end
end

%remove empty rows
R([5 15 18],:) = [];

meanR = mean(R,2)
meanRAll  = mean(meanR)
stdRAll  = std(meanR)

q_check = q_check(find([q_check.badEarChan] == 0))

%% a: get number of rejected epochs
load([MAINPATH,'rej_ep_a.mat']);

%attended
m_ep = 0;
for x = 1:length(rej_ep)
    n(x) = numel(rej_ep{x,1});
end

for x = 1:length(rej_ep)
    m(x) = numel(rej_ep{x,3});
end
m_ep = mean([n m])

meanp_a_att = m_ep/(150*7) *100

%attended
m_ep = 0;
for x = 1:length(rej_ep)
    n(x) = numel(rej_ep{x,2});
end

for x = 1:length(rej_ep)
    m(x) = numel(rej_ep{x,4});
end
m_ep = mean([n m])

meanp_a_unatt = m_ep/(150*7) *100
%% b/c: get number of rejected epochs
load([MAINPATH,'rej_ep_c.mat']);
m_ep = 0;
for x = 1:length(rej_ep_sta)
    n(x) = numel(rej_ep_sta{x});
end
m_ep = mean(n)

meanp_b_sta = m_ep/800 *100

m_ep = 0;
for x = 1:length(rej_ep_dev)
    n(x) = numel(rej_ep_dev{x});
end
m_ep = mean(n)

meanp_b_dev = m_ep/200 *100

%% d: get number of rejected epochs
load([MAINPATH,'rej_ep_d.mat']);
m_ep = 0;
for x = 1:length(rej_ep_incongr)
    n(x) = numel(rej_ep_incongr{x});
end
m_ep = mean(n)

meanp_d_incongr = m_ep/100 *100

m_ep = 0;
for x = 1:length(rej_ep_congr)
    n(x) = numel(rej_ep_congr{x});
end
m_ep = mean(n)

meanp_d_congr = m_ep/100 *100