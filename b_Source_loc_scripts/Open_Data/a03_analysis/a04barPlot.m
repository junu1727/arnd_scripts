%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a03combi_plot.m
% plot the effect sizes and ERPs for each component. Show scalp(ind.),
% scalp(sta.) and ear-conditions.
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set some variables
taskLetter    = {'N1';'a';'b';'c';'d'};
window_start  = [50 50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
timeVec = linspace(-200,790,100);
uniR = {'E18 - E24','E18 - E71','E18 - E72','E18 - E85','E18 - E92','E24 - E71','E24 - E72','E24 - E85',...
        'E24 - E92','E71 - E72','E71 - E85','E71 - E92','E72 - E85','E72 - E92','E85 - E92'};
uniL = {'E22 - E28','E22 - E80','E22 - E81','E18 - E90','E22 - E95','E28 - E80','E28 - E81','E28 - E90',...
        'E28 - E95','E80 - E81','E80 - E90','E80 - E95','E81 - E90','E81 - E95','E90 - E95'};
names = {'N1 (n=15)','N1(att)(n=15)','MMN (n=16)','P300 (n=14)','N400 (n=16)'};

%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };

MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

load([MAINPATH,'MeanES.mat']);
load([MAINPATH,'MaxVar.mat']);

%% prepare data for effect size plots. Split the ear-data into right/left unilateral and bilateral data

%get fieldnames for looping through each layer
con = fieldnamesr(maxVar,1);
tasks = fieldnamesr(mean_es.cap,1);
vars  = fieldnamesr(mean_es.cap.N1,1);
pksMax = zeros(length(SBJ),5,length(taskLetter));

for t = 1:length(taskLetter) %loop through tasks
    count = 0;
    for s = 1:length(SBJ) %loop through subjects
        
        load([MAINPATH,'quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        q_check(5)= q_check(1);
        q_check = circshift(q_check,1);
        
        if t == 1
            timeWin = 1:20; %shorter time window for N1-component
        else
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        end
        
        if q_check(t).badEarChan == 0
            
            %get the peak values from every subjects' best channel to
            %display it in the box-plot
            for c = 1:5
                
                if c == 1
                    %scalp(sta.)
                    pksMax(s,c,t)= maxVar.cap.(taskLetter{t})((s)).maxVal_sta; % store the peaks of each subject*condition*task from the best channel
                elseif c == 2
                    %scalp(ind.)
                    pksMax(s,c,t)= maxVar.cap.(taskLetter{t})((s)).maxVal(maxVar.cap.(taskLetter{t})((s)).best_loc(1)); % store the peaks of each subject*condition*task from the best channel
                elseif c == 3 %for the bilateral condition, take the best channel among those built from both ears
                    %ear (bilateral)
                    pksMax(s,c,t)= maxVar.ear.(taskLetter{t})((s)).maxVal(maxVar.ear.(taskLetter{t})((s)).best_loc(1)); % store the peaks of each subject*condition*task from the best channel
                elseif c == 4
                    %ear (unilateral, right)
                    for x = 1:length(uniR) %loop through unilateral channels (right ear) to get their indices in the channel lsit
                        bestR(x) = find(strcmp(uniR(x),maxVar.ear.(taskLetter{t})(s).bestchans));
                    end
                    m1 = maxVar.ear.(taskLetter{t})(s).es_matrix_mean(maxVar.ear.(taskLetter{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                    pksMax(s,c,t)= max(m1(1,timeWin)); % store the peaks of each subject*condition*task from the best channel
                else
                    %ear (unilateral, left)
                    for x = 1:length(uniL) %loop through unilateral channels (left ear) to get their indices in the channel lsit
                        bestL(x) = find(strcmp(uniL(x),maxVar.(con{3}).(taskLetter{t})(s).bestchans));
                    end
                    m1 = maxVar.ear.(taskLetter{t})(s).es_matrix_mean(maxVar.ear.(taskLetter{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                    pksMax(s,c,t)= max(m1(1,timeWin)); % store the peaks of each subject*condition*task from the best channel
                end
                
            end
        else
            count = count + 1; %mean_es only has as many rows as subjects (18 of 20 when two subjects got kicked). maxVar always has 20. This variable alignes the counting
        end
    end
end

pksMax(find(pksMax == 0)) = NaN; %delte the NAN from the tasks that were not processed due to bad ear-channels

grey = [.75 .75 .75]; %color-coding
darkgrey = [.2 .2 .2];

cnums = 1:20;
P1 = zeros(length(taskLetter),length(SBJ));
P2 = zeros(length(taskLetter),length(SBJ));
P3 = zeros(length(taskLetter),length(SBJ));

barfigure = figure;
for j = 1:length(taskLetter)
    noNaN = ~isnan(pksMax(:,1,j)); %get indices of subjects per task (NaN-row = subject removed)
    ntasks = sum(noNaN); %number of subjects in each tasks
    colcount = cnums(noNaN);
    
    ax = subplot(1,5,j);
    boxp = boxplot([pksMax(noNaN,1,j) pksMax(noNaN,2,j) pksMax(noNaN,3,j) pksMax(noNaN,4,j) pksMax(noNaN,5,j)]);
    set(boxp,{'linew'},{3})
    boxcolor = get(boxp,'children');   % Get the handles of all the objects
    
    hold on
    
    lineplot = plot([repmat(1,ntasks,1),repmat(2,ntasks,1),repmat(3,ntasks,1),repmat(4,ntasks,1),repmat(5,ntasks,1)]',...
              [pksMax(noNaN,1,j) pksMax(noNaN,2,j) pksMax(noNaN,3,j) pksMax(noNaN,4,j) pksMax(noNaN,5,j)]','-','Color','k');
    
    p1       = plot(repmat(1,ntasks,1),pksMax(noNaN,1,j), '.','color','cyan', 'MarkerSize', 20);%cap sta, 1
    p2       = plot(repmat(2,ntasks,1),pksMax(noNaN,2,j), '.','color','b', 'MarkerSize', 20);%cap uni, 1
    p3       = plot(repmat(3,ntasks,1),pksMax(noNaN,3,j), '.','color','r', 'MarkerSize', 20);%ear bilateral, 1
    p4       = plot(repmat(4,ntasks,1),pksMax(noNaN,4,j), '.','color','r', 'MarkerSize', 20);%ear unilateral right, 1
    p5       = plot(repmat(5,ntasks,1),pksMax(noNaN,5,j), '.','color','r', 'MarkerSize', 20);%ear unilateral left, 1
    
    dropp{1,j} = [p1.YData; p2.YData; p3.YData; p4.YData; p5.YData;];
    
    %make a table from all data to be plotted
    table2go(j,1) = median(p1.YData);
    table2go(j,2) =   mean(p1.YData);
    table2go(j,3) =    std(p1.YData);
    
    table2go(j,4) = median(p2.YData);
    table2go(j,5) =   mean(p2.YData);
    table2go(j,6) =    std(p2.YData);
    
    table2go(j,7) = median(p3.YData);
    table2go(j,8) =   mean(p3.YData);
    table2go(j,9) =    std(p3.YData);
    
    table2go(j,10) = median(p4.YData);
    table2go(j,11) =   mean(p4.YData);
    table2go(j,12) =    std(p4.YData);
    
    table2go(j,13) = median(p5.YData);
    table2go(j,14) =   mean(p5.YData);
    table2go(j,15) =    std(p5.YData);
    
    
    P1(j,1:length(p1.YData)) = p1.YData;
    P2(j,1:length(p2.YData)) = p2.YData;
    P3(j,1:length(p3.YData)) = p3.YData;
    P4(j,1:length(p4.YData)) = p4.YData;
    P5(j,1:length(p5.YData)) = p5.YData;
    
    title(names{j},'FontSize', 12, 'FontWeight', 'bold')
    box off
    xticks([1 2 3 4 5])
    xticklabels({'Scalp (sta.)','Scalp (ind.)','Ear bilateral', 'Ear right', 'Ear left'})
    
    ylim([0.08 2.2])
    
    a = get(gca,'XTickLabel');
    set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
    xtickangle(45)
    
    if j == 1
        ylabel('Hedges� g','FontSize', 15, 'FontWeight', 'bold')
    elseif j == 5
        legend([p1, p2, p3 p4 p5],{'Scalp (sta.)','Scalp (ind.)','Ear bil.', 'Ear right', 'Ear left'},'location', 'northeast','FontSize', 12, 'FontWeight', 'bold');
        legend boxoff
    end
end
set(gcf,'Color','w')
set(gcf, 'Position', get(0, 'Screensize'));
print(barfigure,'C:\Users\arnd\Pictures\Barfigure','-dpng') %store figure

%% get signal loss (mean hedges'g from scalp(ind.) to all other conditions)

for t = 1:length(taskLetter)
    sl(t,1) = 100 - table2go(t,2) /  table2go(t,5) * 100;
    sl(t,2) = 100 - table2go(t,8) /  table2go(t,5) * 100;
    sl(t,3) = 100 - table2go(t,11) / table2go(t,5) * 100;
    sl(t,4) = 100 - table2go(t,14) / table2go(t,5) * 100;
end

sl = array2table(sl);
writetable(sl,'table_sl.xlsx','Sheet','MyNewSheet','WriteVariableNames',false);
table2go = array2table(table2go);
writetable(table2go,'table_es.xlsx','Sheet','MyNewSheet','WriteVariableNames',false);

%% ANOVA to find differences in signal loss between tasks (factor 1) and ear-EEGs (factor 2)
%dropp: 1 = sta channel, 2 = ind. scalp, 3-5 = ear (bil, uni r., uni. l)
%get all signal losses in percent from scalp(ind.)-condition to the three
%ear-conditions
x=0;
for t = 1:length(taskLetter)
    drop_bi(1,x+1:x+length(dropp{1,t})) = 100 - dropp{1,t}(3,:) ./ dropp{1,t}(2,:) *100; % is the drop from scalp to bilateral ear sign. different between tasks?
    drop_ur(1,x+1:x+length(dropp{1,t})) = 100 - dropp{1,t}(4,:) ./ dropp{1,t}(2,:) *100;
    drop_ul(1,x+1:x+length(dropp{1,t})) = 100 - dropp{1,t}(5,:) ./ dropp{1,t}(2,:) *100;
    g1(1,x+1:x+length(dropp{1,t})) = t;
    x = x + length(dropp{1,t});
end
% g1 = different tasks, g2 = different ear-EEGs
g1 = repmat(g1,1,3);
g2 = {'bi','ur','ul'};
g2 = repelem(g2,1,length(drop_bi));

drop_all = [drop_bi drop_ur drop_ul];

[p,tbl,stats,terms] = anovan(drop_all,{g1,g2},'model','interaction','varnames',{'ERP','ear-EEG'});

[ptask,tbltask,statstask,termstask] = anovan(drop_all,{g1},'varnames',{'tasks'});
[pear,tblear,statsear,termsear]     = anovan(drop_all,{g2},'varnames',{'ear-EEG'});

[mcomp,m,h,nms] = multcompare(statstask,'display','on');
set(gcf,'Color','w')
[mcomp,m,h,nms] = multcompare(statsear ,'display','on');
set(gcf,'Color','w')

%end of script