%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s03d_preprocessing.m
% Get epochs for task d, semantic congruency (N400). 
% Epochs are calculated for the ear- and the scalp-condition.
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Directories and paths

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
EPOCH_ON = -0.2;            % epoch start
EPOCH_OFF =0.8;             % epoch end
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 30;              % cut-off frequency low-pass filter [Hz]
ev = {'condition 7','S 11','S  9','S  1','S  2'}; % event marker for epoching
fs = 100;                  % sampling rate of EEG recording (downsample to this rate!)
baseline = [EPOCH_ON*1000 0]; % definition of baseline
bslength = 0.2; %length of baseline
bslength_m = 0.8; %length of baseline for motor responses
maxR = 2; %longest accepted reaction time
minR = 0.2; %shortest accepted reaction time

load([MAINPATH,'quality_check.mat'],'q_check');
q_check = q_check(4:4:end); % only keep task d, easier for indexing

%% start preprocessing of data after ICA cleaning
for s = 1:length(SBJ)
    if q_check(s).badEarChan == 0
        for ear = 0:1
            
            if ear ==0
                PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
            else
                PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
            end
            PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'d_congruency_oddball',filesep);    % path for script output
            if ~exist (PATHOUT)
                mkdir(PATHOUT)
            end
            
            % locate rawdata-sets
            if ear == 0
                listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\03_ICA_clean\*d_ica_cleaned.set']);  % reads all .set files in PATHIN
            else ear == 1
                listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*d*','_ear.set']);  % reads all .set files in PATHIN
            end
            
            % load ICA cleaned dataset
            subj{s} = listExp.name;
            EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);
            EEG.setname = strrep(EEG.setname, ' resampled', '');
           
            %change marker types (sometimes condition6, sometimes condition 7 for some reason)
            for changemarker = 1: length(EEG.event)
                if contains(EEG.event(changemarker).type,'condition')
                    EEG.event(changemarker).type = ev{1};
                end
            end
            
            if ear == 0
                % apply low and high pass filter
                EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
                EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
            end
            
            % load critical word onsets from the log file and adjust to sampling rate of
            % experiment (CW-onset in audioSBJ given in seconds)
            PATHAUDIO = fullfile(MAINPATH,SBJ{s},filesep, 'log', filesep);    % path for audio-order
            load([PATHAUDIO,'CW_onsets.mat']);
            audioSBJ = audioSBJ*fs*10; %turn seconds into ms, same as in EEG.event.latency
            
            %% add the CW latency to the latency of the stimulus onset. This way, we can
            % epoch around the CW-onset. For baseline correction, find the window
            % before sentence onset
                      
            findstimonset = find(strcmp(ev(3),{EEG.event.type})); %find indices of stimulus onset triggers
            
            for tr = 1:length(findstimonset)
            EEG.event(findstimonset(tr)).latency = EEG.event(findstimonset(tr)).latency + audioSBJ(tr); %add to those trigger latencies the CW-onset latencies
            end
            %now we have both the epoch around the CW-onset and the baseline value from before sentence-onset
            
            % remove unnecessary event marker (e.g. here fixation cross)
            % find all events and save position of EEG.event
            c=1;
            idx = [];
            for e = 1 : length(EEG.event)
                if ~ strcmp(EEG.event(e).type,ev)
                    idx(c) = e;
                    c=c+1;
                end
            end
            
            %find trials with correct/incorrect responses plus missing
            %and wrong responses
            count = [];
            count.congr      = find(strcmp({EEG.event.type}, ev(2)));
            count.incongr    = find(strcmp({EEG.event.type}, ev(1)));
            count.no_resp    = strcmp({EEG.event.type}, ev(3));
            count.no_resp    = find(diff(count.no_resp)==0);
            count.wrong_answ = [find(strcmp({EEG.event.type}, ev(4))) find(strcmp({EEG.event.type}, ev(5)))];
            count.resptrig   = find(strcmp({EEG.event.type}, ev(3)));
            
            if numel([count.congr count.incongr count.no_resp count.wrong_answ]) ~= numel(count.resptrig) 
                warning('Response count does not add up. Check the markers')
            end          
            
            save([MAINPATH,SBJ{s},'\log\resp_d.mat','count']);

            %% Baseline correction
        
            congr_sentences   = EEG;
            incongr_sentences = EEG;
            
            % REMOVE all event markers except the CW-onsets (S9) of all CONGRUENT
            % sentences. This includes every response trigger, every
            % stimulus trigger that does not belond to a congruent
            % sentence, every wrong/no answer and every boundary trigger
            congr_sentences = pop_editeventvals(EEG,'delete',[count.congr count.incongr-1 count.incongr idx count.wrong_answ count.wrong_answ-1 count.no_resp]); %delete the response triggers and the stim-triggers of all incongruent sentences
            congr_sentences = eeg_checkset(congr_sentences);
            congr_sentences = pop_resample(congr_sentences,fs);
            
            % remove all event markers except the CW-onsets (S9) of all INCONGRUENT
            % sentences
            incongr_sentences = pop_editeventvals(EEG,'delete',[count.congr count.congr-1 count.incongr idx count.wrong_answ count.wrong_answ-1 count.no_resp]); %delete the response triggers and the stim-triggers of all congruent sentences
            incongr_sentences = eeg_checkset(incongr_sentences); % the epoch that are generated now are already baseline-corrected
            incongr_sentences = pop_resample(incongr_sentences,fs);
            
            % epoching and baseline
            incongr_sentences = pop_epoch(incongr_sentences, ev(3), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no'); %epoch around the remaining CW-onsets
            congr_sentences   = pop_epoch(congr_sentences, ev(3), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no'); %epoch around the remaining CW-onsets
            
            incongr_sentences = pop_rmbase(incongr_sentences,[-200 0]);
              congr_sentences = pop_rmbase(  congr_sentences,[-200 0]);

            %% some artefact rejection
            incongr_sentences = pop_jointprob(incongr_sentences, 1, [1:incongr_sentences.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            incongr_sentences = eeg_rejsuperpose( incongr_sentences, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep_incongr(s) = {find(incongr_sentences.reject.rejglobal == 1)};
            incongr_sentences = pop_rejepoch( incongr_sentences, incongr_sentences.reject.rejglobal ,0);
            
            congr_sentences = pop_jointprob(congr_sentences, 1, [1:congr_sentences.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            congr_sentences = eeg_rejsuperpose( congr_sentences, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep_congr(s) = {find(congr_sentences.reject.rejglobal == 1)};
            congr_sentences = pop_rejepoch( congr_sentences, congr_sentences.reject.rejglobal ,0);
            
            % save pruned and epoched data set
            if ear == 1
                incongr_sentences.setname = strrep(listExp.name, '_ica_ear.set', '_incongr_ear');
            else ear == 0
                incongr_sentences.setname = strrep(listExp.name, '_ica_cleaned.set', '_incongr');
            end
            incongr_sentences = pop_saveset(incongr_sentences, [incongr_sentences.setname, '.set'], PATHOUT);
            
            if ear == 1
                congr_sentences.setname = strrep(listExp.name, '_ica_ear.set', '_congr_ear');
            else ear == 0
                congr_sentences.setname = strrep(listExp.name, '_ica_cleaned.set', '_congr');
            end
            congr_sentences = pop_saveset(congr_sentences, [congr_sentences.setname, '.set'], PATHOUT);
            
            %get the epochs for sanity check plots
            if ear == 0
                inc(:,:,s) = mean(incongr_sentences.data(:,:,:),3);
                con(:,:,s) = mean(  congr_sentences.data(:,:,:),3);
            else
                incEar(:,:,s) = mean(incongr_sentences.data(:,:,:),3);
                conEar(:,:,s) = mean(  congr_sentences.data(:,:,:),3);
            end
        end
    end
end

save([MAINPATH,'rej_ep_d.mat'], 'rej_ep_congr','rej_ep_incongr' ); %save number of rejected epochs

eeglab redraw

%some plots for sanity checks 
for s = 1:length(SBJ)-1
    figure
    subplot(1,2,1)
    plot(incongr_sentences.times,mean(inc(:,:,s),3))
    title('incongruent')
    ylim([-10 10])
    subplot(1,2,2)
    plot(congr_sentences.times,mean(con(:,:,s),3))
    title('congruent')
    ylim([-10 10])
end

figure
subplot(1,2,1)
plot(incongr_sentences.times,mean(inc(:,:,:),3))
title('incongruent')
ylim([-10 10])
subplot(1,2,2)
plot(congr_sentences.times,mean(con(:,:,:),3))
title('congruent')
ylim([-10 10])

%end of script