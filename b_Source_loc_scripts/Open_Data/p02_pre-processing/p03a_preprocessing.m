%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s03a_preprocessing.m
% Get epochs for task a, directional hearing (N1(att.) and N1). Epochs are
% calculated for the ear- and the scalp-condition.
% Arnd Meiser 30/11/2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Directories and paths

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

load([MAINPATH,'quality_check.mat'],'q_check');
q_check = q_check(1:4:end); % only keep task a, easier for indexing

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 30;                % cut-off frequency low-pass filter [Hz]
ev = {'S  9', 'S  1', 'S  2'}; % event marker for epoching: S9 = stimulus onset, S1 = button press, attention left, S2 = attention right
fs = 100;                  % sampling rate of EEG recording (downsample to this rate!)
baseline = [-200  0]; % definition of baseline period
bslength = 0.2;
epochlength = 0.8;

epochl = 0.75; %1 tone every .75 seconds, making it 4 tones in 3s
epochr = 0.6;

msfac = 1000; % from seconds to milliseconds
bsinms = bslength*msfac;

% on-/offsets left
eOnL0 = epochl * 0 - bslength; % onset tone mix
eOffL0 = epochl * 0 + epochlength; % offset tone mix

eOnL1 = epochl * 1 - bslength; % onset first left tone (the one at 0 will be neglected)
eOffL1 = epochl * 1 + epochlength; % offset of first tone after 800ms

eOnL2 = epochl * 2 - bslength; % onset second left tone (the one at 0 will be neglected)
eOffL2 = epochl * 2 + epochlength; % offset of second tone after 800ms

eOnL3 = epochl * 3 - bslength; % onset third left tone (the one at 0 will be neglected)
eOffL3 = epochl * 3 + epochlength; % offset of third tone after 800ms

%on-/offsets right
eOnR0 = epochr * 0 - bslength; % onset tone mix
eOffR0 = epochr * 0 + epochlength; % offset tone mix

eOnR1 = epochr * 1 - bslength;
eOffR1 = epochr * 1 + epochlength;

eOnR2 = epochr * 2 - bslength;
eOffR2 = epochr * 2 + epochlength;

eOnR3 = epochr * 3 - bslength;
eOffR3 = epochr * 3 + epochlength;

eOnR4 = epochr * 4 - bslength;
eOffR4 = epochr * 4 + epochlength;

for s = 1:length(SBJ)
    if q_check(s).badEarChan == 0 %only process data sets with bad ear-channels
        for ear = 0:1
            
            % locate rawdata-sets and log
            if ear == 0
                listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\03_ICA_clean\*ica_cleaned.set']);  % reads all .set files in PATHIN
            else ear == 1
                listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*_ear.set']);  % reads all .set files in PATHIN
            end
            listLog = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\log\*_a*.log']);
            
            %% start preprocessing of data after ICA cleaning
            h=1;
            
            % set some paths
            if ear ==0
                PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
            else
                PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
            end
            PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'a_attended_speaker',filesep);    % path for script output
            if ~exist (PATHOUT)
                mkdir(PATHOUT)
            end
            
            %load ICA cleaned dataset
            subj{s} = listExp(h).name;
            EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);
            EEG.setname = strrep(EEG.setname, ' resampled', '');
            
            if ear == 0 % ear data has already been filtered
                % apply low and high pass filter
                EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
                EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
                %         else ear == 1
                %             EEG = pop_select( EEG, 'nochannel',{'E92'});
            end
            
            EEG = pop_resample(EEG, fs);
            for t = 1:length([EEG.event.latency])
                EEG.event(t).latency = ceil(EEG.event(t).latency);
            end
            EEGsave = EEG; %helps with debugging
            
            % load in log files to identify stimuli (only used to get number of stimuli to calc number of correct answers)
            logs  = loadtxt( [listLog(h).folder,'\',listLog(h).name], 'blankcell', 'off' );
            sounds = find(strcmp('Sound',logs(:,3)));
            logs = logs(sounds,[3 4 5]);
            logs(1:10,:) = [];
            
            %corr_decisions0 = numel(find(strcmp('condition 10',{EEGsave.event.type})));
            corr_decisions1 = numel(find(strcmp('condition 11',{EEGsave.event.type}))); % correctly identified alternating tone
            corr_decisions2 = numel(find(strcmp('S 13',{EEGsave.event.type}))); % correctly identified ascending tone
            corr_decisions3 = numel(find(strcmp('S 14',{EEGsave.event.type}))); % correctly identified descending tone
            
            corr_decision_sum = corr_decisions1 + corr_decisions2 + corr_decisions3;
            corr_decisions = 100/length(logs) *corr_decision_sum %percentage of correct identification of asc., des. and alt. tones
            
            % remove unnecessary event marker (all except stim onset and left/right markers)
            c=1;
            idx = [];
            for e = 1 : length(EEG.event)
                if ~ strcmp(EEG.event(e).type,ev)
                    idx(c) = e;
                    c=c+1;
                end
            end
            
            % remove all events which are not the stimulus onset or l/r decision
            EEG = pop_editeventvals(EEG,'delete',idx);
            EEG = eeg_checkset(EEG);
            
            % if the first event is a stimulus (s9), but without preceding user
            % decision (S1/S2), delete it. If the last is not a stimulus,
            % delete it as well
            if strcmp({EEG.event(1).type}, 'S  9')
                EEG = pop_editeventvals(EEG,'delete',1);
                EEG = eeg_checkset(EEG);
            end

            % if something went wrong and there is no stimulus after a
            % button press, delete it as well
            try
                for x = 1:2:length(EEG.event)
                    if ~strcmp(EEG.event(x+1).type, 'S  9')
                        EEG.event(x+1) = [];
                    end
                end
            catch
            end
            
            % identify left/right decision of participant (S1 = left, S2 = right)
            leftorright = zeros(1,length(EEG.event)/2);
            z = 1;
            for m = 1:2:length(EEG.event) % if trigger = 1, left side was attended. If not, right
                if strcmp(EEG.event(m).type, 'S  1') % use left triggers (could also use right triggers, does not matter)
                    leftorright(z) = 1;
                    z = z + 1;
                else
                    z = z + 1;
                end
            end
            numberoflefts = numel(find(leftorright==1))
            leftorright = logical(leftorright);
            
            % identify the left/right markers, we don't need them any more
            c=1;
            idx = [];
            for e = 1 : length(EEG.event)
                if ~ strcmp(EEG.event(e).type,ev(1))
                    idx(c) = e;
                    c=c+1;
                end
            end
            
            % remove the left/right markers, we don't need them any more
            EEG = pop_editeventvals(EEG,'delete',idx);
            EEG = eeg_checkset(EEG);
            
            % define new structures containing only left OR right
            EEGla = pop_editeventvals(EEG,'delete',~leftorright); % left attended
            EEGla = eeg_checkset(EEGla);
            EEGra = pop_editeventvals(EEG,'delete',leftorright); % right attended
            EEGra = eeg_checkset(EEGra);
            
            EEG_all_0 = pop_epoch(EEG, ev(1), [eOnL0 eOffL0], 'newname', EEG.setname,'epochinfo', 'no');
            EEG_all_0 = pop_rmbase(EEG_all_0,baseline);
            EEG_all_0 = pop_jointprob(EEG_all_0, 1, [1:EEG_all_0.nbchan],PRUNE, PRUNE,0,0,0); %use jpc on all but the reference channel
            EEG_all_0 = eeg_rejsuperpose( EEG_all_0, 1, 1, 1, 1, 1, 1, 1, 1);
            EEG_all_0 = pop_rejepoch( EEG_all_0, EEG_all_0.reject.rejglobal ,0);
            
            %% epoch around the specified onsets of the attended left trials and
            % remove baseline + bad epochs
            %         EEGra0 = pop_epoch(EEGla, ev(1), [eOnR0 eOffR0], 'newname', EEG.setname,'epochinfo', 'no');
            
            % left, attended side epochs for each tone
            EEGla1 = pop_epoch(EEGla, ev(1), [eOnL1 eOffL1], 'newname', EEG.setname,'epochinfo', 'no');
            EEGla1 = pop_rmbase(EEGla1,[eOnL1*msfac eOnL1*msfac+bsinms]);
            EEGla2 = pop_epoch(EEGla, ev(1), [eOnL2 eOffL2], 'newname', EEG.setname,'epochinfo', 'no');
            EEGla2 = pop_rmbase(EEGla2,[eOnL2*msfac eOnL2*msfac+bsinms]);
            EEGla3 = pop_epoch(EEGla, ev(1), [eOnL3 eOffL3], 'newname', EEG.setname,'epochinfo', 'no');
            EEGla3 = pop_rmbase(EEGla3,[eOnL3*msfac eOnL3*msfac+bsinms]);
            
            %save the mean over trials, we want to look at it later
            la_stage1.EEGla1 = mean(EEGla1.data,3);
            la_stage1.EEGla2 = mean(EEGla2.data,3);
            la_stage1.EEGla3 = mean(EEGla3.data,3);
            
            % now manually create a dataset with the mean over tone-epochs over all trials
            catData = [];
            catData = cat(4,EEGla1.data,EEGla2.data,EEGla3.data); %concatinate with tones 1:3 as the forth dimension (channel*time*trial*toneNum)
            EEG_left_att = EEGla1;
            EEG_left_att.data = mean(catData,4); %mean over tones 1:3
            EEG_left_att.times = baseline(1):1:epochlength*EEG.srate-1;
            EEG_left_att = pop_jointprob(EEG_left_att, 1, [1:EEG_left_att.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            EEG_left_att = eeg_rejsuperpose( EEG_left_att, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep(s,1) = {find(EEG_left_att.reject.rejglobal == 1)};
            EEG_left_att = pop_rejepoch( EEG_left_att, EEG_left_att.reject.rejglobal ,0);
            
            % right,unattended epochs (epochs of the right-sided tones, but from the attended left trials)
            EEGru1 = pop_epoch(EEGla, [ev(1)], [eOnR1 eOffR1], 'newname', EEG.setname,'epochinfo', 'no');
            EEGru2 = pop_epoch(EEGla, [ev(1)], [eOnR2 eOffR2], 'newname', EEG.setname,'epochinfo', 'no');
            EEGru3 = pop_epoch(EEGla, [ev(1)], [eOnR3 eOffR3], 'newname', EEG.setname,'epochinfo', 'no');
            EEGru4 = pop_epoch(EEGla, [ev(1)], [eOnR4 eOffR4], 'newname', EEG.setname,'epochinfo', 'no');
            EEGru1 = pop_rmbase(EEGru1,ceil([eOnR1*msfac eOnR1*msfac+bsinms]));
            EEGru2 = pop_rmbase(EEGru2,ceil([eOnR2*msfac eOnR2*msfac+bsinms]));
            EEGru3 = pop_rmbase(EEGru3,ceil([eOnR3*msfac eOnR3*msfac+bsinms]));
            EEGru4 = pop_rmbase(EEGru4,ceil([eOnR4*msfac eOnR4*msfac+bsinms]));

            
            %save the mean over trials, we want to look at it later
            ru_stage1.EEGru1 = mean(EEGru1.data,3);
            ru_stage1.EEGru2 = mean(EEGru2.data,3);
            ru_stage1.EEGru3 = mean(EEGru3.data,3);
            ru_stage1.EEGru4 = mean(EEGru4.data,3);
            
            catData = [];
            catData =    cat(4,EEGru1.data,EEGru2.data,EEGru3.data,EEGru4.data);
            EEG_right_unatt = EEGru1;
            EEG_right_unatt.data = mean(catData,4);
            EEG_right_unatt.times = baseline(1):1:epochlength*EEG.srate-1;
            EEG_right_unatt = pop_jointprob(EEG_right_unatt, 1, [1:EEG_right_unatt.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            EEG_right_unatt = eeg_rejsuperpose( EEG_right_unatt, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep(s,4) = {find(EEG_right_unatt.reject.rejglobal == 1)};
            EEG_right_unatt = pop_rejepoch( EEG_right_unatt, EEG_right_unatt.reject.rejglobal ,0);
            
            %% epoch around the specified onsets of the attended right trials and
            % remove baseline + bad epochs
            
            % right, attended and unattended side epochs, tone 1
            EEGra1 = pop_epoch(EEGra, [ev(1)], [eOnR1 eOffR1], 'newname', EEG.setname,'epochinfo', 'no');
            EEGra2 = pop_epoch(EEGra, [ev(1)], [eOnR2 eOffR2], 'newname', EEG.setname,'epochinfo', 'no');
            EEGra3 = pop_epoch(EEGra, [ev(1)], [eOnR3 eOffR3], 'newname', EEG.setname,'epochinfo', 'no');
            EEGra4 = pop_epoch(EEGra, [ev(1)], [eOnR4 eOffR4], 'newname', EEG.setname,'epochinfo', 'no');
            EEGra1 = pop_rmbase(EEGra1,ceil([eOnR1*msfac eOnR1*msfac+bsinms]));
            EEGra2 = pop_rmbase(EEGra2,ceil([eOnR2*msfac eOnR2*msfac+bsinms]));
            EEGra3 = pop_rmbase(EEGra3,ceil([eOnR3*msfac eOnR3*msfac+bsinms]));
            EEGra4 = pop_rmbase(EEGra4,ceil([eOnR4*msfac eOnR4*msfac+bsinms]));
            
            %save the mean over trials, we want to look at it later
            ra_stage1.EEGra1 = mean(EEGra1.data,3);
            ra_stage1.EEGra2 = mean(EEGra2.data,3);
            ra_stage1.EEGra3 = mean(EEGra3.data,3);
            ra_stage1.EEGra4 = mean(EEGra4.data,3);
            
            catData = [];
            catData = cat(4,EEGra1.data,EEGra2.data,EEGra3.data,EEGra4.data);
            EEG_right_att = EEGra1;
            EEG_right_att.data = mean(catData,4);
            EEG_right_att.times = baseline(1):1:epochlength*EEG.srate-1;
            EEG_right_att = pop_jointprob(EEG_right_att, 1, [1:EEG_right_att.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            EEG_right_att = eeg_rejsuperpose( EEG_right_att, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep(s,3) = {find(EEG_right_att.reject.rejglobal == 1)};
            EEG_right_att = pop_rejepoch( EEG_right_att, EEG_right_att.reject.rejglobal ,0);
            
            % left, attended side epochs
            EEGlu1 = pop_epoch(EEGra, ev(1), [eOnL1 eOffL1], 'newname', EEG.setname,'epochinfo', 'no');
            EEGlu2 = pop_epoch(EEGra, ev(1), [eOnL2 eOffL2], 'newname', EEG.setname,'epochinfo', 'no');
            EEGlu3 = pop_epoch(EEGra, ev(1), [eOnL3 eOffL3], 'newname', EEG.setname,'epochinfo', 'no');
            EEGlu1 = pop_rmbase(EEGlu1,ceil([eOnL1*msfac eOnL1*msfac+bsinms]));
            EEGlu2 = pop_rmbase(EEGlu2,ceil([eOnL2*msfac eOnL2*msfac+bsinms]));
            EEGlu3 = pop_rmbase(EEGlu3,ceil([eOnL3*msfac eOnL3*msfac+bsinms]));

            %save the mean over trials, we want to look at it later
            lu_stage1.EEGlu1 = mean(EEGlu1.data,3);
            lu_stage1.EEGlu2 = mean(EEGlu2.data,3);
            lu_stage1.EEGlu3 = mean(EEGlu3.data,3);

            catData = [];
            catData =    cat(4,EEGlu1.data,EEGlu2.data,EEGlu3.data); %concatinate with tones 1:3 as the forth dimension (channel*time*trial*toneNum)
            EEG_left_unatt = EEGlu1;
            EEG_left_unatt.data = mean(catData,4); %mean over tones 1:3
            EEG_left_unatt.times = baseline(1):1:epochlength*EEG.srate-1;
            EEG_left_unatt = pop_jointprob(EEG_left_unatt, 1, [1:EEG_left_unatt.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            EEG_left_unatt = eeg_rejsuperpose( EEG_left_unatt, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep(s,2) = {find(EEG_left_unatt.reject.rejglobal == 1)};
            EEG_left_unatt = pop_rejepoch( EEG_left_unatt, EEG_left_unatt.reject.rejglobal ,0);
            
            %% now do the average over left/ right to get final data for attended/unattended
            catData = [];
            catData =    cat(3,EEG_right_att.data,EEG_left_att.data);%% save pruned and epoched data set for attended left (attended and unattended)
            EEG_att =  EEG_right_att;
            EEG_att.times = linspace(-200,799,1000);
            EEG_att.xmax = 0.799; % EEG_att.times(end);
            EEG_att.xmin = -0.2; % EEG_att.times(1);
            EEG_att.data = catData;
            EEG_att.trials = size(EEG_att.data,3);
            
            catData = [];
            catData =    cat(3,EEG_right_unatt.data,EEG_left_unatt.data);%% save pruned and epoched data set for attended left (attended and unattended)
            EEG_unatt =  EEG_right_unatt;
            EEG_unatt.times = linspace(-200,799,1000);
            EEG_unatt.xmax = 0.799; % EEG_att.times(end);
            EEG_unatt.xmin = -0.2; % EEG_att.times(1);
            EEG_unatt.data = catData;
            EEG_unatt.trials = size(EEG_unatt.data,3);
            
            %% save files
            if ear == 1 %change naming of the files depending on condition
                naming = '_ear';
                subj{s}= strrep(listExp(h).name, '_ica_ear.set', '');
            else ear == 0
                naming = [];
                subj{s}= strrep(listExp(h).name, '_ica_cleaned.set', '');
            end
            
            % save the final variables, STAGE 3
            
            % save unattended
            EEG_unatt = eeg_checkset(EEG_unatt);
            EEG_unatt.setname = [subj{s}, '_unatt',naming];
%             EEG_unatt = pop_saveset(EEG_unatt, [EEG_unatt.setname, '.set'], PATHOUT);
            
            % save attended
            EEG_att = eeg_checkset(EEG_att);
            EEG_att.setname = [subj{s}, '_att',naming];
%             EEG_att = pop_saveset(EEG_att, [EEG_att.setname, '.set'], PATHOUT);
            
%             save([PATHOUT,'rej_ep.m'],'rej_ep');
            
            %% Explanation of variables
            %STAGE 0: EEGla + EEGra
            
            % in this stage, e.g. EEGla contains al trials where the left side
            % was attended. We save this variable not as a mean over trials, as
            % we do in stage 1, but we simply save the entire length of each
            % trial where the left side was attended (3s). Here, it does not
            % make sense to split e.g. EEGla into EEGla_attended/unattended,
            % because EEGra is the same as EEGlu (a trial where right is
            % attended is the same as one where left is unattended).
            
            %STAGE 1: la_stage1 + ra_stage1 + lu_stage1 + ru_stage1
            
            % in this stage, e.g. EEGla_all contains each left tone in a trial,
            % where the left side was attended. The tones are stored in this
            % varibale separatly to look into differences between tones within
            % a trial in the same condition (left/right AND attended/unattended)
            
            %STAGE 2: EEG_left_att/EEG_right_att +  EEG_left_unatt/EEG_right_unatt
            
            % in this stage, e.g. EEG_left_att contains the same as EEGla_all,
            % but averaged over tones within a trial: the mean over all for
            % tones from the left side, in a trial where the left side was
            % attended
            
            %STAGE 3: EEG_att +  EEG_unatt
            
            % in this stage, e.g. EEG_att contains the mean over left/right
            % condition in trials where the left/right side was attended,
            % respectively. This is the variable that goes into the effect size
            % calculation
            
            %% PS: remove the BadTrials- entry from the protocol
            % if h==1
            %     listBad = dir(['C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\CvsC2\data\',SBJ{s},'\p*']);
            % else
            %     listBad = dir('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\CvsC\data\a2_attended_speaker\p*');
            % end
            %
            % for xp = 1:length(listBad)
            %     z = load([listBad(xp).folder,'\',listBad(xp).name,'\brainstormstudy.mat']);
            %     z.BadTrials = [];
            %     save([listBad(xp).folder,'\',listBad(xp).name,'\brainstormstudy.mat'],'z');
            % end
            
            %% if needed, save the single left/right/attended/unattended epochs and the full length epochs
            
            CHECKPATH = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'a_attended_speaker',filesep,'checkdata',filesep);    % path for script output
            if ~exist (CHECKPATH)
                mkdir(CHECKPATH)
            end
            
            if ear ==0
                subj{s}= strrep(listExp(h).name, '_ica_cleaned.set', '');
            else
                subj{s}= strrep(listExp(h).name, '_ica_ear.set', '');
            end
            
            % save single tones, STAGE 1
            save([CHECKPATH,SBJ{s},'_singletones',naming,'.mat'],'la_stage1','ra_stage1','lu_stage1','ru_stage1');
            
            % save average over tones within trials, STAGE 2
            
            % save attended left
            EEG_left_att = eeg_checkset(EEG_left_att);
            EEG_left_att.setname = [subj{s}, '_la'];
            EEG_left_att = pop_saveset(EEG_left_att, [EEG_left_att.setname, '_epoch',naming,'.set'], CHECKPATH);
            
            % save attended right
            EEG_right_att = eeg_checkset(EEG_right_att);
            EEG_right_att.setname = [subj{s}, '_ra'];
            EEG_right_att = pop_saveset(EEG_right_att, [EEG_right_att.setname, '_epoch',naming,'.set'], CHECKPATH);
            
            % save unattended left
            EEG_left_unatt = eeg_checkset(EEG_left_unatt);
            EEG_left_unatt.setname = [subj{s}, '_lu'];
            EEG_left_unatt = pop_saveset(EEG_left_unatt, [EEG_left_unatt.setname, '_epoch',naming,'.set'], CHECKPATH);
            
            % save unattended right
            EEG_right_unatt = eeg_checkset(EEG_right_unatt);
            EEG_right_unatt.setname = [subj{s}, '_ru'];
            EEG_right_unatt = pop_saveset(EEG_right_unatt, [EEG_right_unatt.setname, '_epoch',naming,'.set'], CHECKPATH);
            
            % save the complete stimulus length( 200ms baseline + 3s stimulus),
            % STAGE 0
            EEGla = pop_epoch(EEGla, ev(1), [-0.2 3], 'newname', EEG.setname,'epochinfo', 'no');
            EEGra = pop_epoch(EEGra, ev(1), [-0.2 3], 'newname', EEG.setname,'epochinfo', 'no');
            EEGla = pop_rmbase(EEGla,baseline);
            EEGra = pop_rmbase(EEGra,baseline);

            EEGla = pop_jointprob(EEGla, 1, [1:EEGla.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            EEGla = eeg_rejsuperpose( EEGla, 1, 1, 1, 1, 1, 1, 1, 1);
            EEGla = pop_rejepoch( EEGla, EEGla.reject.rejglobal ,0);
            EEGra = pop_jointprob(EEGra, 1, [1:EEGra.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            EEGra = eeg_rejsuperpose( EEGra, 1, 1, 1, 1, 1, 1, 1, 1);
            EEGra = pop_rejepoch( EEGra, EEGra.reject.rejglobal ,0);
            
            EEGla = pop_saveset(EEGla, [EEGla.setname, '_la_full',naming,'.set'], CHECKPATH);
            EEGra = pop_saveset(EEGra, [EEGla.setname, '_ra_full',naming,'.set'], CHECKPATH);
            
            %save the N1-trials
            EEG_all_0.setname = strrep(EEG_all_0.setname, ' resampled', '');
            EEG_all_0 = pop_saveset(EEG_all_0, [EEG_all_0.setname, '_N1_all',naming,'.set'], PATHOUT);
            
            
        end
    end
end

save([MAINPATH,'rej_ep_a.mat'],'rej_ep');

%end of script