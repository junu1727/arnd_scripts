%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s01_ICA.m
% perform an ICA with previous filtering for the rawdata. Data is split up
% into ear- and scalp data, ICA is performed only for the scalp data. 
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Computer-specific DIRECTORIES

%SBJ = {'p01','p02','p03','p04','timing_test'};
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
       ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
       ,'fuhi52','okum71','kuma55','pigo79'
       };
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% locate rawdata-sets
%% PARAMETERS

HP = 1;         % cut-off frequency high-pass filter [Hz] only for ICA
LP = 30;        % cut-off frequency low-pass filter [Hz] only for ICA
SRATE = 100;    % downsample data for ICA
PRUNE = 3;      % artifact rejection threshold in SD (for ICA only)
PCA = 1;        % choose PCA option for ICA
PCADIMS = 64;   % PCA dimension if PCA option is true
%% prepare data and run ICA
load([MAINPATH,'quality_check.mat'],'q_check');

% loop over subjects
for s = 1:length(SBJ) %llop thorugh subjects
    for ear = 0:1 %loop through EEG-conditions (ear- or scalp-EEG)
        listSBJ = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*']);
        listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\01_raw_eeglab\*.set']);  % reads all .set files in PATHIN
        PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'01_raw_eeglab',filesep); % path containing rawdata
        PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '02_ICA_filt', filesep);    % path for script output
        if ~exist (PATHOUT)
            mkdir(PATHOUT)
        end
        
        for h = 1:length(listExp) % loop thorugh tasks         
            % define subject name (based on set-files in PATHIN)
            subj{s} = strrep(listExp(h).name, '.set', '');
            badChan = find(strcmp({q_check.SBJ},subj{s})); %index of the current subject in q_check to find the bad channels
            
            if q_check(badChan).badEarChan == 1 %if there is a bad ear-channel, don't process the dataset any further
                notanalysed(badChan) = 1;
            else
                % load rawdata (already saved as set file)
                EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
                EEG.setname = [subj{s}, '_dummy_ICA'];
                lengthchan = EEG.nbchan;
                
                % transform the naming of the channels (E01 etc.). This way, it
                % will match the naming that it used in pop_select. Otherwise, the
                % channel names are not recognized and the selection will be
                % ignored
                x=1;
                loc = [];
                for g = 1 : lengthchan
                    str = 'E';
                    if g <10
                        num = sprintf( '%02d', g ) ;
                    else
                        num = num2str(g);
                    end
                    strapp = append(str,num);
                    EEG.chanlocs(g).labels = strapp;
                    
                end
                
                %define the needed ear channels, cut the others
                if ear == 1 % ear-channels: only basic filtering
                    
                    EEG = pop_select( EEG, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'}); %ear channels. Depends on used electrode file!
                    EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
                    EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
                    
                else ear == 0 %scalp-channels, ICA plus joint-prop. crit.
                    
                    countChan = []; 
                    if isempty(q_check(badChan).remChans)
                        countChan = []; %if no bad channels are in the dataset, do nothing
                    else
                        for r = 1:length(q_check(badChan).remChans)
                            if iscell(q_check(badChan).remChans)
                                countChan(r) = str2num(q_check(badChan).remChans{1,r}(end-1:end)); %if there are, write them as numbers
                            else
                                countChan(r) = str2num(q_check(badChan).remChans(end-1:end));
                            end
                        end
                        EEG = pop_interp(EEG, countChan, 'spherical'); %interpolate bad channels channels
                    end
                    
                    EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
                    EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
                    EEG = pop_resample(EEG, SRATE);
                    EEGold = EEG;
                    
                    % create dummy events and epoch data to these dummy events
                    EEG = eeg_regepochs(EEG, 'recurrence', 1, 'eventtype', '999');
                    EEG = eeg_checkset(EEG, 'eventconsistency');
                    
                    % remove epochs with artefacts to improve ICA training
                    EEG = pop_jointprob(EEG, 1, [1:size(EEG.data,1)], PRUNE, PRUNE, 0, 1, 0);
                    
                    EEGsave = EEG;
                    
                    % run ICA optional with our without PCA
                    if PCA == 1
                        EEG = pop_runica(EEG, 'icatype', 'runica', 'extended', 1, 'pca',PCADIMS);
                    else
                        EEG = pop_runica(EEG, 'icatype', 'runica', 'extended', 1);
                    end
                    
                    % save clean dataset with ICA weights
                    EEG.setname = [subj{s}, '_ica_filt'];
                    EEGsavestop = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);
                    % EEG = pop_loadset('filename', [subj{s}, '_ica_filt.set'], 'filepath', PATHOUT);
                    EEGsave1 = EEG;
                    
                    
                    % store ICA weights in temporary variables
                    icawinv = EEG.icawinv;
                    icas = EEG.icasphere;
                    icaw = EEG.icaweights;
                    
                    % load original rawdata
                    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
                    
                    %update channel information
                    for f = 1 : length(EEG.chanlocs)
                        EEG.chanlocs(f).labels = EEGsave1.chanlocs(f).labels;
                        EEG.chanlocs(f).labels = EEG.chanlocs(f).labels(find(~isspace(EEG.chanlocs(f).labels)));
                    end
                    
                    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'gui','off');
                    EEG = eeg_checkset( EEG );
                    
                    % write ICA weights to rawdata
                    EEG.icawinv = icawinv;
                    EEG.icasphere = icas;
                    EEG.icaweights = icaw;
                    EEG = eeg_checkset(EEG);
                    
                end
                
                % save new dataset with ICA weights
                if ear == 0
                    EEG.setname = [subj{s}, '_ica'];
                else ear == 1
                    EEG.setname = [subj{s}, '_ica_ear'];
                end
                EEG = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);
                
                eeglab redraw
            end
           % end
        end
    end
end

% end of script