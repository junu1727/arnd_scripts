
%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [50 130 300 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [150 230 600 600]; % end of the time window of interest per task. Should contain the desired effect
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','b','c','d',};
sta_elec = [43 55 55]; %channel number of the standard electrode for all three conditions (cap, earc and ear)
conditionString = {'cap','earc','ear'};
special_N1 = 0; % do plots for the N1 of the attended speaker task
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
tasktitle = {'DATT (N1, n=14)','DATT (n=14)','AODD (n=18)','PODD (n=15)','SECO (n=16)'};
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');
load([MAINPATH,'MaxVar.mat'],'maxVar');


%% Plot 1: GND AVG ERPs for the standard channels

con = fieldnamesr(maxVar,1);
tasks = fieldnamesr(maxVar.cap,1);

for c = 1:length(con)
    for t = 1:length(tasks)
        count = 0; %don't allow empty lines in the final variable. If a task is empty, skip that line instead of writing a zero line
        for s = 1:length(maxVar.cap.c)
            if ~isempty(maxVar.(con{c}).(tasks{t})(s).ERP_cond1)
                
                mean_es.(con{c}).(tasks{t}).ERP_cond1(s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_cond1(1,:);
                mean_es.(con{c}).(tasks{t}).ERP_cond2(s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_cond2(1,:);
                mean_es.(con{c}).(tasks{t}).ERP_stacond1(s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_stacond1;
                mean_es.(con{c}).(tasks{t}).es_matrix_mean(s-count,:) = maxVar.(con{c}).(tasks{t})(s).es_matrix_mean(maxVar.(con{c}).(tasks{t})(s).best_loc(1),:);
                mean_es.(con{c}).(tasks{t}).ERP_stacond2(s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_stacond2;
                mean_es.(con{c}).(tasks{t}).es_matrix_sta(s-count,:) = maxVar.(con{c}).(tasks{t})(s).es_matrix_mean(sta_elec(c),:);
            else
                count = count+1;
            end
        end
    end
end

for c = 1:length(con)
mean_es.(con{c}).('N1').ERP_cond1 = mean_es.(con{c}).('N1').ERP_cond1 *-1;
mean_es.(con{c}).('N1').ERP_cond2 = mean_es.(con{c}).('N1').ERP_cond2 *-1;
mean_es.(con{c}).('a').ERP_cond1 = mean_es.(con{c}).('a').ERP_cond1 *-1;
%mean_es.(con{c}).('b').ERP_cond1 = mean_es.(con{c}).('b').ERP_cond1 *-1;
%mean_es.(con{c}).('b').ERP_cond2 = mean_es.(con{c}).('b').ERP_cond2 *-1;
mean_es.(con{c}).('c').ERP_cond1 = mean_es.(con{c}).('c').ERP_cond1 *-1;
mean_es.(con{c}).('c').ERP_cond2 = mean_es.(con{c}).('c').ERP_cond2 *-1;
%mean_es.(con{c}).('d').ERP_cond1 = mean_es.(con{c}).('d').ERP_cond1 *-1;
mean_es.(con{c}).('d').ERP_cond2 = mean_es.(con{c}).('d').ERP_cond2 *-1;
end

%% plot 
% pure_red_0_5 = [236,168,139]/255;
% pure_blue_0_5 = [127,184,222]/255;
Colors = {'red','blue','yellow','green'};
pure_green =[0.4660 0.6740 0.1880];
pure_red = [0.8500, 0.3250, 0.0980];
pure_blue = [0,0.4470,0.7410];
pure_yellow = [0.9290 0.6940 0.1250];
Shades = [pure_red;pure_blue;pure_yellow;pure_green];
transparency = 0.5;
cd('C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)');

taskLegend1 = {'N1','att','target','target','congruent',};
taskLegend2 = {'N1','unatt','standard','standard','incongruent',};
ylims = [-9.2 3; -3.5 3; -3 3; -8 9.2; -3 4];
vars = fieldnamesr(maxVar.cap.N1,1);
vars = vars([2,5:8]);
vars{6} = 'es_matrix_sta';

for ind = 1:2 % 1 = standard channel, 2 = ind. channel
    if ind == 1
        v1 = 4;
        v2 = 5;
        fig1 = figure;
        f = 6;
    else
        v1 = 2;
        v2 = 3;
        fig2 = figure;
        f = 1;
    end
    
    count = 1;
    d=1;
    
    for c = [3 1]
        for t = 1:length(tasks)
            
            if c == 1 && t == 1
                count = count +5;
            end
            
            ax = subplot(3,5,count);
            
            if t == 1
                ax.Position(3) = ax.Position(3)/5; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
                timeVec =  linspace(50,240,20);
            else
                timeVec =  linspace(-200,790,100);
            end
                        
            ci1 = ciplot(mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1) - std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),...
                mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1) + std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),...
                timeVec,Shades(3,:),transparency);
            ci1.EdgeColor = 'none';
            
            hold on
                        
            if t ~= 1
                ci2 = ciplot(mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),...
                    mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) + std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),...
                    timeVec,Shades(4,:),transparency);
                ci2.EdgeColor = 'none';
                
                ci3 = ciplot((mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)) ...
                    - (std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) -  std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)),...
                    (mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)) ...
                    + (std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) -  std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1)),...
                    timeVec,Shades(d,:),transparency);
                
                ci3.EdgeColor = 'none';
                
                plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),'color',Colors{4},'LineWidth',3)                
            end
            plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),'color',Colors{3},'LineWidth',3)
            
            if t ~= 1
                plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),'color',Colors{d},'LineWidth',3)
            end
            
            ylim(ylims(t,:))
            if count == 1 || count == 11
                xlim([50 250])
            else
                xlim([-200 800])
            end
            
            if count < 6
                title(tasktitle{count},'FontSize', 12, 'FontWeight', 'bold')
            end
            
            count = count +1;

            if t == 1 || t == 2
                ylabel('Amplitude [�V]','FontSize', 12, 'FontWeight', 'bold')
                xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
                %title('N1 (tone onset)','FontSize', 25, 'FontWeight', 'bold')
            end
            if t == 5 && c == 3
                lgd = legend(['Unatt./Stand./' newline 'Stand./Congr.'],['Att./Target/' newline 'Target/Incongr.'],'diff','FontSize', 10, 'FontWeight', 'bold');
                lgd.Position = [0.91,0.77,0.08,0.06];
                legend('boxoff')
            elseif t == 5 && c == 1
                lgd = legend(['Unatt./Stand./' newline 'Stand./Congr.'],['Att./Target/' newline 'Target/Incongr.'],'diff','FontSize', 10, 'FontWeight', 'bold');
                lgd.Position = [0.9075,0.16,0.0762,0.057];
                legend('boxoff')
            else
            end
            box off  
        end
        d = d + 1;
    end
    
    count = 6;

    for t = 1:length(tasks)
        
            ax2 = subplot(3,5,count);
            if t == 1
                ax2.Position(3) = ax2.Position(3)/5; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
                timeVec =  linspace(50,240,20);
            else
                timeVec =  linspace(-200,790,100);
            end
            
            count = count +1;

        ci1 = ciplot(mean(mean_es.(con{1}).(tasks{t}).(vars{f}),1) - std(mean_es.(con{1}).(tasks{t}).(vars{f}),1),...
            mean(mean_es.(con{1}).(tasks{t}).(vars{f}),1) + std(mean_es.(con{1}).(tasks{t}).(vars{f}),1),...
            timeVec,Shades(2,:),transparency);
        ci1.EdgeColor = 'none';
        hold on
        
        ci2 = ciplot(mean(mean_es.(con{2}).(tasks{t}).(vars{f}),1) - std(mean_es.(con{2}).(tasks{t}).(vars{f}),1),...
            mean(mean_es.(con{2}).(tasks{t}).(vars{f}),1) + std(mean_es.(con{2}).(tasks{t}).(vars{f}),1),...
            timeVec,Shades(1,:),transparency);
        ci2.EdgeColor = 'none';
        
        plot(timeVec,mean(mean_es.(con{1}).(tasks{t}).(vars{f}),1),'color',Colors{2},'LineWidth',3)
        plot(timeVec,mean(mean_es.(con{3}).(tasks{t}).(vars{f}),1),'color',Colors{1},'LineWidth',3)
        
        if t ==1
            ylabel('Hedges� g [abs.]','FontSize', 12, 'FontWeight', 'bold')
            xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
            legend('cap','ear','FontSize', 10, 'FontWeight', 'bold','Position',[0.9075,0.47,0.0762,0.057])
            legend('boxoff')
        end
        box off
    end
    
    sgtitle('EEG-signal and effect sizes, Grand Average (N=20)','FontSize', 18, 'FontWeight', 'bold')
    set(gcf,'Color','w')
    set(gcf, 'Position', get(0, 'Screensize'));  
    annotation('textbox',[0.07708,0.9126,0.017708,0.045283],'String','A','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
    annotation('textbox',[0.07708,0.6137,0.017708,0.045283],'String','B','FontSize', 22, 'FontWeight', 'bold','LineStyle','none')
    annotation('textbox',[0.07708,0.3128,0.017708,0.045283],'String','C','FontSize', 22, 'FontWeight', 'bold','LineStyle','none')

end

cd('O:\arm_testing\Experimente\cEEGrid_vs_Cap\')
% f1 = imread('Fig1.jpg'); 
% f2 = imread('Fig2.jpg'); 
% f3 = imread('Fig3.jpg'); 
% f4 = imread('Fig4.jpg'); 
% f5 = imread('Fig5.jpg'); 

axes('pos',[-0.4202 0.0029 0.2 0.2])
imshow('Fig1.jpg'); 
axes('pos',[-0.4202    1.9872 0.2 0.2])
imshow('Fig2.jpg'); 
axes('pos',[-0.4202    2.0651 0.2 0.2])
imshow('Fig3.jpg'); 
axes('pos',[-0.4202    6.5200 0.2 0.2])
imshow('Fig4.jpg'); 
axes('pos',[-0.4202    2.9093 0.2 0.2])
imshow('Fig5.jpg'); 



print(fig1,'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Standard','-dpng')
print(fig2,'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Best','-dpng')
save([MAINPATH,'MeanES.mat'],'mean_es');


