%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana03_preprocessing.m
% Script 3: load ICA cleaned data from ana02
%           filter data based on your experimental design
%           epoch data around your experimental events
%           reject contaminated epochs
%           save pre-processed data in PATHOUT
%
%
% Output: pre-processed EEG set-file cleaned, filtered and epoched
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
% define project folder

SBJ = {'p01','p02'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 40;                % cut-off frequency low-pass filter [Hz]
ev = {'S  9', 'S  1', 'S  2'}; % event marker for epoching
fs = 1000;                  % sampling rate of EEG recording
bslength = 0.2;
baseline = [-0.2*fs  0]; % definition of baseline period
epochl = 0.75;
epochr = 0.6;
epochlength = 0.6;

% on-/offsets left
eOnL1 = epochl - bslength; % onset first left tone (the one at 0 will be neglected)
eOffL1 = epochl + epochlength; % offset of first tone after 600ms

eOnL2 = epochl * 2 - bslength; % onset second left tone (the one at 0 will be neglected)
eOffL2 = epochl * 2 + epochlength; % offset of second tone after 600ms

eOnL3 = epochl * 3 - bslength; % onset third left tone (the one at 0 will be neglected)
eOffL3 = epochl * 3 + epochlength; % offset of third tone after 600ms

%on-/offsets right
eOnR1 = epochr - bslength;
eOffR1 = epochl + epochlength;

eOnR2 = epochr * 2 - bslength;
eOffR2 = epochr * 2 + epochlength;

eOnR3 = epochr * 3 - bslength;
eOffR3 = epochr * 3 + epochlength;

eOnR4 = epochr * 4 - bslength;
eOffR4 = epochr * 4 + epochlength;

%% start preprocessing of data after ICA cleaning

s=1;
h=2;

for s = 1:SBJ
    for h = 1: 4
% locate rawdata-sets
listSBJ = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*']);
listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*ica.set']);  % reads all .set files in PATHIN
listLog = dir('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\*\log\*.log');

% set some paths
PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'a2_attended_speaker',filesep);    % path for script output
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

%load ICA cleaned dataset
subj{s} = listExp(h).name;
EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);
EEGsave = EEG; %helps with debugging

% apply low and high pass filter
EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',1);
EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',1);


    end
end
% end of script

% PS: remove the BadTrials- entry from the protocol (no idea why it marks everything as bad)

listBad = dir('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\CvsC\data\a2_attended_speaker\p*');

for xp = 1:length(listBad)
    z = load([listBad(xp).folder,'\',listBad(xp).name,'\brainstormstudy.mat']);
    z.BadTrials = [];
    save([listBad(xp).folder,'\',listBad(xp).name,'\brainstormstudy.mat'],'z');
end




