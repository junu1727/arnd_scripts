




for taskNum = 2:4
    for conditionNum = 1:3
        
        if taskLetter{taskNum} == 'b' || taskLetter{taskNum} == 'c'
            
            if conditionNum == 1
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta.set']); %standards for the cleaned cap data
                con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev.set']); %deviants for the cleaned cap data
                FullorEar     = 1; % 1 = full, 2 = ear
            elseif conditionNum == 2
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta.set']); %standards for the cleaned cap data
                con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev.set']); %deviants for the cleaned cap data
                FullorEar     = 2; % 1 = full, 2 = ear
            else conditionNum == 3
                %get ear data sets
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta_ear.set']); %standards for the cleaned cap data
                con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev_ear.set']); %deviants for the cleaned cap data
                FullorEar     = 2; % 1 = full, 2 = ear
            end
            
        else taskLetter{taskNum} == 'd'
            if conditionNum == 1
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_congr.set']); %standards for the cleaned cap data
                con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*incongr.set']); %deviants for the cleaned cap data
                FullorEar     = 1; % 1 = full, 2 = ear
            elseif conditionNum == 2
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_congr.set']); %standards for the cleaned cap data
                con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*incongr.set']); %deviants for the cleaned cap data
                FullorEar     = 2; % 1 = full, 2 = ear
            else conditionNum == 3
                %get ear data sets
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_congr_ear.set']); %standards for the cleaned cap data
                con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*incongr_ear.set']); %deviants for the cleaned cap data
                FullorEar     = 2; % 1 = full, 2 = ear
            end
        end
        
        % get folder names
        if taskLetter{taskNum} == 'a'
            folderName = 'a_attended_speaker';
        elseif taskLetter{taskNum} == 'b'
            folderName = 'b_passive_oddball';
        elseif taskLetter{taskNum} == 'c'
            folderName = 'c_active_oddball';
        elseif taskLetter{taskNum} == 'd'
            folderName = 'd_conguency';
        else
            error('please enter a valid taskLetter (a-d)')
        end
        
        PATHIN  = con2path.folder;
        PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
        if ~exist (PATHOUT)
            mkdir(PATHOUT)
        end
        
        % load data: 2 conditions
        condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
        condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
        
        condition1  = pop_resample(condition1, srate);
        condition2  = pop_resample(condition2, srate);
        
        load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\',folderName,'\',conditionString{conditionNum},'_loc.mat'],'best_loc');
        
        
        % use channel_expansion to get all possible bipolar channels from the 96 electrodes
        if size(condition1.data,3) > size(condition2.data,3)
            best_condition1 = mbl_bipolarchannelexpansion(condition1);
            best_condition2 = mbl_bipolarchannelexpansion(condition2);
            sameSize = [];
        elseif size(condition1.data,3) < size(condition2.data,3)
            best_condition2 = mbl_bipolarchannelexpansion(condition1);
            best_condition1 = mbl_bipolarchannelexpansion(condition2);
            sameSize = [];
        else size(condition1.data,3) == size(condition2.data,3)
            sameSize = 1;
        end
        
        
        ERPs.cond1 = best_condition1.data(best_loc(1:10),:,:);
        ERPs.cond2 = best_condition2.data(best_loc(1:10),:,:);
        
        figure
        plot(best_condition1.times, mean(best_condition2.data(best_loc(1:10),:,:),3) - mean(best_condition1.data(best_loc(1:10),:,:),3))
        
        save(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\',folderName,'\',conditionString{conditionNum},'_ERPs.mat'],'ERPs');
        
        
    end
end








