%% calculate and compare the best electrodes between sets

%clear all; close all; clc

%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [50 50 130 250 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 250 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
srate         = 100; % downsample to ...
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','a','b','c','d'};
staElec    = [8 8 8 8 8]; % channel that is usually used to detect the task effect in the literature
conditionString = {'cap','earc','ear'};
special_N1 = 1;
special_M = 0; %unrefined. Not ready to use

%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

if special_N1 == 1 && special_M == 1
    error('You can only do motor analysis OR N1 analysis at a time');
end

for cb = 1:2 % 1 to calc all bipolar channels, 2 to calculate all channels to a common reference
    for s = 1%:length(SBJ)
        
        load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        
        for taskNum = 1:length(taskLetter)
            
            if q_check(taskNum).badEarChan == 0
                
                try
                    for conditionNum = 1:3
                        
                        if conditionNum == 1 || conditionNum == 2
                            isear = [];
                        else conditionNum == 3
                            isear = '_ear';
                        end
                        
                        if taskLetter{taskNum} == 'a'
                            if special_N1 == 0
                                naming1st = ['_att',isear];
                                naming2nd = ['_unatt',isear];
                            else
                                naming1st = ['_N1_all',isear];
                            end
                        elseif taskLetter{taskNum} == 'b' || taskLetter{taskNum} == 'c'
                            naming1st = ['_sta',isear];
                            naming2nd = ['_dev',isear];
                        else taskLetter{taskNum} == 'd'
                            if special_M == 0
                                naming1st = ['_congr',isear];
                                naming2nd = ['_incongr',isear];
                            else
                                naming1st = ['motor_bs_incongr',isear];
                                naming2nd = ['motor_data_incongr',isear];
                            end
                        end
                        
                        %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                        con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'*',naming1st,'.set']); %set 1 for the cleaned cap data
                        con1path = con1path(1);
                        if special_N1 == 0
                        con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},naming2nd,'.set']); %set 2 for the cleaned cap data
                        con2path = con2path(1);
                        end
                        
                        if conditionNum == 1
                            FullorEar     = 1; % 1 = full, 2 = ear
                        else conditionNum == 2 || conditionNum == 3;
                            FullorEar     = 2; % 1 = full, 2 = ear
                        end
                        
                        % get folder names
                        if taskLetter{taskNum} == 'a'
                            folderName = 'a_attended_speaker';
                        elseif taskLetter{taskNum} == 'b'
                            folderName = 'b_passive_oddball';
                        elseif taskLetter{taskNum} == 'c'
                            folderName = 'c_active_oddball';
                        elseif taskLetter{taskNum} == 'd'
                            folderName = 'd_conguency';
                        else
                            error('please enter a valid taskLetter (a-d)')
                        end
                        
                        PATHIN  = con1path.folder;
                        PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
                        if ~exist (PATHOUT)
                            mkdir(PATHOUT)
                        end
                        
                        % load data: 2 conditions
                        condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                        if special_N1 == 0
                            condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
                        else
                            condition2 = [];
                        end
                        
                        %% call function to calculate the largest effect size, the channels contributing to it and the matrix of random permutations used as subsamples from the condition with more trials
                        if   cb == 1
                            [effect_size_matrix, trials, best_loc, bestchans, effect_size_matrix_standard,cond1ERP_it_mean,cond2ERP_it_mean] = effect(...
                                special_N1,condition1,condition2,srate,FullorEar,unilateral,[window_start(taskNum) window_stop(taskNum)],iterations,staElec(taskNum));
                            saveCB = '_bipolar';
                        else cb == 2
                            [effect_size_matrix, trials, best_loc, bestchans, effect_size_matrix_standard,cond1ERP_it_mean,cond2ERP_it_mean] = effect_96(...
                                special_N1,condition1,condition2,srate,FullorEar,unilateral,[window_start(taskNum) window_stop(taskNum)],iterations,staElec(taskNum));
                            saveCB = '_common';
                        end
                        
                        ERPs.cond1 = cond1ERP_it_mean;
                        ERPs.cond2 = cond2ERP_it_mean;
                        
                        if special_N1 == 0
                            specialN = [];
                        else
                            specialN = '_N1';
                        end
                        
                        if unilateral == 1
                            earFolder = '\05_ERP_uni\';
                        elseif unilateral == 0
                            earFolder = '\05_ERP\';
                        end
                        
                        if ~exist([MAINPATH,SBJ{s},earFolder,folderName])
                            mkdir([MAINPATH,SBJ{s},earFolder,folderName])
                        end
                        
                        save([MAINPATH,SBJ{s},earFolder,folderName,'\trials_',conditionString{conditionNum},specialN,saveCB,'.mat'],'trials');
                        save([MAINPATH,SBJ{s},earFolder,folderName,'\g_',conditionString{conditionNum},specialN,'_',num2str(iterations),saveCB,'.mat'],'effect_size_matrix', '-v7.3');
                        save([MAINPATH,SBJ{s},earFolder,folderName,'\',conditionString{conditionNum},specialN,saveCB,'_loc.mat'],'best_loc');
                        save([MAINPATH,SBJ{s},earFolder,folderName,'\',conditionString{conditionNum},specialN,saveCB,'_bestchans.mat'],'bestchans');
                        save([MAINPATH,SBJ{s},earFolder,folderName,'\',conditionString{conditionNum},specialN,saveCB,'_ERPs.mat'],'ERPs');
                        
                        if conditionNum == 1 % if the cap was used, also save the effect size of its standard channel
                            save(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\g_standardcap_effect_',num2str(iterations),specialN,'.mat'],'effect_size_matrix_standard');
                        end
                    end
                catch
                end
            end
        end
    end
end

