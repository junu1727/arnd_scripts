function [effect_size_matrix, trials, best_loc, bestchans, effect_size_matrix_standard,cond1ERP_it_mean,cond2ERP_it_mean] = effect_96(special_N1,condition1,condition2,srate,FullorEar,unilateral,timeWin,iterations,staElec)

%srate: downsample dataset to...Hz
%datasets: names of the two loaded datasets (condition1, the one with more trials, condition2, the one with less trials)
%full or ear: determine whether a full cap or only a subset of electrodes is used: 1 = full, 2 = ear
%unilateral: if only ear is used, decide whether to use both ears or one 0 = bilateral, 1 = unilateral, [] = cap is used, no subselection ofelectrodes needed
%time window: to find the channels with the largest effects, determine the time window in which the effect is expected to be the highest (ms) [timeStart timeStop]
%iterations: how many subsamples should be drown from the condition with more trials?
%taskLetter: a,b,c or d, depending on what task you are checking (string, e.g. 'b')
%staElec: if wanted, get the effect size from a standard cap-electrode for the task (e.g. FPz for P300). Input: index of electrode in chanlocs
%% calculate and compare the best electrodes with the standards + a comparison between cap and ear

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data
if FullorEar == 2 % if only ear was chosen...
    if unilateral == 0 % if both ears are chosen, delete the other cap electrodes
        condition1 = pop_select( condition1, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
        if special_N1 == 0
            condition2 = pop_select( condition2, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
        end
    elseif unilateral == 1 % if only one ear is of interest, delete the other electrodes for both ear- datasets (ICA-cleaned and uncleaened)
        condition1 = pop_select( condition1, 'channel',{'E18','E24','E71','E72','E85','E92'});
        if special_N1 == 0
            condition2 = pop_select( condition2, 'channel',{'E18','E24','E71','E72','E85','E92'});
        end
    else isempty(unilateral)
        disp('Using all cap electrodes')
    end
    % re-reference the ear-data a to common reference behind the right
    % ear (E92)
    condition1 = pop_reref( condition1,  find(strcmp({condition1.chanlocs.labels},{'E92'})));
    condition2 = pop_reref( condition2,  find(strcmp({condition2.chanlocs.labels},{'E92'})));
end


if isempty(timeWin)
    disp('no time window for effect size specified. Using the entire epoch length')
else
    timeStart = find(condition1.times == timeWin(1));
    timeEnd   = find(condition1.times == timeWin(2)) -1;
    if isempty(timeStart) || isempty(timeStart)
        error('invalid time parameters. Please check the sampling rate and the possible time windos')
    end
end

%% Hedge's g

%% get the condition effects from the best channels

% use channel_expansion to get all possible bipolar channels from the 96 electrodes
if special_N1 == 0
    if size(condition1.data,3) > size(condition2.data,3)
        best_condition1 = condition1;
        best_condition2 = condition2;
        sameSize = [];
    elseif size(condition1.data,3) < size(condition2.data,3)
        best_condition2 = condition1;
        best_condition1 = condition2;
        sameSize = [];
    else size(condition1.data,3) == size(condition2.data,3)
        best_condition1 = condition1;
        best_condition2 = condition2;
        sameSize = 1;
    end
else
    best_condition1 = condition1;
    sameSize = 1;
end

if isempty(sameSize)
    % loop through all channel combinations and all trials to get the effect
    % size for every bipolar channel
    trials = zeros(iterations,size(best_condition2.data,3));
    for r = 1:iterations
        trials(r,:) = randperm(size(best_condition1.data,3),size(best_condition2.data,3)); % random permutation of the standard trials matching the number of deviant trials
    end
    
    diff_all = cell(size(best_condition2.data,1), iterations);
    
    tic
    for c = 1:size(best_condition2.data,1)
        if mod(c,500) == 0
            disp([num2str(c),' of ',num2str(size(best_condition2.data,1)),' done!'])
        end
        sta = squeeze(best_condition1.data(c,:,:));
        dev = squeeze(best_condition2.data(c,:,:));
        parfor it = 1:iterations
            diff_all{c,it} = mes(sta(:,trials(it,:))',dev','hedgesg','isDep',0); %where the magic happens. Find the effect size over time
        end
    end
    toc
    
    effect_size_matrix = zeros(size(diff_all,1),size(diff_all,2),iterations);
    for x = 1:size(best_condition2.data,1)
        for y = 1:iterations
            effect_size_matrix(x,:,y) = diff_all{x,y}.hedgesg;
        end
    end
    
    %take the mean over all 1000 iterations of random subsamples
    g_all_mean  = mean(effect_size_matrix,3);
    
else ~isempty(sameSize)
    trials = []; %name variable so there is an output,  even if trials is not needed in this scenario
    diff_all = cell(size(best_condition1.data,1), 1);
    bsWin = find(best_condition1.times == -200):find(best_condition1.times == 0)-1; % baseline time window
    for c = 1:size(best_condition1.data,1)
        if special_N1 == 0
            sta = squeeze(best_condition1.data(c,:,:));
            dev = squeeze(best_condition2.data(c,:,:));
        else
            sta = squeeze(best_condition1.data(c,timeStart:timeEnd,:));
            dev = squeeze(best_condition1.data(c,bsWin,:)); %this is the baseline of all tone-mixes. It is used as a null-comparison
        end
        diff_all{c,1} = mes(sta',dev','hedgesg','isDep',0); %where the magic happens. Find the effect size over time
    end
    
    if special_N1 == 0
        effect_size_matrix = zeros(size(diff_all,1),length(condition1.times),1);
    else
        effect_size_matrix = zeros(size(diff_all,1),length(bsWin),1);
    end
    
    for x = 1:size(best_condition1.data,1)
        effect_size_matrix(x,:) = diff_all{x,1}.hedgesg;
    end
    
    %the mean is not necessary, there are no iterations if both conditions have the same size
    g_all_mean  = effect_size_matrix;
    warning('the trial number for the two conditions is the same. Using no permutations')
end

% find for every channel the largest positive peak
maxAll = [];
valAll = [];
for c = 1:size(g_all_mean,1)
    if special_N1 == 0
        [maxAll(c).pks maxAll(c).locs] = findpeaks(abs(g_all_mean(c,timeStart:timeEnd))); % find the peaks (largest, positive effect size)
    else
        [maxAll(c).pks maxAll(c).locs] = findpeaks(abs(g_all_mean(c,:))); % find the peaks (largest, positive effect size)
    end
    if isempty(maxAll(c).pks) % if there is no positive peak in the effect size over time, set the value to NAN
        maxAll(c).pks = NaN;
        maxAll(c).locs = NaN;
    end
    
    [valAll(c), loc] = max(maxAll(c).pks); % find and store the value...
    locAll(c) = maxAll(c).locs(loc); % ...and location of the most negative peak
end

% get the n best channels
if FullorEar == 0
[~, best_loc] = maxk(valAll,length(best_condition1.chanlocs)); %identify the channels with the largest effect sizes
else FullorEar == 1
[~, best_loc] = maxk(valAll,66); %identify the channels with the largest effect sizes  
end
bestchans = {best_condition1.chanlocs(best_loc).labels}; % these channels have the largest effect sizes for sta and dev

% standard electrode
if FullorEar == 1 &&  ~isempty(staElec)% if full cap is used, also define a standard electrode and find its effect size
    if special_N1 == 0
        
        if size(condition1.data,3) > size(condition2.data,3)
            staCapGcond1 = squeeze(condition1.data(staElec(1),:,:));
            staCapGcond2 = squeeze(condition2.data(staElec(1),:,:));
            sameSize = [];
        elseif size(condition1.data,3) < size(condition2.data,3)
            staCapGcond2 = squeeze(condition1.data(staElec(1),:,:));
            staCapGcond1 = squeeze(condition2.data(staElec(1),:,:));
            sameSize = [];
        else size(condition1.data,3) == size(condition2.data,3)
            staCapGcond1 = squeeze(condition1.data(staElec(1),:,:));
            staCapGcond2 = squeeze(condition2.data(staElec(1),:,:));
            sameSize = 1;
        end
    end
    maxstaCap = [];
    stacapVal = [];
    
    if isempty(sameSize) % if the trial number is not equal in both conditions, use a random subsample of the trials in the larger condition
        
        effect_size_matrix_standard = zeros(iterations,length(condition1.times));
        for it = 1:iterations
            % calc random subselection of trials from the standards to have equal trial
            % lengths
            trials_standard = randperm(size(staCapGcond1,2),size(staCapGcond2,2));
            sta = staCapGcond1(:,trials_standard);
            % calc the effect size over time
            stats_sta_cap_all = mes(sta',staCapGcond2','hedgesg','isDep',0);
            
            effect_size_matrix_standard(it,:) = stats_sta_cap_all.hedgesg;
        end
        
    else ~isempty(sameSize) % if the trial number is equal in both conditions, just use them as they are to get the effect size
        if special_N1 == 0
            stats_sta_cap_all = mes(staCapGcond1',staCapGcond2','hedgesg','isDep',0);
        else
            stats_sta_cap_all = mes(squeeze(condition1.data(staElec(1),timeStart:timeEnd,:))',squeeze(condition1.data(staElec(1),bsWin,:))','hedgesg','isDep',0); % calc the effect size of the standard electrode against its baseline
        end
        effect_size_matrix_standard(1,:) = stats_sta_cap_all.hedgesg;
        
    end
else
    effect_size_matrix_standard = [];
end

if special_N1 == 0
    % get the ERPs of both conditions for the n best channels
    cond1ERP_it_mean = best_condition1.data(best_loc,:,:);
    cond2ERP_it_mean = best_condition2.data(best_loc,:,:);
    
    % plot the effect sizes of the n channels with the largest effect size
    figure
    plot(best_condition1.times, abs(mean(g_all_mean(best_loc(1:5),:,:),3)))
    
    % plot the difference waves of the n channels with the largest effect size
    figure
    plot(best_condition1.times, mean(best_condition2.data(best_loc(1:5),:,:),3) - mean(best_condition1.data(best_loc(1:5),:,:),3))
    
else
    % get the ERPs of both conditions for the n best channels
    cond1ERP_it_mean = best_condition1.data(best_loc,timeStart:timeEnd,:);
    cond2ERP_it_mean = best_condition1.data(best_loc,bsWin,:);
    
    % plot the effect sizes of the n channels with the largest effect size
    figure
    plot(linspace(timeWin(1),timeWin(2)-1,20), abs(mean(g_all_mean(best_loc(1:5),:,:),3)))
    
    % plot the difference waves of the n channels with the largest effect size
    figure
    plot(linspace(50,250,20),  mean(best_condition1.data(best_loc(1:5),bsWin,:),3) -mean(best_condition1.data(best_loc(1:5),timeStart:timeEnd,:),3))
end
end