%% calculate and compare the best electrodes with the standards + a comparison between cap and ear
% Task b: passive auditory oddball

clear all; close all; clc

SBJ = {'p01','p02','p03','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

s = 3;
unilateral = 1;
%get all datasets from the passive oddball task
devcappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*dev.set']);
stacappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*sta.set']);
devearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*dev_ear.set']);
staearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*sta_ear.set']);

% set some paths
PATHIN  = devcappath.folder; % path containing rawdata
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,'c_active_oddball',filesep);    % path for script output
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

%load cap data: dev, sta
dev_cap = pop_loadset('filename', devcappath.name, 'filepath',...
    PATHIN);
sta_cap = pop_loadset('filename', stacappath.name, 'filepath',...
    PATHIN);
%load ear data: dev, sta
dev_ear = pop_loadset('filename', devearpath.name, 'filepath',...
    PATHIN);
sta_ear = pop_loadset('filename', staearpath.name, 'filepath',...
    PATHIN);
%load cap data for clean ear data: dev, sta
clean_dev_ear = pop_loadset('filename', devcappath.name, 'filepath',...
    PATHIN);
clean_sta_ear = pop_loadset('filename', stacappath.name, 'filepath',...
    PATHIN); 

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data 
clean_dev_ear = pop_select( clean_dev_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_dev_ear = pop_reref( clean_dev_ear, find(strcmp({clean_dev_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear
clean_sta_ear = pop_select( clean_sta_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_sta_ear = pop_reref( clean_sta_ear, find(strcmp({clean_sta_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear

if unilateral == 1
    clean_dev_ear = pop_select( clean_dev_ear, 'channel',{'E18','E24','E71','E72','E85'});
    clean_sta_ear = pop_select( clean_sta_ear, 'channel',{'E18','E24','E71','E72','E85'});
    dev_ear       = pop_select( dev_ear,       'channel',{'E18','E24','E71','E72','E85'});
    sta_ear       = pop_select( sta_ear,       'channel',{'E18','E24','E71','E72','E85'});
end

% parameters 
timeStart = find(dev_cap.times == 200);
timeEnd   = find(dev_cap.times == 500) -1;
timewin = timeStart:timeEnd;
fs = dev_cap.srate;
sta_elecs = [ 1 8 51 68 84];


%% ERPs best_cap
dev_cap1 = dev_cap;
sta_cap1 = sta_cap;

dev_cap1.data = mean(dev_cap.data,3);
sta_cap1.data = mean(sta_cap.data,3);

best_cap_dev = mbl_bipolarchannelexpansion(dev_cap1);
best_cap_sta = mbl_bipolarchannelexpansion(sta_cap1);

diff_best_cap = best_cap_sta;
diff_best_cap.data = best_cap_dev.data - best_cap_sta.data;

maxCap = [];
capVal = [];
for c = 1:size(diff_best_cap.data,1)
[maxCap(c).pks maxCap(c).locs] = findpeaks(diff_best_cap.data(c,timewin)); % find the negative peaks
[capVal(c).val loc] = max(maxCap(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxCap(c).locs(loc);
end

best_chan_cap = find(max([capVal.val])==[capVal.val]); %identify the channel with the highest diff. between sta and dev tones
bestchans.cap = best_cap_dev.chanlocs(best_chan_cap).labels; % this channel has the largest diff. amplitude between sta and dev

best_chan_cap_sta = sta_cap1.data(str2num(bestchans.cap(2:3)),:) - sta_cap1.data(str2num(bestchans.cap(end-1:end)),:);
best_chan_cap_dev = dev_cap1.data(str2num(bestchans.cap(2:3)),:) - dev_cap1.data(str2num(bestchans.cap(end-1:end)),:);
best_chan_cap_diff = best_chan_cap_dev - best_chan_cap_sta ;

figure
plot(dev_cap1.times,best_chan_cap_sta)
hold on
plot(dev_cap1.times,best_chan_cap_dev)
plot(dev_cap1.times,best_chan_cap_diff)
legend('sta','dev','diff')
title(['Best cap channel: ',bestchans.cap ])

%% ERPs sta_cap
bestchans.sta_cap = sta_cap1.chanlocs(sta_elecs(2)).labels; 

best_chan_stacap_sta = sta_cap1.data(sta_elecs(2),:);
best_chan_stacap_dev = dev_cap1.data(sta_elecs(2),:);
best_chan_stacap_diff = best_chan_stacap_dev - best_chan_stacap_sta;

chan1.sta_cap = bestchans.sta_cap;

figure
plot(dev_cap1.times,best_chan_stacap_sta)
hold on
plot(dev_cap1.times,best_chan_stacap_dev)
plot(dev_cap1.times,best_chan_stacap_diff)
legend('sta','dev','diff')
title(['Standard cap channel: ',bestchans.sta_cap,' - nose ref.' ])

%% ERPs clean ear
clean_dev_ear1 = clean_dev_ear;
clean_sta_ear1 = clean_sta_ear;

clean_dev_ear1.data = mean(clean_dev_ear.data,3);
clean_sta_ear1.data = mean(clean_sta_ear.data,3);

best_cleanear_dev = mbl_bipolarchannelexpansion(clean_dev_ear1);
best_cleanear_sta = mbl_bipolarchannelexpansion(clean_sta_ear1);

diff_best_cleanear = best_cleanear_dev;
diff_best_cleanear.data = best_cleanear_dev.data - best_cleanear_sta.data;

maxEar = [];
capVal = [];
for c = 1:size(diff_best_cleanear.data,1)
[maxEar(c).pks maxEar(c).locs] = findpeaks(diff_best_cleanear.data(c,timewin)); % find the negative peaks
[capVal(c).val loc] = max(maxEar(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxEar(c).locs(loc);
end

best_chan_cleanear = find(max([capVal.val])==[capVal.val]); % identify the channel with the highest diff. between sta and dev tones
bestchans.cleanear = best_cleanear_dev.chanlocs(best_chan_cleanear).labels; % this channel has the largest diff. amplitude between sta and dev

chan1.clean_ear = find(strcmp({clean_sta_ear1.chanlocs.labels},bestchans.cleanear(1:3)));
chan2.clean_ear = find(strcmp({clean_sta_ear1.chanlocs.labels},bestchans.cleanear(end-2:end)));

best_chan_cleanear_sta = clean_sta_ear1.data(chan1.clean_ear,:) - clean_sta_ear1.data(chan2.clean_ear,:);
best_chan_cleanear_dev = clean_dev_ear1.data(chan1.clean_ear,:) - clean_dev_ear1.data(chan2.clean_ear,:);
best_chan_cleanear_diff = best_chan_cleanear_dev - best_chan_cleanear_sta;

figure
plot(dev_cap1.times,best_chan_cleanear_sta)
hold on
plot(dev_cap1.times,best_chan_cleanear_dev)
plot(dev_cap1.times,best_chan_cleanear_diff)
legend('sta','dev','diff')
title(['Best clean_ear channel: ',bestchans.cleanear ])

%% ERPs ear
dev_ear1 = dev_ear;
sta_ear1 = sta_ear;

dev_ear1.data = mean(dev_ear.data,3);
sta_ear1.data = mean(sta_ear.data,3);

best_ear_dev = mbl_bipolarchannelexpansion(dev_ear1);
best_ear_sta = mbl_bipolarchannelexpansion(sta_ear1);

diff_best_ear = best_ear_dev;
diff_best_ear.data = best_ear_dev.data - best_ear_sta.data;

maxEar = [];
capVal = [];
for c = 1:size(diff_best_ear.data,1)
[maxEar(c).pks maxEar(c).locs] = findpeaks(diff_best_ear.data(c,timewin)); % find the negative peaks
[capVal(c).val loc] = max(maxEar(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxEar(c).locs(loc);
end

best_chan_ear = find(max([capVal.val])==[capVal.val]); % identify the channel with the highest diff. between sta and dev tones
bestchans.ear = best_ear_dev.chanlocs(best_chan_ear).labels; % this channel has the largest diff. amplitude between sta and dev

chan1.ear = find(strcmp({sta_ear1.chanlocs.labels},bestchans.ear(1:3)));
chan2.ear = find(strcmp({sta_ear1.chanlocs.labels},bestchans.ear(end-2:end)));

best_chan_ear_sta = sta_ear1.data(chan1.ear,:) - sta_ear1.data(chan2.ear,:);
best_chan_ear_dev = dev_ear1.data(chan1.ear,:) - dev_ear1.data(chan2.ear,:);
best_chan_ear_diff = best_chan_ear_dev - best_chan_ear_sta;

figure
plot(dev_cap1.times,best_chan_ear_sta)
hold on
plot(dev_cap1.times,best_chan_ear_dev)
plot(dev_cap1.times,best_chan_ear_diff)
legend('sta','dev','diff')
title(['Best ear channel: ',bestchans.ear ])

%% group figures
figure
subplot(2,2,1)
plot(dev_cap1.times,best_chan_cap_sta)
hold on
plot(dev_cap1.times,best_chan_cap_dev)
plot(dev_cap1.times,best_chan_cap_diff)
legend('sta','dev','diff')
title(['Best cap channel: ',bestchans.cap ],'FontSize', 20)
ylim([-8 15])

subplot(2,2,2)
plot(dev_cap1.times,best_chan_stacap_sta)
hold on
plot(dev_cap1.times,best_chan_stacap_dev)
plot(dev_cap1.times,best_chan_stacap_diff)
legend('sta','dev','diff')
title(['Standard cap channel: ',bestchans.sta_cap,' - nose ref.' ],'FontSize', 20)
ylim([-8 15])

subplot(2,2,3)
plot(dev_cap1.times,best_chan_cleanear_sta)
hold on
plot(dev_cap1.times,best_chan_cleanear_dev)
plot(dev_cap1.times,best_chan_cleanear_diff)
legend('sta','dev','diff')
title(['Best clean ear channel: ',bestchans.cleanear ],'FontSize', 20)
ylim([-8 15])

subplot(2,2,4)
plot(dev_cap1.times,best_chan_ear_sta)
hold on
plot(dev_cap1.times,best_chan_ear_dev)
plot(dev_cap1.times,best_chan_ear_diff)
legend('sta','dev','diff')
title(['Best ear channel: ',bestchans.ear ],'FontSize', 20)
ylim([-8 15])


figure
plot(dev_cap1.times,best_chan_cap_diff)
hold on
plot(dev_cap1.times,best_chan_stacap_diff)
plot(dev_cap1.times,best_chan_ear_diff)
plot(dev_cap1.times,best_chan_cleanear_diff)
legend('cap','standard cap','ear','clean ear')
title('Best channels: difference waves','FontSize', 20)
ylim([-8 15])

%% Hedge's g

%% get the sta/dev ERPs from the best channel (CAP)

% get the respective best channel
cap(1) = str2num(bestchans.cap(2:3));
cap(2) = str2num(bestchans.cap(end-1:end));

capGdev = squeeze(dev_cap.data(cap(1),:,:) - dev_cap.data(cap(2),:,:));
capGsta = squeeze(sta_cap.data(cap(1),:,:) - sta_cap.data(cap(2),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
trials = randperm(size(capGsta,2),size(capGdev,2));
capGsta = capGsta(:,trials);

% calc
stats = mes(capGdev',capGsta','hedgesg','isDep',0);

figure
plot(dev_cap.times,stats.hedgesg)
legend('Hedge�s g for the best cap electrodes')
title('Best channels: effect size','FontSize', 20)

%% get the sta/dev ERPs from the best channel (STA_CAP)

staCap(1) = sta_elecs(2);

staCapGdev = squeeze(dev_cap.data(staCap(1),:,:));
staCapGsta = squeeze(sta_cap.data(staCap(1),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
trials = randperm(size(staCapGsta,2),size(staCapGdev,2));
staCapGsta = staCapGsta(:,trials);

% calc
stats = mes(staCapGdev',staCapGsta','hedgesg','isDep',0);

figure
plot(dev_cap.times,stats.hedgesg)
legend('Hedge�s g for the standard cap electrodes')
title('Best channels: effect size','FontSize', 20)

%% get the sta/dev ERPs from the best channel (EAR_CLEAN)

ind_earclean(1) = find(strcmp({clean_sta_ear1.chanlocs.labels},bestchans.cleanear(1:3)));
ind_earclean(2) = find(strcmp({clean_sta_ear1.chanlocs.labels},bestchans.cleanear(end-2:end)));

EarCleanGdev = squeeze(clean_dev_ear.data(ind_earclean(1),:,:) - clean_dev_ear.data(ind_earclean(2),:,:));
EarCleanGsta = squeeze(clean_sta_ear.data(ind_earclean(1),:,:) - clean_sta_ear.data(ind_earclean(2),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
trials = randperm(size(EarCleanGsta,2),size(EarCleanGdev,2));
EarCleanGsta = EarCleanGsta(:,trials);

% calc
stats = mes(EarCleanGdev',EarCleanGsta','hedgesg','isDep',0);

figure
plot(dev_cap.times,stats.hedgesg)
legend('Hedge�s g for the cleaned ear electrodes')
title('Best channels: effect size','FontSize', 20)

%% get the sta/dev ERPs from the best channel (EAR)

ind_ear(1) = find(strcmp({sta_ear1.chanlocs.labels},bestchans.ear(1:3)));
ind_ear(2) = find(strcmp({sta_ear1.chanlocs.labels},bestchans.ear(end-2:end)));

EarGdev = squeeze(dev_ear.data(ind_ear(1),:,:) - dev_ear.data(ind_ear(2),:,:));
EarGsta = squeeze(sta_ear.data(ind_ear(1),:,:) - sta_ear.data(ind_ear(2),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
trials = randperm(size(EarGsta,2),size(EarGdev,2));
EarGsta = EarGsta(:,trials);

% calc
stats = mes(EarGdev',EarGsta','hedgesg','isDep',0);

figure
plot(dev_cap.times,stats.hedgesg)
legend('Hedge�s g for the ear electrodes')
title('Best channels: effect size','FontSize', 20)

