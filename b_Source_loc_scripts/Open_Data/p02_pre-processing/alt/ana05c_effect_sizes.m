%% calculate and compare the best electrodes with the standards + a comparison between cap and ear
% Task b: passive auditory oddball

clear all; close all; clc

% set paths and subject list
SBJ = {'p01','p02','p03','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% settings
s = 3;
unilateral =1; %if 0, use both ears. If 1, use only right ear
window_start = 200; % time window of interest. Should contain the desired effect
window_stop = 500;
SRATE = 100; % sownsample to ...
iterations = 100; % number of iterations to draw subsamples from the condition with higher trial number

%get all datasets from the passive oddball task
devcappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*dev.set']); %deviants for the cleaned cap data
stacappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*sta.set']); %standards for the cleaned cap data
devearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*dev_ear.set']); %deviants for the cleaned ear data
staearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_c_*sta_ear.set']); %standards for the cleaned ear data

% set some paths
PATHIN  = devcappath.folder;
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,'c_active_oddball',filesep); % path for script output
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

%load cap data: dev, sta
dev_cap = pop_loadset('filename', devcappath.name, 'filepath',...
    PATHIN);
sta_cap = pop_loadset('filename', stacappath.name, 'filepath',...
    PATHIN);
%load ear data: dev, sta
dev_ear = pop_loadset('filename', devearpath.name, 'filepath',...
    PATHIN);
sta_ear = pop_loadset('filename', staearpath.name, 'filepath',...
    PATHIN);
%load cap data for clean ear data: dev, sta
clean_dev_ear = pop_loadset('filename', devcappath.name, 'filepath',...
    PATHIN);
clean_sta_ear = pop_loadset('filename', stacappath.name, 'filepath',...
    PATHIN);

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data
clean_dev_ear = pop_select( clean_dev_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_dev_ear = pop_reref( clean_dev_ear, find(strcmp({clean_dev_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear
clean_sta_ear = pop_select( clean_sta_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_sta_ear = pop_reref( clean_sta_ear, find(strcmp({clean_sta_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear

if unilateral == 1 % if only one ear is of interest, delete the other electrodes for both ear- datasets (ICA-cleaned and uncleaened)
    clean_dev_ear = pop_select( clean_dev_ear, 'channel',{'E18','E24','E71','E72','E85'});
    clean_sta_ear = pop_select( clean_sta_ear, 'channel',{'E18','E24','E71','E72','E85'});
    dev_ear       = pop_select( dev_ear,       'channel',{'E18','E24','E71','E72','E85'});
    sta_ear       = pop_select( sta_ear,       'channel',{'E18','E24','E71','E72','E85'});
end

sta_cap       = pop_resample(sta_cap, SRATE);
dev_cap       = pop_resample(dev_cap, SRATE);
clean_sta_ear = pop_resample(clean_sta_ear, SRATE);
clean_dev_ear = pop_resample(clean_dev_ear, SRATE);
sta_ear       = pop_resample(sta_ear, SRATE);
dev_ear       = pop_resample(dev_ear, SRATE);


% parameters
timeStart = find(dev_cap.times == window_start);
timeEnd   = find(dev_cap.times == window_stop) -1;
timewin = timeStart:timeEnd; % recommended time window from ERP-Core for MMN
fs = dev_cap.srate; %sampling rate of all datasets. Should always be the same
sta_elecs = [ 1 8 51 68 84]; % some electrodes that are usually used to detect MMN in the literature

%% Hedge's g

%% get the sta/dev effects from the best channels (CAP)

% use channel_expansion to get all possible bipolar channels from the 96
% electrodes and store their names in chanlocs
best_cap_devg = mbl_bipolarchannelexpansion(dev_cap);
best_cap_stag = mbl_bipolarchannelexpansion(sta_cap);

% loop through all channel combinations and all trials to get the effect
% size for every bipolar channel
maxCap = [];
capVal = [];
trials_cap = zeros(iterations,size(best_cap_devg.data,3));
for r = 1:iterations
trials_cap(r,:) = randperm(size(best_cap_stag.data,3),size(best_cap_devg.data,3)); % random permutation of the standard trials matching the number of deviant trials
end
save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\trials_cap.mat','trials_cap');

diff_cap = cell(size(best_cap_devg.data,1), iterations);

tic
for c = 1:size(best_cap_devg.data,1)      
    sta = squeeze(best_cap_stag.data(c,:,:));
    dev = squeeze(best_cap_devg.data(c,:,:));
    parfor l = 1:iterations
        diff_cap{c,l} = mes(sta(:,trials_cap(l,:))',dev','hedgesg','isDep',0); %where the magic happens. Find the effect size over time       
    end   
end
toc

effect_cap = zeros(length(diff_cap),size(diff_cap,2),iterations);
for x = 1:size(best_cap_devg.data,1)      
    for y = 1:iterations
        effect_cap(x,:,y) = diff_cap{x,y}.hedgesg;
    end
end

% store it immediately after it is created
save(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_cap_',num2str(iterations),'.mat'],'effect_cap', '-v7.3'); 

%take the mean over all 1000 iterations of random subsamples
g_all_cap_mean  = mean(effect_cap,3);

% find for every electrode the largest positive peak
for c = 1:length(g_all_cap_mean)
    [maxCap(c).pks maxCap(c).locs] = findpeaks(abs(g_all_cap_mean(c,:))); % find the peaks (largest, positive effect size)
    
    if isempty(maxCap(c).pks) % if there is no positive peak in the effect size over time, set the value to NAN
        maxCap(c).pks = NaN;
        maxCap(c).locs = NaN;
    end
    
    [capVal(c), loc] = max(maxCap(c).pks); % find and store the value...
    capLoc(c) = maxCap(c).locs(loc); % ...and location of the most negative peak
end

% get the n best channels
[best_chan_cap, best_loc_cap] = maxk(capVal,20); %identify the channels with the largest effect sizes
bestchans = {best_cap_devg.chanlocs(best_loc_cap).labels}; % these channels have the largest effect sizes for sta and dev

save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\cap_loc.mat','best_loc_cap');

% plot the effect sizes of the n channels with the largest effect size
figure
plot(best_cap_stag.times, abs(mean(g_all_cap_mean(1:10,:,:),3)))

% plot the difference waves of the n channels with the largest effect size
figure
plot(best_cap_stag.times, mean(best_cap_devg.data(1:10,:,:),3) - mean(best_cap_stag.data(1:10,:,:),3))


%% get the sta/dev effects from the best channel (STA_CAP)

staCap = sta_elecs(2);

staCapGdev = squeeze(dev_cap.data(staCap(1),:,:));
staCapGsta = squeeze(sta_cap.data(staCap(1),:,:));

maxCap = [];
capVal = [];
g_all_stan = zeros(length(dev_cap.data),iterations);
for l = 1:iterations
    % calc random subselection of trials from the standards to have equal trial
    % lengths
    trials = randperm(size(staCapGsta,2),size(staCapGdev,2));
    sta = staCapGsta(:,trials);
    % calc the effect size over time
    stats_sta_cap_all = mes(sta',staCapGdev','hedgesg','isDep',0);
    
    g_all_stan(l,:) = stats_sta_cap_all.hedgesg;
end

save(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_standard_',num2str(iterations),'.mat'],'g_all_stan');

effect_standard = mean(g_all_stan,1);

% find for the electrode the largest positive peak
[maxCap.pks maxCap.locs] = findpeaks(abs(effect_standard)); % find the peaks (largest, positive effect size)      !!!!!!adjust time window for best channel selection!!!!!!!
[capVal, loc] = max(maxCap.pks); % find and store the value...
capLoc = maxCap.locs(loc); % ...and location of the most negative peak

figure
plot(dev_cap.times,abs(effect_standard))
legend('Hedge�s g for the standard cap electrodes')

%% get the sta/dev effects from the best channel (EAR_CLEAN)

% use channel_expansion to get all possible bipolar channels from the 96
% electrodes and store their names in chanlocs
best_earc_stag = mbl_bipolarchannelexpansion(clean_sta_ear);
best_earc_devg = mbl_bipolarchannelexpansion(clean_dev_ear);

% loop through all channel combinations and all trials to get the effect
% size for every bipolar channel
maxCap = [];
capVal = [];
trials_earc = zeros(iterations,size(best_earc_devg.data,3));
for r = 1:iterations
trials_earc(r,:) = randperm(size(best_earc_stag.data,3),size(best_earc_devg.data,3)); % random permutation of the standard trials matching the number of deviant trials
end
save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\trials_earc.mat','trials_earc');

diff_earc = cell(size(best_earc_devg.data,1), iterations);

tic
for c = 1:size(best_earc_devg.data,1)      
    sta = squeeze(best_earc_stag.data(c,:,:));
    dev = squeeze(best_earc_devg.data(c,:,:));
    parfor l = 1:iterations
        diff_earc{c,l} = mes(sta(:,trials_earc(l,:))',dev','hedgesg','isDep',0); %where the magic happens. Find the effect size over time       
    end   
end
toc

effect_earc = zeros(size(best_earc_devg.data,1)   ,size(diff_earc,2),iterations);
for x = 1:size(best_earc_devg.data,1)
    for y = 1:iterations
        effect_earc(x,:,y) = diff_earc{x,y}.hedgesg;
    end
end

% store it immediately after it is created
save(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_earc_',num2str(iterations),'.mat'],'effect_earc', '-v7.3'); 

%take the mean over all 1000 iterations of random subsamples
g_all_earc_mean  = mean(effect_earc,3);

% find for every electrode the largest positive peak
for c = 1:size(g_all_earc_mean,1)
    [maxCap(c).pks maxCap(c).locs] = findpeaks(abs(g_all_earc_mean(c,:))); % find the peaks (largest, positive effect size)      !!!!!!adjust time window for best channel selection!!!!!!!
    
    if isempty(maxCap(c).pks) % if there is no positive peak in the effect size over time, set the value to NAN
        maxCap(c).pks = NaN;
        maxCap(c).locs = NaN;
    end
    
    [capVal(c), loc] = max(maxCap(c).pks); % find and store the value...
    capLoc(c) = maxCap(c).locs(loc); % ...and location of the most negative peak
end

% get the n best channels
[best_chan_earc, best_loc_earc] = maxk(capVal,10); %identify the channels with the largest effect sizes
bestchans_earc = {best_earc_devg.chanlocs(best_loc_earc).labels}; % these channels have the largest effect sizes for sta and dev

save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\earc_loc.mat','best_loc_earc');

% plot the effect sizes of the n channels with the largest effect size
figure
plot(best_earc_stag.times, abs(g_all_earc_mean(best_loc_earc,:)))

% plot the difference waves of the n channels with the largest effect size
figure
plot(best_earc_stag.times, mean(best_earc_devg.data(best_loc_earc,:,:),3) - mean(best_earc_stag.data(best_loc_earc,:,:),3))

% note: the difference waves are based on channel combinations where every
% electrode constituting the channel is sometimes being subtracted from and
% sometimes subtracted from the other (e.g. {'E71 - E72'} or {'E24 - E71'})
% As a result, the complete time course of the difference wave is sometimes
% reversed. For the effect sizes, we can just take the absolut of these
% values, it does not change the outcome, but it does take away the
% information of the direction of the effect.
%% get the sta/dev effects from the best channel (EAR)

% use channel_expansion to get all possible bipolar channels from the 96
% electrodes and store their names in chanlocs
best_ear_stag = mbl_bipolarchannelexpansion(sta_ear);
best_ear_devg = mbl_bipolarchannelexpansion(dev_ear);

% loop through all channel combinations and all trials to get the effect
% size for every bipolar channel
maxCap = [];
capVal = [];
trials_ear = zeros(iterations,size(best_ear_devg.data,3));
for r = 1:iterations
trials_ear(r,:) = randperm(size(best_ear_stag.data,3),size(best_ear_devg.data,3)); % random permutation of the standard trials matching the number of deviant trials
end
save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\trials_ear.mat','trials_ear');

diff_ear = cell(size(best_ear_devg.data,1), iterations);

tic
for c = 1:size(best_ear_devg.data,1)      
    sta = squeeze(best_ear_stag.data(c,:,:));
    dev = squeeze(best_ear_devg.data(c,:,:));
    parfor l = 1:iterations
        diff_ear{c,l} = mes(sta(:,trials_ear(l,:))',dev','hedgesg','isDep',0); %where the magic happens. Find the effect size over time       
    end   
end
toc

effect_ear = zeros(length(diff_ear),size(diff_ear,2),iterations);
for x = 1:size(best_ear_devg.data,1)
    for y = 1:iterations
        effect_ear(x,:,y) = diff_ear{x,y}.hedgesg;
    end
end

% store it immediately after it is created
save(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_ear_',num2str(iterations),'.mat'],'effect_ear', '-v7.3'); 

%take the mean over all 1000 iterations of random subsamples
g_all_ear_mean  = mean(effect_ear,3);

% find for every electrode the largest positive peak
for c = 1:size(g_all_ear_mean,1)
    [maxCap(c).pks maxCap(c).locs] = findpeaks(abs(g_all_ear_mean(c,:))); % find the peaks (largest, positive effect size)      !!!!!!adjust time window for best channel selection!!!!!!!
    
    if isempty(maxCap(c).pks) % if there is no positive peak in the effect size over time, set the value to NAN
        maxCap(c).pks = NaN;
        maxCap(c).locs = NaN;
    end
    
    [capVal(c), loc] = max(maxCap(c).pks); % find and store the value...
    capLoc(c) = maxCap(c).locs(loc); % ...and location of the most negative peak
end

% get the n best channels
[best_chan_ear, best_loc_ear] = maxk(capVal,10); %identify the channels with the largest effect sizes
bestchans_ear = {best_ear_devg.chanlocs(best_loc_ear).labels}; % these channels have the largest effect sizes for sta and dev

save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\ear_loc.mat','best_loc_ear');

% plot the effect sizes of the n channels with the largest effect size
figure
plot(best_ear_stag.times, abs(g_all_ear_mean(best_loc_ear,:)))

% plot the difference waves of the n channels with the largest effect size
figure
plot(best_ear_stag.times, mean(best_ear_devg.data(best_loc_ear,:,:),3) - mean(best_ear_stag.data(best_loc_ear,:,:),3))

%run('O:\arm_testing\Scripts\b_Source_loc_scripts\pre-processing\ana05c_effect_sizes.m');

%% group plots for g

%% load the effect sizes, channel*time*iteration
% % load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_cap_',num2str(iterations),'.mat']);
% % load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_earc_',num2str(iterations),'.mat']);
% % load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_ear_',num2str(iterations),'.mat']);
% % load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\g_standard_',num2str(iterations),'.mat']);
% 
% % load the locations of the best channels
% load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\cap_loc.mat');
% load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\earc_loc.mat');
% load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\ear_loc.mat');
% 
% % load the channel files for indexing
% load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\cap_chan.mat');
% load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\ear_chan.mat');
% 
% % load time axis
% load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p03\05_ERP\c_active_oddball\time_axis.mat','timing');

cap_mean  = mean(effect_cap,3);
earc_mean = mean(effect_earc,3);
ear_mean  = mean(effect_ear,3);

figure
subplot(2,2,1)
plot(best_cap_devg.times,abs(cap_mean(best_loc_cap(1),:,:)))
legend('Hedge�s g for the best cap electrodes')
ylim([0 1])
subplot(2,2,2)
plot(best_cap_devg.times,abs(effect_standard))
legend('Hedge�s g for the standard cap electrode')
ylim([0 1])
subplot(2,2,3)
plot(best_cap_devg.times,abs(earc_mean(best_loc_earc(1),:,:)))
legend('Hedge�s g for the cleaned ear electrodes')
ylim([0 1])
subplot(2,2,4)
plot(best_cap_devg.times,abs(ear_mean(best_loc_ear(1),:,:)))
legend('Hedge�s g for the ear electrodes')
ylim([0 1])

figure
plot(best_cap_devg.times,abs(cap_mean(best_loc_cap(1),:,:)))
hold on
plot(best_cap_devg.times,abs(effect_standard))
plot(best_cap_devg.times,abs(earc_mean(best_loc_earc(1),:,:)))
plot(best_cap_devg.times,abs(ear_mean(best_loc_ear(1),:,:)))
legend('cap','standard','ear cleaned','ear','FontSize', 12, 'FontWeight', 'bold')
ylim([0 1])
ylabel('Hedge�s g [abs]','FontSize', 12, 'FontWeight', 'bold')
xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
title('Hedges g over time, MMN (sta/dev)','FontSize', 12, 'FontWeight', 'bold')



% figure
% p1 = plot(dev_cap.times,stats_cap.hedgesg)
% p1(1).LineWidth = 2;
% hold on
% p2 = plot(dev_cap.times,g_all_stan.hedgesg)
% p2(1).LineWidth = 2;
% p3 = plot(dev_cap.times,stats_ear_clean.hedgesg)
% p3(1).LineWidth = 2;
% p4 = plot(dev_cap.times,stats_ear.hedgesg)
% p4(1).LineWidth = 2;
% ax = gca;
% ax.FontSize = 16;
% legend(['Full cap ',bestchans.cap ],['Standard cap ',bestchans.sta_cap ],['Clean ear ',bestchans.cleanear ],['Ear ',bestchans.ear ],'FontSize',25)
% title('MMN best channels: effect size','FontSize',35,'FontWeight','bold')
% xlabel('time [ms]','FontSize',25,'FontWeight','bold');
% ylabel('Hedge�s g','FontSize',25,'FontWeight','bold');
% 
% bar_effect_g(1) = max(stats_cap.hedgesg(300:500))
% bar_effect_g(2) = max(g_all_stan.hedgesg(300:500))
% bar_effect_g(3) = max(stats_ear_clean.hedgesg(300:500))
% bar_effect_g(4) = max(stats_ear.hedgesg(300:500))
% 
% naming = categorical({'Full cap','Standard cap','Clean ear','Ear'});
% naming = reordercats(naming,{'Full cap','Standard cap','Clean ear','Ear'});
% figure
% bar(naming,bar_effect_g)
% a = get(gca,'XTickLabel');
% set(gca,'XTickLabel',a,'FontName','Times','fontsize',25)
% ylabel('Hedge�s g')
% 
% perc_loss =  100- bar_effect_g(4)/ bar_effect_g(1)*100
