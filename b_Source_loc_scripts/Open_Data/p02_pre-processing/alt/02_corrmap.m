%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana02_corrmap.m
%  Script 2: visually inspect ICA components of all subjects and remove them. Save the
%  cleaned subject data
%
%
% Output: EEG set-file pruned with ICA
%         MAT-file with saved components (output from CORRMAP)
%
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Computer-specific DIRECTORIES

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
       ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
       ,'fuhi52','okum71','kuma55','pigo79'
       };
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% 1. plot and save all ICA components for later manual selection of template
% components

for s = 1:length(SBJ)
    % set some paths
    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '03_ICA_clean', filesep);    % path for script output
    if ~exist (PATHOUT)
        mkdir(PATHOUT)
    end
    
    listSBJ = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*']);
    listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*ica.set']);  % reads all .set files in PATHIN
    numComps = 64; %number of ICA components to be shown
    
    for h = 1: length(listExp)
        
        % load dataset
        subj{s}= strrep(listExp(h).name, '.set', '');
        EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
        
%         %reject alpha-related activity (experimental, need to find the reason for this time-locked activity)
%         EEG = pop_eegfiltnew(EEG,'locutoff',8,'plotfreqz',0);
%         EEG = pop_eegfiltnew(EEG,'hicutoff',12,'plotfreqz',0);
        

        % plot ERPs of the components to check if some components load
        % explicitly on single channels
%         figure;
%         for x = 1:size(EEG.icaweights,1)
%             subplot(12,8,x)
%             plot((EEG.icaweights(x,:)));
%         end
        
        % plot ICA components for each subject and inspect the time course of
        % suspicious components
        pop_selectcomps(EEG, 1:numComps); %this function was altered - in line 171 of the original code, replace pop_prop with pop_prop_extended
        
        [val EEG.badcomps] = find(EEG.reject.gcompreject == 1 ); %store ind of bad components
        
        % remove selected components from dataset
        EEG = pop_subcomp(EEG, [], 1);
        EEG = eeg_checkset(EEG);
        
        % save ICA cleaned dataset in ana02
        EEG.setname = [subj{s}, '_cleaned'];
        
        EEG = pop_saveset( EEG, 'filename', [EEG.setname, '.set'], 'filepath',...
            PATHOUT);
        eeglab redraw
    end
end
% end of script