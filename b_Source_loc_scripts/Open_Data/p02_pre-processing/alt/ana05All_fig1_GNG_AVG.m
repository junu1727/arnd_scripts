
%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [50 130 300 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [150 230 600 600]; % end of the time window of interest per task. Should contain the desired effect
srate         = 100; % downsample to ...
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','b','c','d',};
staElec    = [8 8 8 8]; % channel that is usually used to detect the task effect in the literature
conditionString = {'cap','earc','ear'};
special_N1 = 0; % do plots for the N1 of the attended speaker task
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,250,20);
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');

%% plots for g
for cb = 1
    for s = 1:length(SBJ)
        
        load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        
        effect_cap  = [];
        effect_earc = [];
        effect_ear  = [];
        
        for taskNum = 1:4
            if q_check(taskNum).badEarChan == 0
                for conditionNum = 1:3
                    
                    timeWin = find(timeVec == window_start(taskNum)) : find(timeVec == window_stop(taskNum)) -1;
                    
                    % get folder names
                    if taskLetter{taskNum} == 'a'
                        folderName = 'a_attended_speaker';
                    elseif taskLetter{taskNum} == 'b'
                        folderName = 'b_passive_oddball';
                    elseif taskLetter{taskNum} == 'c'
                        folderName = 'c_active_oddball';
                    elseif taskLetter{taskNum} == 'd'
                        folderName = 'd_conguency';
                    else
                        error('please enter a valid taskLetter (a-d)')
                    end
                    
                    %             folderName = 'a_attended_speaker';
                    %             specialN = [];
                    %             conditionNum = 1;
                    %             taskNum = 4;
                    
                    if   cb == 1
                        saveCB = '_bipolar';
                    else cb == 2
                        saveCB = '_common';
                    end
                    
                    % load the effect sizes, channel*time*iteration
                    load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\trials_',conditionString{conditionNum},saveCB,'.mat'],'trials');
                    load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\g_',conditionString{conditionNum},'_',num2str(iterations),saveCB,'.mat'],'effect_size_matrix');
                    load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},saveCB,'_loc.mat'],'best_loc');
                    load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},saveCB,'_bestchans.mat'],'bestchans');
                    load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},saveCB,'_ERPs.mat'],'ERPs');
                    %                     if conditionNum == 1 % if the cap was used, also save the effect size of its standard channel
                    %                         load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\g_standardcap_effect_',num2str(iterations),saveCB,'.mat'],'effect_size_matrix_standard');
                    %                     end
                    
                    %transform the ERPs to be in the right direction (because of the way we get all bipolar channels,
                    %the same channel is sometimes subtracted and sometimes subtracted from. Therefore, the sign is sometimes reversed)
                    if taskNum == 1
                        timeWin = 1:size(effect_size_matrix,2);
                    end
                    
                    if taskLetter{taskNum} ~= 'c'
                        [pks, locs]= max(abs(smoothdata(mean(effect_size_matrix(best_loc(1),timeWin,:),3),2,'movmean',4)));
                        [val, loc] = max(pks); % find and store the value...
                        locs = locs(loc); % ...and location of the largest peak
                        
                        if smoothdata(mean(ERPs.cond1(1,timeWin(1)+locs-1,:),3) - mean(ERPs.cond2(1,timeWin(1)+locs-1,:),3),2,'movmean',4) > 0
                            ERPs.cond1 = ERPs.cond1 * -1;
                            ERPs.cond2 = ERPs.cond2 * -1;
                        end
                    else
                        [pks, locs]= max(abs(smoothdata(mean(effect_size_matrix(best_loc(1),timeWin,:),3),2,'movmean',4)));
                        [val, loc] = max(pks); % find and store the value...
                        locs = locs(loc); % ...and location of the largest peak
                        if smoothdata(mean(ERPs.cond1(1,timeWin(1)+locs-1,:),3) - mean(ERPs.cond2(1,timeWin(1)+locs-1,:),3),2,'movmean',4) < 0
                            ERPs.cond1 = ERPs.cond1 * -1;
                            ERPs.cond2 = ERPs.cond2 * -1;
                        end
                    end
                    
                    if taskNum == 1
                        if conditionNum == 1
                            effect_cap.a.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.a.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.a.best_loc           = best_loc; % indices of the best n channels
                            effect_cap.a.bestchans          = bestchans; % string names of the n best channels
                            effect_cap.a.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.a.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.a.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.a.best_loc           = best_loc; % indices of the best n channels
                            effect_earc.a.bestchans          = bestchans; % string names of the n best channels
                            effect_earc.a.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.a.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.a.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.a.best_loc           = best_loc; % indices of the best n channels
                            effect_ear.a.bestchans          = bestchans; % string names of the n best channels
                            effect_ear.a.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                        
                    elseif taskNum == 2
                        if conditionNum == 1
                            effect_cap.b.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.b.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.b.best_loc           = best_loc; % indices of the best n channels
                            effect_cap.b.bestchans          = bestchans; % string names of the n best channels
                            effect_cap.b.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.b.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.b.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.b.best_loc           = best_loc; % indices of the best n channels
                            effect_earc.b.bestchans          = bestchans; % string names of the n best channels
                            effect_earc.b.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.b.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.b.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.b.best_loc           = best_loc; % indices of the best n channels
                            effect_ear.b.bestchans          = bestchans; % string names of the n best channels
                            effect_ear.b.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                    elseif taskNum == 3
                        if conditionNum == 1
                            effect_cap.c.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.c.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.c.best_loc           = best_loc; % indices of the best n channels
                            effect_cap.c.bestchans          = bestchans; % string names of the n best channels
                            effect_cap.c.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.c.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.c.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.c.best_loc           = best_loc; % indices of the best n channels
                            effect_earc.c.bestchans          = bestchans; % string names of the n best channels
                            effect_earc.c.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.c.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.c.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.c.best_loc           = best_loc; % indices of the best n channels
                            effect_ear.c.bestchans          = bestchans; % string names of the n best channels
                            effect_ear.c.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                    else taskNum == 4
                        if conditionNum == 1
                            effect_cap.d.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.d.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.d.best_loc           = best_loc; % indices of the best n channels
                            effect_cap.d.bestchans          = bestchans; % string names of the n best channels
                            effect_cap.d.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.d.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.d.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.d.best_loc           = best_loc; % indices of the best n channels
                            effect_earc.d.bestchans          = bestchans; % string names of the n best channels
                            effect_earc.d.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.d.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.d.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.d.best_loc           = best_loc; % indices of the best n channels
                            effect_ear.d.bestchans          = bestchans; % string names of the n best channels
                            effect_ear.d.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                    end
                end
            end
        end
        
        %     aN1_cap = smoothdata(abs(effect_cap.aN1.es_matrix_mean(effect_cap.aN1.best_loc(1),:,:)),2,'movmean',4);
        %     aN1_earc = smoothdata(abs(effect_earc.aN1.es_matrix_mean(effect_earc.aN1.best_loc(1),:,:)),2,'movmean',4);
        %     aN1_ear = smoothdata(abs(effect_ear.aN1.es_matrix_mean(effect_ear.aN1.best_loc(1),:,:)),2,'movmean',4);
        
        if isfield(effect_cap,'a')
            a_cap(s,:) = smoothdata(abs(effect_cap.a.es_matrix_mean(effect_cap.a.best_loc(1),:,:)),2,'movmean',4);
            a_earc(s,:) = smoothdata(abs(effect_earc.a.es_matrix_mean(effect_earc.a.best_loc(1),:,:)),2,'movmean',4);
            a_ear(s,:) = smoothdata(abs(effect_ear.a.es_matrix_mean(effect_ear.a.best_loc(1),:,:)),2,'movmean',4);
        end
        if isfield(effect_cap,'b')
            b_cap(s,:) = smoothdata(abs(effect_cap.b.es_matrix_mean(effect_cap.b.best_loc(1),:,:)),2,'movmean',4);
            b_earc(s,:) = smoothdata(abs(effect_earc.b.es_matrix_mean(effect_earc.b.best_loc(1),:,:)),2,'movmean',4);
            b_ear(s,:) = smoothdata(abs(effect_ear.b.es_matrix_mean(effect_ear.b.best_loc(1),:,:)),2,'movmean',4);
        end
        if isfield(effect_cap,'c')
            c_cap(s,:) = smoothdata(abs(effect_cap.c.es_matrix_mean(effect_cap.c.best_loc(1),:,:)),2,'movmean',4);
            c_earc(s,:) = smoothdata(abs(effect_earc.c.es_matrix_mean(effect_earc.c.best_loc(1),:,:)),2,'movmean',4);
            c_ear(s,:) = smoothdata(abs(effect_ear.c.es_matrix_mean(effect_ear.c.best_loc(1),:,:)),2,'movmean',4);
        end
        if isfield(effect_cap,'d')
            d_cap(s,:) = smoothdata(abs(effect_cap.d.es_matrix_mean(effect_cap.d.best_loc(1),:,:)),2,'movmean',4);
            d_earc(s,:) = smoothdata(abs(effect_earc.d.es_matrix_mean(effect_earc.d.best_loc(1),:,:)),2,'movmean',4);
            d_ear(s,:) = smoothdata(abs(effect_ear.d.es_matrix_mean(effect_ear.d.best_loc(1),:,:)),2,'movmean',4);
        end
        %     aN1_capd = smoothdata(mean(effect_cap.aN1.ERPs.cond1(1,:,:),3) - mean(effect_cap.aN1.ERPs.cond2(1,:,:),3),2,'movmean',4);
        %     aN1_earcd = smoothdata(mean(effect_earc.aN1.ERPs.cond1(1,:,:),3) - mean(effect_earc.aN1.ERPs.cond2(1,:,:),3),2,'movmean',4);
        %     aN1_eard = smoothdata(mean(effect_ear.aN1.ERPs.cond1(1,:,:),3) - mean(effect_ear.aN1.ERPs.cond2(1,:,:),3),2,'movmean',4);
        if isfield(effect_cap,'a')
            a_capd(s,:) = smoothdata(mean(effect_cap.a.ERPs.cond1(1,:,:),3) - mean(effect_cap.a.ERPs.cond2(1,:,:),3),2,'movmean',4);
            a_earcd(s,:) = smoothdata(mean(effect_earc.a.ERPs.cond1(1,:,:),3) - mean(effect_earc.a.ERPs.cond2(1,:,:),3),2,'movmean',4);
            a_eard(s,:) = smoothdata(mean(effect_ear.a.ERPs.cond1(1,:,:),3) - mean(effect_ear.a.ERPs.cond2(1,:,:),3),2,'movmean',4);
        end
        if isfield(effect_cap,'b')
            b_capd(s,:) = smoothdata(mean(effect_cap.b.ERPs.cond1(1,:,:),3) - mean(effect_cap.b.ERPs.cond2(1,:,:),3),2,'movmean',4);
            b_earcd(s,:) = smoothdata(mean(effect_earc.b.ERPs.cond1(1,:,:),3) - mean(effect_earc.b.ERPs.cond2(1,:,:),3),2,'movmean',4);
            b_eard(s,:) = smoothdata(mean(effect_ear.b.ERPs.cond1(1,:,:),3) - mean(effect_ear.b.ERPs.cond2(1,:,:),3),2,'movmean',4);
        end
        if isfield(effect_cap,'c')
            c_capd(s,:) = smoothdata(mean(effect_cap.c.ERPs.cond1(1,:,:),3) - mean(effect_cap.c.ERPs.cond2(1,:,:),3),2,'movmean',4);
            c_earcd(s,:) = smoothdata(mean(effect_earc.c.ERPs.cond1(1,:,:),3) - mean(effect_earc.c.ERPs.cond2(1,:,:),3),2,'movmean',4);
            c_eard(s,:) = smoothdata(mean(effect_ear.c.ERPs.cond1(1,:,:),3) - mean(effect_ear.c.ERPs.cond2(1,:,:),3),2,'movmean',4);
        end
        if isfield(effect_cap,'d')
            d_capd(s,:) = smoothdata(mean(effect_cap.d.ERPs.cond1(1,:,:),3) - mean(effect_cap.d.ERPs.cond2(1,:,:),3),2,'movmean',4);
            d_earcd(s,:) = smoothdata(mean(effect_earc.d.ERPs.cond1(1,:,:),3) - mean(effect_earc.d.ERPs.cond2(1,:,:),3),2,'movmean',4);
            d_eard(s,:) = smoothdata(mean(effect_ear.d.ERPs.cond1(1,:,:),3) - mean(effect_ear.d.ERPs.cond2(1,:,:),3),2,'movmean',4);
        end
    end
    
    
    pure_red_0_5 = [236,168,139]/255;
    pure_blue_0_5 = [127,184,222]/255;
    pure_red = [0.8500, 0.3250, 0.0980];
    pure_blue = [0,0.4470,0.7410];
    transparency = 0.5;
    cd('C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)')
    
    fig = figure;
    
    %N1, effect size
    subplot(2,4,5)
    ci1 = ciplot(mean(a_cap)-std(a_cap),(mean(a_cap)+std(a_cap)),timeVec,pure_red_0_5,transparency);
    ci1.EdgeColor = 'none';
    hold on
    ci2 = ciplot(mean(a_ear)-std(a_ear),mean(a_ear)+std(a_ear),timeVec,pure_blue_0_5,transparency);
    ci2.EdgeColor = 'none';
    plot(timeVec,mean(a_cap),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(a_ear),'color',pure_blue,'LineWidth',2)   
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('Mult. speaker (att/unatt)','FontSize', 12, 'FontWeight', 'bold')
    legend('cap','ear','FontSize', 12, 'FontWeight', 'bold','Location','northeast')
    
    %MMN, effect size
    subplot(2,4,6)
    ci3 = ciplot(mean(b_cap)-std(b_cap),(mean(b_cap)+std(b_cap)),timeVec,pure_red_0_5,transparency);
    ci3.EdgeColor = 'none';
    hold on
    ci4 = ciplot(mean(b_ear)-std(b_ear),mean(b_ear)+std(b_ear),timeVec,pure_blue_0_5,transparency);
    ci4.EdgeColor = 'none';
    plot(timeVec,mean(b_cap),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(b_ear),'color',pure_blue,'LineWidth',2)  
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('MMN (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %P300, effect size
    subplot(2,4,7)
    ci5 = ciplot(mean(c_cap)-std(c_cap),(mean(c_cap)+std(c_cap)),timeVec,pure_red_0_5,transparency);
    ci5.EdgeColor = 'none';
    hold on
    ci6 = ciplot(mean(c_ear)-std(c_ear),mean(c_ear)+std(c_ear),timeVec,pure_blue_0_5,transparency);
    ci6.EdgeColor = 'none';
    plot(timeVec,mean(c_cap),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(c_ear),'color',pure_blue,'LineWidth',2)
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('P300 (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %N400, effect size
    subplot(2,4,8)
    ci7 = ciplot(mean(d_cap)-std(d_cap),(mean(d_cap)+std(d_cap)),timeVec,pure_red_0_5,transparency);
    ci7.EdgeColor = 'none';
    hold on
    ci8 = ciplot(mean(a_ear)-std(a_ear),mean(a_ear)+std(a_ear),timeVec,pure_blue_0_5,transparency);
    ci8.EdgeColor = 'none';
    plot(timeVec,mean(d_cap),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(a_ear),'color',pure_blue,'LineWidth',2)
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N400 (congr/incongr)','FontSize', 12, 'FontWeight', 'bold')
    
    %N1, difference wave
    subplot(2,4,1)
    ci9 = ciplot(mean(a_capd)-std(a_capd),(mean(a_capd)+std(a_capd)),timeVec,pure_red_0_5,transparency);
    ci9.EdgeColor = 'none';
    hold on
    ci10 = ciplot(mean(a_eard)-std(a_eard),mean(a_eard)+std(a_eard),timeVec,pure_blue_0_5,transparency);
    ci10.EdgeColor = 'none';
    plot(timeVec,mean(a_capd),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(a_eard),'color',pure_blue,'LineWidth',2)   
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N1 (att/unatt)','FontSize', 12, 'FontWeight', 'bold')
    
    %MMN, difference wave
    subplot(2,4,2)
    ci11 = ciplot(mean(b_capd)-std(b_capd),(mean(b_capd)+std(b_capd)),timeVec,pure_red_0_5,transparency);
    ci11.EdgeColor = 'none';
    hold on
    ci12 = ciplot(mean(b_eard)-std(b_eard),mean(b_eard)+std(b_eard),timeVec,pure_blue_0_5,transparency);
    ci12.EdgeColor = 'none';
    plot(timeVec,mean(b_capd),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(b_eard),'color',pure_blue,'LineWidth',2) 
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('MMN (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %P300, difference wave
    subplot(2,4,3)
    ci13 = ciplot(mean(c_capd)-std(c_capd),(mean(c_capd)+std(c_capd)),timeVec,pure_red_0_5,transparency);
    ci13.EdgeColor = 'none';
    hold on
    ci14 = ciplot(mean(c_eard)-std(c_eard),mean(c_eard)+std(c_eard),timeVec,pure_blue_0_5,transparency);
    ci14.EdgeColor = 'none';
    plot(timeVec,mean(c_capd),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(c_eard),'color',pure_blue,'LineWidth',2) 
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('P300 (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %N400, difference wave
    subplot(2,4,4)
    ci15 = ciplot(mean(d_capd)-std(d_capd),(mean(d_capd)+std(d_capd)),timeVec,pure_red_0_5,transparency);
    ci15.EdgeColor = 'none';
    hold on
    ci16 = ciplot(mean(d_eard)-std(d_eard),mean(d_eard)+std(d_eard),timeVec,pure_blue_0_5,transparency);
    ci16.EdgeColor = 'none';
    plot(timeVec,mean(d_capd),'color',pure_red,'LineWidth',2)
    plot(timeVec,mean(d_eard),'color',pure_blue,'LineWidth',2) 
    ylim([-7 7])
    ylabel('Difference wave [�V ]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N400 (congr/incongr)','FontSize', 12, 'FontWeight', 'bold')
    
    sgtitle('Best channel per data set, Grand Average','FontSize', 20, 'FontWeight', 'bold')
    set(gcf,'Color','w')
    set(gcf, 'Position', get(0, 'Screensize'));
    
    print(fig,'O:\arm_testing\Experimente\cEEGrid_vs_Cap\GND_AVG','-dpng')

            
end
