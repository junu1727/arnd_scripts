function [effect_size_matrix, best_loc, bestchans,cond1ERP_it_mean,cond2ERP_it_mean,cond1ERP_it_sta,cond2ERP_it_sta,maxAll] = tval(special_N1,condition1,condition2,FullorEar,unilateral,timeWin,bestN,sta_elec,fs)

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data
if FullorEar == 2 % if only ear was chosen...
    if unilateral == 0 % if both ears are chosen, delete the other cap electrodes
        condition1 = pop_select( condition1, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
        if special_N1 == 0
            condition2 = pop_select( condition2, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
        end
    elseif unilateral == 1 % if only one ear is of interest, delete the other electrodes for both ear- datasets (ICA-cleaned and uncleaened)
        condition1 = pop_select( condition1, 'channel',{'E18','E24','E71','E72','E85','E92'});
        if special_N1 == 0
            condition2 = pop_select( condition2, 'channel',{'E18','E24','E71','E72','E85','E92'});
        end
    else isempty(unilateral)
        disp('Using all cap electrodes')
    end
end

if isempty(timeWin)
    disp('no time window for effect size specified. Using the entire epoch length')
else
    timeStart = find(condition1.times == timeWin(1));
    timeEnd   = find(condition1.times == timeWin(2)) -1;
    if isempty(timeStart) || isempty(timeStart)
        error('invalid time parameters. Please check the sampling rate and the possible time windos')
    end
end

%% get the condition effects from the best channels

if special_N1 == 0
    best_condition1 = mbl_bipolarchannelexpansion(condition1,1,FullorEar);
    best_condition2 = mbl_bipolarchannelexpansion(condition2,1,FullorEar);
    sameSize = [];
else
    best_condition1 = mbl_bipolarchannelexpansion(condition1,1,FullorEar);
    sameSize = 1;
end
end