
%% DIRECTORIES
clear; close all;
% define project folder

SBJ = {'p01','p02','p03','p04','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
EPOCH_ON = -0.2;            % epoch start
EPOCH_OFF =0.8;             % epoch end
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 40;                % cut-off frequency low-pass filter [Hz]
ev = {'condition 6','S 70','S 80'}; % event marker for epoching
fs = 100;                  % sampling rate of EEG recording (downsample to this rate!)
baseline = [EPOCH_ON*fs 0]; % definition of baseline

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% start preprocessing of data after ICA cleaning
for s = 3:4
    for ear = 0:1
        
        h=2;
        
        % set some paths
        if ear ==0
            PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
        else
            PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
        end
        PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'b_passive_oddball',filesep);    % path for script output
        if ~exist (PATHOUT)
            mkdir(PATHOUT)
        end
        
        % locate rawdata-sets
        if ear == 0
            listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\03_ICA_clean\*ica_cleaned.set']);  % reads all .set files in PATHIN
        else ear == 1
            listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*_ear.set']);  % reads all .set files in PATHIN
        end
        
        %load ICA cleaned dataset
        subj{s} = listExp(h).name;
        EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);
        EEG = pop_resample(EEG, fs);
        EEG = pop_select( EEG, 'nochannel',{'E92'});

        if ear ==0
            % apply low and high pass filter
            EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',1);
            EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',1);
        end
        
        % remove unnecessary event marker (e.g. here fixation cross)
        % find all events and save position of EEG.event
        c=1;
        idx = [];
        for e = 1 : length(EEG.event)
            if ~ strcmp(EEG.event(e).type,ev)
                idx(c) = e;
                c=c+1;
            end
        end
        
        % remove all events which are not the stimulus onset (here event marker 10)
        EEG = pop_editeventvals(EEG,'delete',idx);
        EEG = eeg_checkset(EEG);
        EEGsave = EEG;
        
        % EEG = pop_epoch(EEG, [ev(3) ev(2)], [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
        % EEG = pop_rmbase(EEG, baseline);
        %
        % EEG = pop_jointprob(EEG, 1, [1:EEG.nbchan], PRUNE, PRUNE, 0, 0 , 0);
        % EEG = eeg_rejsuperpose( EEG, 1, 1, 1, 1, 1, 1, 1, 1);
        % rej_ep(s) = {find(EEG.reject.rejglobal == 1)};
        % EEG = pop_rejepoch( EEG, EEG.reject.rejglobal ,0);
        %
        % % save pruned and epoched data set
        % EEG = eeg_checkset(EEG);
        % subj{s}= strrep(listExp(h).name, '_ica_cleaned.set', '');
        % EEG.setname = [subj{s}, '_all'];
        % EEG = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);
        
        % % remove unnecessary event marker (e.g. here fixation cross)
        % % find all events and save position of EEG.event
        % c=1;
        % idx = [];
        % for e = 1 : length(EEG.event)
        %     if  ~strcmp(EEG.event(e).type,ev(3))
        %         idx(c) = e;
        %         c=c+1;
        %     end
        % end
        %
        % % remove all events which are not the stimulus onset (here event marker 10)
        % sta = pop_editeventvals(EEG,'delete',idx);
        % sta = eeg_checkset(sta);
        
        % epoching and baseline
        dev = pop_epoch(EEG, ev(2), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
        sta = pop_epoch(EEG, ev([1,3]), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
        dev = pop_rmbase(dev, baseline);
        sta = pop_rmbase(sta, baseline);
        
        dev = pop_jointprob(dev, 1, [1:dev.nbchan], PRUNE, PRUNE, 0, 0 , 0);
        dev = eeg_rejsuperpose( dev, 1, 1, 1, 1, 1, 1, 1, 1);
        rej_ep_dev(s) = {find(dev.reject.rejglobal == 1)};
        dev = pop_rejepoch( dev, dev.reject.rejglobal ,0);
        
        sta = pop_jointprob(sta, 1, [1:sta.nbchan], PRUNE, PRUNE, 0, 0 , 0);
        sta = eeg_rejsuperpose( sta, 1, 1, 1, 1, 1, 1, 1, 1);
        rej_ep_sta(s) = {find(sta.reject.rejglobal == 1)};
        sta = pop_rejepoch( sta, sta.reject.rejglobal ,0);
        
        sta = eeg_checkset(sta);
        if ear == 0
            sta.setname = strrep(listExp(h).name, '_ica_cleaned.set', '_sta');
        else ear == 1
            sta.setname = strrep(listExp(h).name, '_ica_ear.set', '_sta_ear');
        end
        sta = pop_saveset(sta, [sta.setname, '.set'], PATHOUT);
        
        dev = eeg_checkset(dev);
        if ear == 0
            dev.setname = strrep(listExp(h).name, '_ica_cleaned.set', '_dev');
        else ear == 1
            dev.setname = strrep(listExp(h).name, '_ica_ear.set', '_dev_ear');
        end
        dev = pop_saveset(dev, [dev.setname, '.set'], PATHOUT); %use get all epochs in bst!
        
        eeglab redraw
        
    end
end
save([PATHOUT 'info'], 'rej_ep_sta','rej_ep_dev' )
% % end of script
