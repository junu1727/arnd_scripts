%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana03_preprocessing.m
% Script 3: load ICA cleaned data from ana02
%           filter data based on your experimental design
%           epoch data around your experimental events
%           reject contaminated epochs
%           save pre-processed data in PATHOUT
%
%
% Output: pre-processed EEG set-file cleaned, filtered and epoched
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
% define project folder

SBJ = {'p01'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% locate rawdata-sets
listSBJ = dir('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p*');
listExp = dir('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\*\03_ICA_clean\*.set');  % reads all .set files in PATHIN


%% PARAMETERS
PRUNE = 2;                  % artifact rejection threshold in SD
EPOCH_ON = -0.2;            % epoch start
EPOCH_OFF =0.8;             % epoch end
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 40;                % cut-off frequency low-pass filter [Hz]
ev = {'condition 6','S 70','S 80'}; % event marker for epoching
fs = 1000;                  % sampling rate of EEG recording
baseline = [EPOCH_ON*fs 0]; % definition of baseline

s=1;
h=4;
%% start preprocessing of data after ICA cleaning
% for s = 1:lenght(listSBJ)
%     for h = 1: length(listExp)
        
        % set some paths
        PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
        PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'c_active_oddball',filesep);    % path for script output
        if ~exist (PATHOUT)
            mkdir(PATHOUT)
        end
        
        %load ICA cleaned dataset
        subj{s} = listExp(h).name;
        EEG = pop_loadset('filename', subj{s}, 'filepath',...
            PATHIN);
        
        % apply low and high pass filter
        EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',1);
        EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',1);
%         
%         EEGbadchan = EEG;
%         EEGbadchan = pop_rejchan(EEGbadchan, 'elec',[1:96] ,'threshold',4,'norm','on','measure','prob');
%         
%         p = 1;
%         for x = 1:length(EEGbadchan.chaninfo.removedchans)
%             interpChans(x) = str2num(strrep(EEGbadchan.chaninfo.removedchans(x).labels,'E',''))- p;
%             p = p+1; %-p because for some reason, the index of the channel after the bad channel is stored
%         end
%         
%         checkInterp = length(interpChans)
%         
%         EEG = pop_interp(EEG, interpChans, 'spherical');
        
        % remove unnecessary event marker (e.g. here fixation cross)
        % find all events and save position of EEG.event
        c=1;
        idx = [];
        for e = 1 : length(EEG.event)
            if ~ strcmp(EEG.event(e).type,ev)
                idx(c) = e;
                c=c+1;
            end
        end
        
        % remove all events which are not the stimulus onset (here event marker 10)
        EEG = pop_editeventvals(EEG,'delete',idx);
        EEG = eeg_checkset(EEG);
        
        EEG = pop_epoch(EEG, [ev(3) ev(2)], [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
        EEG = pop_rmbase(EEG, baseline);
        
        EEG = pop_jointprob(EEG, 1, [1:EEG.nbchan], PRUNE, PRUNE, 0, 0 , 0);
        EEG = eeg_rejsuperpose( EEG, 1, 1, 1, 1, 1, 1, 1, 1);
        rej_ep(s) = {find(EEG.reject.rejglobal == 1)};
        EEG = pop_rejepoch( EEG, EEG.reject.rejglobal ,0);
        
        % save pruned and epoched data set
        EEG = eeg_checkset(EEG);
        subj{s}= strrep(listExp(h).name, '_ica_cleaned.set', '');
        EEG.setname = [subj{s}, '_all'];
        EEG = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);

        % epoching and baseline
        dev = pop_epoch(EEG, ev(2), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
        sta = pop_epoch(EEG, ev(3), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
        dev = pop_rmbase(dev, baseline);
        sta = pop_rmbase(sta, baseline);
        
        dev = pop_jointprob(dev, 1, [1:dev.nbchan], PRUNE, PRUNE, 0, 0 , 0);
        dev = eeg_rejsuperpose( dev, 1, 1, 1, 1, 1, 1, 1, 1);
        rej_ep(s) = {find(dev.reject.rejglobal == 1)};
        dev = pop_rejepoch( dev, dev.reject.rejglobal ,0);
        
        sta = pop_jointprob(sta, 1, [1:sta.nbchan], PRUNE, PRUNE, 0, 0 , 0);
        sta = eeg_rejsuperpose( sta, 1, 1, 1, 1, 1, 1, 1, 1);
        rej_ep(s) = {find(sta.reject.rejglobal == 1)};
        sta = pop_rejepoch( sta, sta.reject.rejglobal ,0);

        sta = eeg_checkset(sta);
        sta.setname = [subj{s}, '_sta'];
        sta = pop_saveset(sta, [sta.setname, '.set'], PATHOUT);
        
        dev = eeg_checkset(dev);
        dev.setname = [subj{s}, '_dev'];
        dev = pop_saveset(dev, [dev.setname, '.set'], PATHOUT);

%         % epoching and baseline
%         stim1 = pop_epoch(EEG, ev(1), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
%         corr_con = pop_epoch(EEG, ev(2), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
%         corr_incon = pop_epoch(EEG, ev(3), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
%         
%         stim1 = pop_rmbase(stim1, baseline);
%         corr_con = pop_rmbase(corr_con, baseline);
%         corr_incon = pop_rmbase(corr_incon, baseline);
%         
%         stim1 = pop_jointprob(stim1, 1, [1:stim1.nbchan], PRUNE, PRUNE, 0, 0 , 0);
%         stim1 = eeg_rejsuperpose( stim1, 1, 1, 1, 1, 1, 1, 1, 1);
%         rej_ep(s) = {find(stim1.reject.rejglobal == 1)};
%         stim1 = pop_rejepoch( stim1, stim1.reject.rejglobal ,0);
%         
%         corr_con = pop_jointprob(corr_con, 1, [1:corr_con.nbchan], PRUNE, PRUNE, 0, 0 , 0);
%         corr_con = eeg_rejsuperpose( corr_con, 1, 1, 1, 1, 1, 1, 1, 1);
%         rej_ep(s) = {find(corr_con.reject.rejglobal == 1)};
%         corr_con = pop_rejepoch( corr_con, corr_con.reject.rejglobal ,0);
%         
%         corr_incon = pop_jointprob(corr_incon, 1, [1:corr_incon.nbchan], PRUNE, PRUNE, 0, 0 , 0);
%         corr_incon = eeg_rejsuperpose( corr_incon, 1, 1, 1, 1, 1, 1, 1, 1);
%         rej_ep(s) = {find(corr_incon.reject.rejglobal == 1)};
%         corr_incon = pop_rejepoch( corr_incon, corr_incon.reject.rejglobal ,0);
%         
%         % save pruned and epoched data set
%         stim1 = eeg_checkset(stim1);
%         stim1.setname = [subj{s}, '_stim1'];
%         stim1 = pop_saveset(stim1, [stim1.setname, '.set'], PATHOUT);
%         
%         corr_con = eeg_checkset(corr_con);
%         corr_con.setname = [subj{s}, '_corr_con'];
%         corr_con = pop_saveset(corr_con, [corr_con.setname, '.set'], PATHOUT);
%         
%         corr_incon = eeg_checkset(corr_incon);
%         corr_incon.setname = [subj{s}, '_corr_incon'];
%         corr_incon = pop_saveset(corr_incon, [corr_incon.setname, '.set'], PATHOUT);
        
%     end
% end
% optional: save rejected epochs per subject and ICA components that were
% rejected

eeglab redraw
% 
% save([PATHOUT 'info'], 'rej_ep','comps' )
% 
% %% All preprocessed datasets are stored in a EEGLAB STUDY structure
% % This STUDY can then be loaded into EEGLAB for further GUI based analysis
% % such as plotting time courses and topographies.
% cd(PATHOUT)
% list=dir('*.set');  % reads all .set files in PATHIN
% len=length(list);   % total number of datasets that will be evaluated
% subj=cell(1,len);   % create a subject vector
% 
% 
% % STUDY parameters
% studyname = 'sensor_level_ERP';
% % initialize STUDY index
% index = 1;
% STUDY = []; CURRENTSTUDY = 0; ALLEEG=[]; EEG=[]; CURRENTSET=[];
% 
% % set memory options to allow to open more than one dataset:
% % in pop_editoptions set 'option_storedisk', 1 or use eeglab GUI
% for s = 1:len % for each subject
%     % load dataset in study
%     subj{s}= strrep(list(s).name, '.set', '');
%     dataset = [PATHOUT, subj{s}, '.set'];
%     [STUDY ALLEEG] = std_editset(STUDY, ALLEEG, 'name', studyname,...
%         'commands',{{'index' index 'load' dataset 'subject' subj{s}}},...
%         'updatedat', 'off', 'savedat', 'off', 'filename', [PATHOUT, studyname]);
%     index = index + 1;
%     
% end
% 
% CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
% STUDY.design = [];
% [STUDY, ALLEEG] = std_checkset(STUDY, ALLEEG);
% 
% 
% [STUDY EEG] = pop_savestudy( STUDY, EEG, 'filename',[studyname, '.study'],...
%     'filepath',PATHOUT);
% 
% % end of script
% 
% 
