%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana00_convert_rawdata.m
% Script 0:
%     loads in Smarting-data (.xdf) files and converts them to EEGALB set
%     files

% Output: EEG set-files
%
% Arnd Meiser 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
% define project folder

% execute this script from within the 'scripts' folder.
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'Pilot_Arnd_Daniel',filesep);
PATHIN  = fullfile(MAINPATH,'rawdata',filesep,'raw_xdf', filesep); % path containing rawdata
PATHOUT = fullfile(MAINPATH,'rawdata',filesep, 'raw_eeglab', filesep);    % path for script output
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
chan_file = dir(fullfile(MAINPATH,'Channel_files',filesep,'*.elc'));

% Datasets
% Input files
cd(PATHIN)
list=dir('*.xdf'); %reads all .vhdr files in that path
len=length(list); %total number of datasets that will be evaluated
subj={'arm','dah'};
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% Loop over subjects
for s = 1:len
    
    EEG = pop_loadxdf([PATHIN,list(s).name] , 'streamtype', 'EEG', 'exclude_markerstreams', {});
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',subj{s},'gui','on');
    
    locs  = loadtxt( [chan_file(s).folder,'\',chan_file(s).name], 'blankcell', 'off' );
    locs = cell2mat(locs(4:99,3:5));
    
    % Paste in chanlocs (EEG.chanlocs)
    for x = 1: length(locs)
    EEG.chanlocs(x).X = locs(x,1);
    EEG.chanlocs(x).Y = locs(x,2);
    EEG.chanlocs(x).Z = locs(x,3);
    end
    
        %remove strange writing from the markers
    if length(EEG.event(1).type) > 10 %if there are strange markers...
        for t = 1:length(EEG.event)
            x = strfind(EEG.event(t).type,'<ecode>');
            y = strfind(EEG.event(t).type,'</ecode>');
            EEG.event(t).type = EEG.event(t).type(x(1)+7:(y(1)-1));
        end
    end
    
    startlat = find( strcmp({EEG.event.type},{'instruct_start'}));%index of start latency
    EEG = eeg_eegrej(EEG, [1 EEG.event(startlat(end)).latency + EEG.srate*8]); % remove data from before the experiment starts + 8 seconds
    endlat = find( strcmp({EEG.event.type},{'end'}));%index of end latency
    EEG = eeg_eegrej(EEG, [EEG.event(endlat(end)).latency - EEG.srate*2 length(EEG.data)]); % remove data from the end of the experiment (2 seconds)

    EEG.comments='';
    EEG.setname = [subj{s}];
    EEG = pop_saveset(EEG, [EEG.setname,'.set'], PATHOUT);
        
    eeglab redraw % redraw interface
    
end
% end of script
