

nameCounter = 1:40;
nameCounter = repelem(nameCounter,8);
numberCounter = 1:8;
numberCounter = repmat(numberCounter,40,1)';
abcdCounter = {'a','a','b','b','c','c','d','d'};
euCounter = {'e','u'};

numberingText = {'01','02','03','04','05','06','07','08','09'};
for s = 10:40
numberingText{1,s} = num2str(s);
end

t=1;
for i = 1:length(nameCounter)
    if mod(i,8)==1 && i >1
        t = t+1;
    end
    if mod(i,2)==1
        e = 1;
    else
        e = 2;
    end
    textArray{i,1} = ['sound {wavefile {filename = "',numberingText{1,t},'_',abcdCounter{1,numberCounter(i)},'_',euCounter{1,e},'.wav";};}event',num2str(nameCounter(i)),'_',num2str(numberCounter(i)),';'];
    
end

xlswrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\Paradigms\N400\textArray.xlsx',textArray);


nameCounter = 1:40;
nameCounter = repelem(nameCounter,8);
numberCounter = 1:8;
numberCounter = repmat(numberCounter,40,1)';

for i = 1:486
    textArraySound{i,1} = ['sound ', num2str(nameCounter(i)),'_',num2str(numberCounter(i)),';'];
    textArrayNames{1,i} =  ['"',num2str(nameCounter(i)),'_',num2str(numberCounter(i)),'";'];
end

for i = 1:320
    %textArraySound{i,1} = ['sound ', num2str(nameCounter(i)),'_',num2str(numberCounter(i)),';'];
    textArrayNames{1,i} =  ['stim',num2str(i)];
    textName{1,i} = [num2str(i),';'];
end

count = 1;
for i = 1:486%length(nameCounter)
    if mod(i,2) ==0
    textArrayNumbers{1,i} = num2str(count);
    count=count+1;
    elseif mod(i,2) == 1
    textArrayNumbers{1,i} = ',';
    end
end

x = 1:2:320;
count = 1;
for i = 1:320%length(nameCounter)
    if mod(i,2) ==0
    CounterOdd{1,i} = num2str(x(count));
    count=count+1;
    elseif mod(i,2) == 1
    CounterOdd{1,i} = ',';
    end
end

x = 2:2:320;
count = 1;
for i = 1:320%length(nameCounter)
    if mod(i,2) ==0
    CounterEven{1,i} = num2str(x(count));
    count=count+1;
    elseif mod(i,2) == 1
    CounterEven{1,i} = ',';
    end
end


count = 1;
for i = 1:2880%length(nameCounter)
    if mod(i,2) ==0
    textArrayNames{1,i} = ['"stim',num2str(count),'"'];
    count=count+1;
    elseif mod(i,2) == 1
    textArrayNames{1,i} = ',';
    end
end
xlswrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\Paradigms\N400\textArraySound.xlsx',textArraySound);
xlswrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\textArray.xlsx',textArrayNames);
xlswrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\textName.xlsx',textName);
xlswrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\Paradigms\Attended Speaker\textArrayNumbers.xlsx',textArrayNumbers);

%writecell(textArrayNumbers,'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Paradigms\Attended Speaker\textArrayNumbers.xlsx')

for s = 1:length(textStimuli)
    [a b(s,1)] = ismember('_',textStimuli(s).name);
end

textStimuli = dir('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\Resampled_files_44.1kHZ\*.wav*');
for i = 1:length(textStimuli)
    textArrayStimuli{i,1} = ['sound {wavefile {filename = "',textStimuli(i).name,'";};}stim_',num2str(i),';'];    
end

xlswrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\Paradigms\Attended Speaker\textArraySoundsN1.xlsx',textArrayStimuli);
