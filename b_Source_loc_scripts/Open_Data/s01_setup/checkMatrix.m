function  [backmat numbeNonRepeats] = checkMatrix(Mat,a,trials)

Mat.tone = Mat.tone(:,end-max(a)+1:end);
del = [];
% delete lines with only ones/zeros
for k = length(Mat.tone):-1:1
	% if the product is one, or
	if	prod(Mat.tone(k,:)) == true || ...
			sum(Mat.tone(k,:)) == 0
		del = [del; Mat.tone(k,:)];
		Mat.tone(k,:) = [];
	end
end

% delete lines with more than one zigzag
for kk = length(Mat.tone):-1:1
	% get number of zeros and ones
	zeros = find(Mat.tone(kk,:) == 0);
	ones = find(Mat.tone(kk,:) == 1);
	if length(zeros)>1
		dist_zeros = max(zeros)-min(zeros);
	end
	if length(ones)>1
		dist_ones = max(ones)-min(ones);
	end
	if length(zeros)>1 && length(ones)>1
		if dist_zeros+dist_ones >= 4 && dist_zeros ~=1 && dist_ones ~=1 ...
				&& ~isequal(zeros,[1 length(Mat.tone(kk,:))])...
				&& ~isequal(ones,[1 length(Mat.tone(kk,:))])
			del = [del;Mat.tone(kk,:)];
			Mat.tone(kk,:) = [];
		end
	end
end

% add missing lines if the number doesn�t match to num of tri(this procedure should be odd by now)
numbeNonRepeats = size(Mat.tone, 1); 
while 1
	if length(Mat.tone)<trials
		Mat.tone = [Mat.tone;Mat.tone(1:trials-length(Mat.tone),:)];
	else break
	end
end

backmat=Mat.tone;
end