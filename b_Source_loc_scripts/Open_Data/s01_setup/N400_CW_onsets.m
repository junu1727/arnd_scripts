%% load in the sentences without CW. extract their length to use as latencies in ana03d 
clear all;
close all;
clc

load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\audionames.mat','audionames'); %load in the names of all sentences

audiodir = dir('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\old\N400\Forcosineramp\New\*.wav');

for i = 1:length(audiodir) %load every sentence without CW, extract their lengths in seconds
[testrun FS] = audioread([audiodir(1).folder,filesep,audiodir(i).name]);
audiolength(i,1) = length(testrun)/FS; %store length in new variable
end

audiostruc.names = audionames;
audiostruc.length = audiolength;

save('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\audiostruc.mat','audiostruc');



% load the stim log to get the order of the presented stimuli
senorder = loadtxt('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\p01\log\p01_d-N400-20201207-141448-GTBUJ.log');
senorder = senorder(:,4);
senorder = senorder(find(strcmp('stim',senorder))-1)

% find the critical word onset for every sentence in the CW-list
load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\audiostruc.mat');

audio_p01 = zeros(320,1);
for i = 1:length(senorder)
x = strcmp(senorder{i,1},audiostruc.names);
audio_p01(i) = audiostruc.length(x);
end



