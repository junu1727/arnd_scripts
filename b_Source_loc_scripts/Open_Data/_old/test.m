%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana00_convert_rawdata.m
% Script 0:
%     loads in Smarting-data (.xdf) files and converts them to EEGALB set
%     files

% Output: EEG set-files
%
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DIRECTORIES
clear; close all;
% define project folder

SBJ = {'p01','p02','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
loadList = dir([MAINPATH,'*\log\*N400*.log']); %load in the log of the order of stimulus presentation (task d, N400)
load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\audiostruc.mat'); % load in the list of all CW-onsets (task d, N400)
GoodChans = 0; %0 for customized elec positions from bst, 1 if the original digitization was fine

for s = 3%1:length(SBJ)
    % set some paths
    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '01_raw_eeglab', filesep);    % path for script output
    chan_file = dir(fullfile(MAINPATH,SBJ{s},'channel positions',filesep,'*.elc'));
    if ~exist (PATHOUT)
        mkdir(PATHOUT)
    end
    
    cd(PATHIN)
    
    if GoodChans == 1
        locs  = loadtxt( [chan_file.folder,'\',chan_file.name], 'blankcell', 'off' );
        locs = cell2mat(locs(4:99,3:5));
    end
    
    list=dir('*.vhdr'); %reads all .vhdr files in that path
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    
    for h = 1:length(list)
        % Loop over subjects
        EEG = pop_biosig([MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep,list(h).name]);
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off');
        
        if GoodChans == 0
            EEG=pop_chanedit(EEG, 'load',{['O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\',SBJ{s},'\\channel positions\\channel.xyz'],'filetype','xyz'});
        else
            % Paste in chanlocs (EEG.chanlocs)
            for x = 1: length(locs)
                EEG.chanlocs(x).X = locs(x,1);
                EEG.chanlocs(x).Y = locs(x,2);
                EEG.chanlocs(x).Z = locs(x,3);
            end
        end
        
        startlat = find( strcmp({EEG.event.type},{'S  7'}));%index of start latency
        %             if isempty(startlat) %if baseline is shorter than 60s...
        %                 EEG = eeg_eegrej(EEG, [1 EEG.event(2).latency - EEG.srate]); % remove baseline data (first real marker - 1s)
        %             else
        EEG = eeg_eegrej(EEG, [1 EEG.event(startlat(end)+1).latency - EEG.srate]); % remove baseline data (first real marker - 1s)
        %             end
        if h == 4
            PATHAUDIO = fullfile(MAINPATH,SBJ{s},filesep, 'log', filesep);    % path for audio-order
            if ~exist (PATHAUDIO)
                mkdir(PATHAUDIO)
            end
            
            % load the stim log to get the order of the presented stimuli
            senorder = loadtxt([loadList(s).folder,filesep,loadList(s).name]); %load in the table containing the names of the stimuli in order of presentation
            senorder = senorder(:,4); %cut the table to the necessary columns
            senorder = senorder(find(strcmp('stim',senorder))-1); % cut if further to only the names of the stimuli (e.g. 10_1)
            senorder(1:2,:) = [];%remove first to sentences, those were training
            
            % find the critical word onset for every sentence in the CW-list
            audioSBJ = zeros(200,1);
            for i = 1:length(senorder)
                x = strcmp(senorder{i,1},audiostruc.names); %find the name of the presented stimulus in the list
                audioSBJ(i,1) = audiostruc.length(x); %store the CW-onset from the list in a new variable. Do it for every presented stimulus
            end
            save([PATHAUDIO,'CW_onsets.mat'],'audioSBJ');
        end
        
        EEG.comments='';
        EEG.setname = strrep(list(h).name, '.vhdr', '');
        EEG = pop_saveset(EEG, [EEG.setname,'.set'], PATHOUT);
        
        eeglab redraw % redraw interface
    end
end

         
%         EEGbadchan = EEG;
%         EEGbadchan = pop_rejchan(EEGbadchan, 'elec',[1:96] ,'threshold',4,'norm','on','measure','prob');
%         
%         p = 1;
%         for x = 1:length(EEGbadchan.chaninfo.removedchans)
%             interpChans(x) = str2num(strrep(EEGbadchan.chaninfo.removedchans(x).labels,'E',''))- p;
%             p = p+1; %-p because for some reason, the index of the channel after the bad channel is stored
%         end
%         
%         checkInterp = length(interpChans)
%         
%         EEG = pop_interp(EEG, interpChans, 'spherical');