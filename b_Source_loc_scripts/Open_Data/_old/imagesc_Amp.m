%% set some variables
taskLetter    = {'N1';'a';'b';'c';'d'};
conditionString = {'cap','earc','ear'};
sta_elec = [43 55 55]; %channel number of the standard electrode for all three conditions (cap, earc and ear)
numChans = [96 12 12];
special_N1 = 0;
window_start  = [50 50 130 250 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 250 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
tnames = {'N1 (n=15)','N1(att)(n=15)','MMN (n=16)','P300 (n=14)','N400 (n=15)','N1 (n=15)','N1(att)(n=15)','MMN (n=16)','P300 (n=14)','N400 (n=15)'};
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

% load([MAINPATH,'leftChans.mat']);
% load([MAINPATH,'rightChans.mat']);
% load([MAINPATH,'biChans.mat']);
load([MAINPATH,'MaxVar.mat']);
load([MAINPATH,'MeanES.mat'],'mean_es');

%get fieldnames for looping through each layer
con = fieldnamesr(maxVar,1);

%% calc distances and orientations of the bipolar channels
eeglab
chanPath = dir([MAINPATH,SBJ{1},'\04_clean\*\*_a','*','ear.set']); %set 1 for the cleaned cap data
EEG = pop_loadset('filename', chanPath(1).name, 'filepath',chanPath(1).folder);

%get indices of channels from left/right ear and bilateral
posR = [1 3 5 6 9 11];
posL = [2 4 7 8 10 12];
chanR = [EEG.chanlocs(posR).X; EEG.chanlocs(posR).Y; EEG.chanlocs(posR).Z]';
chanL = [EEG.chanlocs(posL).X; EEG.chanlocs(posL).Y; EEG.chanlocs(posL).Z]';

%loop through channels to get their distance and names of the bipolar channel
collectR=[];
collectL=[];
index=1;
for ch = 1:length(posR)-1
    for b = ch + 1:length(posR)
        collectR(index) = norm(chanR(ch,:)  - chanR(b,:));
        namesR{index} = [EEG.chanlocs(posR(ch)).labels,' - ', EEG.chanlocs(posR(b)).labels];
        collectL(index) = norm(chanL(ch,:)  - chanL(b,:));
        namesL{index} = [EEG.chanlocs(posL(ch)).labels,' - ', EEG.chanlocs(posL(b)).labels];
        index=index+1;
    end
end

% do the same for all bilateral bipolar channels
collectB=[];
index = 1;
for ch = 1:length(posR)
    for b = 1:length(posL)
        collectB(index) = norm(chanR(ch,:)  - chanL(b,:));
        namesB{index} = [EEG.chanlocs(posR(ch)).labels,' - ', EEG.chanlocs(posL(b)).labels];
        index=index+1;
    end
end

%% change the naming and order of electrodes

horR = namesR([4 9 10]);
verR = namesR([3 14 5 11 6 8]);
qpaR  = namesR([2 12 15]); %from posterior to anterior
qapR  = namesR([1 7 13]); %from anterior to posterior

horL = namesL([10 4 9]);
verL = namesL([13 7 8 2 12 5]);
qpaL  = namesL([14 15 3]); %from posterior to anterior
qapL  = namesL([1 6 11]); %from anterior to posterior

% sort after orientation, horizontal, vertical, right diagonal, left diagonal
sortingr = [4 9 10 3 14 5 11 6 8 2 12 15 1 7 13];
[pholder indR] = sort(collectR,'desc');
newsortR = [4 10 9 6 14 8 3 5 11 12 15 2 7 13 1]; %sort after distance within a category

sortingL = [10 4 9 13 7 8 2 12 5 14 15 3 1 6 11];
[pholder indL] = sort(collectR,'desc');
newsortL = [4 10 9 12 7 2 13 8 5 14 15 3 6 1 11];%sort after distance within a category

collectR = collectR(newsortR);
collectL = collectL(newsortL);
namesR = namesR(newsortR);
namesL = namesL(newsortL);

%sort all bilateral channels from highest to lowest distance (does not make sense to include angles here)
[collectB indB] = sort(collectB,'desc');
namesB = namesB(indB);

%change the names so that the lower electrode number always comes first
for c = 1:length(namesR)
    if str2num(namesR{1,c}(2:3)) >  str2num(namesR{1,c}(end-1:end))
        str1 = namesR{1,c}(2:3);
        str2 = namesR{1,c}(end-1:end);
        namesR{1,c}(2:3) = str2;
        namesR{1,c}(end-1:end) = str1;
    end
end

for c = 1:length(namesL)
    if str2num(namesL{1,c}(2:3)) >  str2num(namesL{1,c}(end-1:end))
        str1 = namesL{1,c}(2:3);
        str2 = namesL{1,c}(end-1:end);
        namesL{1,c}(2:3) = str2;
        namesL{1,c}(end-1:end) = str1;
    end
end

for c = 1:length(namesB)
    if str2num(namesB{1,c}(2:3)) >  str2num(namesB{1,c}(end-1:end))
        str1 = namesB{1,c}(2:3);
        str2 = namesB{1,c}(end-1:end);
        namesB{1,c}(2:3) = str2;
        namesB{1,c}(end-1:end) = str1;
    end
end

%% loop through task, conditions and subjects 
for s = 1:length(SBJ)-1 %loop through subjects (without the last, it was excluded completely)
    
    for t = 1:length(taskLetter) %loop through tasks
        
        load([MAINPATH,'quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        q_check(5)= q_check(1);
        q_check = circshift(q_check,1);
        
        if t ~= 1
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        else
            timeWin = find(timeVecN1 == window_start(t)) : find(timeVecN1 == 240) -1;
        end
        
        if q_check(t).badEarChan == 0
            
            %get the indices of the channels (same channels in the same order every time)
            for x = 1:length(namesR)
                numsright(x) = find(strcmp(namesR(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            for x = 1:length(namesL)
                numsleft(x) = find(strcmp(namesL(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            for x = 1:length(namesB)
                numsbi(x) = find(strcmp(namesB(x),maxVar.ear.(taskLetter{t})(s).bestchans));
            end
            
            %now get the peaks from each of these ERPs from every right, left and bilateral channel channel
            pksRight(s,t,1:length(numsright)) = max(abs(maxVar.ear.(taskLetter{t})(s).ERP_cond2(numsright,timeWin) - maxVar.ear.(taskLetter{t})(s).ERP_cond1(numsright,timeWin)),[],2);
            pksLeft(s,t,1:length(numsleft))   = max(abs(maxVar.ear.(taskLetter{t})(s).ERP_cond2(numsleft,timeWin) - maxVar.ear.(taskLetter{t})(s).ERP_cond1(numsleft,timeWin)),[],2);
            pksBi(s,t,1:length(numsbi))       = max(abs(maxVar.ear.(taskLetter{t})(s).ERP_cond2(numsbi,timeWin) - maxVar.ear.(taskLetter{t})(s).ERP_cond1(numsbi,timeWin)),[],2);
        end
    end
end 
    %% change the names of the electrodes for easier reading
tempNames = {'71','72','85','18','24','92','81','80','90','22','28','95',};
newNames = {'R1','R2','R3','R4','R5','R6','L1','L2','L3','L4','L5','L6',};

for st = 1:length(tempNames)
namesR = strrep(namesR,tempNames{st},newNames{st});
namesL = strrep(namesL,tempNames{st},newNames{st});
namesB = strrep(namesB,tempNames{st},newNames{st});
end

namesR = strrep(namesR,'E','');
namesL = strrep(namesL,'E','');
namesB = strrep(namesB,'E','');

names = {namesR; namesL};

% add line 20 for the rejected subject 20 plus 21 for the mean over all
% subjects
pksRight(20:21,:,:) = 0;
pksLeft(20:21,:,:) = 0;
pksBi(20:21,:,:) = 0;

%read in the images of the ear-electrodes
cd('C:\Users\arnd\Pictures\')
imr = imread('earR.jpg'); 
iml = imread('earL.jpg'); 
imb = imread('earB.jpg'); 
imrcolor = imread('earRcolor.jpg'); 
imlcolor = imread('earLcolor.jpg'); 
%im = {iml; imr; imb; imlcolor; imrcolor; imb};
im = {imrcolor; imlcolor};

colors = {'red' 'red' 'red' 'blue' 'blue' 'blue' 'blue' 'blue' 'blue' 'magenta' 'magenta' 'magenta' 'green' 'green' 'green'};
%% create the figure
g=4;
tz = [1 2 3 4 5];
imgpic = figure;
subp = 1;
for rl = 1:length(names)
    count = 1;
    for i = 1:length(taskLetter)
        
        subplot(2,5,subp)
        
        if i == 6 || i == 12
            imshow(im{rl,1})
        else
            

            pksR(:,:,tz(count)) = abs(squeeze(pksRight(:,tz(count),:)));
            pksL(:,:,tz(count)) = abs(squeeze(pksLeft (:,tz(count),:)));
            pksB(:,:,tz(count)) = abs(squeeze(pksBi   (:,tz(count),:)));
            
            scaleR = max(pksR(:,:,tz(count)),[],2);
            scaleL = max(pksL(:,:,tz(count)),[],2);
            scaleB = max(pksB(:,:,tz(count)),[],2);
            
%             [valR, iR] = sort(scaleR,'des');
%             [valL, iL] = sort(scaleL,'des');
%             [valB, iB] = sort(scaleB,'des');
% 
%             for num = 1:length(iR)
%                 xlab_r{num} = num2str(iR(num));
%                 xlab_l{num} = num2str(iL(num));
%                 xlab_b{num} = num2str(iB(num));
%             end
%             
%             xlab_r{end} = 'Mean';
%             xlab_l{end} = 'Mean';
%             xlab_b{end} = 'Mean';
%             
%             xlab = [xlab_r; xlab_l; xlab_b];
            
            pksR(:,:,tz(count)) = rescale(pksR(:,:,tz(count)),'InputMin',0,'InputMax',scaleR);
            pksL(:,:,tz(count)) = rescale(pksL(:,:,tz(count)),'InputMin',0,'InputMax',scaleL);
            pksB(:,:,tz(count)) = rescale(pksB(:,:,tz(count)),'InputMin',0,'InputMax',scaleB);

%             pksR(:,:,tz(count)) = pksR(iR,:,tz(count));
%             pksL(:,:,tz(count)) = pksL(iL,:,tz(count));
%             pksB(:,:,tz(count)) = pksB(iB,:,tz(count));
            
            pksR(21,:,tz(count)) = mean(pksR(find(pksR(:,1)~=0),:,tz(count)),1);
            pksL(21,:,tz(count)) = mean(pksL(find(pksL(:,1)~=0),:,tz(count)),1);
            pksB(21,:,tz(count)) = mean(pksB(find(pksB(:,1)~=0),:,tz(count)),1);
            
            pksR(21,:,tz(count)) = rescale(pksR(21,:,tz(count)),'InputMin',0,'InputMax',max(pksR(21,:,tz(count)),[],2));
            pksL(21,:,tz(count)) = rescale(pksL(21,:,tz(count)),'InputMin',0,'InputMax',max(pksL(21,:,tz(count)),[],2));
            pksB(21,:,tz(count)) = rescale(pksB(21,:,tz(count)),'InputMin',0,'InputMax',max(pksB(21,:,tz(count)),[],2));
            
            
            [highR(tz(count),:) indR(tz(count),:)] = sort(pksR(21,:,tz(count)),'desc');
            highnameR{tz(count)} = namesR(indR(tz(count),:));
            [highL(tz(count),:) indL(tz(count),:)] = sort(pksL(21,:,tz(count)),'desc');
            highnameL{tz(count)} = namesL(indL(tz(count),:));
            
            
            pks = {squeeze(pksR(:,:,tz(count))) squeeze(pksL(:,:,tz(count))) squeeze(pksB(:,:,tz(count)))};
            
            pic = imagesc(pks{1,rl});
            if i == 1
%                 cbar = colorbar;
%                 cbar.Label.String = 'Hedges� g (scaled to [0 1])';
                yticks(1:21)
                yticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','Mean',})
                %yticklabels({xlab{rl,:}})
                ylabel('Subject')
                set(gca,'fontweight','bold','fontsize',8);
            else
                set(gca,'YTickLabel',[]);
            end
            if subp > 5
                xticks(1:size(pks{1,rl},2))
                xticklabels({names{rl,1}{1,1:size(pks{1,rl},2)}});
                %yticklabels({xlab{rl,:}})
                xtickangle(45)
                ax=gca;
                for w = 1:length(colors)
                    ax.XTickLabel{w} = [['\color{',colors{w},'}'] ax.XTickLabel{w}];
                end
                set(ax,'fontweight','bold','fontsize',8);
                ax.Position = ax.Position + [0 0.21 0.02 0];
            else
                ax=gca;
                set(ax,'XTickLabel',[]);
                set(ax,'fontweight','bold','fontsize',8);
                ax.Position = ax.Position + [0 0 0.0197 0];
            end
            axis square;
            box 'off'
        end
        
        if subp == 6
            xlabel('Channel')
        end
        
        if subp < 6
            title(tnames{tz(count)},'FontSize',14,'FontWeight','bold')
        end
        count = count +1;
        subp = subp + 1;
    end
    g=g+1;
    set(gcf,'Color','w')
    set(gcf, 'Position', get(0, 'Screensize'));
end

%  print(imgpic(1),'C:\Users\arnd\Pictures\\RightLeft_AMP','-dpng')
% print(imgpic(2),'C:\Users\arnd\Pictures\\Imright','-dpng')
% print(imgpic(3),'C:\Users\arnd\Pictures\\Imbi','-dpng')

%% plot the mean sorted by magnitude
% plotfigs = [1 2 4 5];
% 
% figure
% for t = 1:length(taskLetter)
%     colorR = colors(indR(t,:));
%     subplot(2,3,plotfigs(t))
%     imagesc(highR(t,:))
%     cbar = colorbar;
%     title(cbar,'Hedges� g')
%     xticks(1:size(pks{1,1},2))
%     xticklabels(highnameR{1,t});
%     yticks(1)
%     yticklabels({['Mean task ',taskLetter{t}]})
%     xtickangle(45)
%     axis square;
%     ylabel('Subject')
%     xlabel('Channel')
%     box 'off'
%     
%     ax=gca;
%     for w = 1:length(colorR)
%         ax.XTickLabel{w} = [['\color{',colorR{w},'}'] ax.XTickLabel{w}];
%     end
% end
% set(gcf,'Color','w')
% set(gcf, 'Position', get(0, 'Screensize'));
% 
% subplot(2,3,3)
% imshow(imr)
% subplot(2,3,6)
% imshow(imrcolor)
% 
% figure
% for t = 1:length(taskLetter)
%     colorL = colors(indL(t,:));
%     subplot(2,3,plotfigs(t))
%     imagesc(highL(t,:))
%     cbar = colorbar;
%     title(cbar,'Hedges� g')
%     xticks(1:size(pks{1,2},2))
%     xticklabels(highnameL{1,t});
%     yticks(1)
%     yticklabels({['Mean task ',taskLetter{t}]})
%     xtickangle(45)
%     axis square;
%     ylabel('Subject')
%     xlabel('Channel')
%     box 'off'
%     
%     ax=gca;
%     for w = 1:length(colorL)
%         ax.XTickLabel{w} = [['\color{',colorL{w},'}'] ax.XTickLabel{w}];
%     end
% end
% set(gcf,'Color','w')
% set(gcf, 'Position', get(0, 'Screensize'));
% 
% 
% subplot(2,3,3)
% imshow(iml)
% subplot(2,3,6)
% imshow(imlcolor)
% 
% figure
% subplot(221)
% plot(highR(1,:))
%     hold on
% for t = 2:length(taskLetter)
% plot(highR(t,:))
% end
% xticks(1:size(pks{1,1},2))
% legend('task a','task b','task c','task d','FontSize', 10, 'FontWeight', 'bold','Location','northeast')
% xlim([1 size(pks{1,1},2)])
% set(gcf,'Color','w')
% title('right ear, sorted by highest Hedges� g')
% 
% subplot(222)
% plot(squeeze(pksR(21,:,1)))
%     hold on
% for t = 2:length(taskLetter)
% plot(squeeze(pksR(21,:,t)))
% end
% xticks(1:size(pks{1,1},2))
% legend('task a','task b','task c','task d','FontSize', 10, 'FontWeight', 'bold','Location','northeast')
% xlim([1 size(pks{1,1},2)])
% set(gcf,'Color','w')
% title('right ear, sorted by orientation/distance')
% 
% subplot(223)
% plot(highL(1,:))
%     hold on
% for t = 2:length(taskLetter)
% plot(highL(t,:))
% end
% xticks(1:size(pks{1,1},2))
% legend('task a','task b','task c','task d','FontSize', 10, 'FontWeight', 'bold','Location','northeast')
% xlim([1 size(pks{1,1},2)])
% set(gcf,'Color','w')
% title('left ear, sorted by highest Hedges� g')
% 
% subplot(224)
% plot(squeeze(pksL(21,:,1)))
%     hold on
% for t = 2:length(taskLetter)
% plot(squeeze(pksL(21,:,t)))
% end
% xticks(1:size(pks{1,1},2))
% legend('task a','task b','task c','task d','FontSize', 10, 'FontWeight', 'bold','Location','northeast')
% xlim([1 size(pks{1,1},2)])
% set(gcf,'Color','w')
% title('left ear, sorted by orientation/distance')


