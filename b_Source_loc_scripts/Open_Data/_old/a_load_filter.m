clear all;
close all;
clc

addpath('C:\Users\arnd\Desktop\toolboxes_matlab\fieldtrip');
addpath('C:\Users\arnd\Desktop\toolboxes_matlab\spm12');
MAINPATH = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Pilot_Arnd_Daniel\';

list_all=dir([MAINPATH,'rawdata\*.set']); %list all fif-files

%daniel = xdf2fieldtrip([list_all(end).folder,'\',list_all(end).name]);

ft_defaults;

cfg=[];
cfg.dataset = fullfile([MAINPATH,'rawdata\',list_all(end).name]); %load in the dataset
cfg.channel = 'all';
data = ft_preprocessing(cfg); %Daniels dataset already contains the chanlocs that were manually written from the elc file into the ALLEEG-structure in EEGlab

%% filtering
cfg = [];
cfg.lpfilter = 'yes';
cfg.hpfilter = 'yes';
cfg.lpfreq = 30;
cfg.hpfreq = 1;
data = ft_preprocessing(cfg, data);

%% read in the events
event = ft_read_event([MAINPATH,'rawdata\',list_all(end).name]);

%% epoch around trigger event
cfg                         = [];
cfg.dataset                 = [MAINPATH,'rawdata\',list_all(end).name];
cfg.trialdef.eventtype      = 'trigger';
cfg.trialdef.eventvalue     = '1'; 
cfg.trialdef.prestim        = 0.2; % in seconds
cfg.trialdef.poststim       = 0.8; % in seconds
cfg = ft_definetrial(cfg);

cfg.channel    = 'all';
cfg.continuous = 'yes';
data_all = ft_preprocessing(cfg);

%% plot data
cfg = [];
cfg.xlim = [-0.2 0.8];
cfg.ylim = [0 50];
cfg.channel = 'all';
ft_singleplotER(cfg,data_all);
ft_multiplotER(cfg,data_all);








%% frequency analysis
%% plotffts
cfg = [];
cfg.method = 'mtmfft';
cfg.channel = 'all';
cfg.output    = 'powandcsd'; %returns power- and crosssprectra
cfg.foilim     = [0 30];
cfg.taper = 'hanning';
[freq] = ft_freqanalysis(cfg, data_all);

%% find IAF
freq.avgPow = mean(freq.powspctrm(:,:),1);%calc the mean of the pow-spec of the chosen sensors
alpha_idx = find(freq.freq >= 7 & freq.freq <= 14); %in the freq-vector, find the range from 8-12
alpha_max = findpeaks(freq.avgPow(1, alpha_idx),'SortStr','descend');         %in the averaged pow-spec, find the max-value within that range
alpha_max = alpha_max(1);
IAF_idx = find(freq.avgPow(1,:) == alpha_max);      %find the indicies of the max-value
IAF_freq(1,1) = freq.freq(IAF_idx);                 %find the equivalent in the freq-vector. This is the IAF

%% plot results
cfg = [];
cfg.parameter = 'powspctrm';
cfg.layout = elec;
cfg.channel= 'all'; %for finding alpha peak that is likely to be found in these channels

h = figure; %look at both single- and multiplot
subplot(2,1,1);
ft_multiplotER(cfg,freq);
title(['multi/singleplot IAF ',num2str(IAF_freq(i,1)),'Hz sbj',num2str(list_all(i).name(3:7))]);
subplot(2,1,2);
ft_singleplotER(cfg,freq); %this function will not show the same IAF as can be read from the calc above. That is because the function averages over ALL channels. The calc of the IAF only uses the channels of interest


save([MEG_MAIN,filesep,'a_filtering',filesep,'maxfilt',filesep,'subject_',list_all(i).name(3:7),'_filtered.mat'],'data','freq');
%set(h,'PaperOrientation','portrait'); %otherwise, png is rotated by 90�
savefig(gcf,[MEG_MAIN,filesep,'figures',filesep,'freq_single_multi_plot',filesep,'maxfilt',filesep,'IAF_sbj_',list_all(i).name(3:7),'.fig'])
close all
