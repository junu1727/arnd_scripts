The Matlab scripts assume a certain file structure

|-- project_folder
|   |-- rawdata (place here the data of all 10 participants)
|   |-- scripts (place here the analysis scripts)
|   |-- config  (place here the electrode position file)
|   |-- data 
|	|--ana01 (will be created by ana01_ICA.m)
|	|--ana02 (will be created by ana02_corrmap.m)
|	|--ana03 (will be created by ana03_preprocessing.m)
|	|--ana04 (will be created by ana04_brainstorm.m or ana04_brainstorm_TF.m)
|-- brainstorm_db