%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana00_convert_rawdata.m
% Script 0: 
%     loads in BrainAmp acquisition files and converts them to EEGALB set
%     files

% Output: EEG set-files
% 
% Martin Bleichner 11/01/2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
% define project folder

% execute this script from within the 'scripts' folder. 
MAINPATH = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Pilot_Arnd_Daniel\rawdata\raw_xdf\';
PATHIN  = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Pilot_Arnd_Daniel\rawdata\raw_xdf\'; % path containing rawdata                   
PATHOUT = fullfile(MAINPATH, 'rawdata', filesep);    % path for script output

if ~exist(PATHOUT)
    mkdir(PATHOUT);
end
addpath(fullfile(MAINPATH,'Software','eeglab14_1_1b'))
chan_file = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\Pilot_Arnd_Daniel\rawdata\raw_xdf\elec_64ch.elp';

% Datasets
% Input files
cd(PATHIN)
list=dir('*.vhdr'); %reads all .vhdr files in that path
len=length(list); %total number of datasets that will be evaluated
subj={'s01','s02','s03','s04','s05','s06','s07','s08','s09','s10'};
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
% Loop over subjects 
for s=1:len 
  
    name{s} = strrep(list(s).name, '.vhdr', '');
    % load rawdata -> pop_biosig toolbox
    % EEG = pop_biosig([PATHIN, name{s},'.vhdr']);
    
    % load rawdata BrainVision
    EEG = pop_loadbv(PATHIN,[ name{s} ,'.vhdr']);

    % Load electrode file
    EEG = pop_chanedit(EEG, 'lookup',chan_file,'load',{chan_file 'filetype' 'autodetect'});
    EEG.comments=''
    EEG.setname = [subj{s}];
    EEG = pop_saveset(EEG, [EEG.setname,'.set'], PATHOUT);
    
end
cd(fullfile(MAINPATH, 'scripts', filesep));  
% end of script
