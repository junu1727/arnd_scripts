%% set some variables
taskLetter    = {'N1';'a';'b';'c';'d'};
conditionString = {'cap','earc','ear'};
window_start  = [50 50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
uniR = {'E18 - E24','E18 - E71','E18 - E72','E18 - E85','E18 - E92','E24 - E71','E24 - E72','E24 - E85',...
        'E24 - E92','E71 - E72','E71 - E85','E71 - E92','E72 - E85','E72 - E92','E85 - E92'};   
uniL = {'E22 - E28','E22 - E80','E22 - E81','E18 - E90','E22 - E95','E28 - E80','E28 - E81','E28 - E90',...
        'E28 - E95','E80 - E81','E80 - E90','E80 - E95','E81 - E90','E81 - E95','E90 - E95'}; 
names = {'N100 (n=15)','N100(att.)(n=15)','MMN (n=16)','P300 (n=14)','N400 (n=16)'};

       %% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
       ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
       ,'fuhi52','okum71','kuma55','pigo79'
       };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

load([MAINPATH,'MeanES.mat']);
load([MAINPATH,'MaxVar.mat']);

%get fieldnames for looping through each layer
con = fieldnamesr(maxVar,1);
tasks = fieldnamesr(mean_es.cap,1);
vars  = fieldnamesr(mean_es.cap.N1,1);
pksMax = zeros(length(SBJ),5,length(taskLetter));

for t = 1:length(taskLetter) %loop through tasks
    count = 0;
    for s = 1:length(SBJ) %loop through subjects
        
        load([MAINPATH,'quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        q_check(5)= q_check(1);
        q_check = circshift(q_check,1);
        
        if t == 1
            timeWin = 1:20;
        else
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        end
        
        if q_check(t).badEarChan == 0
            
            for c = 1:5
                
                if c == 1
                    %cap, sta.
                    pksMax(s,c,t)= maxVar.cap.(taskLetter{t})((s)).maxVal_sta; % store the peaks of each subject*condition*task from the best channel
                elseif c == 2
                    %cap, ind.
                    pksMax(s,c,t)= maxVar.cap.(taskLetter{t})((s)).maxVal(maxVar.cap.(taskLetter{t})((s)).best_loc(1)); % store the peaks of each subject*condition*task from the best channel
                elseif c == 3 %for the bilateral condition, take the best channel among those built from both ears
                    %ear (bilateral), ind.
                    pksMax(s,c,t)= maxVar.ear.(taskLetter{t})((s)).maxVal(maxVar.ear.(taskLetter{t})((s)).best_loc(1)); % store the peaks of each subject*condition*task from the best channel
                elseif c == 4
                    %ear (unilateral, right), ind.
                    for x = 1:length(uniR) %loop through unilateral channels (right ear) to get their indices in the channel lsit
                        bestR(x) = find(strcmp(uniR(x),maxVar.ear.(taskLetter{t})(s).bestchans));
                    end
                    m1 = maxVar.ear.(taskLetter{t})(s).es_matrix_mean(maxVar.ear.(taskLetter{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                    pksMax(s,c,t)= max(m1(1,timeWin)); % store the peaks of each subject*condition*task from the best channel
                else
                    %ear (unilateral, left), ind.
                    for x = 1:length(uniL) %loop through unilateral channels (left ear) to get their indices in the channel lsit
                        bestL(x) = find(strcmp(uniL(x),maxVar.(con{3}).(taskLetter{t})(s).bestchans));
                    end
                    m1 = maxVar.ear.(taskLetter{t})(s).es_matrix_mean(maxVar.ear.(taskLetter{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                    pksMax(s,c,t)= max(m1(1,timeWin)); % store the peaks of each subject*condition*task from the best channel
                end
                
            end
        else
         count = count + 1; %mean_es only has as many rows as subjects (18 of 20 when two subjects got kicked). maxVar always has 20. This variable alignes the counting
        end
    end
end

pksMax(find(pksMax == 0)) = NaN;


%lightgrey = [.95, .95, .95];
grey = [.75 .75 .75];
darkgrey = [.2 .2 .2];
% darkblue = [55 96 146]/255;
% lightblue = [225 231 239]/255;
% darkgreen = [79 115 76]/255;
% darkred = [192 80 77]/255;
% purple = [180 30 130]/255;
% orange = [210 100 10]/255;

cmap = colormap(jet);
cmap = cmap(1:3:60,:); % colors as subjects
cnums = 1:20;
P1 = zeros(length(taskLetter),length(SBJ));
P2 = zeros(length(taskLetter),length(SBJ));
P3 = zeros(length(taskLetter),length(SBJ));

barfigure = figure;
for j = 1:length(taskLetter)
    noNaN = ~isnan(pksMax(:,1,j)); %get indices of subjects per task (NaN-row = subject removed)
    ntasks = sum(noNaN); %number of subjects in each tasks
    colcount = cnums(noNaN);
    
    ax = subplot(1,5,j);
    %ax.Position(3) = ax.Position(3)*1.25;
    boxp = boxplot([pksMax(noNaN,1,j) pksMax(noNaN,2,j) pksMax(noNaN,3,j) pksMax(noNaN,4,j) pksMax(noNaN,5,j)]);
    set(boxp,{'linew'},{3})
    boxcolor = get(boxp,'children');   % Get the handles of all the objects
    %set(gca,'linew',2)
    
    hold on
    
    lineplot = plot([repmat(1,ntasks,1),repmat(2,ntasks,1),repmat(3,ntasks,1),repmat(4,ntasks,1),repmat(5,ntasks,1)]',...
                    [pksMax(noNaN,1,j) pksMax(noNaN,2,j) pksMax(noNaN,3,j) pksMax(noNaN,4,j) pksMax(noNaN,5,j)]','-','Color','k');
    %     lineplot = plot([repmat(1,ntasks,1),repmat(2,ntasks,1),repmat(3,ntasks,1)]',[pksMax(noNaN,1,j) pksMax(noNaN,3,j) pksMax(noNaN,2,j)]','--');
%     cc = 1;
%     for xp = 1:colcount
%         lineplot(cc).Color = [0 0 0]; %cmap(xp,:);
%         cc= cc+1;
%     end
    
    p1       = plot(repmat(1,ntasks,1),pksMax(noNaN,1,j), '.','color','cyan', 'MarkerSize', 20);%cap sta, 1
    p2       = plot(repmat(2,ntasks,1),pksMax(noNaN,2,j), '.','color','b', 'MarkerSize', 20);%cap uni, 1
    p3       = plot(repmat(3,ntasks,1),pksMax(noNaN,3,j), '.','color','r', 'MarkerSize', 20);%ear bilateral, 1
    p4       = plot(repmat(4,ntasks,1),pksMax(noNaN,4,j), '.','color','r', 'MarkerSize', 20);%ear unilateral right, 1
    p5       = plot(repmat(5,ntasks,1),pksMax(noNaN,5,j), '.','color','r', 'MarkerSize', 20);%ear unilateral left, 1
    
    dropp{1,j} = [p1.YData; p2.YData; p3.YData; p4.YData; p5.YData;];
 
    table2go(j,1) = median(p1.YData);
    table2go(j,2) =   mean(p1.YData);
    table2go(j,3) =    std(p1.YData);
    plot_table{j,1} = p1.YData;
    
    table2go(j,4) = median(p2.YData);
    table2go(j,5) =   mean(p2.YData);
    table2go(j,6) =    std(p2.YData);
    plot_table{j,2} = p2.YData;

    table2go(j,7) = median(p3.YData);
    table2go(j,8) =   mean(p3.YData);
    table2go(j,9) =    std(p3.YData);
    plot_table{j,3} = p3.YData;

    table2go(j,10) = median(p4.YData);
    table2go(j,11) =   mean(p4.YData);
    table2go(j,12) =    std(p4.YData);

    table2go(j,13) = median(p5.YData);
    table2go(j,14) =   mean(p5.YData);
    table2go(j,15) =    std(p5.YData);
    

    P1(j,1:length(p1.YData)) = p1.YData;
    P2(j,1:length(p2.YData)) = p2.YData;
    P3(j,1:length(p3.YData)) = p3.YData;
    P4(j,1:length(p4.YData)) = p4.YData;
    P5(j,1:length(p5.YData)) = p5.YData;
    
    [rho pval]= corrcoef(P2(j,P2(j,:)~=0),P3(j,P3(j,:)~=0));
    p(j,1) = pval(2);
    r(j,1) = rho(2);
    [rho pval]= corrcoef(P2(j,P2(j,:)~=0),P4(j,P4(j,:)~=0));
    p(j,2) = pval(2);
    r(j,2) = rho(2);
    [rho pval]= corrcoef(P2(j,P2(j,:)~=0),P5(j,P5(j,:)~=0));
    p(j,3) = pval(2);
    r(j,3) = rho(2);

    title(names{j},'FontSize', 12, 'FontWeight', 'bold')
    box off
    xticks([1 2 3 4 5])
    xticklabels({'Scalp (sta.)','Scalp (ind.)','Ear bilateral', 'Ear right', 'Ear left'})

    ylim([0.08 2.2])
    
    a = get(gca,'XTickLabel');  
    set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
    xtickangle(45)
    
    if j == 1
        ylabel('Hedges� g','FontSize', 15, 'FontWeight', 'bold')
        %     elseif j == 2
    elseif j == 5
        legend([p1, p2, p3 p4 p5],{'Scalp (sta.)','Scalp (ind.)','Ear bil.', 'Ear right', 'Ear left'},'location', 'northeast','FontSize', 12, 'FontWeight', 'bold');
        legend boxoff
    end
end

[corrected_p, h] = bonf_holm(p,0.05);

set(gcf,'Color','w')
set(gcf, 'Position', get(0, 'Screensize'));

%% get signal loss (mean hedges'g from scalp(ind.) to all other conditions)

for t = 1:length(taskLetter)
sl(t,1) = 100 - table2go(t,2) /  table2go(t,5) * 100;
sl(t,2) = 100 - table2go(t,8) /  table2go(t,5) * 100;
sl(t,3) = 100 - table2go(t,11) / table2go(t,5) * 100;
sl(t,4) = 100 - table2go(t,14) / table2go(t,5) * 100;
end

sl = array2table(sl);
writetable(sl,'table_sl.xlsx','Sheet','MyNewSheet','WriteVariableNames',false);

print(barfigure,'C:\Users\arnd\Pictures\Barfigure','-dpng')

table2go = array2table(table2go);
writetable(table2go,'table_es.xlsx','Sheet','MyNewSheet','WriteVariableNames',false);

%% get differences in signal loss between tasks and ear-EEGs (uni- bilateral)

%dropp: 1 = sta channel, 2 = ind. scalp, 3-5 = ear (bil, uni r., uni. l)
%get all signal losses in percent from scalp(ind.)-condition to the three
%ear-conditions
x=0;
for t = 1:length(taskLetter)
drop_bi(1,x+1:x+length(dropp{1,t})) = 100 - dropp{1,t}(3,:) ./ dropp{1,t}(2,:) *100; % is the drop from scalp to bilateral ear sign. different between tasks?
drop_ur(1,x+1:x+length(dropp{1,t})) = 100 - dropp{1,t}(4,:) ./ dropp{1,t}(2,:) *100;
drop_ul(1,x+1:x+length(dropp{1,t})) = 100 - dropp{1,t}(5,:) ./ dropp{1,t}(2,:) *100;
g1(1,x+1:x+length(dropp{1,t})) = t;
x = x + length(dropp{1,t}); 
end
% g1 = different tasks, g2 = different ear-EEGs
g1 = repmat(g1,1,3);
g2 = {'bi','ur','ul'};
g2 = repelem(g2,1,length(drop_bi));

drop_all = [drop_bi drop_ur drop_ul];

[p,tbl,stats,terms] = anovan(drop_all,{g1,g2},'model','interaction','varnames',{'ERP','ear-EEG'});

[ptask,tbltask,statstask,termstask] = anovan(drop_all,{g1},'varnames',{'tasks'});
[pear,tblear,statsear,termsear]     = anovan(drop_all,{g2},'varnames',{'ear-EEG'});

[mcomp,m,h,nms] = multcompare(statstask,'display','on');
set(gcf,'Color','w')
box off
axis square
print(gcf,'C:\Users\arnd\Pictures\ANOVA_task','-dpng')

[mcomp,m,h,nms] = multcompare(statsear ,'display','on');
set(gcf,'Color','w')
box off
axis square
print(gcf,'C:\Users\arnd\Pictures\ANOVA_EEG','-dpng')

%% make a figure for the mean signal losses per task

bar_task1 = reshape([plot_table{1,:}],[length([plot_table{1,:}])/3],3);
bar_task2 = reshape([plot_table{2,:}],[length([plot_table{2,:}])/3],3);
bar_task3 = reshape([plot_table{3,:}],[length([plot_table{3,:}])/3],3);
bar_task4 = reshape([plot_table{4,:}],[length([plot_table{4,:}])/3],3);
bar_task5 = reshape([plot_table{5,:}],[length([plot_table{5,:}])/3],3);

drop_task1(:,1) = round(bar_task1(:,1) ./ bar_task1(:,2) *100,2);
drop_task1(:,2) = 100;
drop_task1(:,3) = round(bar_task1(:,3) ./ bar_task1(:,2) *100,2);
drop_task2(:,1) = round(bar_task2(:,1) ./ bar_task2(:,2) *100,2);
drop_task2(:,3) = round(bar_task2(:,3) ./ bar_task2(:,2) *100,2);
drop_task2(:,2) = 100;
drop_task3(:,1) = round(bar_task3(:,1) ./ bar_task3(:,2) *100,2);
drop_task3(:,3) = round(bar_task3(:,3) ./ bar_task3(:,2) *100,2);
drop_task3(:,2) = 100;
drop_task4(:,1) = round(bar_task4(:,1) ./ bar_task4(:,2) *100,2);
drop_task4(:,3) = round(bar_task4(:,3) ./ bar_task4(:,2) *100,2);
drop_task4(:,2) = 100;
drop_task5(:,1) = round(bar_task5(:,1) ./ bar_task5(:,2) *100,2);
drop_task5(:,3) = round(bar_task5(:,3) ./ bar_task5(:,2) *100,2);
drop_task5(:,2) = 100;

bar_mean = [mean(drop_task1,1)',mean(drop_task2,1)',mean(drop_task3,1)',mean(drop_task4,1)',mean(drop_task5,1)'];
bar_sta = [std(drop_task1,1)',std(drop_task2,1)',std(drop_task3,1)',std(drop_task4,1)',std(drop_task5,1)'];

figure
bar_plot = errorbar([1 1 1],bar_mean(:,2),bar_sta(:,2),'.r', 'MarkerSize',20)
hold on
bar_plot = errorbar([2 2 2],bar_mean(:,3),bar_sta(:,3),'o')
bar_plot = errorbar([3 3 3],bar_mean(:,4),bar_sta(:,4),'o')
bar_plot = errorbar([4 4 4],bar_mean(:,5),bar_sta(:,5),'o')



