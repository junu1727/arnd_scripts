function [effect_size_matrix, best_loc, bestchans,cond1ERP_it_mean,cond2ERP_it_mean,cond1ERP_it_sta,cond2ERP_it_sta,maxAll] = effect(special_N1,condition1,condition2,FullorEar,unilateral,timeWin,bestN,sta_elec,fs)

%datasets: names of the two loaded datasets (condition1, the one with more trials, condition2, the one with less trials)
%full or ear: determine whether a full cap or only a subset of electrodes is used: 1 = full, 2 = ear
%unilateral: if only ear is used, decide whether to use both ears or one 0 = bilateral, 1 = unilateral, [] = cap is used, no subselection ofelectrodes needed
%time window: to find the channels with the largest effects, determine the time window in which the effect is expected to be the highest (ms) [timeStart timeStop]
%iterations: how many subsamples should be drown from the condition with more trials?
%taskLetter: a,b,c or d, depending on what task you are checking (string, e.g. 'b')
%staElec: if wanted, get the effect size from a standard cap-electrode for the task (e.g. FPz for P300). Input: index of electrode in chanlocs
%% calculate and compare the best electrodes with the standards + a comparison between cap and ear

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data
if FullorEar == 2 % if only ear was chosen...
    if unilateral == 0 % if both ears are chosen, delete the other cap electrodes
        condition1 = pop_select( condition1, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
        if special_N1 == 0
            condition2 = pop_select( condition2, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
        end
    elseif unilateral == 1 % if only one ear is of interest, delete the other electrodes for both ear- datasets (ICA-cleaned and uncleaened)
        condition1 = pop_select( condition1, 'channel',{'E18','E24','E71','E72','E85','E92'});
        if special_N1 == 0
            condition2 = pop_select( condition2, 'channel',{'E18','E24','E71','E72','E85','E92'});
        end
    else isempty(unilateral)
        disp('Using all cap electrodes')
    end
end

if isempty(timeWin)
    disp('no time window for effect size specified. Using the entire epoch length')
else
    timeStart = find(condition1.times == timeWin(1));
    timeEnd   = find(condition1.times == timeWin(2)) -1;
    if isempty(timeStart) || isempty(timeStart)
        error('invalid time parameters. Please check the sampling rate and the possible time windos')
    end
end
%% get the condition effects from the best channels

if special_N1 == 0
    best_condition1 = mbl_bipolarchannelexpansion(condition1,1,FullorEar);
    best_condition2 = mbl_bipolarchannelexpansion(condition2,1,FullorEar);
    sameSize = [];
else
    best_condition1 = mbl_bipolarchannelexpansion(condition1,1,FullorEar);
    sameSize = 1;
end

diff_all = cell(size(best_condition1.data,1),1);
bsWin = find(best_condition1.times == -200):find(best_condition1.times == 0)-1; % baseline time window

tic
for c = 1:size(best_condition1.data,1)
    if mod(c,500) == 0
        disp([num2str(c),' of ',num2str(size(best_condition1.data,1)),' done!'])
    end
    
    if special_N1 == 0
        sta = squeeze(best_condition1.data(c,:,:));
        dev = squeeze(best_condition2.data(c,:,:));
    else
        sta = squeeze(best_condition1.data(c,timeStart:timeEnd,:));
        dev = squeeze(best_condition1.data(c,bsWin,:)); %this is the baseline of all tone-mixes. It is used as a null-comparison
    end

    diff_all{c,1} = mes(sta',dev','hedgesg','isDep',0); %where the magic happens. Calc the effect size over time
    
end
toc

if special_N1 == 0
    effect_size_matrix = zeros(size(diff_all,1),100);
else
    effect_size_matrix = zeros(size(diff_all,1),20);
end
        
for x = 1:size(best_condition1.data,1)
    effect_size_matrix(x,:) = diff_all{x}.hedgesg;
end

effect_size_matrix  = smoothdata(effect_size_matrix,2,'movmean',4);

% find for every channel the largest positive peak
maxAll = [];

%  For the best bilateral channel, only look into the bipolar channels. The first 96/12 are the channels against nose ref
if FullorEar == 1
    sizePeaks = size(condition1.data,1)+1 :size(effect_size_matrix,1); %cap, only bipolar channels
else
    sizePeaks = 1:size(effect_size_matrix,1); % ear, all channels (those are all bipolar from the start)
end

count = 1;
for c = sizePeaks
    if special_N1 == 0
        [maxAll(count).pks maxAll(count).locs] = max(abs(effect_size_matrix(c,timeStart:timeEnd))); % find the peaks (largest, positive effect size)
    else
        [maxAll(count).pks maxAll(count).locs] = max(abs(effect_size_matrix(c,:))); % find the peaks (largest, positive effect size)
    end
    
    count = count + 1;
end

% get the n best channels
[~, best_loc] = maxk([maxAll.pks],bestN);

if FullorEar == 1 % if the cap was chosen, adjust the locations to fit the data size
best_loc = best_loc + size(condition1.data,1);
end
    
bestchans = {best_condition1.chanlocs(best_loc).labels}; % these channels have the largest effect sizes for sta and dev

if special_N1 == 0
    cond1ERP_it_mean = best_condition1.data(best_loc,:,:);
    cond2ERP_it_mean = best_condition2.data(best_loc,:,:);
    
    cond1ERP_it_sta = best_condition1.data(sta_elec,:,:);
    cond2ERP_it_sta = best_condition2.data(sta_elec,:,:);
else
    % get the ERPs of both conditions for the n best channels
    cond1ERP_it_mean = best_condition1.data(best_loc,timeStart:timeEnd,:);
    cond2ERP_it_mean = best_condition1.data(best_loc,bsWin,:);
    
    % get the ERP of the standard channel
    cond1ERP_it_sta = best_condition1.data(sta_elec,timeStart:timeEnd,:);
    cond2ERP_it_sta = best_condition1.data(sta_elec,bsWin,:);
end
end