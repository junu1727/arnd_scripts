%% set some variables
taskLetter    = {'N1';'a';'b';'c';'d'};
conditionString = {'cap','earc','ear'};
window_start  = [50 50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
uniR = {'E18 - E24','E18 - E71','E18 - E72','E18 - E85','E18 - E92','E24 - E71','E24 - E72','E24 - E85',...
        'E24 - E92','E71 - E72','E71 - E85','E71 - E92','E72 - E85','E72 - E92','E85 - E92'};   
uniL = {'E22 - E28','E22 - E80','E22 - E81','E18 - E90','E22 - E95','E28 - E80','E28 - E81','E28 - E90',...
        'E28 - E95','E80 - E81','E80 - E90','E80 - E95','E81 - E90','E81 - E95','E90 - E95'}; 
names = {'N100 (n=15)','N100(att.)(n=15)','MMN (n=16)','P300 (n=14)','N400 (n=16)'};

       %% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
       ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
       ,'fuhi52','okum71','kuma55','pigo79'
       };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

load([MAINPATH,'MeanES.mat']);
load([MAINPATH,'MaxVar.mat']);

%get fieldnames for looping through each layer
con = fieldnamesr(maxVar,1);
tasks = fieldnamesr(mean_es.cap,1);
vars  = fieldnamesr(mean_es.cap.N1,1);
pksMax = zeros(length(SBJ),5,length(taskLetter));

for t = 1:length(taskLetter) %loop through tasks
    count = 0;
    for s = 1:length(SBJ) %loop through subjects
        
        load([MAINPATH,'quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        q_check(5)= q_check(1);
        q_check = circshift(q_check,1);
        
        if t == 1
            timeWin = 1:20;
        else
            timeWin = find(timeVec == window_start(t)) : find(timeVec == window_stop(t)) -1;
        end
        
        if q_check(t).badEarChan == 0
            
            for c = 1:5
                
                if c == 1
                    %cap, sta.
                    pksMax(s,c,t)= maxVar.cap.(taskLetter{t})((s)).maxVal_sta; % store the peaks of each subject*condition*task from the best channel
                elseif c == 2
                    %cap, ind.
                    pksMax(s,c,t)= maxVar.cap.(taskLetter{t})((s)).maxVal(maxVar.cap.(taskLetter{t})((s)).best_loc(1)); % store the peaks of each subject*condition*task from the best channel
                elseif c == 3 %for the bilateral condition, take the best channel among those built from both ears
                    %ear (bilateral), ind.
                    pksMax(s,c,t)= maxVar.ear.(taskLetter{t})((s)).maxVal(maxVar.ear.(taskLetter{t})((s)).best_loc(1)); % store the peaks of each subject*condition*task from the best channel
                elseif c == 4
                    %ear (unilateral, right), ind.
                    for x = 1:length(uniR) %loop through unilateral channels (right ear) to get their indices in the channel lsit
                        bestR(x) = find(strcmp(uniR(x),maxVar.ear.(taskLetter{t})(s).bestchans));
                    end
                    m1 = maxVar.ear.(taskLetter{t})(s).es_matrix_mean(maxVar.ear.(taskLetter{t})(s).best_loc(min(bestR)),:); % take the smallest number, aka the best channel among the unilateral channels
                    pksMax(s,c,t)= max(m1(1,timeWin)); % store the peaks of each subject*condition*task from the best channel
                else
                    %ear (unilateral, left), ind.
                    for x = 1:length(uniL) %loop through unilateral channels (left ear) to get their indices in the channel lsit
                        bestL(x) = find(strcmp(uniL(x),maxVar.(con{3}).(taskLetter{t})(s).bestchans));
                    end
                    m1 = maxVar.ear.(taskLetter{t})(s).es_matrix_mean(maxVar.ear.(taskLetter{t})(s).best_loc(min(bestL)),:); % take the smallest number, aka the best channel among the unilateral channels
                    pksMax(s,c,t)= max(m1(1,timeWin)); % store the peaks of each subject*condition*task from the best channel
                end
                
            end
        else
         count = count + 1; %mean_es only has as many rows as subjects (18 of 20 when two subjects got kicked). maxVar always has 20. This variable alignes the counting
        end
    end
end

pksMax(find(pksMax == 0)) = NaN;


%lightgrey = [.95, .95, .95];
grey = [.75 .75 .75];
darkgrey = [.2 .2 .2];
% darkblue = [55 96 146]/255;
% lightblue = [225 231 239]/255;
% darkgreen = [79 115 76]/255;
% darkred = [192 80 77]/255;
% purple = [180 30 130]/255;
% orange = [210 100 10]/255;

cmap = colormap(jet);
cmap = cmap(1:3:60,:); % colors as subjects
cnums = 1:20;
P1 = zeros(length(taskLetter),length(SBJ));
P2 = zeros(length(taskLetter),length(SBJ));
P3 = zeros(length(taskLetter),length(SBJ));

barfigure = figure;
for j = 2:length(taskLetter)
    noNaN = ~isnan(pksMax(:,1,j)); %get indices of subjects per task (NaN-row = subject removed)
    ntasks = sum(noNaN); %number of subjects in each tasks
    colcount = cnums(noNaN);
    
    ax = subplot(1,5,j-1);
    %ax.Position(3) = ax.Position(3)*1.25;
    boxp = boxplot([pksMax(noNaN,1,j) pksMax(noNaN,2,j) pksMax(noNaN,3,j)]);
    set(boxp,{'linew'},{3})
    boxcolor = get(boxp,'children');   % Get the handles of all the objects
    %set(gca,'linew',2)
    
    hold on
    
    lineplot = plot([repmat(1,ntasks,1),repmat(2,ntasks,1),repmat(3,ntasks,1)]',...
        [pksMax(noNaN,1,j) pksMax(noNaN,2,j) pksMax(noNaN,3,j)]','-','Color','k');
    
    p1 = plot(repmat(1,ntasks,1),pksMax(noNaN,1,j), '.','color','cyan', 'MarkerSize', 20);%cap sta, 1
    p2 = plot(repmat(2,ntasks,1),pksMax(noNaN,2,j), '.','color','b', 'MarkerSize', 20);%cap uni, 1
    p3 = plot(repmat(3,ntasks,1),pksMax(noNaN,3,j), '.','color','r', 'MarkerSize', 20);%ear bilateral, 1
    
    P1(j,1:length(p1.YData)) = p1.YData;
    P2(j,1:length(p2.YData)) = p2.YData;
    P3(j,1:length(p3.YData)) = p3.YData;

    title(names{j},'FontSize', 12, 'FontWeight', 'bold')
    box off
    xticks([1 2 3])
    xticklabels({'Scalp (sta.)','Scalp (ind.)','Ear (ind.)'})

    ylim([0.08 2.2])
    
    a = get(gca,'XTickLabel');  
    set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
    xtickangle(45)
    
    if j == 1
        ylabel('Hedges� g','FontSize', 15, 'FontWeight', 'bold')
        %     elseif j == 2
    elseif j == 3
        legend([p1, p2],{'Scalp (sta.)','Scalp (ind.)','Ear (uni.)'},'location', 'northeast','FontSize', 12, 'FontWeight', 'bold');
        legend boxoff
    end
end

set(gcf,'Color','w')
set(gcf, 'Position', get(0, 'Screensize'));

print(barfigure,'C:\Users\arnd\Pictures\Barfigure_Poster','-dpng')

%% signal loss

s_loss = [45.33,54.63,29.6,50.27;22.28,33.05,41.81,21.2];


