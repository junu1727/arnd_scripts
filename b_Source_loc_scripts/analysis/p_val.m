%% calculate and compare the best electrodes between sets

%clear all; close all; clc

%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [ 50  50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'N1';'a';'b';'c';'d'};
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
conditionString = {'cap','earc','ear'};
bestN = 66; %The bestN channels are stored for each setup (cap or ear)
sta_elec = [43 55 55; 43 55 55; 31 55 55; 5 55 55; 34 55 55]; %channel number of the standard electrode for all three conditions (cap, earc and ear)
fs = 100; %sampling rate
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

load([MAINPATH,'MeanES.mat'],'mean_es');

eeglab
tempPath = dir([MAINPATH,SBJ{1},'\04_clean\*\*_a','*','ear.set']); %set 1 for the cleaned cap data
superCon = pop_loadset('filename', tempPath(1).name, 'filepath',tempPath(1).folder);

for s = 1:length(SBJ)
    
    load([MAINPATH,'quality_check.mat'],'q_check');
    getSBJ = contains({q_check.SBJ},SBJ{s});
    q_check = q_check(getSBJ);
    
    for taskNum = 1:length(taskLetter)
        
        q_task = taskNum; %counter for the tasks. there are 4 tasks, but task a has two runs, one for N1 from tone onset, one from all later tones. Therefore, we need an additional counter
        if taskNum > 1
            q_task = q_task - 1;
            special_N1 = 0;
        else
            special_N1 = 1;
        end
        
        if q_check(q_task).badEarChan == 0
            
            for c = [1 1 3]
                
                if c == 1
                    isear = [];
                else c == 3
                    isear = '_ear';
                end
                
                if taskLetter{taskNum} == 'N1'
                    naming1st = isear;
                elseif taskLetter{taskNum} == 'a'
                    naming1st = ['_unatt',isear];
                    naming2nd = ['_att',isear];
                elseif taskLetter{taskNum} == 'b' || taskLetter{taskNum} == 'c'
                    naming1st = ['_sta',isear];
                    naming2nd = ['_dev',isear];
                else taskLetter{taskNum} == 'd'
                    naming1st = ['_congr',isear];
                    naming2nd = ['_incongr',isear];
                end
                
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'*',naming1st,'.set']); %set 1 for the cleaned cap data
                con1path = con1path(1);
                if special_N1 == 0
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},naming2nd,'.set']); %set 2 for the cleaned cap data
                    con2path = con2path(1);
                end
                
                if c == 1
                    FullorEar     = 1; % 1 = full, 2 = ear
                    pathcond = 'cap';
                else c == 2 || c == 3;
                    FullorEar     = 2; % 1 = full, 2 = ear
                    pathcond = 'cap';
                end
                
                % get folder names
                if taskLetter{taskNum} == 'N1'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{taskNum} == 'a'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{taskNum} == 'b'
                    bintext = 'bin_b.txt';
                    folderName = 'b_passive_oddball';
                elseif taskLetter{taskNum} == 'c'
                    bintext = 'bin_c.txt';
                    folderName = 'c_active_oddball';
                elseif taskLetter{taskNum} == 'd'
                    bintext = 'bin_d.txt';
                    folderName = 'd_conguency';
                else
                    error('please enter a valid taskLetter (a-d)')
                end
                
                PATHIN  = con1path.folder;
                PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
                if ~exist (PATHOUT)
                    mkdir(PATHOUT)
                end
                
                % load data: 2 conditions, smooth both
                condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                condition1.data = smoothdata(condition1.data ,2,'movmean',4);
                
                if special_N1 == 0
                    condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
                    condition2.data = smoothdata(condition2.data ,2,'movmean',4);
                else
                    condition2 = [];
                end
                
                
                if special_N1 == 0
                    best_condition1 = mbl_bipolarchannelexpansion(condition1,0);
                    best_condition2 = mbl_bipolarchannelexpansion(condition2,0);
                    sameSize = [];
                else
                    best_condition1 = mbl_bipolarchannelexpansion(condition1,0);
                    sameSize = 1;
                end
                
                
                %change marker names, bin-function does not work with spaces
                for z = 1:length(best_condition1.event)
                    best_condition1.epoch(z).eventtype = strrep(best_condition1.epoch(z).eventtype,' ','');
                    best_condition1.event(z).type = strrep(best_condition1.event(z).type,' ','');
                    best_condition1.epoch(z).event = z;
                end
                
                for z = 1:length(best_condition2.event)
                    best_condition2.epoch(z).eventtype = strrep(best_condition2.epoch(z).eventtype,' ','');
                    best_condition2.event(z).type = strrep(best_condition2.event(z).type,' ','');
                    %                     best_condition1.epoch(z).event = z;
                end
                
                %give both conditions the same subjectID. The toolbox needs that
                naming = strrep(best_condition1.setname,'_sta','');
                best_condition1.subject = naming;
                best_condition2.subject = naming;
                
                %                 best_condition1.data(:,:,end+1:end+size( best_condition2.data,3)) = best_condition2.data;
                %                 best_condition1.trials = size( best_condition1.data,3);
                
                best_condition1 = pop_saveset(best_condition1, [best_condition1.setname,'.set'], [MAINPATH,'_p_data\pre_bin\']);
                best_condition2 = pop_saveset(best_condition2, [best_condition2.setname,'.set'], [MAINPATH,'_p_data\pre_bin\']);

                if ~exist([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond])
                    mkdir([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond])
                    mkdir([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond])
                end
                
                cd(MAINPATH)
                p_condition1 = bin_info2EEG([MAINPATH,'_p_data\pre_bin\',best_condition1.setname,'.set'],bintext,[MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,best_condition1.setname],3);
                p_condition2 = bin_info2EEG([MAINPATH,'_p_data\pre_bin\',best_condition2.setname,'.set'],bintext,[MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,best_condition2.setname],3);

            end
        end
    end
end

cd([MAINPATH,'_p_data\post_bin\'])
GND=sets2GND('gui','bsln',[-200 0],'verblevel',3);
GND1=bin_dif(GND,1,2,'dev - sta');
GND2=tmaxGND(GND1,3,'time_wind',[-200 800],'verblevel',3,'plot_raster','no');

figure;plot(GND2.grands_t(:,:,3)') 
 
 %% 2 sets
 
 best_condition1.epoch(end+1:end+length( best_condition2.epoch)) = best_condition2.epoch;
 best_condition1.event(end+1:end+length( best_condition2.event)) = best_condition2.event;
 
 
 %change marker names, bin-function does not work with spaces
 for z = 1:length(best_condition1.event)
     best_condition1.epoch(z).eventtype = strrep(best_condition1.epoch(z).eventtype,' ','');
     best_condition1.event(z).type = strrep(best_condition1.event(z).type,' ','');
     best_condition1.epoch(z).event = z;
 end
 
 %                 for z = 1:length(best_condition2.event)
 %                     best_condition2.epoch(z).eventtype = strrep(best_condition2.epoch(z).eventtype,' ','');
 %                     best_condition2.event(z).type = strrep(best_condition2.event(z).type,' ','');
 %                     %                     best_condition1.epoch(z).event = z;
 %                 end
 
 best_condition1.data(:,:,end+1:end+size( best_condition2.data,3)) = best_condition2.data;
 best_condition1.trials = size( best_condition1.data,3);
 
 
 best_condition1 = pop_saveset(best_condition1, [best_condition1.setname,'.set'], [MAINPATH,'_p_data\pre_bin\']);
 %                 best_condition2 = pop_saveset(best_condition2, [best_condition2.setname,'.set'], [MAINPATH,'_p_data\pre_bin\']);
 
 naming = strrep(best_condition1.setname,'_sta','');
 if ~exist([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\sta\'])
     mkdir([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\sta\'])
     mkdir([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\dev\'])
 end
 
 cd(MAINPATH)
 p_condition1 = bin_info2EEG([MAINPATH,'_p_data\pre_bin\',best_condition1.setname,'.set'],bintext,[MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\',naming]);
 %                 p_condition2 = bin_info2EEG([MAINPATH,'_p_data\pre_bin\',best_condition2.setname,'.set'],bintext,[MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\dev\',best_condition2.setname]);
 
 