%% calculate and compare the best electrodes between sets

%clear all; close all; clc

%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [ 50  50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
% peak_start  = [130  150  180  400  440]; % start of the time window of interest per task. Should contain the desired effect
% peak_stop   = [150  180  200  440  480]; %   end of the time window of interest per task. Should contain the desired effect
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'N1';'a';'b';'c';'d'};
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
conditionString = {'cap','earc','ear'};
bestN = 66; %The bestN channels are stored for each setup (cap or ear)
sta_elec = [43 55 55; 43 55 55; 31 55 55; 5 55 55; 34 55 55]; %channel number of the standard electrode for all three conditions (cap, earc and ear)
fs = 100; %sampling rate
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

 load([MAINPATH,'MeanES.mat'],'mean_es');

for s = 1:length(SBJ)
    
    load([MAINPATH,'quality_check.mat'],'q_check');
    getSBJ = contains({q_check.SBJ},SBJ{s});
    q_check = q_check(getSBJ);
   
    for taskNum = 1:length(taskLetter)
        
        q_task = taskNum; %counter for the tasks. there are 4 tasks, but task a has two runs, one for N1 from tone onset, one from all later tones. Therefore, we need an additional counter
        if taskNum > 1
            q_task = q_task - 1;
            special_N1 = 0;
        else
            special_N1 = 1;
        end
        
        if q_check(q_task).badEarChan == 0
            
            for conditionNum = 1:3
                
                if conditionNum == 1 || conditionNum == 2
                    isear = [];
                else conditionNum == 3
                    isear = '_ear';
                end
                
                if taskLetter{taskNum} == 'N1'
                    naming1st = isear;
                elseif taskLetter{taskNum} == 'a'
                    naming1st = ['_unatt',isear];
                    naming2nd = ['_att',isear];
                elseif taskLetter{taskNum} == 'b' || taskLetter{taskNum} == 'c'
                    naming1st = ['_sta',isear];
                    naming2nd = ['_dev',isear];
                else taskLetter{taskNum} == 'd'
                    naming1st = ['_congr',isear];
                    naming2nd = ['_incongr',isear];
                end
                
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'*',naming1st,'.set']); %set 1 for the cleaned cap data
                con1path = con1path(1);
                if special_N1 == 0
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},naming2nd,'.set']); %set 2 for the cleaned cap data
                    con2path = con2path(1);
                end
                
                if conditionNum == 1
                    FullorEar     = 1; % 1 = full, 2 = ear
                else conditionNum == 2 || conditionNum == 3;
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
                % get folder names
                if taskLetter{taskNum} == 'N1'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{taskNum} == 'a'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{taskNum} == 'b'
                    folderName = 'b_passive_oddball';
                elseif taskLetter{taskNum} == 'c'
                    folderName = 'c_active_oddball';
                elseif taskLetter{taskNum} == 'd'
                    folderName = 'd_conguency';
                else
                    error('please enter a valid taskLetter (a-d)')
                end
                
                PATHIN  = con1path.folder;
                PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
                if ~exist (PATHOUT)
                    mkdir(PATHOUT)
                end
                
                % load data: 2 conditions, smooth both
                condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                condition1.data = smoothdata(condition1.data ,2,'movmean',4);
                
                if special_N1 == 0
                    condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
                    condition2.data = smoothdata(condition2.data ,2,'movmean',4);
                else
                    condition2 = [];
                end
                
                if unilateral == 1
                    earFolder = '\05_ERP_uni\';
                elseif unilateral == 0
                    earFolder = '\05_ERP\';
                end
                
                if ~exist([MAINPATH,SBJ{s},earFolder,folderName])
                    mkdir([MAINPATH,SBJ{s},earFolder,folderName])
                end
                
                %% call function to calculate the largest effect size, the channels contributing to it and the matrix of random permutations used as subsamples from the condition with more trials
%                 [effect_size_matrix, trials, best_loc, bestchans,cond1ERP_it_mean,cond2ERP_it_mean,cond1ERP_it_sta,cond2ERP_it_sta] = effect(...
%                     special_N1,condition1,condition2,FullorEar,unilateral,[window_start(taskNum) window_stop(taskNum)],iterations,bestN,sta_elec(conditionNum));
               
                [effect_size_matrix, best_loc, bestchans,cond1ERP_it_mean,cond2ERP_it_mean,cond1ERP_it_sta,cond2ERP_it_sta,maxAll] = effect(...
                    special_N1,condition1,condition2,FullorEar,unilateral,[window_start(taskNum) window_stop(taskNum)],bestN,sta_elec(conditionNum),fs);
                
                %% prepare the data to be stored and plotted later
                
                ERPs.cond1    = mean(cond1ERP_it_mean,3);
                ERPs.cond2    = mean(cond2ERP_it_mean,3);
                ERPs.stacond1 = mean(cond1ERP_it_sta,3);
                ERPs.stacond2 = mean(cond2ERP_it_sta,3);
                ERPnames = fieldnamesr(ERPs);
                
                %transform the ERPs to be in the right direction (because of the way we get all bipolar channels,
                %the same channel is sometimes subtracted and sometimes subtracted from. Therefore, the sign is sometimes reversed)
                if taskNum ~= 1
                    timeWin = find(timeVec == window_start(taskNum)) : find(timeVec == window_stop(taskNum)) -1;
                else
                    timeWin = find(timeVecN1 == window_start(taskNum)) : find(timeVecN1 == 240);
                end

                for w = 1:size(ERPs.cond1,1)
                    [pksMax(w), locsMax]= max(ERPs.cond1(w,timeWin));
                    [pksMin(w), locsMin]= min(ERPs.cond1(w,timeWin));
                    
                    for x = 1:2                      
                        % P300 is the only positive deflection, max should be
                        % larger than min
                        if taskLetter{taskNum} ~= 'c'
                            if abs(pksMax(w)) > abs(pksMin(w))
                                ERPs.(ERPnames{x})(w,:) = ERPs.(ERPnames{x})(w,:) * -1;
                            end
                        else
                            if abs(pksMax(w)) < abs(pksMin(w))
                                ERPs.(ERPnames{x})(w,:) = ERPs.(ERPnames{x})(w,:) * -1;
                            end
                        end
                    end
                end
                
                % get abs. effect sizes
                effect_size_matrix = abs(effect_size_matrix); %average over all iterations, get the absolut values of the effect size
                
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).es_matrix_mean     = effect_size_matrix; %effect sizes, channel*time*iteration
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).best_loc           = best_loc; % indices of the best n channels
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).bestchans          = bestchans; % string names of the n best channels
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).ERP_cond1          = ERPs.cond1; % ERPs of both conditions for the n best channels, channel*time*trials
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).ERP_cond2          = ERPs.cond2; % ERPs of both conditions for the n best channels, channel*time*trials
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).ERP_stacond1       = ERPs.stacond1; % ERPs of both conditions for the n best channels, channel*time*trials
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).ERP_stacond2       = ERPs.stacond2; % ERPs of both conditions for the n best channels, channel*time*trials
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).maxVal             = [maxAll.pks]; % highest absolute effect size value
                maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).maxLoc             = [maxAll.locs]; % location of the highest value
                if conditionNum == 1
                    maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).es_matrix_sta  =     maxVar.cap.(taskLetter{taskNum})(s).es_matrix_mean(sta_elec(taskNum,1),:);
                    maxVar.(conditionString{conditionNum}).(taskLetter{taskNum})(s).maxVal_sta     = max(maxVar.cap.(taskLetter{taskNum})(s).es_matrix_mean(sta_elec(taskNum,1),timeWin));
                end
            end
        end
    end
end

%re-arange the subfields to be in order N1,a,b,c and d
maxVar.ear  = orderfields(maxVar.ear);
maxVar.earc = orderfields(maxVar.earc);
maxVar.cap  = orderfields(maxVar.cap);

% add 96 zeros to the maxVals. These are placeholders for the 96 original
% channels
tasks = fieldnamesr(maxVar.cap,1);
for t = 1:length(tasks)
    for s = 1:19
        if ~isempty(maxVar.cap.(tasks{t})(s).es_matrix_mean)
            maxVar.cap.(tasks{t})(s).maxVal = [zeros(1, max(0, numel(maxVar.cap.(tasks{t})(s).es_matrix_mean(:,1))-numel(maxVar.cap.(tasks{t})(s).maxVal))), maxVar.cap.(tasks{t})(s).maxVal];
            maxVar.cap.(tasks{t})(s).maxLoc = [zeros(1, max(0, numel(maxVar.cap.(tasks{t})(s).es_matrix_mean(:,1))-numel(maxVar.cap.(tasks{t})(s).maxLoc))), maxVar.cap.(tasks{t})(s).maxLoc];
        end
    end
end

save([MAINPATH,'MaxVar.mat'],'maxVar');


%% store the variables more comfortably
con = fieldnamesr(maxVar,1);
zb = 1;

counter = 1;
hedgesG_GND_AVG = [];
for c = 1:length(con)
    for t = 1:length(tasks)
        count = 0; %don't allow empty lines in the final variable. If a task is empty, skip that line instead of writing a zero line
%         for s = 1:length(maxVar.cap.c)
%             if ~isempty(maxVar.(con{c}).(tasks{t})(s).ERP_cond1)
%                 
%                                 mean_es.(con{c}).(tasks{t}).ERP_cond1     (s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_cond1(1,:);
%                                 mean_es.(con{c}).(tasks{t}).ERP_cond2     (s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_cond2(1,:);
%                                 mean_es.(con{c}).(tasks{t}).ERP_stacond1  (s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_stacond1;
%                                 mean_es.(con{c}).(tasks{t}).ERP_stacond2  (s-count,:) = maxVar.(con{c}).(tasks{t})(s).ERP_stacond2;
%                                   mean_es.(con{c}).(tasks{t}).es_matrix_mean(s-count,:) = maxVar.(con{c}).(tasks{t})(s).es_matrix_mean(maxVar.(con{c}).(tasks{t})(s).best_loc(1),:);
%                 if c == 1
%                     mean_es.(con{c}).(tasks{t}).es_matrix_sta (s-count,:) = maxVar.(con{c}).(tasks{t})(s).es_matrix_sta;
%                 end
%             else
%                 count = count+1;
%             end
%         end
        
        if counter == 1
            %get the effect sizes over all subjects for the standard channel,
            %scalp only (no standard for ear). Plus, get es for the ind. scalp
            dev = mean_es.cap.(tasks{t}).ERP_stacond1;
            sta = mean_es.cap.(tasks{t}).ERP_stacond2;
            effect_sizes = mes(sta,dev,'hedgesg','isDep',1);
            hedgesG_GND_AVG{1,t} = abs(effect_sizes.hedgesg);
            
            dev = mean_es.(con{c}).(tasks{t}).ERP_cond1;
            sta = mean_es.(con{c}).(tasks{t}).ERP_cond2;
            effect_sizes = mes(sta,dev,'hedgesg','isDep',1);
            hedgesG_GND_AVG{2,t} = abs(effect_sizes.hedgesg);
        elseif counter == 2 %es of earc
            dev = mean_es.(con{c}).(tasks{t}).ERP_cond1;
            sta = mean_es.(con{c}).(tasks{t}).ERP_cond2;
            effect_sizes = mes(sta,dev,'hedgesg','isDep',1);
            hedgesG_GND_AVG{3,t} = abs(effect_sizes.hedgesg);
        else %es of ear
            dev = mean_es.(con{c}).(tasks{t}).ERP_cond1;
            sta = mean_es.(con{c}).(tasks{t}).ERP_cond2;
            effect_sizes = mes(sta,dev,'hedgesg','isDep',1);
            hedgesG_GND_AVG{4,t} = abs(effect_sizes.hedgesg);
        end     
    end
        counter = counter + 1;
end

figure
for x = 1:length(tasks)
    if x == 1
        timev = timeVecN1;
    else
        timev = timeVec;
    end
subplot(1,5,x)
plot(timev, hedgesG_GND_AVG{1,x})
hold on
plot(timev, hedgesG_GND_AVG{2,x})
plot(timev, hedgesG_GND_AVG{4,x})
end



mean_es.cap = orderfields(mean_es.cap);
mean_es.earc = orderfields(mean_es.earc);
mean_es.ear = orderfields(mean_es.ear);


vars = fieldnamesr(mean_es.cap.a,1);
v(1) = find(strcmp(vars,'es_matrix_sta'));
v(2) = find(strcmp(vars,'es_matrix_mean'));
v(3:4) = v(2);

order = [1 1 2 3];
for c = 1:4 %once the cap (standard channel), cap again (ind. channel), then ear cleaned (earc), (ind. channel), then ear again (uncleaned)
    for t = 1:length(tasks)       
        hedgesG_mean{c,t} = mean(mean_es.(con{order(c)}).(tasks{t}).(vars{v(c)}),1);
        hedgesG_std{c,t} = std(mean_es.(con{order(c)}).(tasks{t}).(vars{v(c)}),1);
    end
end

% hedges'g (GND_AVG, mean and std) now contain 4 rows each: 
%1 cap with ind. channels,
%2 cap with standard channels, 
%3 ear     (ICA-cleaned) with ind. channels,
%3 ear (not ICA-cleaned) with ind. channels,
%and 5 columns (one per task). Each entry is hedges'g over time, once
%averaged over subjects, once for all subjects

for c = 1:3
%     mean_es.(con{c}).('N1').ERP_cond1 = mean_es.(con{c}).('N1').ERP_cond1 *-1;
%     mean_es.(con{c}).('N1').ERP_cond2 = mean_es.(con{c}).('N1').ERP_cond2 *-1;
%     mean_es.(con{c}).('a').ERP_cond1 = mean_es.(con{c}).('a').ERP_cond1 *-1;
%     mean_es.(con{c}).('a').ERP_cond2 = mean_es.(con{c}).('a').ERP_cond2 *-1;
%     mean_es.(con{c}).('b').ERP_cond1 = mean_es.(con{c}).('b').ERP_cond1 *-1;
%     mean_es.(con{c}).('b').ERP_cond2 = mean_es.(con{c}).('b').ERP_cond2 *-1;
%     mean_es.(con{c}).('c').ERP_cond1 = mean_es.(con{c}).('c').ERP_cond1 *-1;
%     mean_es.(con{c}).('c').ERP_cond2 = mean_es.(con{c}).('c').ERP_cond2 *-1;
%     mean_es.(con{c}).('d').ERP_cond1 = mean_es.(con{c}).('d').ERP_cond1 *-1;
%     mean_es.(con{c}).('d').ERP_cond2 = mean_es.(con{c}).('d').ERP_cond2 *-1;
end

save([MAINPATH,'MeanES.mat'],'mean_es','hedgesG_mean','hedgesG_std','hedgesG_GND_AVG');


%% testing area (looking at ERPs per subject)

% figure
% subplot(1,3,1)
% plot(timeVecN1, mean_es.cap.('N1').ERP_cond1)
% subplot(1,3,2)
% plot(timeVecN1, mean_es.earc.('N1').ERP_cond1)
% subplot(1,3,3)
% plot(timeVecN1, mean_es.ear.('N1').ERP_cond1)
 
% g(1) = find(strcmp(vars,'ERP_stacond1'));
% g(2) = find(strcmp(vars,'ERP_stacond2'));
% g(3) = find(strcmp(vars,'ERP_cond1'));
% g(4) = find(strcmp(vars,'ERP_cond2'));
% tempc = {'cap','cap','ear'};
% ylimup = [5 10 6; 5 6 2; 2 4 4; 10 5 2; 10 10 5];
% ylimdown = [-8 -8 -2; -10 -5 -3; -4 -4 -4; -5 -5 -5; -10 -8 -5];

% mean_es.cap.N1.ERP_cond1([4 11],:) = mean_es.cap.N1.ERP_cond1([4 11],:) *-1;
% mean_es.cap.a.ERP_cond1([4 5 7 11 14],:) = mean_es.cap.a.ERP_cond1([4 5 7 11 14],:) *-1;
% mean_es.cap.b.ERP_cond1([15 8],:) = mean_es.cap.b.ERP_cond1([15 8],:) *-1;
% mean_es.cap.c.ERP_cond1([],:) = mean_es.cap.c.ERP_cond1([],:) *-1;
% mean_es.cap.d.ERP_cond1([6 13],:) = mean_es.cap.d.ERP_cond1([6 13],:) *-1;
% 
% mean_es.cap.N1.ERP_cond2([4 11],:) = mean_es.cap.N1.ERP_cond2([4 11],:) *-1;
% mean_es.cap.a.ERP_cond2([8  15],:) = mean_es.cap.a.ERP_cond2([8 15],:) *-1;
% mean_es.cap.b.ERP_cond2([6 13 ],:) = mean_es.cap.b.ERP_cond2([6 13],:) *-1;
% mean_es.cap.c.ERP_cond2([7 14],:) = mean_es.cap.c.ERP_cond2([7 14],:) *-1;
% mean_es.cap.d.ERP_cond2([8 12 15],:) = mean_es.cap.d.ERP_cond2([8 12 15],:) *-1;

% mean_es.ear.N1.ERP_cond1([13 6 5],:) = mean_es.ear.N1.ERP_cond1([13 6 5],:) *-1;
% mean_es.ear.a.ERP_cond1([14],:) = mean_es.ear.a.ERP_cond1([14],:) *-1;
% mean_es.ear.b.ERP_cond1([8 15],:) = mean_es.ear.b.ERP_cond1([8 15],:) *-1;
% mean_es.ear.c.ERP_cond1([],:) = mean_es.ear.c.ERP_cond1([],:) *-1;
% mean_es.ear.d.ERP_cond1([4 11],:) = mean_es.ear.d.ERP_cond1([4 11],:) *-1;

% mean_es.ear.N1.ERP_cond2([7],:) = mean_es.ear.N1.ERP_cond2([7],:) *-1;
% mean_es.ear.a.ERP_cond2([4 11],:) = mean_es.ear.a.ERP_cond2([4 11],:) *-1;
% mean_es.ear.b.ERP_cond2([14 7],:) = mean_es.ear.b.ERP_cond2([14 7],:) *-1;
% mean_es.ear.c.ERP_cond2([5 12],:) = mean_es.ear.c.ERP_cond2([5 12],:) *-1;
% mean_es.ear.d.ERP_cond2([15],:) = mean_es.ear.d.ERP_cond2([15],:) *-1;

%mean_es.ear.N1.ERP_cond1([3 10],:) = mean_es.ear.N1.ERP_cond1([3 10],:) *-1;


% for c = 3%1:length(con)
%     count = 1;
%     figure
%     for t = 5%:length(tasks)
%         if c == 1
%             x = 1;
%         else
%             x = 3;
%         end
%         
%         if t == 1
%             timevec = timeVecN1;
%         else
%             timevec = timeVec;
%         end
%         
% subplot(5,2,count)
% h = plot(timevec,mean_es.(tempc{c}).(tasks{t}).(vars{g(x)}));
% getc{c,t} = get(h,'Color');
% %ylim([-8 5])
% subplot(5,2,count+1)
% plot(timevec,mean_es.(tempc{c}).(tasks{t}).(vars{g(x+1)}))
% %ylim([ylimdown(t,c) ylimup(t,c)])
% 
% count = count + 2;
%     end 
% end
 
% m = 0.1;
% figure
% for co = 1:size(mean_es.cap.d.ERP_cond2,1)
% annotation('textbox',[m,0.9224-0.015,0.017708,0.045283],'String',num2str(co),'FontSize', 22, 'FontWeight', 'bold','Color',getc{3,5}{co,1});
% m = m + 0.045;
% end

%% testing area 2 (looking at effect sizes per subject)

% g = find(strcmp(vars,'es_matrix_mean'));
% tempc = {'cap','cap','ear'};
% ylimup = [5 10 6; 5 6 2; 2 4 4; 10 5 2; 10 10 5];
% ylimdown = [-8 -8 -2; -10 -5 -3; -4 -4 -4; -5 -5 -5; -10 -8 -5];

% for c = 1%1:length(con)
%     for t = 1:length(tasks)
%         figure
%         count = 1;
%         for s = 1:length(maxVar.(tempc{c}).(tasks{t}))
%             if ~isempty(maxVar.(tempc{c}).(tasks{t})(s).best_loc)
%                 if t == 1
%                     timevec = timeVecN1;
%                 else
%                     timevec = timeVec;
%                 end
%                 subplot(4,5,count)
%                 plot(timevec,maxVar.(tempc{c}).(tasks{t})(s).(vars{g}),'color','r')
%                 hold on
%                 plot(timevec,maxVar.(tempc{c}).(tasks{t})(s).(vars{g})(maxVar.(con{c}).(tasks{t})(s).best_loc(1),:),'color','b')
%                 plot(maxVar.(tempc{c}).(tasks{t})(s).maxLoc(maxVar.(con{c}).(tasks{t})(s).best_loc(1)),maxVar.(tempc{c}).(tasks{t})(s).maxVal(maxVar.(tempc{c}).(tasks{t})(s).best_loc(1)),'b*')
%                 xline(window_start(t))
%                 xline(window_stop(t))
% 
%                 count = count + 1;
%             end
%         end
%     end
% end

