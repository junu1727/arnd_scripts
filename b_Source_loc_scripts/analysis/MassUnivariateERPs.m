%% Analysis to check for time windows where the difference between two conditions is statistically significant

%% prepare data for the toolbox
%load .set-file with pre-processed data (including the bipolar channel expansion). Both conditions of one task, one
%subject. Call them best_condition1 & best_condition2.

for z = 1:size(best_condition1.data,3)
    %make new events for N1, a and d. Theirs got lost while pre-processing.
    %Could be that your scripting is differnt there, but in mine, these
    %events get lost in a and d. It is mostly important to remove any
    %spaces in the marker names (e.g. S 1 vs. S1)
    if taskNum == 1 || taskNum == 4
        best_condition1.event(z).type        = 'S2';
        best_condition1.event(z).edftype     = 8;
        best_condition1.event(z).edfplustype = [];
        best_condition1.event(z).latency     = z*1000;
        best_condition1.event(z).duration    = 0.1000;
        best_condition1.event(z).urevent     = z;
        best_condition1.event(z).epoch       = z;
        best_condition1.epoch(z).eventtype   = 'S2';
        
        best_condition1.epoch(z).event            = z;
        best_condition1.epoch(z).eventtype        = 'S2';
        best_condition1.epoch(z).eventedftype     = 6;
        best_condition1.epoch(z).eventedfplustype = [];
        best_condition1.epoch(z).eventlatency     = 0;
        best_condition1.epoch(z).eventduration    = 1;
        best_condition1.epoch(z).eventurevent     = z;
    else
        %erase all spaces in the marker names, the toolbox can't handle them
        best_condition1.epoch(z).eventtype = strrep(best_condition1.epoch(z).eventtype,' ','');
        best_condition1.event(z).type = strrep(best_condition1.event(z).type,' ','');
    end
end

%for both conditions
for z = 1:size(best_condition2.data,3) % for every trial, make a new event
    if taskNum == 1 || taskNum == 4
        best_condition2.event(z).type        = 'S1';
        best_condition2.event(z).edftype     = 8;
        best_condition2.event(z).edfplustype = [];
        best_condition2.event(z).latency     = z*1000;
        best_condition2.event(z).duration    = 0.1000;
        best_condition2.event(z).urevent     = z;
        best_condition2.event(z).epoch       = z;
        best_condition2.epoch(z).eventtype   = 'S1';
        
        best_condition2.epoch(z).event            = z;
        best_condition2.epoch(z).eventtype        = 'S1';
        best_condition2.epoch(z).eventedftype     = 6;
        best_condition2.epoch(z).eventedfplustype = [];
        best_condition2.epoch(z).eventlatency     = 0;
        best_condition2.epoch(z).eventduration    = 1;
        best_condition2.epoch(z).eventurevent     = z;
    else
        best_condition2.epoch(z).eventtype = strrep(best_condition2.epoch(z).eventtype,' ','');
        best_condition2.event(z).type = strrep(best_condition2.event(z).type,' ','');
    end
end

%give both conditions the same subjectID. The toolbox needs that to
%identify conditions belonging to one subject. For my data, that means
%removing the ending of the SubjectID (e.g. abcd1234_deviant/abcd1234_standard --> abcd1234/abcd1234)
naming = strrep(best_condition1.setname,'_sta','');
naming = strrep(naming,'_ica_cleaned','');
best_condition1.subject = naming;
best_condition2.subject = naming;

% delete all channels but the best and rename it to E01. We are only
% interested in the best channels per subject here.
best_condition1.data = best_condition1.data(BESTCHANNEL,:,:); %BESTCHANNEL is the index of the channel with highest effect size in your data
best_condition1.nbchan = 1;
best_condition1.chanlocs = best_condition1.chanlocs(BESTCHANNEL);
best_condition1.chanlocs(1).labels = 'E01';

best_condition2.data = best_condition2.data(BESTCHANNEL,:,:);
best_condition2.nbchan = 1;
best_condition2.chanlocs = best_condition2.chanlocs(BESTCHANNEL);
best_condition2.chanlocs(1).labels = 'E01';

%Data is now prepared. New data needs to be saved in a separate file.
%Stupid, because it's unnessecary data instead of just feeding the
%variables from the workspace to the toolbox-functions, but it only works this way.
best_condition1 = pop_saveset(best_condition1, [best_condition1.setname,'.set'], [MAINPATH,'\pre_bin\']);
best_condition2 = pop_saveset(best_condition2, [best_condition2.setname,'.set'], [MAINPATH,'\pre_bin\']);

%create a path for the output
if ~exist([MAINPATH,'\post_bin\',folderName,'\',pathcond])
    mkdir([MAINPATH,'\post_bin\',folderName,'\',pathcond])
end

%where the magic happens
cd(MAINPATH)
p_condition1 = bin_info2EEG([MAINPATH,'\pre_bin\',best_condition1.setname,'.set'],bintext,[MAINPATH,'\post_bin\',folderName,'\',pathcond,'\',best_condition1.setname],3);
p_condition2 = bin_info2EEG([MAINPATH,'\pre_bin\',best_condition2.setname,'.set'],bintext,[MAINPATH,'\post_bin\',folderName,'\',pathcond,'\',best_condition2.setname],3);
%the variable bintext is from a file that the toolbox needs. That file
%contains the names of the conditions (e.g. 1) S1=attended 2) S2=unattended
%for task a). I put the files for each task in your folder (Mass univariate
%ERPs).


cd([MAINPATH,'_p_data\post_bin\'])
%this one opens a GUI where you choose all saved files from one task.
%Choose, for example, all files from task b, regardless of whether it is
%the standard or deviant condition. The substructure best_condition1.subject
%tells the toolbox that these belong to one subject.
GND = sets2GND('gui','bsln',[-200 0],'verblevel',3);
%Group 1 and 2 in GND are standard and target condition, respectively. 
%This one creates a new group, being the difference between standard and target condition
GND = bin_dif(GND,1,2,'dev - sta'); 

%Now, we perform the actual test. 
GND = tmaxGND(GND,3,'time_wind',[-200 800],'verblevel',3,'plot_raster','no','save_GND','no'); %whithin-subject test for differences between conditions
grands{1,1} = GND.grands_t(:,:,3); % saves the t-values over time in a variable
crit_v{1,1} = [GND.t_tests.crit_t(1) GND.t_tests.crit_t(2)]; %saves the critical t-values
%plotting grands and crit_v as two lines should now give you the t-values over time plus
%the critical threshold. Whereever the graph exceeds the threshold, there
%is a statistically significant difference between the conditions. 










