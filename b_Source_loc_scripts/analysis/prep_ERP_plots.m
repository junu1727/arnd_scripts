%% calculate and compare the best electrodes between sets

%clear all; close all; clc

%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [ 50  50 130 250 400]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 200 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
% peak_start  = [130  150  180  400  440]; % start of the time window of interest per task. Should contain the desired effect
% peak_stop   = [150  180  200  440  480]; %   end of the time window of interest per task. Should contain the desired effect
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'N1';'a';'b';'c';'d'};
taskNames     = {'N100';'N100(att.)';'MMN';'P300';'N400'};
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
conditionString = {'cap','earc','ear'};
bestN = 66; %The bestN channels are stored for each setup (cap or ear)
sta_elec = [43 55 55; 43 55 55; 31 55 55; 5 55 55; 34 55 55]; %channel number of the standard electrode for all three conditions (cap, earc and ear)
fs = 100; %sampling rate
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

load([MAINPATH,'MeanES.mat'],'mean_es');
load([MAINPATH,'MaxVar.mat'],'maxVar');
load([MAINPATH,'_p_data\change.mat'],'chng_ear','chng_cap')
load([MAINPATH,'_p_data\mean_ERP.mat'],'mean_erp');
load([MAINPATH,'_p_data\p_data.mat'],'grands','crit_v');

eeglab
tempPath = dir([MAINPATH,SBJ{1},'\04_clean\*\*_a','*','ear.set']); %set 1 for the cleaned cap data
superCon = pop_loadset('filename', tempPath(1).name, 'filepath',tempPath(1).folder);
con = fieldnamesr(maxVar,1);

for s = 1:length(SBJ)
    
    load([MAINPATH,'quality_check.mat'],'q_check');
    getSBJ = contains({q_check.SBJ},SBJ{s});
    q_check = q_check(getSBJ);
    
    for taskNum = 1:length(taskLetter)
        
        q_task = taskNum; %counter for the tasks. there are 4 tasks, but task a has two runs, one for N1 from tone onset, one from all later tones. Therefore, we need an additional counter
        if taskNum > 1
            q_task = q_task - 1;
            special_N1 = 0;
        else
            special_N1 = 1;
        end
        
        count = 1;
        if q_check(q_task).badEarChan == 0
            
            for c = [1 1 3]

                if c == 1
                    isear = [];
                else 
                    isear = '_ear';
                end
                
                if taskLetter{taskNum} == 'N1'
                    naming1st = isear;
                elseif taskLetter{taskNum} == 'a'
                    naming1st = ['_unatt',isear];
                    naming2nd = ['_att',isear];
                elseif taskLetter{taskNum} == 'b' || taskLetter{taskNum} == 'c'
                    naming1st = ['_sta',isear];
                    naming2nd = ['_dev',isear];
                else taskLetter{taskNum} == 'd'
                    naming1st = ['_congr',isear];
                    naming2nd = ['_incongr',isear];
                end
                
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'*',naming1st,'.set']); %set 1 for the cleaned cap data
                con1path = con1path(1);
                if special_N1 == 0
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},naming2nd,'.set']); %set 2 for the cleaned cap data
                    con2path = con2path(1);
                end
                
                if count == 1
                    FullorEar = 1; % 1 = full, 2 = ear
                    pathcond = 'sta';
                    chng = chng_cap;
                elseif count == 2
                    FullorEar = 1; % 1 = full, 2 = ear
                    pathcond = 'cap';
                    subtr = 96; %to also have the standard channel, maxVar includes the original 96 channels. The best_loc, however, is looked up without those in best_condition1/2. Therefore, subtract from the index the number of channels
                    chng = chng_cap;
                else
                    FullorEar = 2; % 1 = full, 2 = ear
                    pathcond = 'ear';
                    subtr = 0;
                    chng = chng_ear;
                end
                
                % get folder names
                if taskLetter{taskNum} == 'N1'
                    folderName = 'a_attended_speaker';
                    bintext = 'bin_N1.txt';
                elseif taskLetter{taskNum} == 'a'
                    folderName = 'a_attended_speaker';
                    bintext = 'bin_a.txt';
                elseif taskLetter{taskNum} == 'b'
                    bintext = 'bin_b.txt';
                    folderName = 'b_passive_oddball';
                elseif taskLetter{taskNum} == 'c'
                    bintext = 'bin_c.txt';
                    folderName = 'c_active_oddball';
                elseif taskLetter{taskNum} == 'd'
                    bintext = 'bin_d.txt';
                    folderName = 'd_conguency';
                else
                    error('please enter a valid taskLetter (N1, a-d)')
                end
                
                PATHIN  = con1path.folder;
                PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
                if ~exist (PATHOUT)
                    mkdir(PATHOUT)
                end
                
                %load data: 2 conditions, smooth both
                condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                condition1.data = smoothdata(condition1.data ,2,'movmean',4);
                
                if taskNum ~= 1
                    condition2 = pop_loadset('filename',  con2path.name, 'filepath',PATHIN);
                    condition2.data = smoothdata(condition2.data ,2,'movmean',4);
                else
                    timeWin = find(condition1.times== window_start(taskNum)):find(condition1.times== window_stop(taskNum));
                    condition2 = condition1; %for ERP 1, we need to extract the baseline and the ERP individually
                    condition2.data = condition2.data(:,timeWin,:);
                    condition2.times = condition2.times(1:21);
                    condition2.pnts = 21;
                    
                    condition1.data = condition1.data(:,1:21,:);
                    condition1.times = condition1.times(1:21);
                    condition1.pnts = 21;      
                end

                if count == 1 %sta-scalp-condition
                    best_condition1 = condition1;
                    best_condition2 = condition2;
                    if taskNum ~= 1
                        mean_erp(s,:,taskNum,count) =  mean(best_condition2.data(sta_elec(taskNum,1),:,:),3);
                    else
                        mean_erp(s,:,taskNum,count) = zeros(1,100);
                        mean_erp(s,timeWin,taskNum,count) =  mean(best_condition2.data(sta_elec(taskNum,1),:,:),3);
                    end
                else %ind.-scalp or ear
                    best_condition1 = mbl_bipolarchannelexpansion(condition1,0,FullorEar);
                    best_condition2 = mbl_bipolarchannelexpansion(condition2,0,FullorEar);
                    if any(s==chng{taskNum}) %change sign of data where needed
                        best_condition1.data = best_condition1.data *-1;
                        best_condition2.data = best_condition2.data *-1;
                    end
                    if taskNum ~= 1
                        mean_erp(s,:,taskNum,count) = mean(best_condition2.data(maxVar.(con{c}).(taskLetter{taskNum})(s).best_loc(1)-subtr,:,:),3);
                    else
                        mean_erp(s,:,taskNum,count) = zeros(1,100);
                        mean_erp(s,timeWin,taskNum,count) = mean(best_condition2.data(maxVar.(con{c}).(taskLetter{taskNum})(s).best_loc(1)-subtr,:,:),3);
                    end
                end
                
                for z = 1:size(best_condition1.data,3)
                    %make new events for N1, a and d. Theirs got lost while pre-processing
                    if taskNum == 1 || taskNum == 2 || taskNum == 5
                         best_condition1.event(z).type        = 'S2';
                         best_condition1.event(z).edftype     = 8;
                         best_condition1.event(z).edfplustype = [];
                         best_condition1.event(z).latency     = z*1000;
                         best_condition1.event(z).duration    = 0.1000;
                         best_condition1.event(z).urevent     = z;
                         best_condition1.event(z).epoch       = z;
                         best_condition1.epoch(z).eventtype   = 'S2';
                         
                         best_condition1.epoch(z).event            = z;
                         best_condition1.epoch(z).eventtype        = 'S2';
                         best_condition1.epoch(z).eventedftype     = 6;
                         best_condition1.epoch(z).eventedfplustype = [];
                         best_condition1.epoch(z).eventlatency     = 0;
                         best_condition1.epoch(z).eventduration    = 1;
                         best_condition1.epoch(z).eventurevent     = z;
                     else
                    best_condition1.epoch(z).eventtype = strrep(best_condition1.epoch(z).eventtype,' ','');
                    best_condition1.event(z).type = strrep(best_condition1.event(z).type,' ','');
                     end
                end
                
                for z = 1:size(best_condition2.data,3)
                    if taskNum == 1 || taskNum == 2 || taskNum == 5
                        best_condition2.event(z).type        = 'S1';
                        best_condition2.event(z).edftype     = 8;
                        best_condition2.event(z).edfplustype = [];
                        best_condition2.event(z).latency     = z*1000;
                        best_condition2.event(z).duration    = 0.1000;
                        best_condition2.event(z).urevent     = z;
                        best_condition2.event(z).epoch       = z;
                        best_condition2.epoch(z).eventtype   = 'S1';
                        
                        best_condition2.epoch(z).event            = z;
                        best_condition2.epoch(z).eventtype        = 'S1';
                        best_condition2.epoch(z).eventedftype     = 6;
                        best_condition2.epoch(z).eventedfplustype = [];
                        best_condition2.epoch(z).eventlatency     = 0;
                        best_condition2.epoch(z).eventduration    = 1;
                        best_condition2.epoch(z).eventurevent     = z;
                    else
                        best_condition2.epoch(z).eventtype = strrep(best_condition2.epoch(z).eventtype,' ','');
                        best_condition2.event(z).type = strrep(best_condition2.event(z).type,' ','');
                    end
                end
                
                %give both conditions the same subjectID. The toolbox needs that
                naming = strrep(best_condition1.setname,'_sta','');
                naming = strrep(naming,'_ica_cleaned','');
                best_condition1.subject = naming;
                best_condition2.subject = naming;
                if taskNum == 1 %special N1, both conditions have the same name initially
                best_condition1.setname = [best_condition1.setname(1:6),'_',taskLetter{taskNum},'_bsline'];
                best_condition2.setname = [best_condition1.setname(1:6),'_',taskLetter{taskNum},'_target'];
                end
                
                % delete all channels but the best and rename it to E01
                if count == 1
                    best_condition1.data = best_condition1.data(sta_elec(taskNum,1),:,:);
                    best_condition1.nbchan = 1;
                    best_condition1.chanlocs = best_condition1.chanlocs(sta_elec(taskNum,1));
                    best_condition1.chanlocs(1).labels = 'E01';
                    
                    best_condition2.data = best_condition2.data(sta_elec(taskNum,1),:,:);
                    best_condition2.nbchan = 1;
                    best_condition2.chanlocs = best_condition2.chanlocs(sta_elec(taskNum,1));
                    best_condition2.chanlocs(1).labels = 'E01';
                elseif count == 2 || count == 3
                    best_condition1.data = best_condition1.data(maxVar.(con{c}).(taskLetter{taskNum})(s).best_loc(1)-subtr,:,:);
                    best_condition1.nbchan = 1;
                    best_condition1.chanlocs = best_condition1.chanlocs(maxVar.(con{c}).(taskLetter{taskNum})(s).best_loc(1)-subtr);
                    best_condition1.chanlocs(1).labels = 'E01';
                    
                    best_condition2.data = best_condition2.data(maxVar.(con{c}).(taskLetter{taskNum})(s).best_loc(1)-subtr,:,:);
                    best_condition2.nbchan = 1;
                    best_condition2.chanlocs = best_condition2.chanlocs(maxVar.(con{c}).(taskLetter{taskNum})(s).best_loc(1)-subtr);
                    best_condition2.chanlocs(1).labels = 'E01';
                end
                
                if c == 1 %only scalp data has had an ICA
                    best_condition1.icaact =  best_condition1.icaact(1,:,:);
                    best_condition1.icawinv = best_condition1.icawinv(1);
                    best_condition1.icasphere =  best_condition1.icaact(1);
                    best_condition1.icaweights =  best_condition1.icaweights(:,1);
                    best_condition1.icachansind = best_condition1.icachansind(1);
                    
                    best_condition2.icaact =  best_condition2.icaact(1,:,:);
                    best_condition2.icawinv = best_condition2.icawinv(1);
                    best_condition2.icasphere =  best_condition2.icaact(1);
                    best_condition2.icaweights =  best_condition2.icaweights(:,1);
                    best_condition2.icachansind = best_condition2.icachansind(1);
                end
                                
                best_condition1 = pop_saveset(best_condition1, [best_condition1.setname,'.set'], [MAINPATH,'_p_data\pre_bin\']);
                best_condition2 = pop_saveset(best_condition2, [best_condition2.setname,'.set'], [MAINPATH,'_p_data\pre_bin\']);
                
                if taskNum == 1
                    folderName = 'a_N1';
                end
                if ~exist([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond])
                    mkdir([MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond])
                end
                
                cd(MAINPATH)
                p_condition1 = bin_info2EEG([MAINPATH,'_p_data\pre_bin\',best_condition1.setname,'.set'],bintext,[MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\',best_condition1.setname],3);
                p_condition2 = bin_info2EEG([MAINPATH,'_p_data\pre_bin\',best_condition2.setname,'.set'],bintext,[MAINPATH,'_p_data\post_bin\',folderName,'\',pathcond,'\',best_condition2.setname],3);
                
                count = count + 1;

            end
        end
    end
end
save([MAINPATH,'_p_data\mean_ERP.mat'],'mean_erp');

cd([MAINPATH,'_p_data\post_bin\'])
GND = sets2GND('gui','bsln',[-200 0],'verblevel',3);
GND = bin_dif(GND,1,2,'dev - sta');

GND = tmaxGND(GND,3,'time_wind',[-200 800],'verblevel',3,'plot_raster','no','save_GND','no');
% grands{1,1} = GND.grands_t(:,:,3);
% crit_v{1,1} = [GND.t_tests.crit_t(1) GND.t_tests.crit_t(2)];


% temp1 = grands{1,1};
% temp2 = grands{2,1};
% temp3 = grands{3,1};
% 
% grands{1,1} = zeros(1,100);
% grands{2,1} = zeros(1,100);
% grands{3,1} = zeros(1,100);
% 
% grands{1,1}(26:46) = temp1;
% grands{2,1}(26:46) = temp2;
% grands{3,1}(26:46) = temp3;
% 
% save([MAINPATH,'_p_data\p_data.mat'],'grands','crit_v');
        
%% load the GND-variables
stadir = dir([MAINPATH,'_p_data\post_bin\*sta.GND']);
capdir = dir([MAINPATH,'_p_data\post_bin\*cap.GND']);
eardir = dir([MAINPATH,'_p_data\post_bin\*ear.GND']);
alldir = [stadir capdir eardir]';
for a = 1:size(alldir,1)
    for b = 1:size(alldir,2)
        load([alldir(a,b).folder,'\',alldir(a,b).name], '-MAT')
        GND = bin_dif(GND,1,2,'dev - sta');
        GND = tmaxGND(GND,3,'time_wind',[-200 800],'verblevel',1,'plot_raster','no');
        %3 conditions * 5 tasks
        grands{a,b} = GND.grands_t(:,:,3);
        crit_v{a,b} = [GND.t_tests.crit_t(1) GND.t_tests.crit_t(2)];
    end
end

temp1 = grands{1,1};
temp2 = grands{2,1};
temp3 = grands{3,1};

grands{1,1} = zeros(1,100);
grands{2,1} = zeros(1,100);
grands{3,1} = zeros(1,100);

grands{1,1}(26:46) = temp1;
grands{2,1}(26:46) = temp2;
grands{3,1}(26:46) = temp3;

save([MAINPATH,'_p_data\p_data.mat'],'grands','crit_v');

Colors = {'cyan','blue','red'};
names = {'sta_chan','cap','ear'};

pfig = figure;
counter = 1;
for td = 1:length(taskLetter)
    subplot(1,5,counter)
    for sd = 1:3
        h(sd) = plot(timeVec,squeeze(grands{sd,td}), 'color', Colors{sd}, 'LineWidth', 2);
        hold on
        yline(crit_v{sd,td}(1), 'k', 'LineWidth', 2, 'color', Colors{sd})
        yline(crit_v{sd,td}(2), 'k', 'LineWidth', 2, 'color', Colors{sd})
        title(taskNames{counter})
        ylim([-9 9.2])
        if counter == 1
            xlim([50 250])
        else
            xlim([-200 800])
        end
    end
    title(taskNames{counter},'FontSize', 14, 'FontWeight', 'bold')
    if counter == 1
        ylabel('t-score','FontSize', 12, 'FontWeight', 'bold')
        xlabel('Time [ms]','FontSize', 12, 'FontWeight', 'bold')
        h = zeros(3, 1);
        h(1) = plot(NaN,NaN,Colors{1});
        h(2) = plot(NaN,NaN,Colors{2});
        h(3) = plot(NaN,NaN,Colors{3});
        legend(h, 'red','blue','black');
        legend(h,'Scalp (sta.)','Scalp (ind.)','Ear','FontSize', 10, 'FontWeight', 'bold')
    end
    ax = gca;
    ax.FontSize = 8;
    ax.FontWeight = 'bold';
    counter = counter +1;
end
set(gcf,'Color','w')
cd('O:\arm_testing\Experimente\cEEGrid_vs_Cap\')
print(pfig,'C:\Users\arnd\Pictures\pfig','-dpng')

%% plot ERPs, flip signs if necessary (always compare to sta-cond., this channel cannot be fliiped)
%mean_erp: sbj*data*task*cond

% taskNum = 1;
% caporear = 2; %2 = cap, 3 = ear
% figure
% counter = 1;
% for sd = [1 caporear]
%     subplot(2,1,counter)
%     
%     colors = jet;
%     colors = colors(1:3:57,:);
%     for x = 1:length(colors)
%         plot(timeVec,mean_erp(x,:,taskNum,sd),'Color',colors(x,:),'LineWidth',2.0)
%         hold all
%     end
%     plot(timeVec,mean(mean_erp(mean_erp(:,30,taskNum,sd) ~= 0,:,taskNum,sd),1),'Color','k','LineWidth',2.0)
%     counter = counter +1;
%     if sd == 1
%     legend
%     end
% end
% 
% mean_erp([],:,taskNum,2) = mean_erp([],:,taskNum,2) * -1;
% 
% chng_cap{1} = [];
% chng_cap{2} = [4 6 7 17 19];
% chng_cap{3} = [2 7 11 12];
% chng_cap{4} = [2 6 11 15];
% chng_cap{5} = [1 3 4 5 8 9 12];
% 
% mean_erp([],:,taskNum,3) = mean_erp([],:,taskNum,3) * -1;
% 
% chng_ear{1} = [6 8 12 13 17 18 19];
% chng_ear{2} = [2 7 12 13 14 16 17 18];
% chng_ear{3} = [3 6 10 12 17];
% chng_ear{4} = [1 2 3 4 12 14 16 17];
% chng_ear{5} = [3 4 6 12];
% 
% save([MAINPATH,'_p_data\change.mat'],'chng_ear','chng_cap');


%% testing area
% figure
% for x = 1:size(mean_es.cap.c.ERP_stacond2,1)
%     if mean_erp(x,1,taskNum,1) ~= 0
%         plot(timeVec,mean_es.cap.c.ERP_stacond2(x,:)  ,'Color',colors(x,:))
%     end
%     hold all
% end
% plot(timeVec,mean(mean_es.cap.c.ERP_stacond2,1),'Color','k','LineWidth',2.0)
% legend

% 
% figure
% for x = 1:length(colors)
%     if mean_erp(x,1,taskNum,1) ~= 0
%         plot(timeVec,mean_erp(mean_erp(:,1,taskNum,2) ~= 0,:,taskNum,1),'Color',colors(x,:))
%     end
%     hold all
% end
% legend
% 
% figure
% for x = 1:length(colors)
%     if mean_erp(x,1,taskNum,1) ~= 0
%         plot(timeVec,mean_erp(mean_erp(:,1,taskNum,3) ~= 0,:,taskNum,1),'Color',colors(x,:))
%     end
%     hold all
% end
% legend