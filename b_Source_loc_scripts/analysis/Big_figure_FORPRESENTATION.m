
%% settings
timeVec = linspace(-200,790,100);
tasktitle = {'(n=15)','(n=15)','(n=16)','(n=15)','(n=16)'};
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };

MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
FIGPATH = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\';
CIPATH = 'C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)';
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

load([MAINPATH,'quality_check.mat'],'q_check');
load([MAINPATH,'MaxVar.mat'],'maxVar');
load([MAINPATH,'MeanES.mat'],'mean_es');
load([MAINPATH,'MeanES.mat'],'hedgesG_mean');
load([MAINPATH,'MeanES.mat'],'hedgesG_std');
load([MAINPATH,'MeanES.mat'],'hedgesG_GND_AVG');

%% Plot 1: GND AVG ERPs for the standard channels

con = fieldnamesr(maxVar,1);
tasks = fieldnamesr(maxVar.cap,1);

%% plot
% pure_red_0_5 = [236,168,139]/255;
% pure_blue_0_5 = [127,184,222]/255;
pure_green =[0.4660 0.6740 0.1880];
pure_red = [0.8500, 0.3250, 0.0980];
pure_blue = [0,0.4470,0.7410];
pure_yellow = [0.9290 0.6940 0.1250];
Shades = [pure_blue;pure_blue;pure_red;pure_yellow;pure_green];
transparency = 0.5;
cd('C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)');

taskLegend1 = {'N1','att','target','target','congruent',};
taskLegend2 = {'N1','unatt','standard','standard','incongruent',};
ylims = [-7 3; -1.8 1.6; -1.8 0.7; -2.2 5; -3.5 3];
vars = fieldnamesr(mean_es.cap.N1,1);
% vars{6} = 'es_matrix_sta';

%%
figpath = dir([FIGPATH,'*.jpg']);
cd(FIGPATH)

movepic = [-0.015 -0.0125 -0.0145 -0.0165 -0.0185];
moveplotx = [0 -0.06 -0.12];
moveploty = [0.01 0.005 0 -0.005 -0.01];

count = 1;
bigfigure = figure('DefaultAxesFontWeight','bold','DefaultAxesFontSize',10);
count2 = 0;
for t = 2:length(tasks)
    imr = imread(figpath(t).name);
    ax0  = subplot(4,5,t-1+count2);
    imshow(imr);
    title(tasktitle{t},'FontSize', 14, 'FontWeight', 'bold')
    ax0.Position(2) = ax0.Position(2) + movepic(t); %plot is moved for better spacing
    ax0.OuterPosition(3:4) = ax0.OuterPosition(3:4) + 0.024;
    count = count+1;
    count2 = count2 + 4;
end

cd(CIPATH);
colorc = 1;
count2 = 2;
for t = 2:length(tasks)
    count = 0;
    count3 = 1;
    bc = 1;
    for c = [1 1 3] %once the cap (standard channel), cap again (ind. channel), then ear, (ind. channel)
        
        ax = subplot(4,5,count2+count);
        
        ax.Position(3) = ax.Position(3)/2; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
        timeVec =  linspace(-200,790,100);
        ax.Position(1) = ax.Position(1) + moveplotx(count3) ; %plot is moved for better spacing
        ax.Position(2) = ax.Position(2) + moveploty(t) ; %plot is moved for better spacing
        count3 = count3+1;
        
        
        if bc == 1
            v1 = find(strcmp(vars,'ERP_stacond1'));
            v2 = find(strcmp(vars,'ERP_stacond2'));
        elseif bc == 2 || bc == 3
            v1 = find(strcmp(vars,'ERP_cond1'));
            v2 = find(strcmp(vars,'ERP_cond2'));
        else
        end
        
                %cond1 (attended, inconcgr., etc.) - spread
                ci1 = ciplot(mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1) - std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),...
                    mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1) + std(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),...
                    timeVec,[50 50 50]/255,transparency);
                ci1.EdgeColor = 'none';
        
                hold on
        
                %cond2 (unattended, concgr., etc.) - spread
                ci2 = ciplot(mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) - std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),...
                    mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1) + std(mean_es.(con{c}).(tasks{t}).(vars{v2}),1),...
                    timeVec,[17 17 17]/255,transparency);
                ci2.EdgeColor = 'none';
        
        %cond2 (attended, inconcgr., etc.)  - mean
        plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v2}),1), 'color','k','LineWidth',3)
        hold on
        %cond1 (unattended, concgr., etc.)  - mean
        plot(timeVec,mean(mean_es.(con{c}).(tasks{t}).(vars{v1}),1),':','color','k','LineWidth',3)
        
        ylim(ylims(t,:))
        if t == 1
            xlim([50 250])
        else
            xlim([-200 800])
        end
        
        xticks(-200:200:800)
        xticklabels({'','0','','400','','800'})
        axx = gca;
        axx.FontSize = 10;
        
        if count == 0 && t == 2
            ylabel('Amplitude [�V]','FontSize', 14, 'FontWeight', 'bold')
            xlabel('Time [ms]','FontSize', 14, 'FontWeight', 'bold')
        end
        
        box off
        
        count = count +1;
        bc = bc+1;
    end
    count2 = count2 + 5;
end

LineStyles = {'-','--'};
Colors2 = {'cyan','blue','red','red'};
Shades2 = [pure_blue;pure_blue;pure_red;pure_yellow;pure_green];
count = 5;
for t = 2:length(tasks)
    
    ax2 = subplot(4,5,count);
    
    ax2.Position(3) = ax2.Position(3)/2; %length of the 1s-epoch (1000ms) divided by 5 to match the length of the N1-epoch (200ms)
    timeVec =  linspace(-200,790,100);
    ax2.Position(1) = ax2.Position(1) - 0.17; %plot is moved for better spacing
    ax2.Position(2) = ax2.Position(2) + moveploty(t); %plot is moved for better spacing
    ax2.FontSize = 20;
    
    for c = [1 2 4]
        ci1 = ciplot(hedgesG_mean{c,t} - hedgesG_std{c,t},hedgesG_mean{c,t} + hedgesG_std{c,t},timeVec,Shades2(c,:),transparency);
        ci1.EdgeColor = 'none';
        hold on
        plot(timeVec,hedgesG_mean{c,t},'color',Colors2{c},'LineWidth',3,'LineStyle',LineStyles{1})
    end
    
 
    xticks(-200:200:800)
    xticklabels({'','0','','400','','800'})
    axx = gca;
    axx.FontSize = 10;
        
    %change the order of the lines in the plot, so that the actual
    %lines are always in front of the shadings
    h = get(gca,'Children');
    set(gca,'Children',[h(1) h(3) h(5) h(6) h(4) h(2)])
    
    if t == 2
        ylabel('Hedges� g [abs.]','FontSize', 14, 'FontWeight', 'bold')
    end
    xlim([-200 800])
    box off
    pos(count,:) = get(gca,'Position');
    count = count +5;
    
end
%     sgtitle('EEG-signal and effect sizes, Grand Average (N=20)','FontSize', 18, 'FontWeight', 'bold')
set(gcf,'Color','w')
set(gcf, 'Position', get(0, 'Screensize'));

% annotation('textbox',[0.11708,0.9224-0.015,0.017708,0.045283],'String','A','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.11708,0.7799-0.015,0.017708,0.045283],'String','B','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.11708,0.6374-0.015,0.017708,0.045283],'String','C','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.11708,0.4949-0.015,0.017708,0.045283],'String','D','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.11708,0.3524-0.045,0.017708,0.045283],'String','E','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');

% subval = 0.163;
% annotation('textbox',[0.1,         0.05,0.017708,0.045283],'String','A','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.1+subval,  0.05,0.017708,0.045283],'String','B','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.1+subval*2,0.05,0.017708,0.045283],'String','C','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.1+subval*3,0.05,0.017708,0.045283],'String','D','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');
% annotation('textbox',[0.1+subval*4,0.05,0.017708,0.045283],'String','E','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');

% annotation('textbox',[0.11708,0.2100-0.045,0.017708,0.045283],'String','F','FontSize', 22, 'FontWeight', 'bold','LineStyle','none');

%     annotation('textarrow',[0.11708,0.9224-0.015,0.017708,0.045283],'String','Cap (sta.)','FontSize', 15, 'FontWeight', 'bold','LineStyle','none', 'TextRotation',90);
%     annotation('textarrow',[0.11708,0.7799-0.015,0.017708,0.045283],'String','Cap (ind.)','FontSize', 15, 'FontWeight', 'bold','LineStyle','none', 'TextRotation',90);
%     annotation('textarrow',[0.11708,0.6374-0.015,0.017708,0.045283],'String','Ear (ind.)','FontSize', 15, 'FontWeight', 'bold','LineStyle','none', 'TextRotation',90);
%     annotation('textarrow',[0.11708,0.4949-0.015,0.017708,0.045283],'String','','FontSize', 15, 'FontWeight', 'bold','LineStyle','none', 'TextRotation',90);
%     annotation('textarrow',[0.11708,0.3524-0.015,0.017708,0.045283],'String','','FontSize', 15, 'FontWeight', 'bold','LineStyle','none', 'TextRotation',90);
%
%     ht = text(0.11708,0.9224-0.015,'My text');
%     set(ht,'Rotation',45)

set(gcf, 'renderer', 'painter');
cd('O:\arm_testing\Experimente\cEEGrid_vs_Cap\')
print(bigfigure,'C:\Users\arnd\Pictures\Bigfigure_PRESENTATIONS','-dpng')


%     lgd = legend('Control condition','Target condition','Difference wave','FontSize', 10, 'FontWeight', 'bold');
%     lgd.Position = [0.9075,0.16,0.0762,0.057];
%     legend('cap (sta.)','cap (ind.)','cap (GND AVG)','ear (ind.)','ear (GND AVG)','FontSize', 10, 'FontWeight', 'bold','Position',[0.9075,0.47,0.0762,0.057])
%     legend('boxoff')