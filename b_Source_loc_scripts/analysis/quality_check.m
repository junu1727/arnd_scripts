%% DIRECTORIES

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
       ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
       ,'fuhi52','okum71','kuma55','pigo79'
       };
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
HP = 1;         % cut-off frequency high-pass filter [Hz] only for ICA
LP = 30;        % cut-off frequency low-pass filter [Hz] only for ICA
SRATE = 250;    % downsample data for ICA
PRUNE = 3;      % artifact rejection threshold in SD (for ICA only)
counter = 1;
naming = {'_a','_b','_c','_d'};

%load and update quality check
load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\_supplementary\quality_check.mat','q_check');

for s = 3:length(SBJ)
    % set some paths
    if exist(fullfile(MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep)) > 0
        PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep); % path containing rawdata
        
        list=dir([PATHIN,'*.vhdr']); %reads all .vhdr files in that path
        [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
        
        for t = 1:length(list)
            
            % Loop over subjects
            EEG = pop_biosig([MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep,list(t).name]);
            [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off');
            
            EEG = pop_chanedit(EEG, 'load',{['O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\',SBJ{s},'\\',SBJ{s},'_elec.xyz'],'filetype','xyz'});
            
            chans = {EEG.chanlocs.labels};
            
            startlat = find( strcmp({EEG.event.type},{'S  7'}));%index of start latency
            if isempty(startlat) %if baseline is shorter than 60s...
                EEG = eeg_eegrej(EEG, [1 EEG.event(2).latency - EEG.srate]); % remove baseline data (first real marker - 1s)
            else
                EEG = eeg_eegrej(EEG, [1 EEG.event(startlat(end)+1).latency - EEG.srate]); % remove baseline data (first real marker - 1s)
            end
            
            
            if t == 1
                % load in log files to identify stimuli (only used to get number of stimuli to calc number of correct answers)
                listLog = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\log\*N1*.log']);
                logs  = loadtxt( [listLog(t).folder,'\',listLog(t).name], 'blankcell', 'off' );
                sounds = find(strcmp('Sound',logs(:,3)));
                logs = logs(sounds,[3 4 5]);
                logs(1:10,:) = [];
                
                corr_decisions00 = numel(find(strcmp('condition 8',{EEG.event.type}))); % correctly identified alternating tone (weird naming)
                corr_decisions0 = numel(find(strcmp('condition 10',{EEG.event.type}))); % correctly identified alternating tone (weird naming)
                corr_decisions1 = numel(find(strcmp('condition 11',{EEG.event.type}))); % correctly identified alternating tone
                corr_decisions2 = numel(find(strcmp('S 13',{EEG.event.type}))); % correctly identified ascending tone
                corr_decisions3 = numel(find(strcmp('S 14',{EEG.event.type}))); % correctly identified descending tone
                corr_decision_sum = corr_decisions00 + corr_decisions0 + corr_decisions1 + corr_decisions2 + corr_decisions3;

                corr_decisions_a = 100/length(logs) *corr_decision_sum %percentage of correct identification of asc., des. and alt. tones
                               
                dec_left  = numel(find(strcmp('S  1',{EEG.event.type})))
                dec_right = numel(find(strcmp('S  2',{EEG.event.type})))
                lr_a = 100/(dec_left+dec_right)*dec_left
            elseif t == 4
                
                %check percentage of correct decisions
                corr_decisions0 = numel(find(strcmp('condition 7',{EEG.event.type})));
                corr_decisions1 = numel(find(strcmp('condition 6',{EEG.event.type})));
                corr_decisions2 = numel(find(strcmp('S 11',{EEG.event.type})));
                num_decisions = numel(find(strcmp('S  9',{EEG.event.type})));
                corr_decision_sum = corr_decisions0 + corr_decisions1 + corr_decisions2
                corr_decisions_d = 100/num_decisions *corr_decision_sum
                
            end
            
            EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
            EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
            EEG = pop_resample(EEG, SRATE);
            
            lengthchan = EEG.nbchan;
            x=1;
            loc = [];
            for g = 1 : lengthchan
                str = 'E';
                if g <10
                    num = sprintf( '%02d', g ) ;
                else
                    num = num2str(g);
                end
                strapp = append(str,num);
                EEG.chanlocs(g).labels = strapp;
                
            end
            
            EEG = eeg_checkset( EEG );
            EEGold = EEG;
            
            EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion',5,...
                'ChannelCriterion',0.8,'LineNoiseCriterion',4,...
                'Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,...
                'BurstRejection','on','Distance','Euclidian',...
                'WindowCriterionTolerances',[-Inf 7] );

            if isfield(EEG.etc, 'clean_channel_mask')
                chans = chans(EEG.etc.clean_channel_mask == 0);
                remChans = chans;
                if isempty(intersect(chans,{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'}))
                    badEarChan = 0;
                else
                    badEarChan = 1;
                end
            else
                badEarChan = 0;
                remChans = [];
            end
            
            q_check(end+1).SBJ      = [SBJ{s},naming{t}];
            q_check(end).badEarChan = badEarChan;
            q_check(end).remChans   = remChans;
            q_check(end).a_corrDec  = corr_decisions_a;
            q_check(end).lr_a       = lr_a;
            if t ==4
                q_check(end).d_corrDec  = corr_decisions_d;
            end
            
            counter = counter + 1;
        end
        save([MAINPATH,'quality_check.mat'],'q_check');
    end
end

for d = 1:length(q_check)
    if any(strcmp(q_check(d).remChans,{'E029'})) || any(strcmp(q_check(d).remChans,{'E030'}))
       eye1 = find(strcmp(q_check(d).remChans,{'E029'}));
       eye2 = find(strcmp(q_check(d).remChans,{'E030'}));
       q_check(d).remChans([eye1 eye2]) = [];                
    end
end

      save([MAINPATH,'quality_check.mat'],'q_check');






