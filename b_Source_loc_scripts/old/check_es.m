timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);

for s = 1:19
    if ~isempty(maxVar.ear.d(s).es_matrix_mean)
figure
plot(timevec,maxVar.ear.d(s).es_matrix_mean(maxVar.ear.d(s).best_loc(1:66),:),'color','r')
hold on
plot(timevec,maxVar.ear.d(s).es_matrix_mean(maxVar.ear.d(s).best_loc(1),:),'color','b')
xline(window_start(t))
xline(window_stop(t))
    end
end


for s = 1:19
    if ~isempty(maxVar.cap.N1(s).es_matrix_sta)
figure
plot(timeVecN1,maxVar.cap.N1(s).es_matrix_sta,'color','r')
xline(window_start(t))
xline(window_stop(t))
    end
end

figure
for s = 18
    if ~isempty(maxVar.cap.a(s).es_matrix_sta)
plot(timeVec,maxVar.cap.a(s).ERP_stacond2)
hold on
    end
end

test = [maxVar.cap.a.ERP_stacond2];
test = reshape(test,15,100);

plot(timeVec,mean(test,1))
xline(window_start(t))
xline(window_stop(t))