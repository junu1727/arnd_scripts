clear;
close all;
clc;

MAINPATH = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\pigo79\';
%MAINPATH = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\template_subject\';


% i = 3:9;
% t = 1:40*length(i);
% g = [0 40 80 120 160 200 240];
% for s = 1:length(i)
%     xdir = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\s01\MRI raw\DICOM\1\2\',num2str(i(s)),'\']);
%     xdir(1:2) = [];
%     
%     for f = 1:length(xdir)
%         load()
%         xdir(f).name = num2str(t(f)+g(s))
%     end
%     save()
% end

dicoms = dir([MAINPATH,'M*']);

for sbj = 1:length(dicoms)
    
    disp(['importing ', dicoms(sbj).name]);
    
    %% get into the DICOM Folder Structure
    dicompath = [MAINPATH, dicoms(sbj).name, filesep, 'DICOM', filesep];
        
    PATHINtemp = dir(dicompath);
    PATHINtemp = setdiff({PATHINtemp.name}, {'.', '..'});
    PATHINtemp = PATHINtemp(end);
    if isempty(PATHINtemp)
        disp('no DICOM found in path');
        %continue
    elseif length(PATHINtemp)>1
        disp('DICOM seem to be imported already. Skipping...');
        %continue
    end
    
    PATHIN = [dicompath, PATHINtemp{1}, '\'];
    
    PATHINtemp = dir(PATHIN);
    PATHINtemp = setdiff({PATHINtemp.name}, {'.', '..'});
    
    for k = 1:length(PATHINtemp)
        
        PATHIN2 = [PATHIN, PATHINtemp{k}, '\'];
        
        PATHIN2temp = dir(PATHIN2);
        PATHIN2temp = setdiff({PATHIN2temp.name}, {'.', '..'});
        
        for j = 1:length(PATHIN2temp)
            
            PATHIN3 = [PATHIN2, PATHIN2temp{j}, filesep];
            dicoms = dir(PATHIN3);
            dicoms = dicoms(arrayfun(@(x) x.name(1), dicoms) ~= '.'); % L�scht . und .. Dateien
            
            for i = 1:length(dicoms)
                X = dicominfo([PATHIN3 dicoms(i,1).name]);
                dicomorig = sprintf('%s_%04d_%06d' ,  X.SeriesInstanceUID, X.SeriesNumber, X.InstanceNumber) ;
                
                movefile([PATHIN3 dicoms(i,1).name],[dicompath filesep dicomorig]);
            end
            
        end
        
    end
    
end