%% active oddball

x=dev.F(72,:) - (dev.F(92,:));
y=sta.F(72,:) - (sta.F(92,:));
figure
plot(dev.Time,x)
hold on
plot(sta.Time,y)

x=dev.F(1,:) - (dev.F(92,:));
y=sta.F(1,:) - (sta.F(92,:));

figure
plot(dev.Time,x)
hold on
plot(sta.Time,y)


figure
for st = 1:25
subplot(5,5,st)
plot(dev.Time,dev.F(st,:))
hold on
plot(sta.Time,sta.F(st,:))
end

%% passive oddball

x=dev_pass.F(72,:) - (dev_pass.F(92,:));
y=sta_pass.F(72,:) - (sta_pass.F(92,:));

figure
plot(dev_pass.Time,x)
hold on
plot(sta_pass.Time,y)

x=dev_pass.F(1,:) - (dev_pass.F(92,:));
y=sta_pass.F(1,:) - (sta_pass.F(92,:));

figure
plot(dev_pass.Time,x)
hold on
plot(sta_pass.Time,y)


figure
for st = 1:25
subplot(5,5,st)
plot(dev_pass.Time,dev_pass.F(st,:))
hold on
plot(sta_pass.Time,sta_pass.F(st,:))
end

%% congruency

x=congr.F(72,:) - (congr.F(92,:));
y=incongr.F(72,:) - (incongr.F(92,:));

figure
plot(dev_pass.Time,x)
hold on
plot(incongr.Time,y)

x=congr.F(1,:) - (congr.F(92,:));
y=incongr.F(1,:) - (incongr.F(92,:));

figure
plot(congr.Time,x)
hold on
plot(incongr.Time,y)


figure
for st = 1:25
subplot(5,5,st)
plot(congr.Time,congr.F(st,:))
hold on
plot(incongr.Time,incongr.F(st,:))
end
