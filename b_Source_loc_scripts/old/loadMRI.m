%% run from script C_badcomp
clear all;
close all;
clc

% fieldtrip --> eeglab --> bst?

MRI_MAIN = [filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'MRI_rawdata',filesep,'Raw_Magda',filesep];
MRI_ALL = dir([filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'MRI_rawdata',filesep,'Raw_Magda',filesep,'P*']);
MAX_MAIN = dir([filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'MEG_maxfilt',filesep,'*.fif']);
MAX_GO = [filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'MEG_maxfilt',filesep];
MEG_MAIN = [filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'data_processing',filesep];
NII_MAIN = [filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'MRI_nifti',filesep];
SCRIPTS = [filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'scripts',filesep,'preparation',filesep];
save([filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'IAF_low.mat'],'IAFs');
list_all=dir([filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'data',filesep,'MEG_rawdata',filesep,'done',filesep,'VP*.fif']); %list all fif-files
addpath([filesep,'data3',filesep,'Arnd',filesep,'MEG',filesep,'toolboxes',filesep,'fieldtrip',filesep]);

ft_defaults

list_DICOM = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\DICOMs\Pilot\Arnd\DICOM\*_0005_*']);

mri = ft_read_mri(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\DICOMs\Pilot\Arnd\DICOM\',list_DICOM(1).name]); %always read in first DICOM file

cfg = [];
resliced2 = ft_volumereslice(cfg,mri);
resliced2.coordsys = 'als';
resliced2 = ft_convert_units(resliced2, 'mm');

cfg=[];
cfg.method        = 'ortho';
ft_sourceplot(cfg, resliced2)

%% Read in sensor and digital head coordinates
shape = ft_read_headshape('O:\arm_testing\Experimente\cEEGrid_vs_Cap\Pilot_Arnd_Daniel\Digitzer_96chan_recoding_dah.elc');
shape = ft_convert_units(shape, 'mm');

sens = ft_read_sens('O:\arm_testing\Experimente\cEEGrid_vs_Cap\Pilot_Arnd_Daniel\Digitzer_96chan_recoding_dah.elc');
sens = ft_convert_units(sens, 'mm');

%% Segment brain and scalp from anatomical MRI
cfg = []; %first, segment brain, plot to check. This part is irrelevant for the realignmend
cfg.output = {'brain'};
segBrain = ft_volumesegment(cfg,resliced);
segBrain = ft_convert_units(segBrain, 'mm');%erst Segmentierung, dann realignment, copy .transform von mri in realignment!

cfg = []; %then, segment scalp and plot + sens + dig. points
cfg.output = {'scalp'};
segScalp2 = ft_volumesegment(cfg,resliced2);
segScalp2 = ft_convert_units(segScalp, 'mm');

cfg = []; %then, segment scalp and plot + sens + dig. points
cfg.output = {'skull'};
segSkull = ft_volumesegment(cfg,resliced);
segSkull = ft_convert_units(segSkull, 'mm');

cfg = [];
cfg.funparameter = 'brain';
cfg.method        = 'ortho';
ft_sourceplot(cfg, segBrain);

cfg = [];
cfg.funparameter = 'scalp';
cfg.method        = 'ortho';
ft_sourceplot(cfg, segScalp2); %scalp2 wieder zur�cksetzen, vorher coordsys checken mit electrode abgleich

cfg = [];
cfg.funparameter = 'skull';
cfg.method        = 'ortho';
ft_sourceplot(cfg, segSkull);

%% prepare headmodel 'scalp' for visual inspection
cfg = [];
cfg.method = 'singleshell';
cfg.tissue = {'scalp'};
cfg.numvertices = 10000;
hdm = ft_prepare_headmodel(cfg,segScalp2);
hdm = ft_convert_units(hdm, 'mm');

%% Plot headmodel 'scalp' for visual inspection
figure;
ft_plot_mesh(shape.pos,  'vertexsize', 10);
ft_plot_mesh(hdm.bnd.pos, 'vertexsize', 1,'vertexcolor', 'b');
ft_plot_sens(sens);
view([-56, 16]);

%% prepare headmodel 'brain' and plot
cfg = [];
cfg.method = 'singleshell';
cfg.tissue = {'brain'};
cfg.numvertices = 10000;
hdm = ft_prepare_headmodel(cfg,segBrain);
hdm = ft_convert_units(hdm, 'mm');
ft_plot_mesh(hdm.bnd.pos, 'vertexsize', 1, 'vertexcolor', 'b');

%% Realignment of head coordinates and headshape + plot for inspection
cfg = [];
cfg.method = 'headshape'; %realignment: MRT wird so transformiert, dass es mit den digitalisierten Punkten �bereinstimmt. Das ver�ndert nur die transf.-Matrix, nicht das MRT selbst
cfg.headshape = shape;
cfg.headshape.icp = 'yes'; %Optimierungsfunction um headshape und Datenpunkte anzun�hern
cfg.headshape.interactive = 'no';
cfg.coordsys = 'neuromag'; %coordsys of the MEG data, result is still in coordsys 'ras'
cfg.parameter = 'anatomy';
cfg.viewresult = 'no';
mri_aligned = ft_volumerealign(cfg, resliced); %transf.-Matrix von resliced wird benutzt. Die wird dann ver�ndert, um an die dig. Punkte angepasst zu werden

%% prepare headmodel 'scalp' for visual inspection in mesh
cfg = [];
cfg.output = 'scalp';
hdm = ft_volumesegment(cfg,mri_aligned);
cfg = [];
cfg.method = 'singleshell';
cfg.tissue = {'scalp'};
cfg.numvertices = 10000;
hdm = ft_prepare_headmodel(cfg,hdm);
hdm = ft_convert_units(hdm, 'mm');

figure;
ft_plot_mesh(hdm.bnd.pos, 'vertexsize', 0.5); axis tight;
ft_plot_mesh(shape.pos)
savefig(gcf,[MEG_MAIN,'figures',filesep,'realignments',filesep,'subject_',list_all(i).name(3:7),'.fig']);
%close figure 1

%% Store realignment as NIFTI for ROAST
cd(NII_MAIN);
cfg = [];
cfg.parameter = 'anatomy';
cfg.filetype = 'nifti';
cfg.filename = ['subject_',list_all(i).name(3:7),'_nifti'];
ft_volumewrite(cfg,mri_aligned);
%%  Overwrite transformation matrix of semgment 'brain' for later use in sourcemodel
segBrain.transform = mri_aligned.transform; %avoids problems with the segmentation/alignment

%% create headmodel from 'brain'
cfg = [];
cfg.method = 'singleshell';
cfg.numvertices = 10000;
hdm = ft_prepare_headmodel(cfg,segBrain);
figure;
ft_plot_mesh(hdm.bnd.pos, 'vertexsize', 0.5); axis tight
ft_plot_mesh(shape.pos)

%% load in clean MEG data and perform frequency_analysis
load([MEG_MAIN,'c_badcomp',filesep,'maxfilt',filesep,'subject_',list_all(i).name(3:7),'_ICA.mat'],'MEGICA');
cfg = [];
cfg.method = 'mtmfft';
cfg.channel = 'MEG';
cfg.output    = 'powandcsd'; %returns power- and crosssprectral density matrix
cfg.foilim     = [0 30];
%cfg.channel   = labelOI; %makes use of only the chosen channels, which is fine for freqanalysis, but not for beamforming!
cfg.taper = 'hanning';
freq_clean = ft_freqanalysis(cfg, MEGICA);

figure;
ft_singleplotER(cfg,freq_clean);
title(['singleplot IAF ',num2str(IAF_all(i)),'Hz sbj',num2str(list_all(i).name(3:7))]);
savefig(gcf,[MEG_MAIN,'figures',filesep,'fft_clean',filesep,'subject_',list_all(i).name(3:7),'.fig']);
close figure 1
%% create sourcemodel from headmodel and aligned data + plot for inspection
cfg = [];
cfg.coordsys = 'neuromag';
cfg.grid.warpmni = 'no';  %not coordsys, performs actual warping on standard mni-brain. Not wanted since we did not alter the original coordsys
cfg.grid.nonlinear = 'yes'; %individual hdm instead of standard mni
cfg.grid.resolution = 3; %change back to 3
cfg.headmodel = hdm;
cfg.grid.unit = 'mm';
%cfg.mri = mri_aligned; %cant be used, overwrites the headmodel
sourcegrid = ft_prepare_sourcemodel(cfg); %if individual hdm is used, the fit of the sourcegrid is much better

figure;
ft_plot_mesh(hdm.bnd.pos, 'vertexsize', 0.1); axis tight;
ft_plot_mesh(sourcegrid.pos(sourcegrid.inside,:), 'vertexcolor', 'g', 'vertexsize',5)
savefig(gcf,[MEG_MAIN,'figures',filesep,'sourcegrid',filesep,'subject_',list_all(i).name(3:7),'.fig']);
close figure 1

%% Calculate leadfield from preprocessed data
cfg = [];
cfg.headmodel = hdm;
cfg.channel = 'MEG';
cfg.grid = sourcegrid;
cfg.grad = MEGICA.grad; %gradiometerdefinitionen
cfg.normalize = 'yes';
leadfield = ft_prepare_leadfield(cfg,MEGICA);

%% average foi
cfg = [];
cfg.frequency = [(IAF_all(i)-2) (IAF_all(i)+2)]; %average over the IAF +-2
cfg.avgoverfreq = 'yes';
freq_clean_avg = ft_selectdata(cfg,freq_clean);
%%  Berechnen der Kovarianzmatrix
%                 cfg = [];
%                 cfg.covariance         = 'yes';
%                 cfg.covariancewindow   = 'all'; %uses entire measurement to calc matrix
%                 cfg.keeptrials         = 'yes'; %return the entire trial, not the average (no timelock, does not make sense to average)
%                 cfg.trials = 'all';
%                 timelock_cov = ft_timelockanalysis(cfg,freq_clean_avg);
%
%         %freq analyse mit crosspectra n�tig, diese werden dann �bertragen
%         %die timelockanalysis nimmt den durchschnitt �ber die trials. das
%         %macht aber nur sinn, wenn es ein ERp oder ERF gibt, sonst hat man
%         %im Grunde nur noise �ber den man mittelt

%% Berechnung des Beamformers anhand von leadfield, headmodel und Kovarianzmatrix
cfg = [];
cfg.method = 'dics';
cfg.grid = leadfield;
cfg.headmodel = hdm;
cfg.dics.keepfilter = 'yes'; %Unterdr�ckung von Varianz durch rausrechnen von Covarianz zwischen Punkten
cfg.dics.fixedori = 'yes';
cfg.dics.lambda = 1e-12;
cfg.dics.projectnoise = 'yes'; %noise wird projeziert, um ihn sp�ter rausrechnen zu k�nnen
cfg.grad = MEGICA.grad;
dics = ft_sourceanalysis(cfg,freq_clean_avg);

dics.avg.pow = dics.avg.pow ./ dics.avg.noise; %punktweise durch noise teilen, wirkt dem bias zur Mitte entgegen
dics.mask = abs(dics.avg.pow);

cfg = [];
cfg.parameter = 'all';
interSources = ft_sourceinterpolate(cfg,dics, mri_aligned);

%% Plotten des Ergebnisses
cfg = [];
cfg.method         = 'slice';
cfg.slicedim       = 1;
cfg.funparameter   = 'pow';
cfg.maskparameter  = 'mask'; %ab einem gewissen Wert werden pow werte rausgelassen, was die darstellung sauberer erscheinen l�sst
cfg.funcolormap    = jet;
cfg.camlight       = 'no';
ft_sourceplot(cfg,interSources);

savefig(gcf,[MEG_MAIN,'figures',filesep,'source_slices',filesep,'subject_',list_all(i).name(3:7),'.fig']);
%standard sourcemodel zur interpolation auf das individuelle gehirn

save([MEG_MAIN,'d_beamforming',filesep,'subject_',list_all(i).name(3:7),'_readybeam.mat'],...
    'mri','freq_clean','interSources','mri_aligned','dics','leadfield','sourcegrid','segBrain','segScalp','freq_clean_avg','hdm');
close all
catch ME
    end
    end
    % cd(SCRIPTS);
    % run([SCRIPTS,'e_roast.m']);
    %nach sourceanalysis ft sourcedescriptives f�r neural activity index (avg)
