%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana03_preprocessing.m
% Script 3: load ICA cleaned data from ana02
%           filter data based on your experimental design
%           epoch data around your experimental events
%           reject contaminated epochs
%           save pre-processed data in PATHOUT
%
% 
% Output: pre-processed EEG set-file cleaned, filtered and epoched
% Martin Bleichner 11/01/2018
% Maren Stropahl 25/09/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
MAINPATH = fileparts(pwd);
PATHIN  = fullfile(MAINPATH,'data','ana02',filesep);      % path containing rawdata                   
PATHOUT = fullfile(MAINPATH, 'data','ana03',filesep);  % path for script output

% create output folder if it does not exist yet
if ~exist(PATHOUT)
    mkdir(PATHOUT);
end
% ** add eeglab to Matlab path
addpath(fullfile(MAINPATH,'Software','eeglab14_1_1b'))


% locate datasets
cd(PATHIN)
list=dir('*.set');  % reads all .set files in PATHIN
len=length(list);   % total number of datasets that will be evaluated
subj=cell(1,len);   % create a subject vector 

% start eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% PARAMETERS
PRUNE = 4;                  % artifact rejection threshold in SD
EPOCH_ON = -0.2;            % epoch start
EPOCH_OFF =0.8;             % epoch end
HP_erp = 0.1;               % cut-off frequency high-pass filter [Hz]
LP_erp = 40;                % cut-off frequency low-pass filter [Hz]
HP_ord = 500;               % high-pass filter order depends on sampling rate
LP_ord = 100;               % low-pass filter order depends on sampling rate
ev = 'S 10';                  % event marker for epoching
fs = 1000;                  % sampling rate of EEG recording
baseline = [EPOCH_ON*fs 0]; % definition of baseline

%% start preprocessing of data after ICA cleaning

for s = 1:len
    %load ICA cleaned dataset
    subj{s} = strrep(list(s).name, '_ica_cleaned.set', '');
    EEG = pop_loadset('filename', [subj{s}, '_ica_cleaned.set'], 'filepath',...
        PATHIN);
    
    % apply low and high pass filter
    EEG = pop_firws(EEG, 'fcutoff', LP_erp, 'ftype', 'lowpass', 'wtype', ...
        'hann', 'forder', LP_ord);
    EEG = pop_firws(EEG, 'fcutoff', HP_erp, 'ftype', 'highpass', 'wtype', ...
        'hann', 'forder', HP_ord);
    
    % optional step: remove unnecessary event marker (e.g. here fixation cross)
    % find all events (but S 10 which is the stimulus onset) and save
    % position of EEG.event
    c=1;
    idx = [];
    for e = 1 : length(EEG.event)
        if ~ strcmp(EEG.event(e).type,ev)
            idx(c) = e;
            c=c+1;
        end
    end
    % remove all events which are not the stimulus onset (here event marker 10)
    EEG = pop_editeventvals(EEG,'delete',idx);
    EEG = eeg_checkset(EEG);
    
    % epoching and baseline
    EEG = pop_epoch(EEG, {ev}, [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,...
        'epochinfo', 'yes');
    EEG = pop_rmbase(EEG, baseline);
    
    % artifact rejection based on joint probability
    % reject artifacts with joint prob. > PRUNE (SD)
    % optional: store the indices of epoch that are going to be rejected
    % otherwise reject epochs immediately:
    % EEG = pop_jointprob(EEG, 1, [1:EEG.nbchan], PRUNE, PRUNE, 0, 1 , 0);
    EEG = pop_jointprob(EEG, 1, [1:EEG.nbchan], PRUNE, PRUNE, 0, 0 , 0);
    EEG = eeg_rejsuperpose( EEG, 1, 1, 1, 1, 1, 1, 1, 1);
    rej_ep(s) = {find(EEG.reject.rejglobal == 1)};
    EEG = pop_rejepoch( EEG, EEG.reject.rejglobal ,0);
    
    % optional: get indices of ICA components that were rejected:
    comps(s) = {EEG.badcomps};
    
    % save pruned and epoched data set
    EEG = eeg_checkset(EEG);
    EEG.setname = [subj{s}, '_ep_ar'];
    EEG = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);
    
end
% optional: save rejected epochs per subject and ICA components that were
% rejected
save([PATHOUT 'info'], 'rej_ep','comps' )

%% All ten preprocessed datasets are stored in a EEGLAB STUDY structure
% This STUDY can then be loaded into EEGLAB for further GUI based analysis
% such as plotting time courses and topographies.
cd(PATHOUT)
list=dir('*.set');  % reads all .set files in PATHIN
len=length(list);   % total number of datasets that will be evaluated
subj=cell(1,len);   % create a subject vector 


% STUDY parameters
studyname = 'sensor_level_ERP';
% initialize STUDY index
index = 1; 
STUDY = []; CURRENTSTUDY = 0; ALLEEG=[]; EEG=[]; CURRENTSET=[];

% set memory options to allow to open more than one dataset: 
% in pop_editoptions set 'option_storedisk', 1 or use eeglab GUI
for s = 1:len % for each subject
    % load dataset in study
    subj{s}= strrep(list(s).name, '.set', '');
    dataset = [PATHOUT, subj{s}, '.set'];
    [STUDY ALLEEG] = std_editset(STUDY, ALLEEG, 'name', studyname,...
        'commands',{{'index' index 'load' dataset 'subject' subj{s}}},...
        'updatedat', 'off', 'savedat', 'off', 'filename', [PATHOUT, studyname]);
    index = index + 1;
    
end

CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
STUDY.design = [];
[STUDY, ALLEEG] = std_checkset(STUDY, ALLEEG);


 [STUDY EEG] = pop_savestudy( STUDY, EEG, 'filename',[studyname, '.study'],...
 'filepath',PATHOUT);

cd(fullfile(MAINPATH, 'scripts', filesep));  
% end of script


