%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana01_ICA.m
% Script 1: This script runs an independent component analysis (ICA) on the raw 
% data. The following steps are performed within a subject loop:
%       - Read in Data
%       - Filter data from 1-40 Hz for ICA training
%       - Epoch data (dummy epochs) and remove artificial epochs for ICA
%       - Run ICA with the runica algorithm and the additional option of PCA
%       - Save ICA weights to original data
%
% 
% Output: EEG set-file with stored ICA weights
% 
% Martin Bleichner 11/01/2018
% Maren Stropahl 25/09/2017

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Computer-specific DIRECTORIES
clear; close all;
MAINPATH = fileparts(pwd);
PATHIN  = fullfile(MAINPATH,'rawdata',filesep);      % path containing rawdata                   
PATHOUT = fullfile(MAINPATH, 'data','ana01',filesep);  % path for script output
% create output folder if it does not exist yet
if ~exist(PATHOUT)
    mkdir(PATHOUT);
end
% ** add eeglab to Matlab path
addpath(fullfile(MAINPATH,'Software','eeglab14_1_1b'))

% locate rawdata-sets
cd(PATHIN)
list=dir('*.set');  % reads all .set files in PATHIN
len=length(list);   % total number of datasets that will be evaluated
subj=cell(1,len);   % create a subject vector 

%% PARAMETERS

HP = 1;         % cut-off frequency high-pass filter [Hz] only for ICA 
LP = 40;        % cut-off frequency low-pass filter [Hz] only for ICA 
SRATE = 250;    % downsample data for ICA 
HP_ord = 500;   % high-pass filter order depends on sampling rate
LP_ord = 100;   % low-pass filter order depends on sampling rate
PRUNE = 3;      % artifact rejection threshold in SD (for ICA only)
PCA = 1;        % choose PCA option for ICA
PCADIMS = 50;   % PCA dimension if PCA option is true
%% prepare data and run ICA

% start eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% loop over subjects    
for s=1:len 
    % define subject name (based on set-files in PATHIN)
    subj{s} = strrep(list(s).name, '.set', '');
    
    % load rawdata (already saved as set file)
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
    EEG.setname = [subj{s}, '_dummy_ICA']; 

    % apply low pass filter
    EEG = pop_firws(EEG, 'fcutoff', LP, 'ftype', 'lowpass', 'wtype', 'hann' , 'forder', LP_ord);
    % downsample data - may be an optional step
    EEG = pop_resample(EEG, SRATE);
    % apply high pass filter
    EEG = pop_firws(EEG, 'fcutoff', HP, 'ftype', 'highpass', 'wtype', 'hann',...
        'forder', HP_ord);
    
    % create dummy events and epoch data to these dummy events
    EEG = eeg_regepochs(EEG, 'recurrence', 1, 'eventtype', '999');
    EEG = eeg_checkset(EEG, 'eventconsistency');
    
    % remove epochs with artefacts to improve ICA training
    EEG = pop_jointprob(EEG, 1, [1:size(EEG.data,1)], PRUNE, PRUNE, 0, 1, 0);

    % run ICA optional with our without PCA
    % a window will pop-up as soon as ICA starts which allows to interrupt
    % the ICA process. Please only press if you want to cancel the process
    if PCA == 1
        EEG = pop_runica(EEG, 'icatype', 'runica', 'extended', 1, 'pca',...
            PCADIMS);
    else
        EEG = pop_runica(EEG, 'icatype', 'runica', 'extended', 1);
    end
    
    % store ICA weights in temporary variables
    icawinv = EEG.icawinv;
    icas = EEG.icasphere;
    icaw = EEG.icaweights;
    
    % load original rawdata
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
      
    % write ICA weights to rawdata
    EEG.icawinv = icawinv;
    EEG.icasphere = icas;
    EEG.icaweights = icaw;
    EEG = eeg_checkset(EEG);
    
    % save new dataset with ICA weights
    EEG.setname = [subj{s}, '_ica'];
    EEG = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);  
end
cd(fullfile(MAINPATH, 'scripts', filesep));  
% end of script