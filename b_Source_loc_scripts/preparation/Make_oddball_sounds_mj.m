%% Make oddball sounds
%Joanna Scanlon (with help from Manuela Jaeger)

ISI_gap = .5; %in seconds
% p.dur = .016; %duration of tone (seconds)
% p.rampDur = .002; % duration of the ramp gradually turning the sound on and off

p.dur = .062; %duration of tone (seconds)
p.rampDur = .010; % duration of the ramp gradually turning the sound on and off


p.nrchannels = 1; % one channel only -> Mono sound
p.sampRate = 2*44100;  %Hz
p.rampSample = round(p.rampDur.*p.sampRate)


t = linspace(0,p.dur,p.dur*p.sampRate);

ramp = ones(size(t));
ramp(t<p.rampDur) = linspace(0,1,sum(t<p.rampDur));
ramp(t>p.dur-p.rampDur) = linspace(1,0,sum(t>p.dur-p.rampDur));

high_tone = sin(2*pi*t*1000).*ramp;
low_tone = sin(2*pi*t*800).*ramp;

%equal rms 
high_tone= (high_tone./rms(high_tone))*0.2;
low_tone= (low_tone./rms(low_tone))*0.2;

sound(low_tone,p.sampRate)
sound(high_tone,p.sampRate)

% create ramp
ramp2 = ones(size(t));
hann_win=hann(p.rampSample.*2);
ramp2(1:p.rampSample)=hann_win(1:p.rampSample);
ramp2(end-p.rampSample+1:end)=hann_win(end-p.rampSample+1:end);

% ramps
high_tone_mj = sin(2*pi*t*1000).*ramp2;
low_tone_mj = sin(2*pi*t*800).*ramp2;

%equal rms 
high_tone_mj= (high_tone_mj./rms(high_tone_mj))*0.2;
low_tone_mj= (low_tone_mj./rms(low_tone_mj))*0.2;

sound(low_tone_mj,p.sampRate)
sound(high_tone_mj,p.sampRate)

audiowrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\b_Passive Auditory Oddball MMN\Stimuli\1000_dev.wav',high_tone_mj,p.sampRate);
audiowrite('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\b_Passive Auditory Oddball MMN\Stimuli\800_sta.wav',low_tone_mj,p.sampRate);

figure
plot(t,low_tone)
hold on
plot(t,low_tone_mj)