
close all;
clear all;
clc

addpath(genpath('C:\Experiments\bom\bom_cgrid'))
addpath(genpath('C:\Experiments\mbl\mbl_cEEGrid\ChoiParadigm\functions'));
%cd('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\a_Attended Speaker N1');

%% run-settings
trials		= 160; % number of presented trials PER TARGET (left,right,visual)
debug		= 0; % run script in a smal window (or in fullscreen -> 1/0)
conditions = 2;
FS = 44100;
% number of tones per stream (CAUTION! - the number of tones is linked to
% the time-length of each other (previous generated!) tone and depends on it)
% that means that every stream-duration has to match with every other stream-duration
%
% e.g. t.center		= 2;	t.left	 = .5;	t.right		= .2;
%	-> num.center	= 2;	num.left = 8;	num.right	= 20;
tone.num.center = 3;
tone.num.left	= 4;
tone.num.right	= 5;

% adjust center-level
level_add = -15; % level in dB
%% file-properties
Path		= ['O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\a_Attended Speaker N1\'];

% load soundfiles
[sig.left_low,pt.Fs] = audioread([Path 'left_low.wav']);
[sig.left_high,~] = audioread([Path 'left_high.wav']);
[sig.right_low,~] = audioread([Path 'right_low.wav']);
[sig.right_high,~] = audioread([Path 'right_high.wav']);
[sig.center_low,~] = audioread([Path 'center_low.wav']);
[sig.center_high,~] = audioread([Path 'center_high.wav']);

%% generate randomized 0/1 - matrices

% get max. value of tone-numbers
names = fieldnames(tone.num);
a = [];
Mat=[];
for i = 1:length(names)
    eval(['a = [a tone.num.' names{i} '];'])
end

Mat.tone = dec2bin((1:2^(nextpow2(trials)+1)).')-'0';
if trials<max(a)
    Mat.tone = dec2bin((1:2^(nextpow2(max(a))+1)).')-'0';
end
Mat.tone = Mat.tone(:,end-max(a)+1:end);

Cmat.tone = dec2bin(1:8-2)-'0';
Lmat.tone = dec2bin(1:16-2)-'0';
Rmat.tone = dec2bin(1:32-2)-'0';

% adjust all matrixes so that uniform and zigzag lines are gone
[rand.tone.c nC]= checkMatrix(   Cmat,3,8-2);
[rand.tone.l nL]= checkMatrix(   Lmat,4,16-2);
[rand.tone.r nR]= checkMatrix(   Rmat,5,32-2);

rand.tone.c = rand.tone.c(1:nC,:);
rand.tone.l = rand.tone.l(1:nL,:);
rand.tone.r = rand.tone.r(1:nR,:);

for l = 1: length(rand.tone.l) % determine which tones are asc, des, or alt
Left(l) = sum(diff(rand.tone.l(l,:))); % if 1 = 'rise', -1 = 'fall', 0 ='alternate';
end

for r = 1: length(rand.tone.r)
Right(r) = sum(diff(rand.tone.r(r,:))); % if 1 = 'asc', -1 = 'des', 0 ='alternate';
end

minTone = numel(find(Left==1)); %this is the number of different asc(or des) tones. It is also the max. number of tone combos we will keep on both sides

deleteCom = find(Right==0); %find the alternating tones
deleteAlt = deleteCom(minTone+1:length(deleteCom)); % find the values to be deleted: all but the first 3 tone combos

deleteCom = find(Right==-1); %find the alternating tones
deleteDes = deleteCom(minTone+1:length(deleteCom)); % find the values to be deleted: all but the first 3 tone combos

deleteCom = find(Right==1); %find the alternating tones
deleteAsc = deleteCom(minTone+1:length(deleteCom)); % find the values to be deleted: all but the first 3 tone combos

deleteCom = find(Left==0); %find the alternating tones in the left matrix
deleteAltLeft = deleteCom(minTone+1:length(deleteCom)); % find the values to be deleted: all but the first 3 tone combos


rand.tone.r([deleteAsc deleteDes deleteAlt],:) = []; %delete those tones. Now, there are 3* alt, des and asc in both left and right
rand.tone.l(deleteAltLeft,:) = [];

% create all possible combinations of center
for k=1:nC
    
    outCenterOnly(k).tone = [];
    
    for k_c = 1:tone.num.center
        if rand.tone.c(k,k_c) == 1
            outCenterOnly(k).tone = [outCenterOnly(k).tone;sig.center_high];
        else
            outCenterOnly(k).tone = [outCenterOnly(k).tone;sig.center_low];
        end
    end
    
    % adjust its level
    outLevel	= 20*log10( outCenterOnly(k).tone);
    outLevelNew = outLevel + level_add;
    outCenterOnly(k).tone	= real( 10.^( outLevelNew/20 ) );
end

% create all possible compbinations of left
for k=1:length(rand.tone.l)
    
    outLeftOnly(k).tone = [];
    
    for k_c = 1:tone.num.left
        if rand.tone.l(k,k_c) == 1
            outLeftOnly(k).tone = [outLeftOnly(k).tone;sig.left_high];
        else
            outLeftOnly(k).tone = [outLeftOnly(k).tone;sig.left_low];
        end
    end
    
    % adjust its level
    outLevel	= 20*log10( outLeftOnly(k).tone);
    outLevelNew = outLevel + level_add;
    outLeftOnly(k).tone	= real( 10.^( outLevelNew/20 ) );
end

% create all possible compbinations of right
for k=1:1:length(rand.tone.l)
    
    outRightOnly(k).tone = [];
    
    for k_c = 1:tone.num.right
        if rand.tone.r(k,k_c) == 1
            outRightOnly(k).tone = [outRightOnly(k).tone;sig.right_high];
        else
            outRightOnly(k).tone = [outRightOnly(k).tone;sig.right_low];
        end
    end
    
    % adjust its level
    outLevel	= 20*log10( outRightOnly(k).tone);
    outLevelNew = outLevel + level_add;
    outRightOnly(k).tone	= real( 10.^( outLevelNew/20 ) );
end

counter = 1;
for r = 1 : 1:length(rand.tone.r)
    for l = 1 : 1:length(rand.tone.l)
        for c = 1 : nC
            
            %combine three streams
            SIG = [outCenterOnly(1,c).tone+outLeftOnly(1,l).tone outCenterOnly(1,c).tone+outRightOnly(1,r).tone];
            
            % normalize output
            SIG(:,1) = (SIG(:,1) - mean(SIG(:,1) )) / max( abs((SIG(:,1)  - mean(SIG(:,1) ))) );
            SIG(:,2) = (SIG(:,2) - mean(SIG(:,2) )) / max( abs((SIG(:,2)  - mean(SIG(:,2) ))) );
            
            
            toneLeft= sum(diff(rand.tone.l(l,:))); % if 1 = 'rise', -1 = 'fall', 0 ='alternate';
            toneRight= sum(diff(rand.tone.r(r,:))); % if 1 = 'rise', -1 = 'fall', 0 ='alternate';
            toneCenter= sum(diff(rand.tone.c(c,:))); % if 1 = 'rise', -1 = 'fall', 0 ='alternate';
            
            if toneLeft == 1
                toneL = 'asc';
                codeL{1,counter} = '1,';
            elseif toneLeft == -1
                toneL = 'des';
                codeL{1,counter} = '2,';
            elseif toneLeft == 0
                toneL = 'alt';
                codeL{1,counter} = '3,';
            end
            
            if toneRight == 1
                toneR = 'asc';
                codeR{1,counter} = '1,';
            elseif toneRight == -1
                toneR = 'des';
                codeR{1,counter} = '2,';
            elseif toneRight == 0
                toneR = 'alt';
                codeR{1,counter} = '3,';
            end
            
            if toneCenter == 1
                toneC = 'asc';
            elseif toneCenter == -1
                toneC = 'des';
            elseif toneCenter == 0
                toneC = 'alt';
            end
            
            if counter < 10 %add leading zeros to the naming of the stimuli
               audiowrite(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\a_Attended Speaker N1\Stimuli\000',...
               num2str(counter),'_l_',toneL,'_r_',toneR,'_c_',toneC,'.wav'],SIG,FS);
                textArray{1,counter} = ['sound {wavefile {filename = "000', num2str(counter),'_l_',toneL,'_r_',toneR,'_c_',toneC,'.wav','";};}stim_',num2str(counter),';'];
            elseif counter >=  10 && counter < 100
                            audiowrite(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\a_Attended Speaker N1\Stimuli\00',...
               num2str(counter),'_l_',toneL,'_r_',toneR,'_c_',toneC,'.wav'],SIG,FS);
                textArray{1,counter} = ['sound {wavefile {filename = "00', num2str(counter),'_l_',toneL,'_r_',toneR,'_c_',toneC,'.wav','";};}stim_',num2str(counter),';'];
            elseif counter >= 100 && counter < 1000
                            audiowrite(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\a_Attended Speaker N1\Stimuli\0',...
               num2str(counter),'_l_',toneL,'_r_',toneR,'_c_',toneC,'.wav'],SIG,FS);
                textArray{1,counter} = ['sound {wavefile {filename = "0', num2str(counter),'_l_',toneL,'_r_',toneR,'_c_',toneC,'.wav','";};}stim_',num2str(counter),';'];
            end
            
            counter = counter +1;
        end
    end
end

