
clear; close all;
% define project folder

SBJ = {'timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

s=1;    % set some paths
PATHIN  = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\timing_test'; % path containing rawdata
PATHOUT = 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\timing_test';    % path for script output
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

cd(PATHIN)

list=dir('*.vhdr'); %reads all .vhdr files in that path
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

h=2;        % Loop over subjects
EEG = pop_biosig(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\timing_test',filesep,list(h).name]);
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off');

EPOCH_ON = 0;            % epoch start
EPOCH_OFF = 0.1;

if h == 1 
    ev = {'S  9'};
else h == 2
    ev = {'S 70';'S 80'};
end
    
checkEEG = pop_epoch(EEG, ev, [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');

epochVec = squeeze(checkEEG.data(1,:,:));

pks=[];
locs=[];
for x = 1:size(epochVec,2)
[pks,locs] = findpeaks(epochVec(:,x));
[d indMaxPeak(x)] = max(diff(pks));
lat(x) = locs(indMaxPeak(x)+1);
end

mean(lat)
std(lat) %this is the deciding value (8ms for attended speaker and active oddball)
max(lat)

figure
plot(epochVec(:,:))


