%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s03_preprocessing.m
% Script 3: load ICA cleaned data from ana02
%           filter data based on your experimental design
%           epoch data around your experimental events
%           reject contaminated epochs
%           save pre-processed data in PATHOUT
%
%
% Output: pre-processed EEG set-file cleaned, filtered and epoched
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
EPOCH_ON = -0.2;            % epoch start
EPOCH_OFF =0.8;             % epoch end
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 30;                % cut-off frequency low-pass filter [Hz]
ev = {'condition 6','S 70','S 80'}; % event marker for epoching
fs = 100;                  % sampling rate of EEG recording (downsample to this rate!)
baseline = [EPOCH_ON*1000 0]; % definition of baseline
taskName = {'b_passive_oddball','c_active_oddball'};
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% start preprocessing of data after ICA cleaning
for bc = 1:2 % loop over tasks...
    
    load([MAINPATH,'quality_check.mat'],'q_check');
    if bc == 1
        q_check = q_check(2:4:end); % only keep task b, easier for indexing
    else
        q_check = q_check(3:4:end); % only keep task c, easier for indexing
    end
    
    for s = 1:length(SBJ) %...subjects...
        if q_check(s).badEarChan == 0
            for ear = 0:1 %...and conditions
                
                % set some paths
                if ear ==0
                    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
                else
                    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
                end
                PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,taskName{bc},filesep);    % path for script output
                if ~exist (PATHOUT)
                    mkdir(PATHOUT)
                end
                
                % locate rawdata-set
                if ear == 0
                    listExp = dir([PATHIN,'*',taskName{bc}(1:2),'*ica_cleaned.set']);  % reads all .set files in PATHIN
                else ear == 1
                    listExp = dir([PATHIN,'*',taskName{bc}(1:2),'*_ear.set']);  % reads all .set files in PATHIN
                end
                
                %load ICA cleaned dataset
                subj{s} = listExp.name;
                EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);
                EEG.setname = strrep(EEG.setname, ' resampled', '');
                
                if ear == 0
                    % apply low and high pass filter
                    EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
                    EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
                    %             else ear == 1
                    %                 EEG = pop_select( EEG, 'nochannel',{'E92'});
                end
                EEG = pop_resample(EEG, fs);
                
                % remove unnecessary event markers
                c=1;
                idx = [];
                for e = 1 : length(EEG.event)
                    if ~strcmp(EEG.event(e).type,ev)
                        idx(c) = e;
                        c=c+1;
                    end
                end
                
                % remove all events which are not the stimulus onset
                EEG = pop_editeventvals(EEG,'delete',idx);
                EEG = eeg_checkset(EEG);
                EEGsave = EEG;
                
                % epoching and baseline
                dev = pop_epoch(EEG, ev(2), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
                sta = pop_epoch(EEG, ev([1,3]), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no');
                dev = pop_rmbase(dev, baseline);
                sta = pop_rmbase(sta, baseline);
                
                dev = pop_jointprob(dev, 1, [1:dev.nbchan], PRUNE, PRUNE, 0, 0 , 0);
                dev = eeg_rejsuperpose( dev, 1, 1, 1, 1, 1, 1, 1, 1);
                rej_ep_dev(s) = {find(dev.reject.rejglobal == 1)};
                dev = pop_rejepoch( dev, dev.reject.rejglobal ,0);
                
                sta = pop_jointprob(sta, 1, [1:sta.nbchan], PRUNE, PRUNE, 0, 0 , 0);
                sta = eeg_rejsuperpose( sta, 1, 1, 1, 1, 1, 1, 1, 1);
                rej_ep_sta(s) = {find(sta.reject.rejglobal == 1)};
                sta = pop_rejepoch( sta, sta.reject.rejglobal ,0);
                
                sta = eeg_checkset(sta);
                if ear == 0
                    sta.setname = strrep(listExp.name, '_ica_cleaned.set', '_sta');
                else ear == 1
                    sta.setname = strrep(listExp.name, '_ica_ear.set', '_sta_ear');
                end
                sta = pop_saveset(sta, [sta.setname, '.set'], PATHOUT);
                
                dev = eeg_checkset(dev);
                if ear == 0
                    dev.setname = strrep(listExp.name, '_ica_cleaned.set', '_dev');
                else ear == 1
                    dev.setname = strrep(listExp.name, '_ica_ear.set', '_dev_ear');
                end
                dev = pop_saveset(dev, [dev.setname, '.set'], PATHOUT); %use get all epochs in bst!
                
                eeglab redraw
                
            end
        end
    end
    
    if bc == 1
        save([MAINPATH,'rej_ep_b.mat'],'rej_ep_sta','rej_ep_dev');
    else
        save([MAINPATH,'rej_ep_c.mat'],'rej_ep_sta','rej_ep_dev');
    end
    
end

cd('O:\arm_testing\Scripts\b_Source_loc_scripts\pre-processing')
run('ana03d_preprocessing.m')

% % end of script
