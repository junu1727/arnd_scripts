%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s02_corrmap.m
%  Script 2: select ICA components with CORRMAP algorithm
%       1. save all ICA components for each subject as a PNG graphic for
%           inspection of ICA components. Use these figures to select a template
%           component manually for each artifact
%       2. create study with all datasets containing ICA weights (ana01)
%       3. run CORRMAP to find ICA components based on templates for each
%           subject: heartbeat, eye blink, lateral eye movements
%       4. save components in MAT-file
%           reload data file with saved ICA weights
%           store selected components as EEG.badcomps in each dataset
%           remove selected components
%           save new dataset in PATHOUT with rejected components
%
%
% Output: EEG set-file pruned with ICA
%         MAT-file with saved components (output from CORRMAP)
% Martin Bleichner 11/01/2018
% Maren Stropahl 25/09/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Computer-specific DIRECTORIES

MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
load([MAINPATH,'quality_check.mat'],'q_check');

% start eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% 1. plot and save all ICA components for later manual selection of template
% components

listSBJ = dir('O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\*\02_ICA_filt\*ica.set*');
% listSBJ = listSBJ(find(contains({listSBJ.date} , 'Okt')));
% listSBJ = listSBJ(find(contains({listSBJ.date} , '14:')));

len = length(listSBJ);   % total number of datasets that will be evaluated
subj = cell(1,len);   % create a subject vector

for s = 1:length(listSBJ)
    PATHIN  = fullfile(MAINPATH,listSBJ(s).name(1:6),filesep,'02_ICA_filt',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,listSBJ(s).name(1:6),filesep, '03_ICA_COMPS', filesep);    % path for script output
    if ~exist (PATHOUT)
        mkdir(PATHOUT)
    end
    
    % load dataset
    subj{s} = strrep(listSBJ(s).name, '.set', '');
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
    
    % plot ICA components for each subject and save figure as PNG graphic for
    % later inspections
    pop_topoplot(EEG, 0, [1:size(EEG.icawinv,2)], [subj{s}], 0, 'electrodes',...
        'off');
    filename = strrep(subj{s}, '_ica_comps', '');
    saveas(gcf,[PATHOUT, filename], 'png');
    close;
end

for s = 1:length(listSBJ)
    %if strcmp(listSBJ(s).name(8),'d')
    PATHOUT = fullfile(MAINPATH,listSBJ(s).name(1:6), '03_ICA_COMPS', filesep);    % path for script output
    pic =  imread([PATHOUT, listSBJ(s).name(1:8), '_ica'],'png');
    figure
    image(pic);
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    title([listSBJ(s).name(1:8)]);
    set(gca,'FontSize', 18);
   % end
end


%% 2. create STUDY with all datasets

% STUDY parameters
studyname = 'ICA_Comps';
% initialize STUDY index
index = 1;
STUDY = []; CURRENTSTUDY = 0; ALLEEG=[]; EEG=[]; CURRENTSET=[];

% set memory options to allow to open more than one dataset:
% in pop_editoptions set 'option_storedisk', 1 or use eeglab GUI
for s = 1:length(listSBJ)     % for each subject and each task
        %if contains(listSBJ(s).name,'_d_')

    PATHIN  = fullfile(MAINPATH,listSBJ(s).name(1:6),filesep,'02_ICA_filt',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,listSBJ(s).name(1:6), '03_ICA_COMPS', filesep);    % path for script output
    
    % load dataset in study
    subj{s}= strrep(listSBJ(s).name, '.set', '');
    dataset = [PATHIN, subj{s}, '.set'];
    [STUDY ALLEEG] = std_editset(STUDY, ALLEEG, 'name', studyname,...
        'commands',{{'index' index 'load' dataset 'subject' subj{s}}},...
        'updatedat', 'off', 'savedat', 'off', 'filename', [PATHIN, studyname]);
    index = index + 1;
        %end
end

CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
STUDY.design = [];
[STUDY, ALLEEG] = std_checkset(STUDY, ALLEEG);

%optional: save study
[STUDY EEG] = pop_savestudy( STUDY, EEG, 'filename',[studyname, '.study'],...
    'filepath','O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\_supplementary\');

eeglab redraw

%% 3. find ICA components with CORRMAP
% watch out: semi-automatic process: TEMPLATE COMPONENT has to be selected
% manually! for help see: pop_corrmap.m
% cf. supplementary material for selected components

% optional load study file if saved earlier and if script was not running
% successively
[STUDY ALLEEG] = pop_loadstudy('filename', [studyname, '.study'],...
'filepath', 'O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\_supplementary');
CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
[STUDY, ALLEEG] = std_checkset(STUDY, ALLEEG);

% defintion of template datasets and components for each of the three
% components that should be used by the semi-automatic corrmap algorithm
blink_set   = 7;    % identified set for the template eye blink component
blink_comp  =  2;    % identified template component included in eye_set
heart_set   = 7;    % identified set for the template heart beat  component
heart_comp  = 8;    % identified template component included in heart_set
eyemov_set  = 7;    % identified set for the template eye movement component
eyemov_comp =  18;    % identified template component included in eye_set
% line_set    =  3;    % identified set for the template line noise component
% line_comp   =  5;    % identified template component included in line noise 


% use corrmap to find components in each dataset (data is accessed through study)
% 1. eye blinks

[CORRMAP, STUDY, ALLEEG] = pop_corrmap(STUDY, ALLEEG, blink_set, blink_comp,...
    'th', '0.8', 'ics', 3, 'pl', '2nd', 'title', 'plot', 'clname', '', ...
    'badcomps', 'no', 'resetclusters', 'off');
eyeblink = [CORRMAP.output.sets{2} CORRMAP.output.ics{2}];
Corr_eye = CORRMAP;

% 2. heartbeats
[CORRMAP, STUDY, ALLEEG] = pop_corrmap(STUDY, ALLEEG, heart_set, heart_comp,...
    'th', '0.8', 'ics', 3, 'pl', '2nd', 'title', 'plot', 'clname', '', ...
    'badcomps', 'no', 'resetclusters', 'off');
heartbeat = [CORRMAP.output.sets{2} CORRMAP.output.ics{2}];
Corr_heart = CORRMAP;

% 3. lateral eye movements
[CORRMAP, STUDY, ALLEEG] = pop_corrmap(STUDY,ALLEEG, eyemov_set, eyemov_comp,...
    'th', '0.8', 'ics', 3, 'pl', '2nd', 'title', 'plot', 'clname', '', ...
    'badcomps', 'no', 'resetclusters', 'off');
eyemovement = [CORRMAP.output.sets{2} CORRMAP.output.ics{2}];
Corr_lateye = CORRMAP;

% % 3. line noise
% [CORRMAP, STUDY, ALLEEG] = pop_corrmap(STUDY,ALLEEG, line_set, line_comp,...
%     'th', '0.8', 'ics', 3, 'pl', '2nd', 'title', 'plot', 'clname', '', ...
%     'badcomps', 'no', 'resetclusters', 'off');
% linenoise = [CORRMAP.output.sets{2} CORRMAP.output.ics{2}];
% Corr_line = CORRMAP;

% save info about components (optional) as MAT-file
save('components.mat', 'eyeblink', 'heartbeat' , 'eyemovement');
save('corrmap_info.mat', 'Corr_eye',  'Corr_heart', 'Corr_lateye');

% load('components.mat', 'eyeblink', 'heartbeat' , 'eyemovement');
% load('corrmap_info.mat', 'Corr_eye',  'Corr_heart', 'Corr_lateye');

%% 4. remove components from original dataset

% optional:
% load('components.mat')

% find components marked for each subject, store components in EEG.badcomps
% and remove selected components from dataset
count = 1;
for s = 1:length(listSBJ)
            %if contains(listSBJ(s).name,'_d_')
    PATHIN  = fullfile(MAINPATH,listSBJ(s).name(1:6),filesep,'02_ICA_filt',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,listSBJ(s).name(1:6), '03_ICA_clean', filesep);    % path for script output
    if ~exist (PATHOUT)
        mkdir(PATHOUT)
    end
    
    % load dataset with ICA weights again
    subj{s}= strrep(listSBJ(s).name, '.set', '');
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
    
    % find selected components for each subject and save as badcomps
    eye = eyeblink(find(eyeblink(:,1) == count), 2)';
    heart = heartbeat(find(heartbeat(:,1) == count), 2)';
    eyemov = eyemovement(find(eyemovement(:,1) == count), 2)';
    %     linen = linenoise(find(linenoise(:,1) == s), 2)';
    
    count = count +1;
    
    % store all components in EEG structure
    EEG.badcomps = [eye heart eyemov];
    
    % plot components again, this time with note about badcomps and save as
    % PNG in PATHOUT folder
    pop_topoplot(EEG, 0, [1:60], [subj{s}, ...
        ' removed components: ', num2str(EEG.badcomps)], [6 10], 0, ...
        'electrodes', 'off');
    filename = strrep(subj{s}, '_ica', '_badcomps');
    saveas(gcf, [PATHOUT, filename], 'png');
    close
    
    % remove selected components from dataset
    EEG = pop_subcomp(EEG, [EEG.badcomps], 0);
    EEG = eeg_checkset(EEG);
    
    % save ICA cleaned dataset in ana02
    EEG.setname = [subj{s}, '_cleaned'];
    EEG = pop_saveset( EEG, 'filename', [EEG.setname, '.set'], 'filepath',...
        PATHOUT);
            %end
end
%cd(fullfile(MAINPATH, 'scripts', filesep));
 
for s = 1:length(listSBJ)
    %if strcmp(listSBJ(s).name(8),'d')
    PATHOUT = fullfile(MAINPATH,listSBJ(s).name(1:6), '03_ICA_clean', filesep);    % path for script output
    pic =  imread([PATHOUT, listSBJ(s).name(1:8), '_badcomps.png']);
    figure
    image(pic);
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    title([listSBJ(s).name(1:8)]);
    set(gca,'FontSize', 18);
    %end
end

% end of script

cd('O:\arm_testing\Scripts\b_Source_loc_scripts\pre-processing')
run('ana03a_preprocessing.m')
