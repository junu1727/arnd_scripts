%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s03_preprocessing.m
% Script 3: load ICA cleaned data from ana02
%           filter data based on your experimental design
%           epoch data around your experimental events
%           reject contaminated epochs
%           save pre-processed data in PATHOUT
%
%
% Output: pre-processed EEG set-file cleaned, filtered and epoched
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES

SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
EPOCH_ON = -0.2;            % epoch start
EPOCH_OFF =0.8;             % epoch end
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 30;              % cut-off frequency low-pass filter [Hz]
ev = {'condition 7','S 11','S  9','S  1','S  2'}; % event marker for epoching
fs = 100;                  % sampling rate of EEG recording (downsample to this rate!)
baseline = [EPOCH_ON*1000 0]; % definition of baseline
bslength = 0.2; %length of baseline
bslength_m = 0.8; %length of baseline for motor responses
maxR = 2; %longest accepted reaction time
minR = 0.2; %shortest accepted reaction time

load([MAINPATH,'quality_check.mat'],'q_check');
q_check = q_check(4:4:end); % only keep task d, easier for indexing

%% start preprocessing of data after ICA cleaning
for s = 1:length(SBJ)
    if q_check(s).badEarChan == 0
        for ear = 0:1
            
            %             if s == 4 % ins SBJ4, the trigger is 6 for some reason
            %                 ev{1} = 'condition 6';
            %             end
            
            if ear ==0
                PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
            else
                PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
            end
            PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'d_congruency_oddball',filesep);    % path for script output
            if ~exist (PATHOUT)
                mkdir(PATHOUT)
            end
            
            % locate rawdata-sets
            if ear == 0
                listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\03_ICA_clean\*d_ica_cleaned.set']);  % reads all .set files in PATHIN
            else ear == 1
                listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*d*','_ear.set']);  % reads all .set files in PATHIN
            end
            
            % load ICA cleaned dataset
            subj{s} = listExp.name;
            EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);
            EEG.setname = strrep(EEG.setname, ' resampled', '');
           
            %change marker types (sometimes condition6, sometimes condition 7 for some reason)
            for changemarker = 1: length(EEG.event)
                if contains(EEG.event(changemarker).type,'condition')
                    EEG.event(changemarker).type = ev{1};
                end
            end
            
            if ear == 0
                % apply low and high pass filter
                EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',0);
                EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',0);
            end
            
%             EEG = pop_resample(EEG, fs);
%             for t = 1:length([EEG.event.latency])
%                 EEG.event(t).latency = ceil(EEG.event(t).latency);
%             end
            
            % load critical word onsets from the log file and adjust to sampling rate of
            % experiment (CW-onset in audioSBJ given in seconds)
            PATHAUDIO = fullfile(MAINPATH,SBJ{s},filesep, 'log', filesep);    % path for audio-order
            load([PATHAUDIO,'CW_onsets.mat']);
            audioSBJ = audioSBJ*fs*10; %turn seconds into ms, same as in EEG.event.latency
            
            %% add the CW latency to the latency of the stimulus onset. This way, we can
            % epoch around the CW-onset. For baseline correction, find the window
            % before sentence onset
                      
            findstimonset = find(strcmp(ev(3),{EEG.event.type})); %find indices of stimulus onset triggers
            
            for tr = 1:length(findstimonset)
            EEG.event(findstimonset(tr)).latency = EEG.event(findstimonset(tr)).latency + audioSBJ(tr); %add to those trigger latencies the CW-onset latencies
            end
%             newlatency = [EEG.event(findstimonset).latency] + audioSBJ'; %add to those trigger latencies the CW-onset latencies
%             bslatency  = [EEG.event(findstimonset).latency] + baseline(1); % find the basline from before the sentence starts
%             bs=[];
%             for b = 1:length(findstimonset)
%                 bs(:,b) = mean(EEG.data(:,[bslatency(b) : bslatency(b) + bslength*EEG.srate-1]),2); %find the mean baseline value for each channel
%                 motor_bs{1,b} = EEG.data(:,[bslatency(b) : bslatency(b) + bslength*EEG.srate-1]); %store the baseline for every sentence in a variable
%             end %each column is the average over the baseline period for all channels
%             
            %now we have both the epoch around the CW-onset and the baseline value from before sentence-onset
            
            % remove unnecessary event marker (e.g. here fixation cross)
            % find all events and save position of EEG.event
            c=1;
            idx = [];
            for e = 1 : length(EEG.event)
                if ~ strcmp(EEG.event(e).type,ev)
                    idx(c) = e;
                    c=c+1;
                end
            end
            
            count = [];
            count.congr      = find(strcmp({EEG.event.type}, ev(2)));
            count.incongr    = find(strcmp({EEG.event.type}, ev(1)));
            count.no_resp    = strcmp({EEG.event.type}, ev(3));
            count.no_resp    = find(diff(count.no_resp)==0);
            count.wrong_answ = [find(strcmp({EEG.event.type}, ev(4))) find(strcmp({EEG.event.type}, ev(5)))];
            count.resptrig   = find(strcmp({EEG.event.type}, ev(3)));
            
            if numel([count.congr count.incongr count.no_resp count.wrong_answ]) ~= numel(count.resptrig) 
                warning('Response count does not add up. Check the markers')
            end          
            
            save([MAINPATH,SBJ{s},'\log\resp_d.mat','count']);

            %% Baseline correction. We need the baseline for every CW-epoch to be from before sentence start.
            % Therefore, we find these baseline periods in the continious data, average them over time (96*1 matrix)
            % and then identify the time range that will be the epoch around the
            % CW-onset, again in the continious data. Then, we subtract the bs-values
            % from each epoch individually. When we then epoch, the epochs are already bs-corrected
%             for  lat = 1:length(findstimonset) % for every CW-epoch, subtract the baseline taken from before SENTENCE-onset
%                 startbs = EEG.event(findstimonset(lat)).latency + baseline(1);
%                 endbs   = EEG.event(findstimonset(lat)).latency + EPOCH_OFF*EEG.srate-1;
%                 EEG.data(:,startbs:endbs) = EEG.data(:,startbs:endbs) - bs(:,lat); %remove the baseline averages from each timepoint in the to-be-epochs
%             end
            
            
            congr_sentences   = EEG;
            incongr_sentences = EEG;
            
            % REMOVE all event markers except the CW-onsets (S9) of all CONGRUENT
            % sentences. This includes every response trigger, every
            % stimulus trigger that does not belond to a congruent
            % sentence, every wrong/no answer and every boundary trigger
            congr_sentences = pop_editeventvals(EEG,'delete',[count.congr count.incongr-1 count.incongr idx count.wrong_answ count.wrong_answ-1 count.no_resp]); %delete the response triggers and the stim-triggers of all incongruent sentences
            congr_sentences = eeg_checkset(congr_sentences);
            congr_sentences = pop_resample(congr_sentences,fs);
            
            % remove all event markers except the CW-onsets (S9) of all INCONGRUENT
            % sentences
            incongr_sentences = pop_editeventvals(EEG,'delete',[count.congr count.congr-1 count.incongr idx count.wrong_answ count.wrong_answ-1 count.no_resp]); %delete the response triggers and the stim-triggers of all congruent sentences
            incongr_sentences = eeg_checkset(incongr_sentences); % the epoch that are generated now are already baseline-corrected
            incongr_sentences = pop_resample(incongr_sentences,fs);
            
            % epoching and baseline
            incongr_sentences = pop_epoch(incongr_sentences, ev(3), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no'); %epoch around the remaining CW-onsets
            congr_sentences   = pop_epoch(congr_sentences, ev(3), [EPOCH_ON EPOCH_OFF], 'newname', EEG.setname,'epochinfo', 'no'); %epoch around the remaining CW-onsets
            
            incongr_sentences = pop_rmbase(incongr_sentences,[-200 0]);
              congr_sentences = pop_rmbase(  congr_sentences,[-200 0]);

            %% some artefact rejection
            incongr_sentences = pop_jointprob(incongr_sentences, 1, [1:incongr_sentences.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            incongr_sentences = eeg_rejsuperpose( incongr_sentences, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep_incongr(s) = {find(incongr_sentences.reject.rejglobal == 1)};
            incongr_sentences = pop_rejepoch( incongr_sentences, incongr_sentences.reject.rejglobal ,0);
            
            congr_sentences = pop_jointprob(congr_sentences, 1, [1:congr_sentences.nbchan], PRUNE, PRUNE, 0, 0 , 0);
            congr_sentences = eeg_rejsuperpose( congr_sentences, 1, 1, 1, 1, 1, 1, 1, 1);
            rej_ep_congr(s) = {find(congr_sentences.reject.rejglobal == 1)};
            congr_sentences = pop_rejepoch( congr_sentences, congr_sentences.reject.rejglobal ,0);
            
            % save pruned and epoched data set
            if ear == 1
                incongr_sentences.setname = strrep(listExp.name, '_ica_ear.set', '_incongr_ear');
            else ear == 0
                incongr_sentences.setname = strrep(listExp.name, '_ica_cleaned.set', '_incongr');
            end
            incongr_sentences = pop_saveset(incongr_sentences, [incongr_sentences.setname, '.set'], PATHOUT);
            
            if ear == 1
                congr_sentences.setname = strrep(listExp.name, '_ica_ear.set', '_congr_ear');
            else ear == 0
                congr_sentences.setname = strrep(listExp.name, '_ica_cleaned.set', '_congr');
            end
            congr_sentences = pop_saveset(congr_sentences, [congr_sentences.setname, '.set'], PATHOUT);
            
            if ear == 0
                inc(:,:,s) = mean(incongr_sentences.data(:,:,:),3);
                con(:,:,s) = mean(  congr_sentences.data(:,:,:),3);
            else
                incEar(:,:,s) = mean(incongr_sentences.data(:,:,:),3);
                conEar(:,:,s) = mean(  congr_sentences.data(:,:,:),3);
            end
        end
    end
end

save([MAINPATH,'rej_ep_d.mat'], 'rej_ep_congr','rej_ep_incongr' );

eeglab redraw

for s = 1:length(SBJ)-1
    figure
    subplot(1,2,1)
    plot(incongr_sentences.times,mean(inc(:,:,s),3))
    title('incongruent')
    ylim([-10 10])
    subplot(1,2,2)
    plot(congr_sentences.times,mean(con(:,:,s),3))
    title('congruent')
    ylim([-10 10])
end


figure
subplot(1,2,1)
plot(incongr_sentences.times,mean(inc(:,:,:),3))
title('incongruent')
ylim([-10 10])
subplot(1,2,2)
plot(congr_sentences.times,mean(con(:,:,:),3))
title('congruent')
ylim([-10 10])

    
cd('O:\arm_testing\Scripts\b_Source_loc_scripts\pre-processing')
run('ana04All_effect_sizes.m')

% figure
% plot(incongr_sentences.times,mean(incEar(:,:,:),3))
% 
% figure
% plot(congr_sentences.times,mean(conEar(:,:,:),3))
% 
% figure
% plot(congr_sentences.times,mean(incEar(:,:,:),3)-mean(conEar(:,:,:),3))
%% get the motor potentials
%             for  lat = 1:length(congr) % for every congr-button press, subtract the baseline taken from before SENTENCE-onset
%                 startbs = EEGm.event(congr(lat)).latency - bslength_m * EEGm.srate-1;
%                 endbs   = EEGm.event(congr(lat)).latency + bslength_m * EEGm.srate-2;
%                 EEGm.data(:,startbs:endbs) = EEGm.data(:,startbs:endbs) - bs(:,count(lat).congr); %remove the baseline averages from each timepoint in the to-be-epochs
%             end
%             
%             for  lat = 1:length(incongr) % for every incongr-button press, subtract the baseline taken from before SENTENCE-onset
%                 startbs = EEGm.event(incongr(lat)).latency - bslength_m * EEGm.srate-1;
%                 endbs   = EEGm.event(incongr(lat)).latency + bslength_m * EEGm.srate-2;
%                 EEGm.data(:,startbs:endbs) = EEGm.data(:,startbs:endbs) - bs(:,count(lat).incongr); %remove the baseline averages from each timepoint in the to-be-epochs
%             end
%             
%             motor_bs_congr = [];
%             xy=1;
%             for i = 1:length([count.congr])
%                 motor_bs_congr(:,:,xy) = motor_bs{1,[count(i).congr]};
%                 xy = xy +1;
%             end
%             xy=1;
%             motor_bs_incongr = [];
%             for i = 1:length([count.incongr])
%                 motor_bs_incongr(:,:,xy) = motor_bs{1,[count(i).incongr]};
%                 xy = xy +1;
%             end
%             
%             get reaction times to exclude unrealistic responses
%             rT_congr = rTime([count.congr]); %load reaction times from CW-onsets
%             rT_incongr = rTime([count.incongr]);
%             
%             motor_bs_congr(:,:,[find(rT_congr <= minR) find(rT_congr > maxR)]) = []; % get the baselines of all congr-clicked trials
%             motor_bs_incongr(:,:,[find(rT_incongr <= minR) find(rT_incongr > maxR)]) = []; % get the baselines of all incongr-clicked trials
%             
%             motor_data_congr   = pop_epoch(EEGm, ev(2), [-bslength_m bslength_m], 'newname', EEGm.setname,'epochinfo', 'no'); % epoch around the response triggers
%             motor_data_incongr = pop_epoch(EEGm,  ev(1), [-bslength_m bslength_m], 'newname', EEGm.setname,'epochinfo', 'no');
%             motor_data_congr   = pop_rejepoch( motor_data_congr,[find(rT_congr <= minR) find(rT_congr > maxR)] ,0);
%             motor_data_incongr = pop_rejepoch( motor_data_incongr,[find(rT_incongr <= minR) find(rT_incongr > maxR)] ,0);
%             
%             motor_data_congr = pop_jointprob(motor_data_congr, 1, [1:motor_data_congr.nbchan], PRUNE, PRUNE, 0, 0 , 0);
%             motor_data_congr = eeg_rejsuperpose( motor_data_congr, 1, 1, 1, 1, 1, 1, 1, 1);
%             motor_bs_congr(:,:,[motor_data_congr.reject.rejglobal]) = []; %reject the baselines that we rejected the corresponding epochs for
%             motor_data_congr = pop_rejepoch( motor_data_congr, motor_data_congr.reject.rejglobal ,0);
%             
%             motor_data_incongr = pop_jointprob(motor_data_incongr, 1, [1:motor_data_incongr.nbchan], PRUNE, PRUNE, 0, 0 , 0);
%             motor_data_incongr = eeg_rejsuperpose( motor_data_incongr, 1, 1, 1, 1, 1, 1, 1, 1);
%             motor_bs_incongr(:,:,[motor_data_incongr.reject.rejglobal]) = [];  %reject the baselines that we rejected the corresponding epochs for
%             motor_data_incongr = pop_rejepoch( motor_data_incongr, motor_data_incongr.reject.rejglobal ,0);
             
% figure
% plot(motor_data_congr.times,mean(motor_data_congr.data(:,:,:),3))
%
% figure
% plot(motor_data_incongr.times,mean(motor_data_incongr.data(:,:,:),3))
%
% figure
% plot(linspace(0,200,20),mean(motor_bs_congr,3))
%
% figure
% plot(linspace(0,200,20),mean(motor_bs_incongr,3))
%
%
%             % save motor responses
%             if ear == 1
%                 motor_data_congr.setname = strrep(listExp.name, '_ica_ear.set', '_motor_data_congr_ear');
%                 save([PATHOUT,'motor_bs_congr_ear.mat'],'motor_bs_congr');
%             else ear == 0
%                 motor_data_congr.setname = strrep(listExp.name, '_ica_cleaned.set', '_motor_data_congr');
%                 save([PATHOUT,'motor_bs_congr.mat'],'motor_bs_congr');
%             end
%             motor_data_congr = pop_saveset(motor_data_congr, [motor_data_congr.setname, '.set'], PATHOUT);
%             
%             if ear == 1
%                 motor_data_incongr.setname = strrep(listExp.name, '_ica_ear.set', '_motor_data_incongr_ear');
%                 save([PATHOUT,'motor_bs_incongr_ear.mat'],'motor_bs_incongr');
%             else ear == 0
%                 motor_data_incongr.setname = strrep(listExp.name, '_ica_cleaned.set', '_motor_data_incongr');
%                 save([PATHOUT,'motor_bs_incongr.mat'],'motor_bs_incongr');
%             end
%             motor_data_incongr = pop_saveset(motor_data_incongr, [motor_data_incongr.setname, '.set'], PATHOUT);