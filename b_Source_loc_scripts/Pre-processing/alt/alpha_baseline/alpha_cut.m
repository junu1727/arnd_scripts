%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana00_convert_rawdata.m
% Script 0:
%     loads in Smarting-data (.xdf) files and converts them to EEGALB set
%     files

% Output: EEG set-files
%
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
% define project folder

SBJ = {'p01'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);


for s = 1:length(SBJ)
    % set some paths
    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, 'alpha_cut', filesep);    % path for script output
    if ~exist (PATHOUT)
    mkdir(PATHOUT)
    end

    chan_file = dir(fullfile(MAINPATH,SBJ{s},'channel positions',filesep,'*.elc'));

    locs  = loadtxt( [chan_file(s).folder,'\',chan_file(s).name], 'blankcell', 'off' );
    locs = cell2mat(locs(4:99,3:5));
    
    cd(PATHIN)
    list=dir('*.vhdr'); %reads all .vhdr files in that path
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    
    for h = 2:length(list) %1 does not have a proper baseline
        % Loop over subjects
        EEG = pop_biosig([MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep,list(h).name]);
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off');
        
        
        if h ==1 || h ==2 %this part can be deleted with new data. From now on, all starting triggers are S 7
        findlat = find( strcmp({EEG.event.type},{'S  6'}),1,'first');%index of start latency
        elseif h == 3 || h == 4
        findlat = find( strcmp({EEG.event.type},{'S  7'}),1,'first');%index of start latency
        else h == 5
        findlat = find( strcmp({EEG.event.type},{'S  3'}),1,'first');%index of start latency
        end
        
        startlat = EEG.event(findlat).latency + 5*EEG.srate; %go from beginning of baseline to 5s later to avoid movement-contaminated data
        endlat = EEG.event(findlat).latency  + EEG.srate*55; %index of baseline end: 55 seconds after baseline begins
        
        EEG = pop_select( EEG, 'time',[startlat/EEG.srate endlat/EEG.srate - 1/EEG.srate] ); %remove everthing except this baseline
        
        EEG.comments='';
        EEG.setname = list(h).name(1:6);
        EEG = pop_saveset(EEG, [EEG.setname,'_baseline.set'], PATHOUT);
        
        eeglab redraw % redraw interface
    end
end
