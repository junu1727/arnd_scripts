%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana01_ICA.m
% Script 1: This script runs an independent component analysis (ICA) on the raw 
% data. The following steps are performed within a subject loop:
%       - Read in Data
%       - Filter data from 1-40 Hz for ICA training
%       - Epoch data (dummy epochs) and remove artificial epochs for ICA
%       - Run ICA with the runica algorithm and the additional option of PCA
%       - Save ICA weights to original data
%
% 
% Output: EEG set-file with stored ICA weights
% 
% Arnd Meiser

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Computer-specific DIRECTORIES
clear; close all;

MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'Pilot_Arnd_Daniel',filesep);
PATHIN  = fullfile(MAINPATH,'rawdata',filesep,'raw_eeglab', filesep); % path containing rawdata
PATHOUT = fullfile(MAINPATH, 'data','ana01',filesep);  % path for script output

% create output folder if it does not exist yet
if ~exist(PATHOUT)
    mkdir(PATHOUT);
end

% add eeglab to Matlab path
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);

% locate rawdata-sets
cd(PATHIN)
list=dir('*.set');  % reads all .set files in PATHIN
len=length(list);   % total number of datasets that will be evaluated
subj=cell(1,len);   % create a subject vector 

%% PARAMETERS

HP = 0.1;         % cut-off frequency high-pass filter [Hz] only for ICA 
LP = 40;        % cut-off frequency low-pass filter [Hz] only for ICA 
SRATE = 250;    % downsample data for ICA 
HP_ord = 500;   % high-pass filter order depends on sampling rate
LP_ord = 100;   % low-pass filter order depends on sampling rate
PRUNE = 2;      % artifact rejection threshold in SD (for ICA only)
PCA = 0;        % choose PCA option for ICA
PCADIMS = 50;   % PCA dimension if PCA option is true
%% prepare data and run ICA

% start eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% loop over subjects    
for s = 1:len 
    % define subject name (based on set-files in PATHIN)
    subj{s} = strrep(list(s).name, '.set', '');
    
    % load rawdata (already saved as set file)
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
    EEG.setname = [subj{s}, '_dummy_ICA']; 

    % apply low pass filter
    EEG = pop_firws(EEG, 'fcutoff', LP, 'ftype', 'lowpass', 'wtype', 'hann' , 'forder', LP_ord);
    % downsample data - may be an optional step
    EEG = pop_resample(EEG, SRATE);
    % apply high pass filter
    EEG = pop_firws(EEG, 'fcutoff', HP, 'ftype', 'highpass', 'wtype', 'hann',...
        'forder', HP_ord);
    
    % create dummy events and epoch data to these dummy events
    EEG = eeg_regepochs(EEG, 'recurrence', 1, 'eventtype', '999');
    EEG = eeg_checkset(EEG, 'eventconsistency');
    
    % remove epochs with artefacts to improve ICA training
    EEG = pop_jointprob(EEG, 1, [1:size(EEG.data,1)], PRUNE, PRUNE, 0, 1, 0);

    % run ICA optional with our without PCA
    % a window will pop-up as soon as ICA starts which allows to interrupt
    % the ICA process. Please only press if you want to cancel the process
    if PCA == 1
        EEG = pop_runica(EEG, 'icatype', 'runica', 'extended', 1, 'pca',...
            PCADIMS);
    else
        EEG = pop_runica(EEG, 'icatype', 'runica', 'extended', 1);
    end
    
    % store ICA weights in temporary variables
    icawinv = EEG.icawinv;
    icas = EEG.icasphere;
    icaw = EEG.icaweights;
    
    % load original rawdata
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
      
    % write ICA weights to rawdata
    EEG.icawinv = icawinv;
    EEG.icasphere = icas;
    EEG.icaweights = icaw;
    EEG = eeg_checkset(EEG);
    
    % save new dataset with ICA weights
    EEG.setname = [subj{s}, '_ica'];
    EEG = pop_saveset(EEG, [EEG.setname, '.set'], PATHOUT);  
    
    eeglab redraw
end

% end of script