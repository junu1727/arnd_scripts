%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana02_corrmap.m
%  Script 2: visually inspect ICA components of all subjects and remove them. Save the
%  cleaned subject data
%
% 
% Output: EEG set-file pruned with ICA
%         MAT-file with saved components (output from CORRMAP)
%
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%% Computer-specific DIRECTORIES
 
clear; close all;
 
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'Pilot_Arnd_Daniel',filesep);
PATHIN  = fullfile(MAINPATH,'data','ana01',filesep);      % path containing rawdata                   
PATHOUT = fullfile(MAINPATH, 'data','ana02',filesep);  % path for script output
% create output folder if it does not exist yet
if ~exist(PATHOUT)
    mkdir(PATHOUT);
end

% add eeglab to Matlab path
addpath(fullfile('C:',filesep,'Users',filesep,'arnd',filesep,'Desktop',filesep,'toolboxes_matlab',filesep,'eeglab2020_0'))
 
% locate datasets containing ICA weights
cd(PATHIN)
list=dir('*.set');  % reads all .set files in PATHIN
len=length(list);   % total number of datasets that will be evaluated
subj=cell(1,len);   % create a subject vector 
 
% start eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
 
%% 1. plot and save all ICA components for later manual selection of template 
% components
 
for s = 1:len % for each subject
    % load dataset
    subj{s}= strrep(list(s).name, '.set', '');
    EEG = pop_loadset('filename', [subj{s}, '.set'], 'filepath', PATHIN);
    
    % plot ICA components for each subject and inspect the time course of
    % suspicious components
    pop_selectcomps(EEG, 1:64); %this function was altered - in line 171 of the original code, replace pop_prop with pop_prop_extended
    
    [val EEG.badcomps] = find(EEG.reject.gcompreject == 1 ); %store ind of bad components
    
    % remove selected components from dataset
    EEG = pop_subcomp(EEG, [], 1);
    EEG = eeg_checkset(EEG);
    
    % save ICA cleaned dataset in ana02
    EEG.setname = [subj{s}, '_cleaned'];
    EEG = pop_saveset( EEG, 'filename', [EEG.setname, '.set'], 'filepath',...
        PATHOUT);
    eeglab redraw
end
% end of script