%clear all; close all; clc

%% settings
window_start  = [ 50  50 130 300 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [150 150 230 600 600]; % end of the time window of interest per task. Should contain the desired effect
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','a','b','c','d',};
conditionString = {'cap','earc','ear'};
timeVec = linspace(-200,790,100);

%% set paths
SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
x=1;

for s = 3
    for taskNum = 3%2:5
        for conditionNum = 3%1:3

            timeWin = find(timeVec == window_start(taskNum)) : find(timeVec == window_stop(taskNum)) -1;
            
            if taskNum == 1
                specialN = '_N1';
            else
                specialN = [];
            end
            
            if taskNum == 1
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'*_N1_all.set']);
                    con2path = [];
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att_ear.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt_ear.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
            elseif taskNum == 2
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*unatt.set']); %deviants for the cleaned cap data
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att_ear.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt_ear.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
            elseif taskNum == 3 || taskNum == 4
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev.set']);
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev.set']);
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta_ear.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev_ear.set']);
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
            else taskNum == 5
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_congr.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_incongr.set']);
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_congr.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_incongr.set']);
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum = 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_congr_ear.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_incongr_ear.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
            end
            
            % load data: 2 conditions
            PATHIN  = con1path.folder;
            condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
            condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
            
            if conditionNum == 2 || conditionNum == 3
                condition1 = pop_select( condition1, 'channel',{'E18','E24','E71','E72','E85','E92'});
                condition2 = pop_select( condition2, 'channel',{'E18','E24','E71','E72','E85','E92'});
            end           
            
            % get folder names
            if taskLetter{taskNum} == 'a'
                folderName = 'a_attended_speaker';
            elseif taskLetter{taskNum} == 'b'
                folderName = 'b_passive_oddball';
            elseif taskLetter{taskNum} == 'c'
                folderName = 'c_active_oddball';
            elseif taskLetter{taskNum} == 'd'
                folderName = 'd_conguency';
            else
                error('please enter a valid taskLetter (a-d)')
            end
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,'_loc.mat'],'best_loc');
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,'_bestchans.mat'],'bestchans');
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\g_',conditionString{conditionNum},specialN,'_',num2str(iterations),'.mat'],'effect_size_matrix');
            effect_size_matrix = mean(effect_size_matrix,3);
            
            figure
            plot(condition1.times,abs(effect_size_matrix(best_loc(10),:)))
            
            for p = 1: length(best_loc)
            [pks, locs]= max(abs(effect_size_matrix(best_loc(p),timeWin)));
            [val(p), loc] = max(pks); % find and store the value...
            locsall(p) = locs(loc); % ...and location of the largest peak
            maxTopoLat(p) = condition1.times(locsall(p) + timeWin(1)-1);
            end
            
            EEG = condition1;
            if taskNum ~= 2
                EEG.data = smoothdata(mean(condition2.data,3) - mean(condition1.data,3),2,'movmean',4);
            else
                EEG.data = smoothdata(mean(condition1.data,3) - mean(condition2.data,3),2,'movmean',4);
            end
            EEG.trials = 1;
            if ~isempty(EEG.epoch)
                EEG.epoch = EEG.epoch(1);
            end
                        
            PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '06_topo', filesep,folderName,filesep); % path for script output
            if ~exist (PATHOUT)
                mkdir(PATHOUT)
            end
            EEG.setname = EEG.setname(1:16);
            EEG = pop_saveset(EEG, [EEG.setname,'_',taskLetter{taskNum},'_', conditionString{conditionNum},'_topo.set'], PATHOUT);
            
            figure; pop_timtopo(EEG, [-200  790], [maxTopoLat(1)], 'map');
            %figure;pop_topoplot(EEG, 1, maxTopoLat,'insertgoodtitle',[1 1] ,0,'electrodes','on');

        end
    end
end


efsize  = max(abs(effect_size_matrix),[],2);
efsize = efsize';

