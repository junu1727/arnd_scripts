%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ana03_preprocessing.m
% Script 3: load ICA cleaned data from ana02
%           filter data based on your experimental design
%           epoch data around your experimental events
%           reject contaminated epochs
%           save pre-processed data in PATHOUT
%
%
% Output: pre-processed EEG set-file cleaned, filtered and epoched
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DIRECTORIES
clear; close all;
% define project folder

SBJ = {'p01','p02','p03','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
ear = 0;
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for s = 3%2:length(SBJ)

% locate rawdata-sets
listSBJ = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*']);
listLog = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\log\*.log']);

if ear == 0
    listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\03_ICA_clean\*ica_cleaned.set']);  % reads all .set files in PATHIN
else ear == 1
    listExp = dir(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'*\02_ICA_filt\*_ear.set']);  % reads all .set files in PATHIN
end

%% PARAMETERS
PRUNE = 3;                  % artifact rejection threshold in SD
HP = 1;               % cut-off frequency high-pass filter [Hz]
LP = 30;                % cut-off frequency low-pass filter [Hz]
ev = {'S  9', 'S  1', 'S  2'}; % event marker for epoching
fs = 1000;                  % sampling rate of EEG recording
bslength = 0.2;
baseline = [-0.2*fs  0]; % definition of baseline period
epochlength = 3;

%% start preprocessing of data after ICA cleaning
h=1;
% load in log files to identify stimuli
logs  = loadtxt( [listLog(h).folder,'\',listLog(h).name], 'blankcell', 'off' );
sounds = find(strcmp('Sound',[logs(:,3)]));
logs = logs(sounds,[3 4 5]);
logs(1:10,:) = [];

% set some paths
if ear == 0
    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'03_ICA_clean',filesep); % path containing rawdata
else ear == 1
    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'02_ICA_filt',filesep); % path containing rawdata
end

if h == 1 
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'a_attended_speaker',filesep);    % path for script output
else 
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '04_clean', filesep,'a2_attended_speaker',filesep);    % path for script output
end
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

%load cleaned dataset
subj{s} = listExp(h).name;
EEG = pop_loadset('filename', subj{s}, 'filepath',PATHIN);

EEGsave = EEG; %helps with debugging

%corr_decisions0 = numel(find(strcmp('condition 10',{EEGsave.event.type})));
corr_decisions1 = numel(find(strcmp('condition 11',{EEGsave.event.type}))); % correctly identified alternating tone
corr_decisions2 = numel(find(strcmp('S 13',{EEGsave.event.type}))); % correctly identified ascending tone
corr_decisions3 = numel(find(strcmp('S 14',{EEGsave.event.type}))); % correctly identified descending tone

corr_decision_sum = corr_decisions1 + corr_decisions2 + corr_decisions3;
corr_decisions = 100/length(logs) *corr_decision_sum %percentage of correct identification of asc., des. and alt. tones

if ear ==0
% apply low and high pass filter
EEG = pop_eegfiltnew(EEG,'locutoff',HP,'plotfreqz',1);
EEG = pop_eegfiltnew(EEG,'hicutoff',LP,'plotfreqz',1);
end

% remove unnecessary event marker (all except stim onset and left/right markers)
c=1;
idx = [];
for e = 1 : length(EEG.event)
    if ~ strcmp(EEG.event(e).type,ev)
        idx(c) = e;
        c=c+1;
    end
end

% remove all events which are not the stimulus onset or l/r
% decisions
EEG = pop_editeventvals(EEG,'delete',idx);
EEG = eeg_checkset(EEG);

% if the first event is a stimulus (s9), but without preceding user
% decision (S1/S2), delete it
if strcmp({EEG.event(1).type}, 'S  9') 
    EEG = pop_editeventvals(EEG,'delete',1);
    EEG = eeg_checkset(EEG);
end

% identify left/right decision of participant (S1 = left, S2 = right)
left = zeros(1,length(EEG.event));

for m = 1:2:length(EEG.event) % if trigger = 1, left side was attended. If not, right
    if strcmp(EEG.event(m).type, 'S  1')
        left(m+1) = 1;
    end
end
left(1:2:end) = [];
left = logical(left);

% remove the left/right markers, we don't need them any more
c=1;
idx = [];
for e = 1 : length(EEG.event)
    if ~ strcmp(EEG.event(e).type,ev(1))
        idx(c) = e;
        c=c+1;
    end
end

% remove all events which are not the stimulus onset
EEG = pop_editeventvals(EEG,'delete',idx);
EEG = eeg_checkset(EEG);

%% epoch around the specified onsets of the attended left trials and remove baseline + bad epochs
% attended left
EEGleft = pop_editeventvals(EEG,'delete',~left); % left attended
EEGleft = pop_epoch(EEGleft, ev(1), [baseline(1)/EEG.srate epochlength], 'newname', EEG.setname,'epochinfo', 'no');
EEGleft = pop_rmbase(EEGleft, [EEGleft.times(1) EEGleft.times(bslength*fs+1)]);

EEGleft = pop_jointprob(EEGleft, 1, [1:EEGleft.nbchan], PRUNE, PRUNE, 0, 0 , 0);
EEGleft = eeg_rejsuperpose( EEGleft, 1, 1, 1, 1, 1, 1, 1, 1);
rej_ep(s,1) = {find(EEGleft.reject.rejglobal == 1)};
EEGleft = pop_rejepoch( EEGleft, EEGleft.reject.rejglobal ,0);
EEGleft.times = baseline(1):1:epochlength*EEG.srate-1;

% attended right
EEGright = pop_editeventvals(EEG,'delete',left) % right attended
EEGright = pop_epoch(EEGright, ev(1), [baseline(1)/EEG.srate epochlength], 'newname', EEG.setname,'epochinfo', 'no');
EEGright = pop_rmbase(EEGright, [EEGright.times(1) EEGright.times(bslength*fs+1)]);

EEGright = pop_jointprob(EEGright, 1, [1:EEGright.nbchan], PRUNE, PRUNE, 0, 0 , 0);
EEGright = eeg_rejsuperpose( EEGright, 1, 1, 1, 1, 1, 1, 1, 1);
rej_ep(s,1) = {find(EEGright.reject.rejglobal == 1)};
EEGright = pop_rejepoch( EEGright, EEGright.reject.rejglobal ,0);
EEGright.times = baseline(1):1:epochlength*EEG.srate-1;

%% save pruned and epoched data set for attended left and right separately
EEGleft = eeg_checkset(EEGleft);
if ear == 0
    subj{s}= strrep(listExp(h).name, '_ica_cleaned.set', '_attended_left');
else ear == 1
    subj{s}= strrep(listExp(h).name, '_ica_ear.set', '_attended_left_ear');
end
EEGleft = pop_saveset(EEGleft, [subj{s}, '.set'], PATHOUT);

EEGright = eeg_checkset(EEGright);
if ear == 0
    subj{s}= strrep(listExp(h).name, '_ica_cleaned.set', '_attended_right');
else ear == 1
    subj{s}= strrep(listExp(h).name, '_ica_ear.set', '_attended_right_ear');
end
EEGright = pop_saveset(EEGright, [subj{s}, '.set'], PATHOUT);

save([PATHOUT,'rej_ep.m'],'rej_ep'); % rej_ep is now:
%[ 1   2   3   4   5   6   7   8   9   10  11  12  13  14]
% la1 ru1 la2 ru2 la3 ru3 ru4 lu1 ra1 lu2 ra2 lu3 ra3 ra4
%l = left, r = right, a = attended, u = unattended, 1-4 = tone number

% end of script

end

% PS: remove the BadTrials- entry from the protocol if necessary(no idea why it marks everything as bad)

% if h==1
%     listBad = dir(['C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\CvsC2\data\a_attended_speaker\',SBJ{s},'*']);
% else
%     listBad = dir('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\CvsC\data\a2_attended_speaker\p*');    
% end
% 
% for xp = 1:length(listBad)
%     z = load([listBad(xp).folder,'\',listBad(xp).name,'\brainstormstudy.mat']);
%     z.BadTrials = [];
%     save([listBad(xp).folder,'\',listBad(xp).name,'\brainstormstudy.mat'],'z');
% end




