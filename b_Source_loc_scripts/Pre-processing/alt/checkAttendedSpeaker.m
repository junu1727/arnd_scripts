%close all; 
%clear all; clc

%% settings
SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj

s = 4;
ear = 0;

if ear == 0 
    naming = [];  
else
    naming = '_ear';
end

%% paths
CHECKPATH = ['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\04_clean\a_attended_speaker\checkdata\'];
PATHIN = ['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\04_clean\a_attended_speaker\'];
CHANPATH = ['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\a_attended_speaker\'];

cap_epoch = dir([CHECKPATH,'*epoch.set*']);
cap_full = dir([CHECKPATH,'*full.set*']);
ear_epoch = dir([CHECKPATH,'*epoch_ear.set*']);
ear_full = dir([CHECKPATH,'*full.set*']);

att_cap_stage3 = dir([PATHIN,'*_att.set*']);
unatt_cap_stage3 = dir([PATHIN,'*_unatt.set*']);
att_ear_stage3 = dir([PATHIN,'*_att_ear.set*']);
unatt_ear_stage3 = dir([PATHIN,'*_unatt_ear.set*']);

%% load files

% CAP

% full epochs, STAGE 0
la_cap_stage0 = pop_loadset([cap_full(1).folder,'\',cap_full(1).name]);
ra_cap_stage0 = pop_loadset([cap_full(1).folder,'\',cap_full(2).name]);

% single tones, no average for l/r OR att/unatt, STAGE 1
cap_stage1 = load([CHECKPATH,SBJ{s},'_singletones.mat'],'la_stage1','ra_stage1','lu_stage1','ru_stage1');

% average over single tones, STAGE 2
la_cap_stage2 = pop_loadset([cap_epoch(1).folder,'\',cap_epoch(1).name]);
ra_cap_stage2 = pop_loadset([cap_epoch(1).folder,'\',cap_epoch(2).name]);

% att/unatt, final. Average over tones and l/r, STAGE 3
att_cap_stage3 = pop_loadset([att_cap_stage3(1).folder,'\',att_cap_stage3(1).name]);
unatt_cap_stage3 = pop_loadset([unatt_cap_stage3(1).folder,'\',unatt_cap_stage3(1).name]);

% EAR

% full epochs, STAGE 0
la_ear_stage0 = pop_loadset([ear_full(1).folder,'\',ear_full(1).name]);
ra_ear_stage0 = pop_loadset([ear_full(1).folder,'\',ear_full(2).name]);

% single tones, no average for l/r OR att/unatt, STAGE 1
ear_stage1 = load([CHECKPATH,SBJ{s},'_singletones_ear.mat'],'la_stage1','ra_stage1','lu_stage1','la_stage1');

% average over single tones, STAGE 2
la_ear_stage2 = pop_loadset([ear_epoch(1).folder,'\',ear_epoch(1).name]);
ra_ear_stage2 = pop_loadset([ear_epoch(1).folder,'\',ear_epoch(2).name]);

% att/unatt, final. Average over tones and l/r, STAGE 3
att_ear_stage3 = pop_loadset([att_ear_stage3(1).folder,'\',att_ear_stage3(1).name]);
unatt_ear_stage3 = pop_loadset([unatt_ear_stage3(1).folder,'\',unatt_ear_stage3(1).name]);



%% load channel infos
cap_chan = load([CHANPATH,'cap_bestchans.mat']);
ear_chan = load([CHANPATH,'ear_bestchans.mat']);


%% get mean over trials
la_cap_stage0 = mean(la_cap_stage0.data,3);
ra_cap_stage0 = mean(ra_cap_stage0.data,3);

la_ear_stage0 = mean(la_ear_stage0.data,3);
ra_ear_stage0 = mean(ra_ear_stage0.data,3);

la_cap_stage2 = mean(la_cap_stage2.data,3);
ra_cap_stage2 = mean(ra_cap_stage2.data,3);

la_ear_stage2 = mean(la_ear_stage2.data,3);
ra_ear_stage2 = mean(ra_ear_stage2.data,3);

att_cap_stage3 = mean(att_cap_stage3.data,3);
unatt_cap_stage3 = mean(unatt_cap_stage3.data,3);
att_ear_stage3 = mean(att_ear_stage3.data,3);
unatt_ear_stage3 = mean(unatt_ear_stage3.data,3);

%% settings
fulltime = linspace(-200,3000,3200);
epochtime = linspace(0,299,300);
epochT = 200:499;

rightSide = [600 1200 1800 2400]+340;
leftSide = [750 1500 2250]+340;

chan1 = str2num(cap_chan.bestchans{1}(2:3))
chan2 = str2num(cap_chan.bestchans{1}(8:9))

%% plots

%% STAGE 0
figure
plot(fulltime,smoothdata(la_cap_stage0(chan1,:)-la_cap_stage0(chan2,:),2,'movmean',40),'o-','MarkerFaceColor','blue','MarkerEdgeColor','blue','MarkerIndices',leftSide)
hold on
plot(fulltime,smoothdata(ra_cap_stage0(chan1,:)-ra_cap_stage0(chan2,:),2,'movmean',40),'o-','MarkerFaceColor','red','MarkerEdgeColor','red','MarkerIndices',rightSide)
legend('attended left','attended right')
xlim([-200 3000])

%% STAGE 1
figure
subplot(2,2,1)
plot(epochtime,smoothdata(cap_stage1.la_stage1.EEGla1(chan1,epochT)-cap_stage1.la_stage1.EEGla1(chan2,epochT),2,'movmean',40))
hold on
plot(epochtime,smoothdata(cap_stage1.la_stage1.EEGla2(chan1,epochT)-cap_stage1.la_stage1.EEGla2(chan2,epochT),2,'movmean',40))
plot(epochtime,smoothdata(cap_stage1.la_stage1.EEGla3(chan1,epochT)-cap_stage1.la_stage1.EEGla3(chan2,epochT),2,'movmean',40))
xline(140,'--b')
legend('tone1','tone2','tone3')
title('left attended tones')
ylabel('Amplitude [�V]')
xlabel('time [ms]')

subplot(2,2,2)
plot(epochtime,smoothdata(cap_stage1.lu_stage1.EEGlu1(chan1,epochT)-cap_stage1.lu_stage1.EEGlu1(chan2,epochT),2,'movmean',40))
hold on
plot(epochtime,smoothdata(cap_stage1.lu_stage1.EEGlu2(chan1,epochT)-cap_stage1.lu_stage1.EEGlu2(chan2,epochT),2,'movmean',40))
plot(epochtime,smoothdata(cap_stage1.lu_stage1.EEGlu3(chan1,epochT)-cap_stage1.lu_stage1.EEGlu3(chan2,epochT),2,'movmean',40))
xline(140,'--b')
legend('tone1','tone2','tone3')
title('left unattended tones')
ylabel('Amplitude [�V]')
xlabel('time [ms]')

subplot(2,2,3)
plot(epochtime,smoothdata(cap_stage1.ra_stage1.EEGra1(chan1,epochT)-cap_stage1.ra_stage1.EEGra1(chan2,epochT),2,'movmean',40))
hold on
plot(epochtime,smoothdata(cap_stage1.ra_stage1.EEGra2(chan1,epochT)-cap_stage1.ra_stage1.EEGra2(chan2,epochT),2,'movmean',40))
plot(epochtime,smoothdata(cap_stage1.ra_stage1.EEGra3(chan1,epochT)-cap_stage1.ra_stage1.EEGra3(chan2,epochT),2,'movmean',40))
plot(epochtime,smoothdata(cap_stage1.ra_stage1.EEGra4(chan1,epochT)-cap_stage1.ra_stage1.EEGra4(chan2,epochT),2,'movmean',40))
xline(140,'--r')
legend('tone1','tone2','tone3','tone4')
title('right attended tones')
ylabel('Amplitude [�V]')
xlabel('time [ms]')

subplot(2,2,4)
plot(epochtime,smoothdata(cap_stage1.ru_stage1.EEGru1(chan1,epochT)-cap_stage1.ru_stage1.EEGru1(chan2,epochT),2,'movmean',40))
hold on
plot(epochtime,smoothdata(cap_stage1.ru_stage1.EEGru2(chan1,epochT)-cap_stage1.ru_stage1.EEGru2(chan2,epochT),2,'movmean',40))
plot(epochtime,smoothdata(cap_stage1.ru_stage1.EEGru3(chan1,epochT)-cap_stage1.ru_stage1.EEGru3(chan2,epochT),2,'movmean',40))
plot(epochtime,smoothdata(cap_stage1.ru_stage1.EEGru4(chan1,epochT)-cap_stage1.ru_stage1.EEGru4(chan2,epochT),2,'movmean',40))
xline(140,'--r')
legend('tone1','tone2','tone3','tone4')
title('right unattended tones')
ylabel('Amplitude [�V]')
xlabel('time [ms]')
sgtitle(['Difference waves of single tones, ',SBJ{s}])

%% STAGE 2
figure
plot(epochtime,smoothdata(la_cap_stage2(chan1,epochT)-la_cap_stage2(chan2,epochT),2,'movmean',40))
hold on
plot(epochtime,smoothdata(ra_cap_stage2(chan1,epochT)-ra_cap_stage2(chan2,epochT),2,'movmean',40))
xline(140,'--k')
legend('left attended','right attended')
title(['Average over single tones, left and right, ',SBJ{s}])
ylabel('Amplitude [�V]')
xlabel('time [ms]')

%% STAGE 3
figure
plot(epochtime,smoothdata(att_cap_stage3(chan1,epochT)-att_cap_stage3(chan2,epochT),2,'movmean',40))
hold on
plot(epochtime,smoothdata(unatt_cap_stage3(chan1,epochT)-unatt_cap_stage3(chan2,epochT),2,'movmean',40))
xline(140,'--k')
legend('attended','unattended')
title(['Average over tones and left and right, ',SBJ{s}])
ylabel('Amplitude [�V]')
xlabel('time [ms]')







