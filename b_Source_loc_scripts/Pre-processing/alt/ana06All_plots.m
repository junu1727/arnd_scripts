%clear all; close all; clc

%% settings
unilateral    = 1; %if 0, use both ears. If 1, use only right ear
window_start  = [ 50  50 130 300 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [150 150 230 600 600]; % end of the time window of interest per task. Should contain the desired effect
srate         = 100; % downsample to ...
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','a','b','c','d',};
staElec    = [8 8 8 8]; % channel that is usually used to detect the task effect in the literature
conditionString = {'cap','earc','ear'};
special_N1 = 1; % do plots for the N1 of the attended speaker task
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,250,20);
%% set paths and load data
SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

%% plots for g
for s = 3:4
    for taskNum = 2:5
        for conditionNum = 1:3
            
            timeWin = find(timeVec == window_start(taskNum)) : find(timeVec == window_stop(taskNum)) -1;
            
            if taskNum == 1
                specialN = '_N1';
            else
                specialN = [];
            end
            
            if taskNum == 1
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'*_N1_all.set']);
                    con2path = [];
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att_ear.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt_ear.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
            elseif taskNum == 2
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*unatt.set']); %deviants for the cleaned cap data
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_att_ear.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*_unatt_ear.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
            elseif taskNum == 3 || taskNum == 4
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev.set']);
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev.set']);
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*sta_ear.set']);
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*dev_ear.set']);
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
            else taskNum == 5
                if conditionNum == 1
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_congr.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_incongr.set']); %deviants for the cleaned cap data
                    FullorEar     = 1; % 1 = full, 2 = ear
                elseif conditionNum == 2
                    %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_congr.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_incongr.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                else conditionNum == 3
                    %get ear data sets
                    con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_congr_ear.set']); %standards for the cleaned cap data
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{taskNum},'_*1_incongr_ear.set']); %deviants for the cleaned cap data
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
            end
            
            % load data: 2 conditions
            if conditionNum ~= 1
                PATHIN  = con1path.folder;
                condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                if ~isempty(special_N1)
                    condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
                else
                    condition2 = [];
                end
            end
            
            % get folder names
            if taskLetter{taskNum} == 'a'
                folderName = 'a_attended_speaker';
            elseif taskLetter{taskNum} == 'b'
                folderName = 'b_passive_oddball';
            elseif taskLetter{taskNum} == 'c'
                folderName = 'c_active_oddball';
            elseif taskLetter{taskNum} == 'd'
                folderName = 'd_conguency';
            else
                error('please enter a valid taskLetter (a-d)')
            end
            
            %             folderName = 'a_attended_speaker';
            %             specialN = [];
            %             conditionNum = 1;
            %             taskNum = 4;
            
            % load the effect sizes, channel*time*iteration
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\trials_',conditionString{conditionNum},specialN,'.mat'],'trials');
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\g_',conditionString{conditionNum},specialN,'_',num2str(iterations),'.mat'],'effect_size_matrix');
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,'_loc.mat'],'best_loc');
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,'_bestchans.mat'],'bestchans');
            load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,'_ERPs.mat'],'ERPs');
            if conditionNum == 1 % if the cap was used, also save the effect size of its standard channel
                load(['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\',folderName,'\g_standardcap_effect_',num2str(iterations),specialN,'.mat'],'effect_size_matrix_standard');
            end
            
            %transform the ERPs to be in the right direction (because of the way we get all bipolar channels,
            %the same channel is sometimes subtracted and sometimes subtracted from. Therefore, the sign is sometimes reversed)
            if taskNum == 1
                timeWin = 1:size(effect_size_matrix,2);
            end
            
            if taskLetter{taskNum} ~= 'c'
                [pks, locs]= max(abs(smoothdata(mean(effect_size_matrix(best_loc(1),timeWin,:),3),2,'movmean',4)));
                [val, loc] = max(pks); % find and store the value...
                locs = locs(loc); % ...and location of the largest peak
                
                if smoothdata(mean(ERPs.cond1(1,timeWin(1)+locs-1,:),3) - mean(ERPs.cond2(1,timeWin(1)+locs-1,:),3),2,'movmean',4) > 0
                    ERPs.cond1 = ERPs.cond1 * -1;
                    ERPs.cond2 = ERPs.cond2 * -1;
                end
            else
                [pks, locs]= max(abs(smoothdata(mean(effect_size_matrix(best_loc(1),timeWin,:),3),2,'movmean',4)));
                [val, loc] = max(pks); % find and store the value...
                locs = locs(loc); % ...and location of the largest peak
                if smoothdata(mean(ERPs.cond1(1,timeWin(1)+locs-1,:),3) - mean(ERPs.cond2(1,timeWin(1)+locs-1,:),3),2,'movmean',4) < 0
                    ERPs.cond1 = ERPs.cond1 * -1;
                    ERPs.cond2 = ERPs.cond2 * -1;
                end
            end
            
            %%%%%% Topoplots
            if conditionNum == 1
                EEG = condition1;
                EEG.data = smoothdata(mean(condition2.data,3) - mean(EEG.data,3),2,'movmean',4);
                EEG.trials = 1;
                if ~isempty(EEG.epoch)
                    EEG.epoch = EEG.epoch(1);
                end
                figure; pop_timtopo(EEG, [-200  790], [NaN], 'ERP data and scalp maps of p03_b_03.02.2021_dev');
            end
            %%%%%%%%%%%%%%%
            
            if taskNum == 1
                if conditionNum == 1
                    effect_cap.aN1.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_cap.aN1.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration, mean over iterations
                    effect_cap.aN1.best_loc           = best_loc; % indices of the best n channels
                    effect_cap.aN1.bestchans          = bestchans; % string names of the n best channels
                    effect_cap.aN1.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                elseif conditionNum == 2
                    effect_earc.aN1.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_earc.aN1.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_earc.aN1.best_loc           = best_loc; % indices of the best n channels
                    effect_earc.aN1.bestchans          = bestchans; % string names of the n best channels
                    effect_earc.aN1.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                else conditionNum == 3
                    effect_ear.aN1.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_ear.aN1.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_ear.aN1.best_loc           = best_loc; % indices of the best n channels
                    effect_ear.aN1.bestchans          = bestchans; % string names of the n best channels
                    effect_ear.aN1.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                end
            elseif taskNum == 2
                if conditionNum == 1
                    effect_cap.a.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_cap.a.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_cap.a.best_loc           = best_loc; % indices of the best n channels
                    effect_cap.a.bestchans          = bestchans; % string names of the n best channels
                    effect_cap.a.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                elseif conditionNum == 2
                    effect_earc.a.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_earc.a.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_earc.a.best_loc           = best_loc; % indices of the best n channels
                    effect_earc.a.bestchans          = bestchans; % string names of the n best channels
                    effect_earc.a.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                else conditionNum == 3
                    effect_ear.a.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_ear.a.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_ear.a.best_loc           = best_loc; % indices of the best n channels
                    effect_ear.a.bestchans          = bestchans; % string names of the n best channels
                    effect_ear.a.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                end
                
            elseif taskNum == 3
                if conditionNum == 1
                    effect_cap.b.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_cap.b.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_cap.b.best_loc           = best_loc; % indices of the best n channels
                    effect_cap.b.bestchans          = bestchans; % string names of the n best channels
                    effect_cap.b.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                elseif conditionNum == 2
                    effect_earc.b.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_earc.b.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_earc.b.best_loc           = best_loc; % indices of the best n channels
                    effect_earc.b.bestchans          = bestchans; % string names of the n best channels
                    effect_earc.b.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                else conditionNum == 3
                    effect_ear.b.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_ear.b.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_ear.b.best_loc           = best_loc; % indices of the best n channels
                    effect_ear.b.bestchans          = bestchans; % string names of the n best channels
                    effect_ear.b.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                end
            elseif taskNum == 4
                if conditionNum == 1
                    effect_cap.c.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_cap.c.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_cap.c.best_loc           = best_loc; % indices of the best n channels
                    effect_cap.c.bestchans          = bestchans; % string names of the n best channels
                    effect_cap.c.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                elseif conditionNum == 2
                    effect_earc.c.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_earc.c.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_earc.c.best_loc           = best_loc; % indices of the best n channels
                    effect_earc.c.bestchans          = bestchans; % string names of the n best channels
                    effect_earc.c.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                else conditionNum == 3
                    effect_ear.c.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_ear.c.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_ear.c.best_loc           = best_loc; % indices of the best n channels
                    effect_ear.c.bestchans          = bestchans; % string names of the n best channels
                    effect_ear.c.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                end
            else taskNum == 5
                if conditionNum == 1
                    effect_cap.d.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_cap.d.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_cap.d.best_loc           = best_loc; % indices of the best n channels
                    effect_cap.d.bestchans          = bestchans; % string names of the n best channels
                    effect_cap.d.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                elseif conditionNum == 2
                    effect_earc.d.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_earc.d.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_earc.d.best_loc           = best_loc; % indices of the best n channels
                    effect_earc.d.bestchans          = bestchans; % string names of the n best channels
                    effect_earc.d.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                else conditionNum == 3
                    effect_ear.d.trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                    effect_ear.d.es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                    effect_ear.d.best_loc           = best_loc; % indices of the best n channels
                    effect_ear.d.bestchans          = bestchans; % string names of the n best channels
                    effect_ear.d.ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                end
            end
        end
    end
    
    aN1_cap = smoothdata(abs(effect_cap.aN1.es_matrix_mean(effect_cap.aN1.best_loc(1),:,:)),2,'movmean',4);
    aN1_earc = smoothdata(abs(effect_earc.aN1.es_matrix_mean(effect_earc.aN1.best_loc(1),:,:)),2,'movmean',4);
    aN1_ear = smoothdata(abs(effect_ear.aN1.es_matrix_mean(effect_ear.aN1.best_loc(1),:,:)),2,'movmean',4);
    
    a_cap = smoothdata(abs(effect_cap.a.es_matrix_mean(effect_cap.a.best_loc(1),:,:)),2,'movmean',4);
    a_earc = smoothdata(abs(effect_earc.a.es_matrix_mean(effect_earc.a.best_loc(1),:,:)),2,'movmean',4);
    a_ear = smoothdata(abs(effect_ear.a.es_matrix_mean(effect_ear.a.best_loc(1),:,:)),2,'movmean',4);
    
    b_cap = smoothdata(abs(effect_cap.b.es_matrix_mean(effect_cap.b.best_loc(1),:,:)),2,'movmean',4);
    b_earc = smoothdata(abs(effect_earc.b.es_matrix_mean(effect_earc.b.best_loc(1),:,:)),2,'movmean',4);
    b_ear = smoothdata(abs(effect_ear.b.es_matrix_mean(effect_ear.b.best_loc(1),:,:)),2,'movmean',4);
    
    c_cap = smoothdata(abs(effect_cap.c.es_matrix_mean(effect_cap.c.best_loc(1),:,:)),2,'movmean',4);
    c_earc = smoothdata(abs(effect_earc.c.es_matrix_mean(effect_earc.c.best_loc(1),:,:)),2,'movmean',4);
    c_ear = smoothdata(abs(effect_ear.c.es_matrix_mean(effect_ear.c.best_loc(1),:,:)),2,'movmean',4);
    
    d_cap = smoothdata(abs(effect_cap.d.es_matrix_mean(effect_cap.d.best_loc(1),:,:)),2,'movmean',4);
    d_earc = smoothdata(abs(effect_earc.d.es_matrix_mean(effect_earc.d.best_loc(1),:,:)),2,'movmean',4);
    d_ear = smoothdata(abs(effect_ear.d.es_matrix_mean(effect_ear.d.best_loc(1),:,:)),2,'movmean',4);
    
    aN1_capd = smoothdata(mean(effect_cap.aN1.ERPs.cond1(1,:,:),3) - mean(effect_cap.aN1.ERPs.cond2(1,:,:),3),2,'movmean',4);
    aN1_earcd = smoothdata(mean(effect_earc.aN1.ERPs.cond1(1,:,:),3) - mean(effect_earc.aN1.ERPs.cond2(1,:,:),3),2,'movmean',4);
    aN1_eard = smoothdata(mean(effect_ear.aN1.ERPs.cond1(1,:,:),3) - mean(effect_ear.aN1.ERPs.cond2(1,:,:),3),2,'movmean',4);
    
    a_capd = smoothdata(mean(effect_cap.a.ERPs.cond1(1,:,:),3) - mean(effect_cap.a.ERPs.cond2(1,:,:),3),2,'movmean',4);
    a_earcd = smoothdata(mean(effect_earc.a.ERPs.cond1(1,:,:),3) - mean(effect_earc.a.ERPs.cond2(1,:,:),3),2,'movmean',4);
    a_eard = smoothdata(mean(effect_ear.a.ERPs.cond1(1,:,:),3) - mean(effect_ear.a.ERPs.cond2(1,:,:),3),2,'movmean',4);
    
    b_capd = smoothdata(mean(effect_cap.b.ERPs.cond1(1,:,:),3) - mean(effect_cap.b.ERPs.cond2(1,:,:),3),2,'movmean',4);
    b_earcd = smoothdata(mean(effect_earc.b.ERPs.cond1(1,:,:),3) - mean(effect_earc.b.ERPs.cond2(1,:,:),3),2,'movmean',4);
    b_eard = smoothdata(mean(effect_ear.b.ERPs.cond1(1,:,:),3) - mean(effect_ear.b.ERPs.cond2(1,:,:),3),2,'movmean',4);
    
    c_capd = smoothdata(mean(effect_cap.c.ERPs.cond1(1,:,:),3) - mean(effect_cap.c.ERPs.cond2(1,:,:),3),2,'movmean',4);
    c_earcd = smoothdata(mean(effect_earc.c.ERPs.cond1(1,:,:),3) - mean(effect_earc.c.ERPs.cond2(1,:,:),3),2,'movmean',4);
    c_eard = smoothdata(mean(effect_ear.c.ERPs.cond1(1,:,:),3) - mean(effect_ear.c.ERPs.cond2(1,:,:),3),2,'movmean',4);
    
    d_capd = smoothdata(mean(effect_cap.d.ERPs.cond1(1,:,:),3) - mean(effect_cap.d.ERPs.cond2(1,:,:),3),2,'movmean',4);
    d_earcd = smoothdata(mean(effect_earc.d.ERPs.cond1(1,:,:),3) - mean(effect_earc.d.ERPs.cond2(1,:,:),3),2,'movmean',4);
    d_eard = smoothdata(mean(effect_ear.d.ERPs.cond1(1,:,:),3) - mean(effect_ear.d.ERPs.cond2(1,:,:),3),2,'movmean',4);
    
    figure
    %N1_FIRST, effect size
    subplot(2,5,1)
    plot(timeVecN1,aN1_cap)
    hold on
    plot(timeVecN1,aN1_earc)
    plot(timeVecN1,aN1_ear)
    
    ylim([0 2])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N1 (att/unatt)','FontSize', 12, 'FontWeight', 'bold')
    
    %N1, effect size
    subplot(2,5,2)
    plot(timeVec,a_cap)
    hold on
    plot(timeVec,a_earc)
    plot(timeVec,a_ear)
    
    ylim([0 1.2])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('Mult. speaker (att/unatt)','FontSize', 12, 'FontWeight', 'bold')
    legend('cap','ear cleaned','ear','FontSize', 12, 'FontWeight', 'bold','Location','northeast')
    
    %MMN, effect size
    subplot(2,5,3)
    plot(timeVec,b_cap)
    hold on
    plot(timeVec,b_earc)
    plot(timeVec,b_ear)
    
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('MMN (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %P300, effect size
    subplot(2,5,4)
    plot(timeVec,c_cap);
    hold on
    plot(timeVec,c_earc);
    plot(timeVec,c_ear);
    
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('P300 (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %N400, effect size
    subplot(2,5,5)
    plot(timeVec,d_cap)
    hold on
    plot(timeVec,d_earc)
    plot(timeVec,d_ear)
    
    ylim([0 0.8])
    ylabel('Hedge�s g [abs.]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N400 (congr/incongr)','FontSize', 12, 'FontWeight', 'bold')
    
    %N1_FIRST, difference wave
    subplot(2,5,6)
    plot(timeVecN1,aN1_capd)
    hold on
    plot(timeVecN1,aN1_earcd)
    plot(timeVecN1,aN1_eard)
    
    ylim([-12 12])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N1 (att/unatt)','FontSize', 12, 'FontWeight', 'bold')
    
    %N1, difference wave
    subplot(2,5,7)
    plot(timeVec,a_capd)
    hold on
    plot(timeVec,a_earcd)
    plot(timeVec,a_eard)
    
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('Mult. speaker (att/unatt)','FontSize', 12, 'FontWeight', 'bold')
    
    %MMN, difference wave
    subplot(2,5,8)
    plot(timeVec,b_capd)
    hold on
    plot(timeVec,b_earcd)
    plot(timeVec,b_eard)
    
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('MMN (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %P300, difference wave
    subplot(2,5,9)
    plot(timeVec,c_capd)
    hold on
    plot(timeVec,c_earcd)
    plot(timeVec,c_eard)
    
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('P300 (sta/dev)','FontSize', 12, 'FontWeight', 'bold')
    
    %N400, difference wave
    subplot(2,5,10)
    plot(timeVec,d_capd)
    hold on
    plot(timeVec,d_earcd)
    plot(timeVec,d_eard)
    
    ylim([-7 7])
    ylabel('Difference wave [�V]','FontSize', 12, 'FontWeight', 'bold')
    xlabel('time [ms]','FontSize', 12, 'FontWeight', 'bold')
    title('N400 (congr/incongr)','FontSize', 12, 'FontWeight', 'bold')
    sgtitle(['Best channel per data set, ',SBJ{s}],'FontSize', 20, 'FontWeight', 'bold')
    set(gcf,'Color','w')
end