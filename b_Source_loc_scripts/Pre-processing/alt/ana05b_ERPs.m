%% calculate and compare the best electrodes with the standards + a comparison between cap and ear
% Task b: passive auditory oddball

clear all; close all; clc

% set paths and subject list
SBJ = {'p01','p02','p03','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

% settings
s = 3;
PATHOUT = ['O:\arm_testing\Experimente\cEEGrid_vs_Cap\data\',SBJ{s},'\05_ERP\b_passive_oddball\'];
unilateral =1; %if 0, use both ears. If 1, use only right ear
window_start = 125; % time window of interest. Should contain the desired effect
window_stop = 225;

%get all datasets from the passive oddball task
devcappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_b_*dev.set']); %deviants for the cleaned cap data
stacappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_b_*sta.set']); %standards for the cleaned cap data
devearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_b_*dev_ear.set']); %deviants for the cleaned ear data
staearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_b_*sta_ear.set']); %standards for the cleaned ear data

% set some paths
PATHIN  = devcappath.folder; 
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,'b_passive_oddball',filesep); % path for script output
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

%load cap data: dev, sta
dev_cap = pop_loadset('filename', devcappath.name, 'filepath',...
    PATHIN);
sta_cap = pop_loadset('filename', stacappath.name, 'filepath',...
    PATHIN);
%load ear data: dev, sta
dev_ear = pop_loadset('filename', devearpath.name, 'filepath',...
    PATHIN);
sta_ear = pop_loadset('filename', staearpath.name, 'filepath',...
    PATHIN);
%load cap data for clean ear data: dev, sta
clean_dev_ear = pop_loadset('filename', devcappath.name, 'filepath',...
    PATHIN);
clean_sta_ear = pop_loadset('filename', stacappath.name, 'filepath',...
    PATHIN); 

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data 
clean_dev_ear = pop_select( clean_dev_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_dev_ear = pop_reref( clean_dev_ear, find(strcmp({clean_dev_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear
clean_sta_ear = pop_select( clean_sta_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_sta_ear = pop_reref( clean_sta_ear, find(strcmp({clean_sta_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear

if unilateral == 1 % if only one ear is of interest, delete the other electrodes for both ear- datasets (ICA-cleaned and uncleaened)
    clean_dev_ear = pop_select( clean_dev_ear, 'channel',{'E18','E24','E71','E72','E85'});
    clean_sta_ear = pop_select( clean_sta_ear, 'channel',{'E18','E24','E71','E72','E85'});
    dev_ear       = pop_select( dev_ear,       'channel',{'E18','E24','E71','E72','E85'});
    sta_ear       = pop_select( sta_ear,       'channel',{'E18','E24','E71','E72','E85'});
end

% parameters 
timeStart = find(dev_cap.times == window_start); 
timeEnd   = find(dev_cap.times == window_stop) -1;
timewin = timeStart:timeEnd; % recommended time window from ERP-Core for MMN
fs = dev_cap.srate; %sampling rate of all datasets. Should always be the same
sta_elecs = [ 1 8 51 68 84]; % some electrodes that are usually used to detect MMN in the literature


%% ERPs best_cap
dev_cap1 = dev_cap;
sta_cap1 = sta_cap;

dev_cap1.data = mean(dev_cap.data,3);
sta_cap1.data = mean(sta_cap.data,3);

best_cap_dev = mbl_bipolarchannelexpansion(dev_cap1);
best_cap_sta = mbl_bipolarchannelexpansion(sta_cap1);

diff_best_cap = best_cap_sta;
diff_best_cap.data = best_cap_dev.data - best_cap_sta.data;

maxCap = [];
capVal = [];
for c = 1:size(diff_best_cap.data,1)
[maxCap(c).pks maxCap(c).locs] = findpeaks(diff_best_cap.data(c,timewin)*-1); % find the negative peaks
[capVal(c).val loc] = max(maxCap(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxCap(c).locs(loc);
end

best_chan_cap = find(max([capVal.val])==[capVal.val]); %identify the channel with the highest diff. between sta and dev tones
bestchans.cap = best_cap_dev.chanlocs(best_chan_cap).labels; % this channel has the largest diff. amplitude between sta and dev

best_chan_cap_sta = sta_cap1.data(str2num(bestchans.cap(2:3)),:) - sta_cap1.data(str2num(bestchans.cap(end-1:end)),:);
best_chan_cap_dev = dev_cap1.data(str2num(bestchans.cap(2:3)),:) - dev_cap1.data(str2num(bestchans.cap(end-1:end)),:);
best_chan_cap_diff = best_chan_cap_dev - best_chan_cap_sta ;

% figure
% plot(dev_cap1.times,best_chan_cap_sta)
% hold on
% plot(dev_cap1.times,best_chan_cap_dev)
% plot(dev_cap1.times,best_chan_cap_diff)
% legend('sta','dev','diff')
% title(['Best cap channel: ',bestchans.cap ])

%% ERPs sta_cap
bestchans.sta_cap = sta_cap1.chanlocs(sta_elecs(2)).labels; 

best_chan_stacap_sta = sta_cap1.data(sta_elecs(2),:);
best_chan_stacap_dev = dev_cap1.data(sta_elecs(2),:);
best_chan_stacap_diff = best_chan_stacap_dev - best_chan_stacap_sta;

chan1.sta_cap = bestchans.sta_cap;

% figure
% plot(dev_cap1.times,best_chan_stacap_sta)
% hold on
% plot(dev_cap1.times,best_chan_stacap_dev)
% plot(dev_cap1.times,best_chan_stacap_diff)
% legend('sta','dev','diff')
% title(['Standard cap channel: ',bestchans.sta_cap,' - nose ref.' ])

%% ERPs clean ear
clean_dev_ear1 = clean_dev_ear;
clean_sta_ear1 = clean_sta_ear;

clean_dev_ear1.data = mean(clean_dev_ear.data,3);
clean_sta_ear1.data = mean(clean_sta_ear.data,3);

best_cleanear_dev = mbl_bipolarchannelexpansion(clean_dev_ear1);
best_cleanear_sta = mbl_bipolarchannelexpansion(clean_sta_ear1);

diff_best_cleanear = best_cleanear_dev;
diff_best_cleanear.data = best_cleanear_dev.data - best_cleanear_sta.data;

maxEar = [];
capVal = [];
for c = 1:size(diff_best_cleanear.data,1)
[maxEar(c).pks maxEar(c).locs] = findpeaks(diff_best_cleanear.data(c,timewin)*-1); % find the negative peaks
[capVal(c).val loc] = max(maxEar(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxEar(c).locs(loc);
end

best_chan_cleanear = find(max([capVal.val])==[capVal.val]); % identify the channel with the highest diff. between sta and dev tones
bestchans.cleanear = best_cleanear_dev.chanlocs(best_chan_cleanear).labels; % this channel has the largest diff. amplitude between sta and dev

chan1.clean_ear = find(strcmp({clean_sta_ear1.chanlocs.labels},bestchans.cleanear(1:3)));
chan2.clean_ear = find(strcmp({clean_sta_ear1.chanlocs.labels},bestchans.cleanear(end-2:end)));

best_chan_cleanear_sta = clean_sta_ear1.data(chan1.clean_ear,:) - clean_sta_ear1.data(chan2.clean_ear,:);
best_chan_cleanear_dev = clean_dev_ear1.data(chan1.clean_ear,:) - clean_dev_ear1.data(chan2.clean_ear,:);
best_chan_cleanear_diff = best_chan_cleanear_dev - best_chan_cleanear_sta;

% figure
% plot(dev_cap1.times,best_chan_cleanear_sta)
% hold on
% plot(dev_cap1.times,best_chan_cleanear_dev)
% plot(dev_cap1.times,best_chan_cleanear_diff)
% legend('sta','dev','diff')
% title(['Best clean ear channel: ',bestchans.cleanear ])

%% ERPs ear
dev_ear1 = dev_ear;
sta_ear1 = sta_ear;

dev_ear1.data = mean(dev_ear.data,3);
sta_ear1.data = mean(sta_ear.data,3);

best_ear_dev = mbl_bipolarchannelexpansion(dev_ear1);
best_ear_sta = mbl_bipolarchannelexpansion(sta_ear1);

diff_best_ear = best_ear_dev;
diff_best_ear.data = best_ear_dev.data - best_ear_sta.data;

maxEar = [];
capVal = [];
for c = 1:size(diff_best_ear.data,1)
[maxEar(c).pks maxEar(c).locs] = findpeaks(diff_best_ear.data(c,timewin)*-1); % find the negative peaks
[capVal(c).val loc] = max(maxEar(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxEar(c).locs(loc);
end

best_chan_ear = find(max([capVal.val])==[capVal.val]); % identify the channel with the highest diff. between sta and dev tones
bestchans.ear = best_ear_dev.chanlocs(best_chan_ear).labels; % this channel has the largest diff. amplitude between sta and dev

chan1.ear = find(strcmp({sta_ear1.chanlocs.labels},bestchans.ear(1:3)));
chan2.ear = find(strcmp({sta_ear1.chanlocs.labels},bestchans.ear(end-2:end)));

best_chan_ear_sta = sta_ear1.data(chan1.ear,:) - sta_ear1.data(chan2.ear,:);
best_chan_ear_dev = dev_ear1.data(chan1.ear,:) - dev_ear1.data(chan2.ear,:);
best_chan_ear_diff = best_chan_ear_dev - best_chan_ear_sta;

% figure
% plot(dev_cap1.times,best_chan_ear_sta)
% hold on
% plot(dev_cap1.times,best_chan_ear_dev)
% plot(dev_cap1.times,best_chan_ear_diff)
% legend('sta','dev','diff')
% title(['Best ear channel: ',bestchans.ear ])

% %% group figures
% figure
% subplot(2,2,1)
% plot(dev_cap1.times,best_chan_cap_sta)
% hold on
% plot(dev_cap1.times,best_chan_cap_dev)
% plot(dev_cap1.times,best_chan_cap_diff)
% legend('sta','dev','diff')
% title(['Best cap channel: ',bestchans.cap ])
% ylim([-6 4])
% 
% subplot(2,2,2)
% plot(dev_cap1.times,best_chan_stacap_sta)
% hold on
% plot(dev_cap1.times,best_chan_stacap_dev)
% plot(dev_cap1.times,best_chan_stacap_diff)
% legend('sta','dev','diff')
% title(['Standard cap channel: ',bestchans.sta_cap,' - nose ref.' ])
% ylim([-6 4])
% 
% subplot(2,2,3)
% plot(dev_cap1.times,best_chan_cleanear_sta)
% hold on
% plot(dev_cap1.times,best_chan_cleanear_dev)
% plot(dev_cap1.times,best_chan_cleanear_diff)
% legend('sta','dev','diff')
% title(['Best clean ear channel: ',bestchans.cleanear ])
% ylim([-6 4])
% 
% subplot(2,2,4)
% plot(dev_cap1.times,best_chan_ear_sta)
% hold on
% plot(dev_cap1.times,best_chan_ear_dev)
% plot(dev_cap1.times,best_chan_ear_diff)
% legend('sta','dev','diff')
% title(['Best ear channel: ',bestchans.ear ])
% ylim([-6 4])
% 
% 
% figure
% p1 = plot(dev_cap1.times,best_chan_cap_diff);
% p1(1).LineWidth = 2;
% hold on
% p2 = plot(dev_cap1.times,best_chan_stacap_diff)
% p2(1).LineWidth = 2;
% p3 = plot(dev_cap1.times,best_chan_ear_diff)
% p3(1).LineWidth = 2;
% p4 = plot(dev_cap1.times,best_chan_cleanear_diff)
% p4(1).LineWidth = 2;
% ax = gca;
% ax.FontSize = 16; 
% legend('cap','standard cap','ear','clean ear','FontSize',25)
% title('MMN Best channels: difference waves','FontSize',35,'FontWeight','bold')
% ylim([-7 4])
% xlabel('time [ms]','FontSize',25,'FontWeight','bold');
% ylabel('amplitude [�A]','FontSize',25,'FontWeight','bold');
% 
% % add simple bar chart showing the largest negative deflection in the
% % effect-time window
% bar_effect(1) = min(best_chan_cap_diff(300:500))
% bar_effect(2) = min(best_chan_stacap_diff(300:500))
% bar_effect(3) = min(best_chan_cleanear_diff(300:500))
% bar_effect(4) = min(best_chan_ear_diff(300:500))
% 
% naming = categorical({'Full cap','Standard cap','Clean ear','Ear'});
% naming = reordercats(naming,{'Full cap','Standard cap','Clean ear','Ear'});
% 
% figure
% bar(naming,bar_effect)
% a = get(gca,'XTickLabel');
% set(gca,'XTickLabel',a,'FontName','Times','fontsize',25)
% ylabel('amplitude [�A]')
% 
% perc_loss =  100- bar_effect(4)/ bar_effect(1)*100;