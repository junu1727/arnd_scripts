%% calculate and compare the best electrodes between sets

%clear all; close all; clc

%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [50 50 130 250 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [250 250 230 500 600]; % end of the time window of interest per task. Should contain the desired effect
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'N1';'a';'b';'c';'d'};
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
conditionString = {'cap','earc','ear'};
bestN = 66; %The bestN channels are stored for each setup (cap or ear)
sta_elec = [43 55 55; 43 55 55; 31 55 55; 43 55 55; 34 55 55]; %channel number of the standard electrode for all three conditions (cap, earc and ear)
fs = 100;

%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for s = 1:length(SBJ)
    
    load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');
    getSBJ = contains({q_check.SBJ},SBJ{s});
    q_check = q_check(getSBJ);
    
    for t = 1:length(taskLetter)
        
        q_task = t; %counter for the tasks. there are 4 tasks, but task a has two runs, one for N1 from tone onset, one from all later tones. Therefore, we need an additional counter
        if t > 1
            q_task = q_task - 1;
            special_N1 = 0;
        else
            special_N1 = 1;
        end
        
        if q_check(q_task).badEarChan == 0
            
            for c = 1:3
                
                if c == 1 || c == 2
                    isear = [];
                elseif c == 3
                    isear = '_ear';
                else
                end
                
                if taskLetter{t} == 'N1'
                    naming1st = isear;
                elseif taskLetter{t} == 'a'
                    naming1st = ['_unatt',isear];
                    naming2nd = ['_att',isear];
                elseif taskLetter{t} == 'b' || taskLetter{t} == 'c'
                    naming1st = ['_sta',isear];
                    naming2nd = ['_dev',isear];
                else taskLetter{t} == 'd'
                    naming1st = ['_congr',isear];
                    naming2nd = ['_incongr',isear];
                end
                
                %get cap (or cleaned ear) data set (cleaned ear = subsample of cap dataset)
                con1path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{t},'*',naming1st,'.set']); %set 1 for the cleaned cap data
                con1path = con1path(1);
                if special_N1 == 0
                    con2path = dir([MAINPATH,SBJ{s},'\04_clean\*\*_',taskLetter{t},naming2nd,'.set']); %set 2 for the cleaned cap data
                    con2path = con2path(1);
                end
                
                if c == 1
                    FullorEar     = 1; % 1 = full, 2 = ear
                else c == 2 || c == 3;
                    FullorEar     = 2; % 1 = full, 2 = ear
                end
                
                % get folder names
                if taskLetter{t} == 'N1'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{t} == 'a'
                    folderName = 'a_attended_speaker';
                elseif taskLetter{t} == 'b'
                    folderName = 'b_passive_oddball';
                elseif taskLetter{t} == 'c'
                    folderName = 'c_active_oddball';
                elseif taskLetter{t} == 'd'
                    folderName = 'd_conguency';
                else
                    error('please enter a valid taskLetter (a-d)')
                end
                
                PATHIN  = con1path.folder;
                PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,folderName,filesep); % path for script output
                if ~exist (PATHOUT)
                    mkdir(PATHOUT)
                end
                
                % load data: 2 conditions
                condition1 = pop_loadset('filename', con1path.name, 'filepath',PATHIN);
                condition1.data = smoothdata(condition1.data ,2,'movmean',4);
                                
                if special_N1 == 0
                    condition2 = pop_loadset('filename', con2path.name, 'filepath',PATHIN);
                    condition2.data = smoothdata(condition2.data ,2,'movmean',4);
                else
                    condition2 = [];
                end
                
                if unilateral == 1
                    earFolder = '\05_ERP_uni\';
                elseif unilateral == 0
                    earFolder = '\05_ERP\';
                end
                
                if ~exist([MAINPATH,SBJ{s},earFolder,folderName])
                    mkdir([MAINPATH,SBJ{s},earFolder,folderName])
                end
                
                AVG_con1{s,t,c} = mean(condition1.data,3);
                if ~isempty(condition2)
                    AVG_con2{s,t,c} = mean(condition2.data,3);
                else
                    AVG_con2{s,t,c} = condition2;
                end

            end
        end
    end
end

meanCap1 = [];
for t  = 1:5
    b=1;
    for s = 1:length(SBJ)-1
        if ~isempty(AVG_con1{s,t,1})
            meanCap1(b,:,t) = AVG_con1{s,t,1}(sta_elec(t,1),:);
            b=b+1;
        end
    end
end

% z=1;
% for t  = 1:5
%         b=1;
%         meanEar = [];
%         for s = 1:length(SBJ)-1
%             if ~isempty(AVG_con1{s,t,3})
%                 meanEar(b,:) = AVG_con1{s,t,3}(sta_elec(t,1),:);
%                 b=b+1;
%             end
%         end
%         matEar1(z,:) = mean(meanEar,1);
%         z=z+1;
% end

meanCap2 = [];
for t  = 1:5
    b=1;
    for s = 1:length(SBJ)-1
        if ~isempty(AVG_con2{s,t,1})
            meanCap2(b,:,t) = AVG_con2{s,t,1}(sta_elec(t,1),:);
            b=b+1;
        end
    end
end

% z=1;
% for t  = 1:5
%         b=1;
%         meanEar = [];
%         for s = 1:length(SBJ)-1
%             if ~isempty(AVG_con2{s,t,3})
%                 meanEar(b,:) = AVG_con2{s,t,3}(sta_elec(t,1),:);
%                 b=b+1;
%             end
%         end
%         matEar2(z,:) = mean(meanEar,1);
%         z=z+1;
% end

for tt = 2:length(taskLetter)
    sta = squeeze(meanCap1(:,:,tt));
    sta = sta(any(sta,2),:);
    dev = squeeze(meanCap2(:,:,tt));
    dev = dev(any(dev,2),:);
    effect_sizes = mes(sta,dev,'hedgesg','isDep',0);
    hedgesGScalp(tt,:) = effect_sizes.hedgesg;
end
hedgesGScalp = smoothdata(hedgesGScalp ,2,'movmean',4);
hedgesGScalp = abs(hedgesGScalp);
% for tt = 1:length(taskLetter)
%     sta = squeeze(matEar1(tt,:));
%     dev = squeeze(matEar2(tt,:));
%     effect_sizes = mes(sta,dev,'hedgesg','isDep',0);
%     hedgesGEar(tt,:) = effect_sizes.hedgesg;
% end
%     hedgesGEar = smoothdata(hedgesGEar ,2,'movmean',4);

figure
for dd = 1:length(taskLetter)
subplot(1,5,dd)
plot(timeVec,hedgesGScalp(dd,:))
% hold on
% plot(timeVec,hedgesGEar(dd,:))
end


hedgesGScalp







           