
%% settings
unilateral    = 0; %if 0, use both ears. If 1, use only right ear
window_start  = [50 130 300 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [150 230 600 600]; % end of the time window of interest per task. Should contain the desired effect
srate         = 100; % downsample to ...
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','b','c','d',};
staElec    = [8 8 8 8]; % channel that is usually used to detect the task effect in the literature
conditionString = {'cap','earc','ear'};
special_N1 = 1; % do plots for the N1 of the attended speaker task
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,240,20);
%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');

%% plots for g
for cb = 1
    
    %pre-allocate the matrices
    effect_cap.a(length(SBJ)).trial              = []; % matrix of permutations of the condition with more trials (for reproducability)
    effect_cap.a(length(SBJ)).es_matrix_mean     = []; %effect sizes, channel*time*iteration
    effect_cap.a(length(SBJ)).best_loc           = []; % indices of the best n channels
    effect_cap.a(length(SBJ)).bestchans          = []; % string names of the n best channels
    effect_cap.a(length(SBJ)).ERPs               = []; % ERPs of b
    
    effect_cap.b =  effect_cap.a;
    effect_cap.c =  effect_cap.a;
    effect_cap.d =  effect_cap.a;
    
    effect_earc = effect_cap;
    effect_ear  = effect_cap;
    
    for s = 1:length(SBJ)
        
        load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');
        getSBJ = contains({q_check.SBJ},SBJ{s});
        q_check = q_check(getSBJ);
        
        for taskNum = 1:4
            if q_check(taskNum).badEarChan == 0
                for conditionNum = 1:3
                    
                    timeWin = find(timeVec == window_start(taskNum)) : find(timeVec == window_stop(taskNum)) -1;
                    
                    % get folder names
                    if taskLetter{taskNum} == 'a'
                        folderName = 'a_attended_speaker';
                    elseif taskLetter{taskNum} == 'b'
                        folderName = 'b_passive_oddball';
                    elseif taskLetter{taskNum} == 'c'
                        folderName = 'c_active_oddball';
                    elseif taskLetter{taskNum} == 'd'
                        folderName = 'd_conguency';
                    else
                        error('please enter a valid taskLetter (a-d)')
                    end
                    
                    if   cb == 1
                        saveCB = '_bipolar';
                    else cb == 2
                        saveCB = '_common';
                    end
                    
                    if special_N1 == 1 && taskNum == 1
                        specialN = '_N1';
                    else
                        specialN = [];
                    end
                    
                    % load the effect sizes, channel*time*iteration
                    load([MAINPATH,SBJ{s},'\05_ERP\',folderName,'\trials_',conditionString{conditionNum},specialN,saveCB,'.mat'],'trials');
                    load([MAINPATH,SBJ{s},'\05_ERP\',folderName,'\g_',conditionString{conditionNum},specialN,'_',num2str(iterations),saveCB,'.mat'],'effect_size_matrix');
                    load([MAINPATH,SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,saveCB,'_loc.mat'],'best_loc');
                    load([MAINPATH,SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,saveCB,'_bestchans.mat'],'bestchans');
                    load([MAINPATH,SBJ{s},'\05_ERP\',folderName,'\',conditionString{conditionNum},specialN,saveCB,'_ERPs.mat'],'ERPs');
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %transform the ERPs to be in the right direction (because of the way we get all bipolar channels,
                    %the same channel is sometimes subtracted and sometimes subtracted from. Therefore, the sign is sometimes reversed)
                    if taskNum == 1 && special_N1 == 0
                        timeWin = find(timeVec == 0) : find(timeVec == 300) -1;
                    elseif taskNum == 1 && special_N1 == 1
                        timeWin = find(timeVecN1 == 100) : find(timeVecN1 == 170) -1;
                    else
                    end
                    
                    if taskLetter{taskNum} ~= 'c'
                        m1 = mean(ERPs.cond1,3);
                        m2 = mean(ERPs.cond2,3);
                        m3 = m1-m2;
                        
                        for w = 1:size(m1,1)
                            [pksMax(w), locsMax]= max(smoothdata(m3(w,timeWin),2,'movmean',4));
                            [pksMin(w), locsMin]= min(smoothdata(m3(w,timeWin),2,'movmean',4));
                            
                            % there should be a negative peak in this time window. If the largest
                            % neg. peak is smaller than the largest positive one (in abs. val.),
                            % the signal is the wrong way around. Inverse with signal * -1
                            if abs(pksMax(w)) > abs(pksMin(w))
                                m1(w,:) = m1(w,:) * -1;
                                m2(w,:) = m2(w,:) * -1;
                            end
                        end
                    else taskLetter{taskNum} == 'c'
                        m1 = mean(ERPs.cond1,3);
                        m2 = mean(ERPs.cond2,3);
                        m3 = m1-m2;
                        
                        for w = 1:size(m1,1)
                            [pksMax(w), locsMax]= max(smoothdata(m3(w,timeWin),2,'movmean',4));
                            [pksMin(w), locsMin]= min(smoothdata(m3(w,timeWin),2,'movmean',4));
                            
                            % P300 is the only positive deflection, max should be
                            % larger than min
                            if abs(pksMax(w)) < abs(pksMin(w))
                                m1(w,:) = m1(w,:) * -1;
                                m2(w,:) = m2(w,:) * -1;
                            end
                        end
                    end
                    
%                     m3 = m1-m2;
%                     for d = 1: size(m1,1)
%                         figure
%                         plot(timeVec,smoothdata(m1-m2,2,'movmean',4))
%                     end
                    
                    ERPs.cond1 = m1;
                    ERPs.cond2 = m2;
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    
                    if taskNum == 1
                        if conditionNum == 1
                            effect_cap.a(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.a(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.a(s).best_loc           = best_loc; % indices of the best n channels
                            effect_cap.a(s).bestchans          = bestchans; % string names of the n best channels
                            effect_cap.a(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.a(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.a(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.a(s).best_loc           = best_loc; % indices of the best n channels
                            effect_earc.a(s).bestchans          = bestchans; % string names of the n best channels
                            effect_earc.a(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.a(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.a(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.a(s).best_loc           = best_loc; % indices of the best n channels
                            effect_ear.a(s).bestchans          = bestchans; % string names of the n best channels
                            effect_ear.a(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                        
                    elseif taskNum == 2
                        if conditionNum == 1
                            effect_cap.b(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.b(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.b(s).best_loc           = best_loc; % indices of the best n channels
                            effect_cap.b(s).bestchans          = bestchans; % string names of the n best channels
                            effect_cap.b(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.b(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.b(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.b(s).best_loc           = best_loc; % indices of the best n channels
                            effect_earc.b(s).bestchans          = bestchans; % string names of the n best channels
                            effect_earc.b(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.b(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.b(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.b(s).best_loc           = best_loc; % indices of the best n channels
                            effect_ear.b(s).bestchans          = bestchans; % string names of the n best channels
                            effect_ear.b(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                    elseif taskNum == 3
                        if conditionNum == 1
                            effect_cap.c(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.c(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.c(s).best_loc           = best_loc; % indices of the best n channels
                            effect_cap.c(s).bestchans          = bestchans; % string names of the n best channels
                            effect_cap.c(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.c(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.c(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.c(s).best_loc           = best_loc; % indices of the best n channels
                            effect_earc.c(s).bestchans          = bestchans; % string names of the n best channels
                            effect_earc.c(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.c(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.c(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.c(s).best_loc           = best_loc; % indices of the best n channels
                            effect_ear.c(s).bestchans          = bestchans; % string names of the n best channels
                            effect_ear.c(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                    else taskNum == 4
                        if conditionNum == 1
                            effect_cap.d(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_cap.d(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_cap.d(s).best_loc           = best_loc; % indices of the best n channels
                            effect_cap.d(s).bestchans          = bestchans; % string names of the n best channels
                            effect_cap.d(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        elseif conditionNum == 2
                            effect_earc.d(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_earc.d(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_earc.d(s).best_loc           = best_loc; % indices of the best n channels
                            effect_earc.d(s).bestchans          = bestchans; % string names of the n best channels
                            effect_earc.d(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        else conditionNum == 3
                            effect_ear.d(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                            effect_ear.d(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                            effect_ear.d(s).best_loc           = best_loc; % indices of the best n channels
                            effect_ear.d(s).bestchans          = bestchans; % string names of the n best channels
                            effect_ear.d(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
                        end
                    end
                end
            end
        end
        
        %% effect sizes
        if ~isempty(effect_cap.a(s).es_matrix_mean)
            a_cap(s,:) = smoothdata(abs(effect_cap.a(s).es_matrix_mean(effect_cap.a(s).best_loc(1),:,:)),2,'movmean',4);
            a_earc(s,:) = smoothdata(abs(effect_earc.a(s).es_matrix_mean(effect_earc.a(s).best_loc(1),:,:)),2,'movmean',4);
            a_ear(s,:) = smoothdata(abs(effect_ear.a(s).es_matrix_mean(effect_ear.a(s).best_loc(1),:,:)),2,'movmean',4);
        end
        if ~isempty(effect_cap.b(s).es_matrix_mean)
            b_cap(s,:) = smoothdata(abs(effect_cap.b(s).es_matrix_mean(effect_cap.b(s).best_loc(1),:,:)),2,'movmean',4);
            b_earc(s,:) = smoothdata(abs(effect_earc.b(s).es_matrix_mean(effect_earc.b(s).best_loc(1),:,:)),2,'movmean',4);
            b_ear(s,:) = smoothdata(abs(effect_ear.b(s).es_matrix_mean(effect_ear.b(s).best_loc(1),:,:)),2,'movmean',4);
        end
        if ~isempty(effect_cap.c(s).es_matrix_mean)
            c_cap(s,:) = smoothdata(abs(effect_cap.c(s).es_matrix_mean(effect_cap.c(s).best_loc(1),:,:)),2,'movmean',4);
            c_earc(s,:) = smoothdata(abs(effect_earc.c(s).es_matrix_mean(effect_earc.c(s).best_loc(1),:,:)),2,'movmean',4);
            c_ear(s,:) = smoothdata(abs(effect_ear.c(s).es_matrix_mean(effect_ear.c(s).best_loc(1),:,:)),2,'movmean',4);
        end
        if ~isempty(effect_cap.d(s).es_matrix_mean)
            d_cap(s,:) = smoothdata(abs(effect_cap.d(s).es_matrix_mean(effect_cap.d(s).best_loc(1),:,:)),2,'movmean',4);
            d_earc(s,:) = smoothdata(abs(effect_earc.d(s).es_matrix_mean(effect_earc.d(s).best_loc(1),:,:)),2,'movmean',4);
            d_ear(s,:) = smoothdata(abs(effect_ear.d(s).es_matrix_mean(effect_ear.d(s).best_loc(1),:,:)),2,'movmean',4);
        end
        
        %% difference waves        
        if ~isempty(effect_cap.a(s).ERPs)
            a_capd(s,:)  = smoothdata(effect_cap.a(s).ERPs.cond1(1,:) - effect_cap.a(s).ERPs.cond2(1,:),2,'movmean',4);
            a_earcd(s,:) = smoothdata(effect_earc.a(s).ERPs.cond1(1,:) - effect_earc.a(s).ERPs.cond2(1,:),2,'movmean',4);
            a_eard (s,:) = smoothdata(effect_ear.a(s).ERPs.cond1(1,:) - effect_ear.a(s).ERPs.cond2(1,:),2,'movmean',4);
        end
        if ~isempty(effect_cap.b(s).ERPs)
            b_capd(s,:) = smoothdata(effect_cap.b(s).ERPs.cond1(1,:) - effect_cap.b(s).ERPs.cond2(1,:),2,'movmean',4);
            b_earcd(s,:) = smoothdata(effect_earc.b(s).ERPs.cond1(1,:) - effect_earc.b(s).ERPs.cond2(1,:),2,'movmean',4);
            b_eard(s,:) = smoothdata(effect_ear.b(s).ERPs.cond1(1,:) - effect_ear.b(s).ERPs.cond2(1,:),2,'movmean',4);
        end
        if ~isempty(effect_cap.c(s).ERPs)
            c_capd(s,:) = smoothdata(effect_cap.c(s).ERPs.cond1(1,:) - effect_cap.c(s).ERPs.cond2(1,:),2,'movmean',4);
            c_earcd(s,:) = smoothdata(effect_earc.c(s).ERPs.cond1(1,:) - effect_earc.c(s).ERPs.cond2(1,:),2,'movmean',4);
            c_eard(s,:) = smoothdata(effect_ear.c(s).ERPs.cond1(1,:) - effect_ear.c(s).ERPs.cond2(1,:),2,'movmean',4);
        end
        if ~isempty(effect_cap.d(s).ERPs)
            d_capd(s,:) = smoothdata(effect_cap.d(s).ERPs.cond1(1,:) - effect_cap.d(s).ERPs.cond2(1,:),2,'movmean',4);
            d_earcd(s,:) = smoothdata(effect_earc.d(s).ERPs.cond1(1,:) - effect_earc.d(s).ERPs.cond2(1,:),2,'movmean',4);
            d_eard(s,:) = smoothdata(effect_ear.d(s).ERPs.cond1(1,:) - effect_ear.d(s).ERPs.cond2(1,:),2,'movmean',4);
        end
        
        fig = figure;
        
        if special_N1 == 1
            %N1, effect size
            subplot(2,4,5)
            if ~isempty(effect_cap.a(s).es_matrix_mean)
                plot(timeVecN1,a_cap(s,:),'LineWidth',3)
                hold on
                %plot(timeVec,a_earc)
                plot(timeVecN1,a_ear(s,:),'LineWidth',3)
                
                ylim([0 0.8])
               % ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
                %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
                title('Mult. speaker N1','FontSize', 25, 'FontWeight', 'bold')
                legend('cap','ear','FontSize', 15, 'FontWeight', 'bold','Location','northeast')
            end
            
        else
            %N1, effect size
            subplot(2,4,5)
            if ~isempty(effect_cap.a(s).es_matrix_mean)
                plot(timeVec,a_cap(s,:),'LineWidth',3)
                hold on
                %plot(timeVec,a_earc)
                plot(timeVec,a_ear(s,:),'LineWidth',3)
                
                ylim([0 0.8])
               % ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
                %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
                title('Mult. speaker (att/unatt)','FontSize', 25, 'FontWeight', 'bold')
                legend('cap','ear','FontSize', 25, 'FontWeight', 'bold','Location','northeast')
            end
        end
        
        
        %MMN, effect size
        subplot(2,4,6)
        if ~isempty(effect_cap.b(s).es_matrix_mean)
            plot(timeVec,b_cap(s,:),'LineWidth',3)
            hold on
            %plot(timeVec,b_earc)
            plot(timeVec,b_ear(s,:),'LineWidth',3)
            
            ylim([0 0.8])
           % ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
            %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
            title('MMN (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
        end
        %P300, effect size
        subplot(2,4,7)
        if ~isempty(effect_cap.c(s).es_matrix_mean)
            plot(timeVec,c_cap(s,:),'LineWidth',3);
            hold on
            %plot(timeVec,c_earc);
            plot(timeVec,c_ear(s,:),'LineWidth',3);
            
            ylim([0 0.8])
           % ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
            %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
            title('P300 (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
        end
        %N400, effect size
        subplot(2,4,8)
        if ~isempty(effect_cap.d(s).es_matrix_mean)
            plot(timeVec,d_cap(s,:),'LineWidth',3)
            hold on
            %plot(timeVec,d_earc)
            plot(timeVec,d_ear(s,:),'LineWidth',3)
            
            ylim([0 0.8])
           % ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
            %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
            title('N400 (congr/incongr)','FontSize', 25, 'FontWeight', 'bold')
        end
        
        if special_N1 == 1
            %N1, difference wave
            subplot(2,4,1)
            if ~isempty(effect_cap.a(s).ERPs)
                plot(timeVecN1,a_capd(s,:),'LineWidth',3)
                hold on
                %plot(timeVec,a_earcd)
                plot(timeVecN1,a_eard(s,:),'LineWidth',3)
                
                ylim([-7 7])
                ylabel('Amplitude [�V]','FontSize', 25, 'FontWeight', 'bold')
                %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
                title('N1','FontSize', 25, 'FontWeight', 'bold')
            end
        else special_N1 == 0
            %N1, difference wave
            subplot(2,4,1)
            if ~isempty(effect_cap.a(s).ERPs)
                plot(timeVec,a_capd(s,:),'LineWidth',3)
                hold on
                %plot(timeVec,a_earcd)
                plot(timeVec,a_eard(s,:),'LineWidth',3)
                
                ylim([-7 7])
                ylabel('Amplitude [�V]','FontSize', 25, 'FontWeight', 'bold')
                %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
                title('N1','FontSize', 25, 'FontWeight', 'bold')
            end
        end
         
         
        %MMN, difference wave
        subplot(2,4,2)
        if ~isempty(effect_cap.b(s).ERPs)
            plot(timeVec,b_capd(s,:),'LineWidth',3)
            hold on
            %plot(timeVec,b_earcd)
            plot(timeVec,b_eard(s,:),'LineWidth',3)
            
            ylim([-7 7])
            ylabel('Difference wave [�V]','FontSize', 25, 'FontWeight', 'bold')
            %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
            title('MMN (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
        end
        %P300, difference wave
        subplot(2,4,3)
        if ~isempty(effect_cap.c(s).ERPs)
            plot(timeVec,c_capd(s,:),'LineWidth',3)
            hold on
            %plot(timeVec,c_earcd)
            plot(timeVec,c_eard(s,:),'LineWidth',3)
            
            ylim([-7 7])
            ylabel('Difference wave [�V]','FontSize', 25, 'FontWeight', 'bold')
            %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
            title('P300 (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
        end
        %N400, difference wave
        subplot(2,4,4)
        if ~isempty(effect_cap.d(s).ERPs)
            plot(timeVec,d_capd(s,:),'LineWidth',3)
            hold on
            %plot(timeVec,d_earcd)
            plot(timeVec,d_eard(s,:),'LineWidth',3)
            
            ylim([-7 7])
            ylabel('Difference wave [�V]','FontSize', 25, 'FontWeight', 'bold')
            %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
            title('N400 (congr/incongr)','FontSize', 25, 'FontWeight', 'bold')
        end
        sgtitle(['Best channel per data set, ',SBJ{s}],'FontSize', 25, 'FontWeight', 'bold')
        set(gcf,'Color','w')
        set(gcf, 'Position', get(0, 'Screensize'));
        
        if ~exist([MAINPATH,SBJ{s},'\figures'])
            mkdir([MAINPATH,SBJ{s},'\figures'])
        end
        
        print(fig,[MAINPATH,SBJ{s},'\figures\fig1_ind',specialN],'-dpng')
    end
    
    %% GND AVG
    pure_red_0_5 = [236,168,139]/255;
    pure_blue_0_5 = [127,184,222]/255;
    pure_red = [0.8500, 0.3250, 0.0980];
    pure_blue = [0,0.4470,0.7410];
    transparency = 0.5;
    cd('C:\Users\arnd\AppData\Roaming\MathWorks\MATLAB Add-Ons\Functions\ciplot(lower,upper,x,colour,alpha)');
    
    a_cap = a_cap(any(a_cap,2),:);
    b_cap = b_cap(any(b_cap,2),:);
    c_cap = c_cap(any(c_cap,2),:);
    d_cap = d_cap(any(d_cap,2),:);
    
    a_earc = a_earc(any(a_earc,2),:);
    b_earc = b_earc(any(b_earc,2),:);
    c_earc = c_earc(any(c_earc,2),:);
    d_earc = d_earc(any(d_earc,2),:);
    
    a_ear = a_ear(any(a_ear,2),:);
    b_ear = b_ear(any(b_ear,2),:);
    c_ear = c_ear(any(c_ear,2),:);
    d_ear = d_ear(any(d_ear,2),:);
    
    
    
    fig = figure;
    
    if special_N1 == 1
        %N1, effect size
        subplot(2,4,5)
        ci1 = ciplot(mean(a_cap)-std(a_cap),(mean(a_cap)+std(a_cap)),timeVecN1,pure_red_0_5,transparency);
        ci1.EdgeColor = 'none';
        hold on
        ci2 = ciplot(mean(a_ear)-std(a_ear),mean(a_ear)+std(a_ear),timeVecN1,pure_blue_0_5,transparency);
        ci2.EdgeColor = 'none';
        plot(timeVecN1,mean(a_cap),'color',pure_red,'LineWidth',3)
        plot(timeVecN1,mean(a_ear),'color',pure_blue,'LineWidth',3)
        ylim([0 1.5])
        ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
        xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
        %title('N1 (tone onset)','FontSize', 25, 'FontWeight', 'bold')
        box off
    
    else special_N1 == 0
        %N1, effect size
        subplot(2,4,5)
        ci1 = ciplot(mean(a_cap)-std(a_cap),(mean(a_cap)+std(a_cap)),timeVec,pure_red_0_5,transparency);
        ci1.EdgeColor = 'none';
        hold on
        ci2 = ciplot(mean(a_ear)-std(a_ear),mean(a_ear)+std(a_ear),timeVec,pure_blue_0_5,transparency);
        ci2.EdgeColor = 'none';
        plot(timeVec,mean(a_cap),'color',pure_red,'LineWidth',3)
        plot(timeVec,mean(a_ear),'color',pure_blue,'LineWidth',3)
        ylim([0 1])
        ylabel('Hedge�s g [abs.]','FontSize', 25, 'FontWeight', 'bold')
        xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
        %title('Mult. speaker (att/unatt)','FontSize', 25, 'FontWeight', 'bold')
        box off
    
    end
    
    %MMN, effect size
    subplot(2,4,6)
    ci3 = ciplot(mean(b_cap)-std(b_cap),(mean(b_cap)+std(b_cap)),timeVec,pure_red_0_5,transparency);
    ci3.EdgeColor = 'none';
    hold on
    ci4 = ciplot(mean(b_ear)-std(b_ear),mean(b_ear)+std(b_ear),timeVec,pure_blue_0_5,transparency);
    ci4.EdgeColor = 'none';
    plot(timeVec,mean(b_cap),'color',pure_red,'LineWidth',3)
    plot(timeVec,mean(b_ear),'color',pure_blue,'LineWidth',3)
    ylim([0 1])
    %title('Mismatch Negativity (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
    box off
    
    %P300, effect size
    subplot(2,4,7)
    ci5 = ciplot(mean(c_cap)-std(c_cap),(mean(c_cap)+std(c_cap)),timeVec,pure_red_0_5,transparency);
    ci5.EdgeColor = 'none';
    hold on
    ci6 = ciplot(mean(c_ear)-std(c_ear),mean(c_ear)+std(c_ear),timeVec,pure_blue_0_5,transparency);
    ci6.EdgeColor = 'none';
    plot(timeVec,mean(c_cap),'color',pure_red,'LineWidth',3)
    plot(timeVec,mean(c_ear),'color',pure_blue,'LineWidth',3)
    ylim([0 1])
    %title('P300 (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
    box off
    
    %N400, effect size
    subplot(2,4,8)
    ci7 = ciplot(mean(d_cap)-std(d_cap),(mean(d_cap)+std(d_cap)),timeVec,pure_red_0_5,transparency);
    ci7.EdgeColor = 'none';
    hold on
    ci8 = ciplot(mean(d_ear)-std(d_ear),mean(d_ear)+std(d_ear),timeVec,pure_blue_0_5,transparency);
    ci8.EdgeColor = 'none';
    plot(timeVec,mean(d_cap),'color',pure_red,'LineWidth',3)
    plot(timeVec,mean(d_ear),'color',pure_blue,'LineWidth',3)
    ylim([0 1])
    %title('N400 (congr/incongr)','FontSize', 25, 'FontWeight', 'bold')
    box off
    
    if special_N1 == 1
        %N1, difference wave
        subplot(2,4,1)
        ci9 = ciplot(mean(a_capd)-std(a_capd),(mean(a_capd)+std(a_capd)),timeVecN1,pure_red_0_5,transparency);
        ci9.EdgeColor = 'none';
        hold on
        ci10 = ciplot(mean(a_eard)-std(a_eard),mean(a_eard)+std(a_eard),timeVecN1,pure_blue_0_5,transparency);
        ci10.EdgeColor = 'none';
        plot(timeVecN1,mean(a_capd),'color',pure_red,'LineWidth',3)
        plot(timeVecN1,mean(a_eard),'color',pure_blue,'LineWidth',3)
        ylim([-8 2.5])
        ylabel('Amplitude [�V]','FontSize', 25, 'FontWeight', 'bold')
        xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
        %title('N1 (tone onset)','FontSize', 25, 'FontWeight', 'bold')
        legend('cap','ear','FontSize', 15, 'FontWeight', 'bold','Location','southeast')
        box off
    
    else special_N1 == 0
        %N1, difference wave
        subplot(2,4,1)
        ci9 = ciplot(mean(a_capd)-std(a_capd),(mean(a_capd)+std(a_capd)),timeVec,pure_red_0_5,transparency);
        ci9.EdgeColor = 'none';
        hold on
        ci10 = ciplot(mean(a_eard)-std(a_eard),mean(a_eard)+std(a_eard),timeVec,pure_blue_0_5,transparency);
        ci10.EdgeColor = 'none';
        plot(timeVec,mean(a_capd),'color',pure_red,'LineWidth',3)
        plot(timeVec,mean(a_eard),'color',pure_blue,'LineWidth',3)
        ylim([-3 2.5])
        ylabel('Difference wave [�V]','FontSize', 25, 'FontWeight', 'bold')
        %xlabel('time [ms]','FontSize', 25, 'FontWeight', 'bold')
        %title('N1 (att/unatt)','FontSize', 25, 'FontWeight', 'bold')
        legend('cap','ear','FontSize', 25, 'FontWeight', 'bold','Location','northwest')
        box off
    
    end
    
    %MMN, difference wave
    subplot(2,4,2)
    ci11 = ciplot(mean(b_capd)-std(b_capd),(mean(b_capd)+std(b_capd)),timeVec,pure_red_0_5,transparency);
    ci11.EdgeColor = 'none';
    hold on
    ci12 = ciplot(mean(b_eard)-std(b_eard),mean(b_eard)+std(b_eard),timeVec,pure_blue_0_5,transparency);
    ci12.EdgeColor = 'none';
    plot(timeVec,mean(b_capd),'color',pure_red,'LineWidth',3)
    plot(timeVec,mean(b_eard),'color',pure_blue,'LineWidth',3)
    ylabel('Difference wave [�V]','FontSize', 25, 'FontWeight', 'bold')
    ylim([-3 1])
    %title('Mismatch Negativity (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
    box off
    
    %P300, difference wave
    subplot(2,4,3)
    ci13 = ciplot(mean(c_capd)-std(c_capd),(mean(c_capd)+std(c_capd)),timeVec,pure_red_0_5,transparency);
    ci13.EdgeColor = 'none';
    hold on
    ci14 = ciplot(mean(c_eard)-std(c_eard),mean(c_eard)+std(c_eard),timeVec,pure_blue_0_5,transparency);
    ci14.EdgeColor = 'none';
    plot(timeVec,mean(c_capd),'color',pure_red,'LineWidth',3)
    plot(timeVec,mean(c_eard),'color',pure_blue,'LineWidth',3)
    ylim([-3 9])
    %title('P300 (sta/dev)','FontSize', 25, 'FontWeight', 'bold')
    box off
    
    %N400, difference wave
    subplot(2,4,4)
    ci15 = ciplot(mean(d_capd)-std(d_capd),(mean(d_capd)+std(d_capd)),timeVec,pure_red_0_5,transparency);
    ci15.EdgeColor = 'none';
    hold on
    ci16 = ciplot(mean(d_eard)-std(d_eard),mean(d_eard)+std(d_eard),timeVec,pure_blue_0_5,transparency);
    ci16.EdgeColor = 'none';
    plot(timeVec,mean(d_capd),'color',pure_red,'LineWidth',3)
    plot(timeVec,mean(d_eard),'color',pure_blue,'LineWidth',3)
    ylim([-3.5 3.5])
    %title('N400 (congr/incongr)','FontSize', 25, 'FontWeight', 'bold')
    box off
    
    sgtitle('EEG-signal and effect sizes, Grand Average (N=20)','FontSize', 25, 'FontWeight', 'bold')
    set(gcf,'Color','w')
    set(gcf, 'Position', get(0, 'Screensize'));
    
    print(fig,['O:\arm_testing\Experimente\cEEGrid_vs_Cap\GND_AVG',specialN],'-dpng')
end
