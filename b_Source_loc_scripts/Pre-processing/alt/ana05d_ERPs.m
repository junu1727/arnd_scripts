%% calculate and compare the best electrodes with the standards + a comparison between cap and ear
% Task b: passive auditory oddball

clear all; close all; clc

SBJ = {'p01','p02','p03','timing_test'};
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

s = 3;
unilateral = 1;
window_start = 370;
window_stop = 530;

%get all datasets from the passive oddball task
incongrcappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_d_*_incongr.set']);
congrcappath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_d_*_congr.set']);
incongrearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_d_*_incongr_ear.set']);
congrearpath = dir([MAINPATH,SBJ{s},'\04_clean\*\*_d_*_congr_ear.set']);

% set some paths
PATHIN  = incongrcappath.folder; % path containing rawdata
PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '05_ERP', filesep,'b_passive_oddball',filesep);    % path for script output
if ~exist (PATHOUT)
    mkdir(PATHOUT)
end

%load cap data: incongr, sta
incongr_cap = pop_loadset('filename', incongrcappath.name, 'filepath',...
    PATHIN);
congr_cap = pop_loadset('filename', congrcappath.name, 'filepath',...
    PATHIN);
%load ear data: incongr, sta
incongr_ear = pop_loadset('filename', incongrearpath.name, 'filepath',...
    PATHIN);
congr_ear = pop_loadset('filename', congrearpath.name, 'filepath',...
    PATHIN);
%load cap data for clean ear data: incongr, sta
clean_incongr_ear = pop_loadset('filename', incongrcappath.name, 'filepath',...
    PATHIN);
clean_congr_ear = pop_loadset('filename', congrcappath.name, 'filepath',...
    PATHIN); 

%kick some channels
incongr_cap = pop_select( incongr_cap, 'nochannel',{'E29','E30','E83','E84','E68'});
congr_cap = pop_select( congr_cap, 'nochannel',{'E29','E30','E83','E84','E68'});

% choose ear electrodes from data that was pre-processed using the full cap, aka the best ear data 
clean_incongr_ear = pop_select( clean_incongr_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_incongr_ear = pop_reref( clean_incongr_ear, find(strcmp({clean_incongr_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear
clean_congr_ear = pop_select( clean_congr_ear, 'channel',{'E18','E22','E24','E28','E71','E72','E80','E81','E85','E90','E92','E95'});
clean_congr_ear = pop_reref( clean_congr_ear, find(strcmp({clean_congr_ear.chanlocs.labels},{'E92'}))); %re-reference the ear-data to the electrode behind the right ear

if unilateral == 1
    clean_incongr_ear = pop_select( clean_incongr_ear, 'channel',{'E18','E24','E71','E72','E85'});
    clean_congr_ear   = pop_select( clean_congr_ear,   'channel',{'E18','E24','E71','E72','E85'});
    incongr_ear       = pop_select( incongr_ear,       'channel',{'E18','E24','E71','E72','E85'});
    congr_ear         = pop_select( congr_ear,         'channel',{'E18','E24','E71','E72','E85'});
end

% parameters 
timeStart = find(incongr_cap.times == window_start);
timeEnd   = find(incongr_cap.times == window_stop) -1;
timewin = timeStart:timeEnd;
fs = incongr_cap.srate;
sta_elecs = [ 1 8 51 68 84];


%% ERPs best_cap
incongr_cap1 = incongr_cap;
congr_cap1 = congr_cap;

incongr_cap1.data = mean(incongr_cap.data,3);
congr_cap1.data = mean(congr_cap.data,3);

best_cap_incongr = mbl_bipolarchannelexpansion(incongr_cap1);
best_cap_congr = mbl_bipolarchannelexpansion(congr_cap1);

diff_best_cap = best_cap_congr;
diff_best_cap.data = best_cap_incongr.data - best_cap_congr.data;

maxCap = [];
capVal = [];
for c = 1:size(diff_best_cap.data,1)
[maxCap(c).pks maxCap(c).locs] = findpeaks(diff_best_cap.data(c,timewin)*-1); % find the negative peaks
[capVal(c).val loc] = max(maxCap(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxCap(c).locs(loc);
end

best_chan_cap = find(max([capVal.val])==[capVal.val]); %identify the channel with the highest diff. between sta and incongr tones
bestchans.cap = best_cap_incongr.chanlocs(best_chan_cap).labels; % this channel has the largest diff. amplitude between sta and incongr

best_chan_cap_congr = congr_cap1.data(str2num(bestchans.cap(2:3)),:) - congr_cap1.data(str2num(bestchans.cap(end-1:end)),:);
best_chan_cap_incongr = incongr_cap1.data(str2num(bestchans.cap(2:3)),:) - incongr_cap1.data(str2num(bestchans.cap(end-1:end)),:);
best_chan_cap_diff = best_chan_cap_incongr - best_chan_cap_congr ;

figure
plot(incongr_cap1.times,best_chan_cap_congr)
hold on
plot(incongr_cap1.times,best_chan_cap_incongr)
plot(incongr_cap1.times,best_chan_cap_diff)
legend('congruent','incongruent','diff')
title(['Best cap channel: ',bestchans.cap ])

%% ERPs sta_cap
bestchans.sta_cap = congr_cap1.chanlocs(sta_elecs(2)).labels; 

best_chan_stacap_congr = congr_cap1.data(sta_elecs(2),:);
best_chan_stacap_incongr = incongr_cap1.data(sta_elecs(2),:);
best_chan_stacap_diff = best_chan_stacap_incongr - best_chan_stacap_congr;

chan1.sta_cap = bestchans.sta_cap;

figure
plot(incongr_cap1.times,best_chan_stacap_congr)
hold on
plot(incongr_cap1.times,best_chan_stacap_incongr)
plot(incongr_cap1.times,best_chan_stacap_diff)
legend('congruent','incongruent','diff')
title(['Standard cap channel: ',bestchans.sta_cap,' - nose ref.' ])

%% ERPs clean ear
clean_incongr_ear1 = clean_incongr_ear;
clean_congr_ear1 = clean_congr_ear;

clean_incongr_ear1.data = mean(clean_incongr_ear.data,3);
clean_congr_ear1.data = mean(clean_congr_ear.data,3);

best_cleanear_incongr = mbl_bipolarchannelexpansion(clean_incongr_ear1);
best_cleanear_congr = mbl_bipolarchannelexpansion(clean_congr_ear1);

diff_best_cleanear = best_cleanear_incongr;
diff_best_cleanear.data = best_cleanear_incongr.data - best_cleanear_congr.data;

maxEar = [];
capVal = [];
for c = 1:size(diff_best_cleanear.data,1)
[maxEar(c).pks maxEar(c).locs] = findpeaks(diff_best_cleanear.data(c,timewin)*-1); % find the negative peaks
[capVal(c).val loc] = max(maxEar(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxEar(c).locs(loc);
end

best_chan_cleanear = find(max([capVal.val])==[capVal.val]); % identify the channel with the highest diff. between sta and incongr tones
bestchans.cleanear = best_cleanear_incongr.chanlocs(best_chan_cleanear).labels; % this channel has the largest diff. amplitude between sta and incongr

chan1.clean_ear = find(strcmp({clean_congr_ear1.chanlocs.labels},bestchans.cleanear(1:3)));
chan2.clean_ear = find(strcmp({clean_congr_ear1.chanlocs.labels},bestchans.cleanear(end-2:end)));

best_chan_cleanear_congr = clean_congr_ear1.data(chan1.clean_ear,:) - clean_congr_ear1.data(chan2.clean_ear,:);
best_chan_cleanear_incongr = clean_incongr_ear1.data(chan1.clean_ear,:) - clean_incongr_ear1.data(chan2.clean_ear,:);
best_chan_cleanear_diff = best_chan_cleanear_incongr - best_chan_cleanear_congr;

figure
plot(incongr_cap1.times,best_chan_cleanear_congr)
hold on
plot(incongr_cap1.times,best_chan_cleanear_incongr)
plot(incongr_cap1.times,best_chan_cleanear_diff)
legend('congruent','incongruent','diff')
title(['Best clean ear channel: ',bestchans.cleanear ])

%% ERPs ear
incongr_ear1 = incongr_ear;
congr_ear1 = congr_ear;

incongr_ear1.data = mean(incongr_ear.data,3);
congr_ear1.data = mean(congr_ear.data,3);

best_ear_incongr = mbl_bipolarchannelexpansion(incongr_ear1);
best_ear_congr = mbl_bipolarchannelexpansion(congr_ear1);

diff_best_ear = best_ear_incongr;
diff_best_ear.data = best_ear_incongr.data - best_ear_congr.data;

maxEar = [];
capVal = [];
for c = 1:size(diff_best_ear.data,1)
[maxEar(c).pks maxEar(c).locs] = findpeaks(diff_best_ear.data(c,timewin)*-1); % find the negative peaks
[capVal(c).val loc] = max(maxEar(c).pks); % find and store the value and location of the most negative peak
 capVal(c).loc = maxEar(c).locs(loc);
end

best_chan_ear = find(max([capVal.val])==[capVal.val]); % identify the channel with the highest diff. between sta and incongr tones
bestchans.ear = best_ear_incongr.chanlocs(best_chan_ear).labels; % this channel has the largest diff. amplitude between sta and incongr

chan1.ear = find(strcmp({congr_ear1.chanlocs.labels},bestchans.ear(1:3)));
chan2.ear = find(strcmp({congr_ear1.chanlocs.labels},bestchans.ear(end-2:end)));

best_chan_ear_congr = congr_ear1.data(chan1.ear,:) - congr_ear1.data(chan2.ear,:);
best_chan_ear_incongr = incongr_ear1.data(chan1.ear,:) - incongr_ear1.data(chan2.ear,:);
best_chan_ear_diff = best_chan_ear_incongr - best_chan_ear_congr;

figure
plot(incongr_cap1.times,best_chan_ear_congr)
hold on
plot(incongr_cap1.times,best_chan_ear_incongr)
plot(incongr_cap1.times,best_chan_ear_diff)
legend('congruent','incongruent','diff')
title(['Best ear channel: ',bestchans.ear ])

%% group figures
figure
subplot(2,2,1)
plot(incongr_cap1.times,best_chan_cap_congr)
hold on
plot(incongr_cap1.times,best_chan_cap_incongr)
plot(incongr_cap1.times,best_chan_cap_diff)
legend('congruent','incongruent','diff')
title(['Best cap channel: ',bestchans.cap ])
ylim([-6 4])

subplot(2,2,2)
plot(incongr_cap1.times,best_chan_stacap_congr)
hold on
plot(incongr_cap1.times,best_chan_stacap_incongr)
plot(incongr_cap1.times,best_chan_stacap_diff)
legend('congruent','incongruent','diff')
title(['Standard cap channel: ',bestchans.sta_cap,' - nose ref.' ])
ylim([-6 4])

subplot(2,2,3)
plot(incongr_cap1.times,best_chan_cleanear_congr)
hold on
plot(incongr_cap1.times,best_chan_cleanear_incongr)
plot(incongr_cap1.times,best_chan_cleanear_diff)
legend('congruent','incongruent','diff')
title(['Best clean ear channel: ',bestchans.cleanear ])
ylim([-6 4])

subplot(2,2,4)
plot(incongr_cap1.times,best_chan_ear_congr)
hold on
plot(incongr_cap1.times,best_chan_ear_incongr)
plot(incongr_cap1.times,best_chan_ear_diff)
legend('congruent','incongruent','diff')
title(['Best ear channel: ',bestchans.ear ])
ylim([-6 4])


figure
plot(incongr_cap1.times,best_chan_cap_diff)
hold on
plot(incongr_cap1.times,best_chan_stacap_diff)
plot(incongr_cap1.times,best_chan_ear_diff)
plot(incongr_cap1.times,best_chan_cleanear_diff)
legend('cap','standard cap','ear','clean ear')
title('Best channels: difference waves')
ylim([-6 4])

%% Hedge's g

%% get the sta/incongr ERPs from the best channel (CAP)

% get the respective best channel
cap(1) = str2num(bestchans.cap(2:3));
cap(2) = str2num(bestchans.cap(end-1:end));

capGincongr = squeeze(incongr_cap.data(cap(1),:,:) - incongr_cap.data(cap(2),:,:));
capGcongr = squeeze(congr_cap.data(cap(1),:,:) - congr_cap.data(cap(2),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
if size(capGcongr,2) > size(capGincongr,2)
    trials = randperm(size(capGcongr,2),size(capGincongr,2));
    capGcongr = capGcongr(:,trials);
else
    trials = randperm(size(capGincongr,2),size(capGcongr,2));
    capGincongr = capGincongr(:,trials);
end

% calc
stats = mes(capGcongr',capGincongr','hedgesg','isDep',0);

figure
plot(incongr_cap.times,stats.hedgesg)
legend('Hedge�s g for the best cap electrodes')
title('Best channels:effect size','FontSize', 20)

%% get the sta/incongr ERPs from the best channel (STA_CAP)

staCap(1) = sta_elecs(2);

staCapGincongr = squeeze(incongr_cap.data(staCap(1),:,:));
staCapGcongr = squeeze(congr_cap.data(staCap(1),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
if size(staCapGincongr,2) > size(staCapGcongr,2)
    trials = randperm(size(staCapGincongr,2),size(staCapGcongr,2));
    staCapGincongr = staCapGincongr(:,trials);
else
    trials = randperm(size(staCapGcongr,2),size(staCapGincongr,2));
    staCapGcongr = staCapGcongr(:,trials);
end

% calc
stats = mes(staCapGcongr',staCapGincongr','hedgesg','isDep',0);

figure
plot(incongr_cap.times,stats.hedgesg)
legend('Hedge�s g for the standard cap electrodes')
title('Best channels: effect size','FontSize', 20)

%% get the sta/incongr ERPs from the best channel (EAR_CLEAN)

ind_earclean(1) = find(strcmp({clean_congr_ear1.chanlocs.labels},bestchans.cleanear(1:3)));
ind_earclean(2) = find(strcmp({clean_congr_ear1.chanlocs.labels},bestchans.cleanear(end-2:end)));

EarCleanGincongr = squeeze(clean_incongr_ear.data(ind_earclean(1),:,:) - clean_incongr_ear.data(ind_earclean(2),:,:));
EarCleanGcongr = squeeze(clean_congr_ear.data(ind_earclean(1),:,:) - clean_congr_ear.data(ind_earclean(2),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
if size(EarCleanGincongr,2) > size(EarCleanGcongr,2)
    trials = randperm(size(EarCleanGincongr,2),size(EarCleanGcongr,2));
    EarCleanGincongr = EarCleanGincongr(:,trials);
else
    trials = randperm(size(EarCleanGcongr,2),size(staCapGincongr,2));
    EarCleanGcongr = EarCleanGcongr(:,trials);
end

% calc
stats = mes(EarCleanGcongr',EarCleanGincongr','hedgesg','isDep',0);

figure
plot(incongr_cap.times,stats.hedgesg)
legend('Hedge�s g for the cleaned ear electrodes')
title('Best channels: effect size','FontSize', 20)

%% get the sta/incongr ERPs from the best channel (EAR)

ind_ear(1) = find(strcmp({congr_ear1.chanlocs.labels},bestchans.ear(1:3)));
ind_ear(2) = find(strcmp({congr_ear1.chanlocs.labels},bestchans.ear(end-2:end)));

EarGincongr = squeeze(incongr_ear.data(ind_ear(1),:,:) - incongr_ear.data(ind_ear(2),:,:));
EarGcongr = squeeze(congr_ear.data(ind_ear(1),:,:) - congr_ear.data(ind_ear(2),:,:));

% calc random subselection of trials from the standards to have equal trial
% lengths
if size(EarGincongr,2) > size(EarGcongr,2)
    trials = randperm(size(EarGincongr,2),size(EarGcongr,2));
    EarGincongr = EarGincongr(:,trials);
else
    trials = randperm(size(EarGcongr,2),size(EarGincongr,2));
    EarGcongr = EarGcongr(:,trials);
end

% calc
stats = mes(EarGcongr',EarGincongr','hedgesg','isDep',0);

figure
plot(incongr_cap.times,stats.hedgesg)
legend('Hedge�s g for the ear electrodes')
title('Best channels: effect size','FontSize', 20)


