
%% settings
unilateral    = 1; %if 0, use both ears. If 1, use only right ear
window_start  = [50 130 300 200]; % start of the time window of interest per task. Should contain the desired effect
window_stop   = [150 230 600 600]; % end of the time window of interest per task. Should contain the desired effect
srate         = 100; % downsample to ...
iterations    = 100; % number of iterations to draw subsamples from the condition with higher trial number
taskLetter    = {'a','b','c','d',};
staElec    = [8 8 8 8]; % channel that is usually used to detect the task effect in the literature
conditionString = {'cap','earc','ear'};
timeVec = linspace(-200,790,100);
timeVecN1 = linspace(50,250,20);
BC = '_bipolar';
conditionNum = 3;

%% set paths and load data
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
    ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
    ,'fuhi52','okum71','kuma55','pigo79'
    };
%SBJ = {'p01','p02','p03','p04','timing_test'}; % subject list, update for every new sbj
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');

%% plots for g
for s = 7%:length(SBJ)
    
    load([MAINPATH,'_supplementary\quality_check.mat'],'q_check');
    getSBJ = contains({q_check.SBJ},SBJ{s});
    q_check = q_check(getSBJ);
    
    for taskNum = 1%:4
        if q_check(taskNum).badEarChan == 0
            
            timeWin = find(timeVec == window_start(taskNum)) : find(timeVec == window_stop(taskNum)) -1;
            
            % get folder names
            if taskLetter{taskNum} == 'a'
                folderName = 'a_attended_speaker';
            elseif taskLetter{taskNum} == 'b'
                folderName = 'b_passive_oddball';
            elseif taskLetter{taskNum} == 'c'
                folderName = 'c_active_oddball';
            elseif taskLetter{taskNum} == 'd'
                folderName = 'd_conguency';
            else
                error('please enter a valid taskLetter (a-d)')
            end
            
            if unilateral == 1
                earFolder = '\05_ERP_uni\';
            elseif unilateral == 0
                earFolder = '\05_ERP\';
            end
            
            % load the effect sizes, channel*time*iteration
            load([MAINPATH,SBJ{s},earFolder,folderName,'\trials_',conditionString{conditionNum},BC,'.mat'],'trials');
            load([MAINPATH,SBJ{s},earFolder,folderName,'\g_',conditionString{conditionNum},'_',num2str(iterations),BC,'.mat'],'effect_size_matrix');
            load([MAINPATH,SBJ{s},earFolder,folderName,'\',conditionString{conditionNum},BC,'_loc.mat'],'best_loc');
            load([MAINPATH,SBJ{s},earFolder,folderName,'\',conditionString{conditionNum},BC,'_bestchans.mat'],'bestchans');
            load([MAINPATH,SBJ{s},earFolder,folderName,'\',conditionString{conditionNum},BC,'_ERPs.mat'],'ERPs');
            
            if taskNum == 1
                effect_ear.a(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                effect_ear.a(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                effect_ear.a(s).best_loc           = best_loc; % indices of the best n channels
                effect_ear.a(s).bestchans          = bestchans; % string names of the n best channels
                effect_ear.a(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
            elseif taskNum == 2
                effect_ear.b(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                effect_ear.b(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                effect_ear.b(s).best_loc           = best_loc; % indices of the best n channels
                effect_ear.b(s).bestchans          = bestchans; % string names of the n best channels
                effect_ear.b(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
            elseif taskNum == 4
                effect_ear.c(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                effect_ear.c(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                effect_ear.c(s).best_loc           = best_loc; % indices of the best n channels
                effect_ear.c(s).bestchans          = bestchans; % string names of the n best channels
                effect_ear.c(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
            else taskNum == 4
                effect_ear.d(s).trial              = trials; % matrix of permutations of the condition with more trials (for reproducability)
                effect_ear.d(s).es_matrix_mean     = mean(effect_size_matrix,3); %effect sizes, channel*time*iteration
                effect_ear.d(s).best_loc           = best_loc; % indices of the best n channels
                effect_ear.d(s).bestchans          = bestchans; % string names of the n best channels
                effect_ear.d(s).ERPs               = ERPs; % ERPs of both conditions for the n best channels, channel*time*trials
            end
            
            %transform the ERPs to be in the right direction (because of the way we get all bipolar channels,
            %the same channel is sometimes subtracted and sometimes subtracted from. Therefore, the sign is sometimes reversed)
            if taskNum == 1
                timeWin = find(timeVec == 0) : find(timeVec == 300) -1;
            end
            
            if taskLetter{taskNum} ~= 'c'
                m1 = mean(effect_ear.a(s).ERPs.cond1,3);
                m2 = mean(effect_ear.a(s).ERPs.cond2,3);
                m3 = m1-m2;
                
                for w = 1:size(m1,1)
                    [pksMax(w), locsMax]= max(smoothdata(m3(w,timeWin),2,'movmean',4));
                    [pksMin(w), locsMin]= min(smoothdata(m3(w,timeWin),2,'movmean',4));
                    
                    % there should be a negative peak in this time window. If the largest
                    % neg. peak is smaller than the largest positive one (in abs. val.),
                    % the signal is the wrong way around. Inverse with signal * -1
                    if abs(pksMax(w)) > abs(pksMin(w))
                        m3(w,:) = m3(w,:) * -1;
                    end
                end
            else
                m1 = mean(effect_ear.a(s).ERPs.cond1,3);
                m2 = mean(effect_ear.a(s).ERPs.cond2,3);
                m3 = m1-m2;
                
                for w = 1:size(m1,1)
                    [pksMax(w), locsMax]= max(smoothdata(m3(w,timeWin),2,'movmean',4));
                    [pksMin(w), locsMin]= min(smoothdata(m3(w,timeWin),2,'movmean',4));
                    
                    % P300 is the only positive deflection, max should be
                    % larger than min
                    if abs(pksMax(w)) < abs(pksMin(w))
                        m3(w,:) = m3(w,:) * -1;
                    end
                end
                
                % for d = 1: size(m1,1)
                figure
                plot(timeVec,smoothdata(m3,2,'movmean',4))
                % end
            end
        end
    end
end


% figure
% for x = 1:length(SBJ)
% subplot(4,5,x)
% imshow([MAINPATH,SBJ{x},'\figures\fig1_ind.png']);
% end




