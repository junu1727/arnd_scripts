%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% s00_convert_rawdata.m
% Prepare files. Load in brainVision files with rawdata, channel files and
% log files from the experiments. Add channel locations and remove baseline
% period from rawdata. Calculate reaction times and make a list of the
% critical word onsets for the congruency task (d).
% Arnd Meiser
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
SBJ = { 'ergi95','tkhc31','vmsl42','mvqj04','blcg82','kobi50','cuxa79','fipo15'...
       ,'olan63','zilu79','elve64','nico34','bilo80','huka66','nili97','yijo14'...
       ,'fuhi52','okum71','kuma55','pigo79'
       };
   
MAINPATH = fullfile('O:',filesep,'arm_testing',filesep,'Experimente',filesep,'cEEGrid_vs_Cap',filesep,'data',filesep);
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
load('O:\arm_testing\Experimente\cEEGrid_vs_Cap\paradigms\d_Congruency N400\audiostruc.mat'); % load in the list of all CW-onsets (task d, N400)

for s = 1:length(SBJ)
    loadList = dir([MAINPATH,'*\log\*',SBJ{s},'_d*.log']); %load in the log of the order of stimulus presentation (task d, N400)
    % set some paths
    PATHIN  = fullfile(MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep); % path containing rawdata
    PATHOUT = fullfile(MAINPATH,SBJ{s},filesep, '01_raw_eeglab', filesep);    % path for script output
     
    chan_file = dir(fullfile(MAINPATH,SBJ{s},filesep,'*.xyz'));

    
    if ~exist (PATHOUT)
        mkdir(PATHOUT)
    end
   
    list=dir([PATHIN,'*.vhdr']); %reads all .vhdr files in that path
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    
    for h = 1:length(list)
        % Loop over subjects
        EEG = pop_biosig([MAINPATH,SBJ{s},filesep,'00_EEG raw',filesep,list(h).name]);
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off');
        
        EEG = pop_chanedit(EEG, 'lookup',['O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\',SBJ{s},'\\',SBJ{s},'_elec.xyz'],...
              'rplurchanloc',1,'load',{['O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\',SBJ{s},'\\',SBJ{s},'_elec.xyz'],'filetype','xyz'});
               
        startlat = find( strcmp({EEG.event.type},{'S  7'}));%index of start latency
        if isempty(startlat) %if baseline is shorter than 60s...
            EEG = eeg_eegrej(EEG, [1 EEG.event(2).latency - EEG.srate]); % remove baseline data (first real marker - 1s)
        else
            EEG = eeg_eegrej(EEG, [1 EEG.event(startlat(end)+1).latency - EEG.srate]); % remove baseline data (first real marker - 1s)
        end
        
        if h == 4
            PATHAUDIO = fullfile(MAINPATH,SBJ{s},filesep, 'log', filesep);    % path for audio-order
            if ~exist (PATHAUDIO)
                mkdir(PATHAUDIO)
            end
            
            % load the stim log to get the order of the presented stimuli
            senorder = importPresentationLog([loadList.folder,filesep,loadList.name]); %load in the table containing the names of the stimuli in order of presentation
            indsen = {senorder(find(strcmp('Sound',{senorder.event_type}))).code};
            indsen(1:2) = [];%remove first to sentences, those were training
            
            rtime = cell2mat({senorder(find(strcmp('Sound',{senorder.event_type}))-1).ttime})
            rtime(1:2) = [];%remove first to sentences, those were training
            rTime = rtime / 10000; %change unit to seconds
            
            % find the critical word onset for every sentence in the CW-list
            audioSBJ = zeros(200,1);
            for i = 1:length(indsen)
                x = strcmp(indsen{1,i},audiostruc.names); %find the name of the presented stimulus in the list
                audioSBJ(i,1) = audiostruc.length(x); %store the CW-onset from the list in a new variable. Do it for every presented stimulus
            end
            save([PATHAUDIO,'CW_onsets.mat'],'audioSBJ','rTime');
        end
        
        EEG.comments='';
        EEG.setname = strrep(list(h).name, '.vhdr', '');
        EEG = pop_saveset(EEG, [EEG.setname,'.set'], PATHOUT);
        
        eeglab redraw % redraw interface
    end
end

%% DO NOT USE LIKE THIS!!!!!!
% 
% [STUDY ALLEEG] = std_editset( STUDY, [], 'name','Attended Speaker','task','a','commands',...
% {{'index',1,'load','O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\ergi95\\01_raw_eeglab\\ergi95_a.set'...
% ,'condition','a','subject','1'}},'updatedat','on','rmclust','on' );
% [STUDY ALLEEG] = std_checkset(STUDY, ALLEEG);
% CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
% 
% task = {'a','b','c','d'};
% f = 1;
% for x = 2:length(SBJ)
% [STUDY ALLEEG] = std_checkset(STUDY, ALLEEG);
% CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
% [STUDY ALLEEG] = std_editset( STUDY, ALLEEG, 'commands',{{'index',x,'load',['O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\',SBJ{x},'\\01_raw_eeglab\\',SBJ{x},'_',task{f},'.set']...
% ,'subject',num2str(x),'condition',task{f}}},'updatedat','on','rmclust','on' );
% [STUDY ALLEEG] = std_checkset(STUDY, ALLEEG);
% end
% 
% [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
% [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 5,'retrieve',1,'study',0); 
% [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'retrieve',2,'study',0); 
% [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'retrieve',[1:3] ,'study',0); 
% [STUDY ALLEEG] = std_editset( STUDY, [], 'name','test','task','test a','commands',{{'index',1,'load','O:\\arm_testing\\Experimente\\cEEGrid_vs_Cap\\data\\mvqj04\\01_raw_eeglab\\mvqj04_a.set','subject','1','session',1,'condition','a'}},'updatedat','on','rmclust','on' );
% [STUDY ALLEEG] = std_checkset(STUDY, ALLEEG);
% CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
% pop_taskinfo(ALLEG);
% 
