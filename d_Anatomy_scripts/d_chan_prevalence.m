%% This script load in the results from d_sensitivity maps (ROI, torchlight
%sources) and analyses the results, showing which channels recorded the
%highest amplitude and showing in addition the location of these
%sources per channel. Also, compute a difference map from the two sensitivity
%maps

%% plot how many times a cEEGrid pair had the highest amplitude

SubjectName = 'Subject04_normal_cEEGrid'; %the subject must be the one with the SURFACE HEADMODEL, not the volume one
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
PATHOUT = 'O:\arm_testing\Experimente\Anatomical_differences\Results_sensitivity_round_grid\'; %store the results here
cd([MAINPATH,'brainstorm3\']);
%brainstorm
EarChannel = '2'; %1 = left, 2 = right, 0 = both
nameResult = 'cEEGrid'; % cap or cEEGrid

ResultsCap = dir([PATHOUT,'results_Cap\',SubjectName,'\results*']); %var names: ResultsC or ResultsF / SourceDist
ResultsCEEGrid = dir([PATHOUT,'results_cEEGrid\',SubjectName,'\results_Right*']); %var names: ResultsC or ResultsF / SourceDist

cEEGrid = load([ResultsCEEGrid.folder,'\',ResultsCEEGrid.name]);
Cap = load([ResultsCap.folder,'\',ResultsCap.name]);

%% when both cEEGrid and full cap were calculated, make a signal loss map
listSens = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_2*']);
export_matlab([listSens(1).folder,'\',listSens(end-1).name],'cEEGrid');
export_matlab([listSens(1).folder,'\',listSens(end).name],'fullCap');

diff = Cap.SourceDist;
diff.Comment = 'DifferenceMap';
for s = 1:length(Cap.SourceDist.ImageGridAmp)
    diff.ImageGridAmp (s,1)= 100 -(cEEGrid.SourceDist.ImageGridAmp(s) / Cap.SourceDist.ImageGridAmp(s) *100);
end

diff.ImageGridAmp(diff.ImageGridAmp<0)= -0.01; %change all negative values to -0.01 to have a uniform display in the bst-figure

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
db_add(ProtocolInfo.iStudy, diff, []);

%% calc the channels that record the best amp per source
for f = 1:length(cEEGrid.ResultsC) %calculate the percentage change from the full cap to the cEEGrid and the bipolar channels
    PerceChangecEEGrid(f,1) = 100 -(ResultsC(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
end

belowAvg = find(PerceChangecEEGrid < 50); %find the indices of the signal losses below average or below a specified value (here, it's 50%)

w=1;
for f = 1:length(belowAvg) %for all signal losses below the average...
    bestPairs(w,1) = cEEGrid.ResultsC(belowAvg(f)).Pair ;
    w=w+1;
end

listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile

if EarChannel == '2'
    outMatrix=zeros(10); %create a matrix with the number of channels of the right cEEGrid
    outSource = repmat({ones(1, 1)}, 10); % create cell array to store indices of the sources
elseif EarChannel == '1'
    outMatrix=zeros(10); %create a matrix with the number of channels of the left cEEGrid
    outSource = repmat({ones(1, 1)}, 10);
else EarChannel == '0'
    outMatrix=zeros(20); %create a matrix with the number of channels of both cEEGrids
    outSource = repmat({ones(1, 1)}, 20);
end

for  k = 1:length(bestPairs)
    if EarChannel == '2' %right cEEGrid
        a = str2num((bestPairs{k}(2:3)))-10; %to make the matrix size fit. Later, the channel names are restored for the final display
        b = str2num((bestPairs{k}(end-1:end)))-10;
        outMatrix(a,b) = outMatrix(a,b)+1;
        outSource{a,b}(end+1) = ResultsC(k).SourceInd; %stores location of the sources per max. recording channel
    elseif EarChannel == '1' %left cEEGrid
        a = str2num((bestPairs{k}(2:3)));
        b = str2num((bestPairs{k}(end-1:end)));
        outMatrix(a,b)=outMatrix(a,b)+1;
        outSource{a,b}(end+1) = ResultsC(k).SourceInd;
    else EarChannel == '0' %both cEEGrids
        a = str2num((bestPairs{k}(2:3)));
        b = str2num((bestPairs{k}(end-1:end)));
        outMatrix(a,b)=outMatrix(a,b)+1;
        outSource{a,b}(end+1) = ResultsC(k).SourceInd;
    end
end

outMatrix(~triu(outMatrix,1)) = 1000; %not optimal. For the final coloring, this must be adjusted (its currently done manually)
outMatrix(~triu(outMatrix,1)) = -0.5;
outMatrix(~triu(outMatrix)) = -1;
outMatrix(outMatrix > 999) = 0;

chanLabelsRight = {'E11','E12','E13','E14','E15','E16','E17','E18','E19','E20'};
% figure
% imagesc(outMatrix);
% xticks(1:10)
% xticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
% yticks(1:10)
% yticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
% set(gca,'fontweight','bold','fontsize',12);
% xlabel('cEEGrid electrodes', 'FontSize', 12,'FontWeight','bold')
% set(gcf,'color','w');
% set(gcf,'color','w');
% axis square
% saveas(gcf,[PATHFIG,'Connections.png']);


figure
imagesc(outMatrix);
xticks(1:10)
xticklabels(chanLabelsRight)
yticks(1:10)
yticklabels(chanLabelsRight)
set(gca,'fontweight','bold','fontsize',12);
xlabel('cEEGrid electrodes', 'FontSize', 12,'FontWeight','bold')
set(gcf,'color','w');
set(gcf,'color','w');
axis square

%% now look at the distribution of the sources per channel recording the max amplitude

%under contruction
% [~,numAmps] = cellfun(@size,outSource);
% 
% Source.ChannelFlag(strcmp({CM.Channel.Name},chanLabelsRight(6))==0) = -1;
% Source.ChannelFlag(strcmp({CM.Channel.Name},chanLabelsRight(10))==0) = -1;

Source = SourceDist;
Source.ImageGridAmp = zeros(size(Source.ImageGridAmp,1),size(Source.ImageGridAmp,2));
Source.ImageGridAmp(outSource{2,9}(2:end)) = 1; %it is 2:end because the first  value is always 1
Source.Comment = 'BestChanSourceLoc_E12_E19';

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
db_add(ProtocolInfo.iStudy, Source, []); %this adds the new source projection to the current protocol




