%% calculate for real data (Daniel, MA) the channels that capture the
% highest amplitude for every trial per subject (sporadic, standards)

load('O:\arm_testing\Experimente\Anatomical_differences\Real_data_Daniel\all_data.mat','originalEEG','trials_sta_long');

collect={};
index=1;
iChannels= 9:size(trials_sta_long{1,1},1); %only left channels
chanName = {};

for s = 1:length(trials_sta_long) %select randomly as many trials as there are for the participant with the fewest trials
    rows = randsample(size(trials_sta_long{1,s},3),74);
    trials{1,s} = trials_sta_long{1,s}(iChannels,51:126,rows);
end

for t = 1:size(trials,2)%participants %find the best channel pair for every participant, every trial and every channel combination
    for s = 1:size(trials{1,1},3) %trials
        index=1;
        for ch=1:length(iChannels)-1 %channel combination
            for b=ch+1:length(iChannels)
                collect{1,t}(index,:,s) = abs((trials{1,t}(ch,:,s))- (trials{1,t}(b,:,s))); %calc every combination of electrodes
                chanName{1,index} = [originalEEG.chanlocs(iChannels(ch)).labels,'-',originalEEG.chanlocs(iChannels(b)).labels];
                index=index+1;
            end
        end
    end
end

for b = 1:size(trials,2) %find the peaks within every trial for every channel, for every subject
    for t = 1:size(trials{1,1},3)
        for s = 1:size(collect{1,1},1)
            [pks{b,s,t},locs{b,s,t}] = findpeaks(collect{1,b}(s,15:50,t)); %find all peaks for every chan-combo in every trial, within the range from 15:50
        end
    end
end

for b = 1:size(trials,2)
    for t = 1:length(pks)
        for s = 1:size(locs,2)
            locs{b,s,t} = locs{b,s,t}(find(pks{b,s,t}==max(pks{b,s,t}))); % reduce locations to max peaks within the range
        end
    end
end

for b = 1:size(trials,2)
    for t = 1:length(pks)
        for s = 1:size(locs,2)
            if isempty(pks{b,s,t})
                pks{b,s,t} = 0;
            end
        end
    end
end

for b = 1:size(trials,2)
    for t = 1:length(pks)
        for s = 1:size(locs,2)
            pks{b,s,t} = pks{b,s,t}(find(pks{b,s,t}==max(pks{b,s,t}))); % reduce locations to max peaks within the range
        end
    end
end

for b = 1:size(trials,2)
    for t = 1:length(pks)
        [placeholder maxloc(b,t) ~] = find([pks{b,:,t}] == max([pks{b,:,t}])); % maxloc = the position of the highest value within pks, aka the channel (look up in chanName)
        maxpk(b,t) =  pks{b,(find([pks{b,:,t}] == max([pks{b,:,t}]))),t}; % maxpk = the value of the amplitude of the best chan
    end
end

outMatrixReal = zeros(size(trials,2),numel(iChannels),numel(iChannels)); %create a matrix with the number of channels of the cEEGrid

for a = 1:size(trials,2)
    for  k = 1:length(maxloc)
        b=str2num((chanName{maxloc(a,k)}(2:3)));
        c=str2num((chanName{maxloc(a,k)}(end-1:end)));
        outMatrixReal(a,b,c) = outMatrixReal(a,b,c)+1; %this is the final matrix giving us for every subject the channel and the number of times it recorded the highest amplitude
    end
end

%these are the locations of the cEEGrid electrodes
el(1,:)=[1778 231];%starting (top right)
el(2,:)=[1669 76 ];
el(3,:)=[1491 76];
el(4,:)=[1384 231];
el(5,:)=[1356 353];
el(6,:)=[1356 475];
el(7,:)=[1384 592];
el(8,:)=[1491 749];
el(9,:)=[1669 749];
el(10,:)=[1778 592];

%we calc every angle of every channel combination
for a = 1:length(el)
    for b = 1:length(el)
        P1 = el(2,:); %this is the vertical orientation that is the standard all other angles are compared to
        P2 = el(9,:);
        P3 = el(a,:);
        P4 = el(b,:);
        
        v1 = P2 - P1;
        v2 = P4 - P3;
        angle(a,b) = rad2deg(acos(sum(v1.*v2)/(norm(v1)*norm(v2)))); %matrix containing
    end
end

x = squeeze(ones(size(outMatrixReal(1,:,:))));
idx = rot90(tril(rot90(x),-1),2); % get the index of only the values above the diagonal

angleCeegrid = angle(idx==1); %this variable hold the angle of each channel relative to a vertical line
angleSim = (0:18:180-18)';

for a = 1:size(trials,2)
    outMtrx = outMatrixReal(a,:,:);
    outMtrx = squeeze(outMtrx);
    outMtrx(~triu(outMtrx,1)) = 1000; %not optimal. For the final coloring, this must be adjusted (its currently done manually)
    outMtrx(~triu(outMtrx,1)) = -0.5;
    outMtrx(~triu(outMtrx)) = -1;
    outMtrx(outMtrx > 999) = 0;
    numCEEGrid(:,a) = outMtrx(idx==1); % this variable stores per subject how many times a channel had the highest amp
    
    if a == 1
        figure
    end
    
    subplot(2,4,a)
    imagesc(outMtrx);
    xticks(1:10)
    xticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
    yticks(1:10)
    yticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
    caxis([0 10]) % defines all plots to have the same colorbar limits. max is actually 15, but next value is 10
    set(gca,'fontweight','bold','fontsize',12);
    xlabel(['cEEGrid Sbj',num2str(a)], 'FontSize', 12,'FontWeight','bold')
    set(gcf,'color','w');
    set(gcf,'color','w');
    axis square
end

%% now with sim data

for t = 5:9
    SubjectName = ['Subject0',num2str(t),'_roundGrid_20']; %specify the name of the subject that will be used in the brainstorm GUI
    load(['O:\arm_testing\Experimente\Anatomical_differences\midlines\sensitivites\',SubjectName,'sensMid.mat'],'ResultsC');
    
    if length(ResultsC) == 44
        outMatrixSim=zeros(5,numel(iChannels),numel(iChannels)); %create a matrix with the number of channels of the round cEEGrid
    else
        ResultsC = ResultsC(round(length(ResultsC)/2)-22 : round(length(ResultsC)/2)+21); %delete the first and last values to prune the data to the length of the data set with fewest values (44)
        outMatrixSim=zeros(5,numel(iChannels),numel(iChannels)); %create a matrix with the number of channels of the round cEEGrid
    end
    
    for  k = 1:length(ResultsC)
        favChan = strfind([ResultsC(k).CheckPair{:,4}],'1');
        a=str2num((ResultsC(1).CheckPair{favChan,2}(2:3)));
        b=str2num((ResultsC(1).CheckPair{favChan,2}(2:3)));
        outMatrixSim(t-4,a,b)=outMatrixSim(t-4,a,b)+1;
    end
    numSim(:,t-4) = diag(squeeze(outMatrixSim(t-4,:,:))); %this variable contains the number of times each angle recorded the highest amps
    
    if t == 5
        figure
    end
    subplot(2,3,t-4)
    imagesc(squeeze(outMatrixSim(t-4,:,:)));
    xticks(1:10)
    xticklabels({'L1','L2','L3','L4','L5','L6','L7','L8','L9','L10'})
    yticks(1:10)
    yticklabels({'L1','L2','L3','L4','L5','L6','L7','L8','L9','L10'})
    set(gca,'fontweight','bold','fontsize',12);
    xlabel('cEEGrid electrodes', 'FontSize', 12,'FontWeight','bold')
    set(gcf,'color','w');
    set(gcf,'color','w');
    axis square
    
    save(['O:\arm_testing\Experimente\Anatomical_differences\midlines\sensitivites\',SubjectName,'sensMid_pruned.mat'],'ResultsC');
    
end

save(['O:\arm_testing\Experimente\Anatomical_differences\Real_data_Daniel\outMtrx.mat'],'outMatrixReal','outMatrixSim');

%with this, we have the number of times a channel recorded the highest
%amplitude for both real and simulated data. Now we compare that to the
%matrix giving us the angle of each channel

%% finally, create matrices that contain the angles the number of times they recorded the highest amplitude

vecSim = {};
counter = 1;
for s = 1:size(numSim,2)
    for t = 1:size(numSim,1)
        matrep = repmat(angleSim(t),numSim(t,s),1);
        if ~isempty(matrep) %some angles don't appear, but their 0s mess up the indexing, so only include the others
            vecSim{1,s}(counter:length(matrep)+counter-1,1) = matrep;
            counter = counter + length(matrep);
        end
    end
    counter = 1;
end

vecCeegrid = {};
counter = 1;
for s = 1:size(numCEEGrid,2)
    for t = 1:size(numCEEGrid,1)
        matrep = repmat(angleCeegrid(t),numCEEGrid(t,s),1);
        if ~isempty(matrep) %some angles don't appear, but their 0s mess up the indexing, so only include the others
            vecCeegrid{1,s}(counter:length(matrep)+counter-1,1) = matrep;
            counter = counter + length(matrep);
        end
    end
    counter = 1;
end

save('O:\arm_testing\Experimente\Anatomical_differences\Real_data_Daniel\angles.mat','angleCeegrid','numCEEGrid','angleSim','numSim','vecSim','vecCeegrid');





