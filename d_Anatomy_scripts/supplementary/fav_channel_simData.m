%% calculate for real data (Daniel, MA) the channels that capture the 
% highest amplitude for every trial per subject (sporadic, standards)



collect=zeros(45,76,148);
index=1;
iChannels= 9:size(trials_sta_long{1,1},1); %only left channels
chanName = {};

for s = 1:size(trials_sta_long{1,1},3)
    index=1;
    for ch=1:length(iChannels)-1
        for b=ch+1:length(iChannels)
            collect(index,:,s) = abs((trials_sta_long{1,1}(iChannels(ch),51:126,s))- (trials_sta_long{1,1}(iChannels(b),51:126,s))); %calc every combination of electrodes
            chanName{1,index} = [originalEEG.chanlocs(iChannels(ch)).labels,'-',originalEEG.chanlocs(iChannels(b)).labels];
            index=index+1;
        end
    end
end

for t = 1:size(trials_sta_long{1,1},3)
    for s = 1:size(collect,1)
        [pks{s,t},locs{s,t}] = findpeaks(collect(s,15:50,t)); %find all peaks for every chan-combo in every trial, within the range from 15:50
    end
end

for t = 1:length(pks)
    for s = 1:size(locs,1)
        locs{s,t} = locs{s,t}(find(pks{s,t}==max(pks{s,t}))); % reduce locations to max peaks within the range
    end
end

for x = 1:size(pks,1)
    for i = 1:length(pks)
        if isempty(pks{x,i})
            pks{x,i} = 0;
        end
    end
end


for t = 1:length(pks)
    for s = 1:size(locs,1)
        pks{s,t} = pks{s,t}(find(pks{s,t}==max(pks{s,t}))); % reduce locations to max peaks within the range
    end
end

for t = 1:length(pks)
  [placeholder maxloc(1,t) ~] = find([pks{:,t}] == max([pks{:,t}])); % maxloc = the position of the highest value within pks, aka the channel (look up in chanName)
  maxpk(1,t) =  pks{(find([pks{:,t}] == max([pks{:,t}]))),t}; % maxpk = the value of the amplitude of the best chan
end

outMatrix=zeros(numel(iChannels)); %create a matrix with the number of channels of the cEEGrid

for  k = 1:length(maxloc)
    a=str2num((chanName{maxloc(k)}(2:3)));
    b=str2num((chanName{maxloc(k)}(end-1:end)));
    outMatrix(a,b)=outMatrix(a,b)+1;
end

outMatrix(~triu(outMatrix,1)) = 1000; %not optimal. For the final coloring, this must be adjusted (its currently done manually)
outMatrix(~triu(outMatrix,1)) = -0.5;
outMatrix(~triu(outMatrix)) = -1;
outMatrix(outMatrix > 999) = 0;

figure
imagesc(outMatrix);
xticks(1:10)
xticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
yticks(1:10)
yticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
set(gca,'fontweight','bold','fontsize',12);
xlabel('cEEGrid electrodes', 'FontSize', 12,'FontWeight','bold')
set(gcf,'color','w');
set(gcf,'color','w');
axis square
saveas(gcf,[PATHFIG,'Connections.png']);







