
clear
%% Load stuff
SubjectName = 'Subject06_roundGrid_20'; %the subject must be the one with the SURFACE HEADMODEL, not the volume one
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]);
if length(HM.Gain) > 30002
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
end
listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); 
Source = load([listSource(1).folder,'\',listSource(1).name]);
Source.ImageGridAmp= zeros(size(Source.ImageGridAmp,1),1);
Source.DataFile=[];
Source.Time = 1;

Scout = load(['O:\arm_testing\Experimente\Anatomical_differences\midlines\scouts\scout_Destrieux_1_',SubjectName,'.mat']);

listAnat = dir([MAINPATH,'\brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\tess_cortex_pial_low.mat']);
export_matlab([listAnat.folder,'\',listAnat.name],'cortex');

%% shortestpath
cfull = full(cortex.VertConn);

b = 1;
for s = 1:length(Scout.Scouts.Vertices)
[df,col,dg] = find(cfull(Scout.Scouts.Vertices(s),:)==1); %find the vertices adjacent to 1 scout vertex
col = col(ismember(col,Scout.Scouts.Vertices)); %only keep those neighbors that are within the scout
vec(b: b-1+length(col),1) = col; %write neighoring vertices for a scout vertex
vert = Scout.Scouts.Vertices(s);
vec(b: b-1+length(col),2) = repelem(vert,length(col)); %write in the scout vertex next to all neighboring vertices
b = b + length(col);
end

for  f = 1:length(vec) %find the distances between neighboring vertices
[~,vec(f,3)] = dsearchn(cortex.Vertices(vec(f,2),:),cortex.Vertices(vec(f,1),:));
end
vec(:,3) = vec(:,3)*1000; %easier reading

% %find the most anterior/posterior points in the scout
[ant, maxloc1] = max(HM.GridLoc(Scout.Scouts.Vertices,1));
maxloc2 = Scout.Scouts.Vertices(maxloc1);
[post, minloc1] = min(HM.GridLoc(Scout.Scouts.Vertices,1));
minloc2 = Scout.Scouts.Vertices(minloc1);

%%midlines, all hand-made
%midline = [22404 26624 22684 26935 27679 27676]; %Subject 02
%midline = [21001 26093 27437 21734]; %Subject 01
%midline = [maxloc2 2472 2025 514 7968 1613 minloc2]; %Subject05
midline = [maxloc2 7983 12237 560 12354 1802 minloc2]; %Subject06
%midline = [7125 7186 1670 6699 12223 minloc2]; %Subject07
%midline = [maxloc2 2159 12142 12322 12347 minloc2]; %Subject08
%midline = [maxloc2 7704 7195 1939 10855 6174 6250 minloc2]; %Subject09

G = graph(vec(:,2)',vec(:,1)',vec(:,3)');
b=1;
for x = 1:length(midline)-1
P = shortestpath(G,midline(x),midline(x+1));
Path(b: b-1+length(P),1) = P;
b = b + length(P);
end

testpoint = Source;
testpoint.ImageGridAmp(Path,1) = 0.1;
testpoint.Comment = [SubjectName,'_midline'];
ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, testpoint, []);

save(['O:\arm_testing\Experimente\Anatomical_differences\midlines\Shortestpaths\',SubjectName,'.mat'],'Path','G','midline','Scout','testpoint');


