save('O:\arm_testing\Dokumente\Martins Zeug\CorrVars.mat','HM');


%load file, contains head model (HM)

IMAGEGRIDAMP = zeros(15002,10000); %create signal matrix

for fd = 1:15002
    IMAGEGRIDAMP(fd,:) = (-5 + (5+5)*rand(1,10000))./1000; %fill with random numbers between -5 and 5
end

F = zeros(20, size(IMAGEGRIDAMP,2)); % Simulation matrix size(Source.ImageGridAmp,2)
F(1:20,:) = HM.Gain(129:148,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source (leadfield*activation)
%129-148 are the cEEGrid channel, the rest in the gain matrix are from the 128 channel cap

signalNoise = zeros(20,10000); %create signal matrix

for fd = 1:20
    signalNoise(fd,:) = (-1 + (1+1)*rand(1,10000)); %create random noise signal between -1 and 1
end

F = F + signalNoise; %We add noise to all electrodes
F = F - F(15,:); % We subtract the ref from all electrodes to get the potential diff
G = mean(F([1:14 17:20],:)-F(15,:)); %mean over all potential differences of all channels expect DRL and REF (electrodes-ref)
F = F-G; % We subtract the mean of all potential differences from all channels
F(15,:)=0; % DRL and REF not interpretable anymore
F(16,:)=0;
[cEEGridCorr p] = corrcoef(F'); %correlation matrix