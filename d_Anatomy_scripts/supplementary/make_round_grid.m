%% create ear-EEG with round grid and arbitrary number of electrodes

% define the x- and y-data for the original line we would like to rotate
x = [1 1];
y = [1 4];

for i = 1:20
    % create a matrix of these points, which will be useful in future calculations
    v = [x;y];
    % choose a point which will be the center of rotation
    x_center = x(1);
    y_center = y(1);
    % create a matrix which will be used later in calculations
    center = repmat([x_center; y_center], 1, length(x));
    % define a xy degree counter-clockwise rotation matrix
    theta = pi/10;       % pi/5 radians = 36 degrees
    R = [cos(theta) -sin(theta); sin(theta) cos(theta)];
    % do the rotation...
    s = v - center;     % shift points in the plane so that the center of rotation is at the origin
    so = R*s;           % apply the rotation about the origin
    vo = so + center;   % shift again so the origin goes back to the desired center of rotation
    % this can be done in one line as:
    % vo = R*(v - center) + center
    % pick out the vectors of rotated x- and y-data
    x_rotated(i,:) = vo(1,:);
    y_rotated(i,:) = vo(2,:);
    
    x(2) = x_rotated(i,2);
    y(2) = y_rotated(i,2);
end

for h = 1:20
    X(h,1) = x_rotated(h,2);
    Y(h,1) = y_rotated(h,2);
    Z(h,1) = 1;
end

figure
scatter3(X,Y,Z)

for x = 1:20
    cEEGrid_chan_locs.Channel(x).Loc(1,:) = (x_rotated(x,2)/100-0.02)*1.2;
    cEEGrid_chan_locs.Channel(x).Loc(2,:) = 0.08;
    cEEGrid_chan_locs.Channel(x).Loc(3,:) = (y_rotated(x,2)/100-0.005)*1.2;
end

for x = 11:20
    cEEGrid_chan_locs.Channel(x).Loc(1,:) = (x_rotated(x-10,2)/100-0.02)*1.2;
    cEEGrid_chan_locs.Channel(x).Loc(2,:) = -0.08;
    cEEGrid_chan_locs.Channel(x).Loc(3,:) = (y_rotated(x-10,2)/100-0.005)*1.2;
end


for h = 1:10
    X(h,1) = cEEGrid_chan_locs.Channel(h).Loc(1);
    Y(h,1) = cEEGrid_chan_locs.Channel(h).Loc(2);
    Z(h,1) = cEEGrid_chan_locs.Channel(h).Loc(3);
end

figure
scatter3(X,Y,Z)

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
bst_process('CallProcess', 'process_channel_project', sFiles, []); %bring the channels down to the surface of the head
