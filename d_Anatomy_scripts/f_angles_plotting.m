%% plot how many times a cEEGrid pair had the highest amplitude
SubjectNameC = 'Subject05_roundGrid_20'; %the subject must be the one with the SURFACE HEADMODEL, not the volume one
SubjectNameF = 'Subject06_cap_128'; %the subject must be the one with the SURFACE HEADMODEL, not the volume one

MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
PATHIN = 'O:\arm_testing\Experimente\Anatomical_differences\Results_sensitivity_round_grid\'; %store the results here
cd([MAINPATH,'brainstorm3\']);
%brainstorm
EarChannel = '2'; %1 = left, 2 = right, 0 = both
nameResult = 'cEEGrid'; % cap or cEEGrid

ResultsCap = dir([PATHIN,'results_Cap\',SubjectNameF,'\results*']); %var names: ResultsC or ResultsF / SourceDist
ResultsCEEGrid = dir([PATHIN,'results_cEEGrid\',SubjectNameC,'\results_Right*']); %var names: ResultsC or ResultsF / SourceDist

cEEGrid = load([ResultsCEEGrid.folder,'\',ResultsCEEGrid.name]);
Cap = load([ResultsCap.folder,'\',ResultsCap.name]);

diff = Cap.SourceDist;
diff.Comment = 'DifferenceMap';
for s = 1:length(Cap.SourceDist.ImageGridAmp)
    diff.ImageGridAmp (s,1)= 100 -(cEEGrid.SourceDist.ImageGridAmp(s) / Cap.SourceDist.ImageGridAmp(s) *100);
end

diff.ImageGridAmp(diff.ImageGridAmp<0)= -0.01; %change all negative values to -0.01 to have a uniform display in the bst-figure

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
db_add(ProtocolInfo.iStudy, diff, []);

% blue
% red
% yellow
% green
% brown
% orange
% pink
% grey
% purple










