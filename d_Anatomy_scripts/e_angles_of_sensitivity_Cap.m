
clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 'Subject06_cap_128'; %the subject must be the one with the SURFACE HEADMODEL, not the volume one
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
PATHOUT = 'O:\arm_testing\Experimente\Anatomical_differences\Results_sensitivity_round_grid\'; %store the results here
cd([MAINPATH,'brainstorm3\']);
%brainstorm
cEEGrid = {'cEEGrid','fullcap'};
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
atlas = '0'; % 2 = Desikan-Killiany 1 = Brodmann 0 = Destrieux
EarChannel = '2'; %1 = left, 2 = right, 0 = both
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\@intra\channel*']);
listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\@intra\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\@intra\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\@intra\headmodel*']);
listAnat = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\tess_cortex_pial_low_02.mat']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in a_PrepareSources.m...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
ANAT = load([listAnat(1).folder,'\',listAnat(1).name]);

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things

Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
SourceDist = Source; % source variable to work in

%calculate the sensitivity for a source (pointwise or for a cluster (ROI
%from atlas)) for the cEEGrids

torchLight = linspace(1,0,51)';
channels=1:length(CM.Channel); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
nameVar = 'Full_128';

minLoc = zeros(length(torchLight),size(Source.ImageGridAmp,1));
for  k = 1:size(Source.ImageGridAmp,1) %loop through every point on the brain mesh
    distLoc = sum((ANAT.Vertices(k,:)   - ANAT.Vertices) .^ 2, 2); %get all distances from one point in the scout to all others
    [val, minLoc(:,k)] = mink(distLoc,length(torchLight)); %get lowest distances
end

    ResultsF(size(Source.ImageGridAmp,1),1).maxDiffAbs = zeros(size(Source.ImageGridAmp,1),1);
    ResultsF(size(Source.ImageGridAmp,1),1).CheckPair = cell(size(Source.ImageGridAmp,1),1);


for  k= 1:size(Source.ImageGridAmp,1) %loop through every point on the brain mesh
    
    IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
    IMAGEGRIDAMP(minLoc(:,k),1) = torchLight(:,1); %put values into the source grid
    
    F = zeros(length(channels), 1); % simulation matrix. CM is the channel model
    F(channels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
    
    collect=[];
    index=1;
    
    for ch=1:length(channels)-1
        for b=ch+1:length(channels)
            collect(index) = abs((F(channels(ch),1))- (F(channels(b),1))); %calc every combination of electrodes
            checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
            index=index+1;
        end
    end
    
    SourceDist.ImageGridAmp(k,1) = max(collect); %get the maximum electrode, aka the one with the highest signal after referencing
    
    if mod(k,1000)==0
        disp('Almost there')
    end
    
    ResultsF(k).maxDiffAbs =  max(collect);
    ResultsF(k).CheckPair = checkPair(collect==max(collect),1);
end

SourceDist.ImageGridAmp = abs(SourceDist.ImageGridAmp);

SourceDist.Comment= 'Sens_Full_128'; %set the name referring to the chosen grid & atlas
ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
db_add(ProtocolInfo.iStudy, SourceDist, []);

mkdir([PATHOUT,'results_Cap\',SubjectName,'\'])
save([PATHOUT,'results_Cap\',SubjectName,'\results_',nameVar,'.mat'],'SourceDist','ResultsF');
