%% Load stuff, calc the sensitivty maps for the midlines
clear
for t = 8
    SubjectName = ['Subject0',num2str(t),'_roundGrid_20']; %specify the name of the subject that will be used in the brainstorm GUI
    MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
    PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
    
    listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);
    HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]);
    
    if length(HM.Gain) > 30002
        HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
    end
    
    listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
    CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
    
    listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']);
    Source = load([listSource(1).folder,'\',listSource(1).name]);
    Source.ImageGridAmp= zeros(size(Source.ImageGridAmp,1),1);
    Source.DataFile=[];
    Source.Time = 1;
    
    listAnat = dir([MAINPATH,'\brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\tess_cortex_pial_low.mat']);
    export_matlab([listAnat.folder,'\',listAnat.name],'cortex');
    
    Scout = load(['O:\arm_testing\Experimente\Anatomical_differences\midlines\scouts\scout_Destrieux_1_',SubjectName,'.mat']);
    paths = load(['O:\arm_testing\Experimente\Anatomical_differences\midlines\Shortestpaths\',SubjectName,'.mat']);
    
    SensMid = Source;
    AngleMid = Source;
    
    channel = load([listChannel(1).folder,'\',listChannel(1).name]);
    channels = 1:20; %1:128;
    iChannels = 1:length(channels);
    
    torchLight = linspace(1,0,20)';
    NumElecPairs = 10;
    Angle = 0:180/NumElecPairs:180-180/NumElecPairs;
    AngleNum = 1:10;
    AngleSym = [0:90/5:90 72 54 36 18];
    
    for  k= 1:length(paths.Path) %loop through every point on the brain mesh
        
        IMAGEGRIDAMP=Source.ImageGridAmp; %reset the matrix to the original 0-matrix every time
        
        distLoc = sum((cortex.Vertices(Scout.Scouts.Vertices,:)   - cortex.Vertices(paths.Path(k),:)) .^ 2, 2);
        [minDist, minLoc] = mink(distLoc,20);
        
        for i = 1:length(torchLight)
            IMAGEGRIDAMP(Scout.Scouts.Vertices(minLoc(i)),1) = torchLight(i,1); %%%%stimmt das mit scout an der stelle minloc?
        end
        
        F = zeros(length(channels), 1); % simulation matrix. CM is the channel model
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        cF(k,:) = F';
        
        collect=[];
        index=1;
        
   for b = 1:length(channels)/2
        collect(index) = abs((F(b,1)) - (F(b+10,1))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(b).Name,' - ',CM.Channel(b+10).Name]};
        checkPair(index,2) = {CM.Channel(b).Name};
        checkPair(index,3) = {CM.Channel(b+10).Name};
        SelectedValues(index,1) = F(b,1);
        SelectedValues(index,2) = F(b+10,1);
        index=index+1;
    end
    
    %store the results in a structure
    SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    ResultsC(k).maxDiffAbs =  max(collect);
    ResultsC(k).CheckPair = checkPair;
    ResultsC(k).CheckPair(:,4) = {'0'};
    ResultsC(k).CheckPair(collect==max(collect),4) = {'1'};
    ResultsC(k).SelectedValues = SelectedValues;
    ResultsC(k).SelectedValues(:,3) = 0;
    ResultsC(k).SelectedValues(collect==max(collect),3) = 1;
    ResultsC(k).Angle = Angle(collect==max(collect));
    ResultsC(k).AngleNum = AngleNum(collect==max(collect));
    ResultsC(k).AngleSym = AngleSym(collect==max(collect));
    ResultsC(k).SourceInd = k;
                
    SensMid.ImageGridAmp(paths.Path(k),1) = max(collect); %get the maximum electrode, aka the one with the highest signal after referencing
    AngleMid.ImageGridAmp(paths.Path(k),1) = AngleSym(collect==max(collect)); %write the angle of the channel with the highest amp into the source file
    end
    
%     SensMid.Comment = [SubjectName,'_sensMid'];
%     ProtocolInfo = bst_get('ProtocolInfo');
%     db_add(ProtocolInfo.iStudy, SensMid, []);
  
    AngleMid.Comment = [SubjectName,'_angleMid'];
    ProtocolInfo = bst_get('ProtocolInfo');
    db_add(ProtocolInfo.iStudy, AngleMid, []);
    
    save(['O:\arm_testing\Experimente\Anatomical_differences\midlines\sensitivites\',SubjectName,'sensMid.mat'],'cF','ResultsC');
    
end












