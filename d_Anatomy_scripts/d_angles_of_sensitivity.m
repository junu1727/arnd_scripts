
% clear;
% close all;
% clc


for t = 5:9

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = ['Subject0',num2str(t),'_roundGrid_20']; %specify the name of the subject that will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
PATHOUT = 'O:\arm_testing\Experimente\Anatomical_differences\Results_sensitivity_round_grid\'; %store the results here
cd([MAINPATH,'brainstorm3\']);
%brainstorm
cEEGrid = {'cEEGrid','fullcap'};
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
atlas = '0'; % 2 = Desikan-Killiany 1 = Brodmann 0 = Destrieux
EarChannel = '2'; %1 = left, 2 = right, 0 = both
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\headmodel*']);
listAnat = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\tess_cortex_pial_low.mat']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in a_PrepareSources.m...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
ANAT = load([listAnat(1).folder,'\',listAnat(1).name]);

HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things

Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
SourceDist = Source; % source variable to work in
SourceAngle = Source;
SourceSym = Source;

torchLight = linspace(1,0,51)';
channels = 1:length(CM.Channel); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
nameVar = 'Left_';
iChannels = 1:length(CM.Channel);
NumElecPairs = 10;
Angle = 0:180/NumElecPairs:180-180/NumElecPairs;
AngleNum = 1:10;
AngleSym = [0:90/5:90 72 54 36 18];

for  k = 1:size(Source.ImageGridAmp,1) %loop through every point on the brain mesh
    
    IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
    distLoc = sum((ANAT.Vertices(k,:)   - ANAT.Vertices) .^ 2, 2); %get all distances from one point in the scout to all others
    [val, minLoc] = mink(distLoc,length(torchLight)); %get lowest distances
    [valX,minLocX] = ismember(val,distLoc); %find indices of lowest values
    IMAGEGRIDAMP(minLocX,1) = torchLight(:,1); %put values into the source grid
    
    F = zeros(length(channels), 1); % Simulation matrix
    F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
    
    collect=[];
    index=1;
    
    for b = 1:length(channels)/2
        collect(index) = abs((F(b,1)) - (F(b+10,1))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(b).Name,' - ',CM.Channel(b+10).Name]};
        checkPair(index,2) = {CM.Channel(b).Name};
        checkPair(index,3) = {CM.Channel(b+10).Name};
        SelectedValues(index,1) = F(b,1);
        SelectedValues(index,2) = F(b+10,1);
        index=index+1;
    end
    
    %store the results in a structure
    SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    ResultsC(k).maxDiffAbs =  max(collect);
    ResultsC(k).CheckPair = checkPair;
    ResultsC(k).CheckPair(:,4) = {'0'};
    ResultsC(k).CheckPair(collect==max(collect),4) = {'1'};
    ResultsC(k).SelectedValues = SelectedValues;
    ResultsC(k).SelectedValues(:,3) = 0;
    ResultsC(k).SelectedValues(collect==max(collect),3) = 1;
    ResultsC(k).Angle = Angle(collect==max(collect));
    ResultsC(k).AngleNum = AngleNum(collect==max(collect));
    ResultsC(k).AngleSym = AngleSym(collect==max(collect));
    ResultsC(k).SourceInd = k;
    
    SourceSym.ImageGridAmp(k,1) = AngleSym (collect==max(collect));
    SourceAngle.ImageGridAmp(k,1) = AngleNum(collect==max(collect));
    SourceDist.ImageGridAmp(k,1) = max(collect); 
end

SourceDist.Comment= 'Sens_cEEGrid'; %set the name referring to the chosen grid & atlas
ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
db_add(ProtocolInfo.iStudy, SourceDist, []);

SourceAngle.Comment= 'Angles_cEEGrid'; %set the name referring to the chosen grid & atlas
ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
db_add(ProtocolInfo.iStudy, SourceAngle, []);

%save all relevant variables
Grid = 'Grid_full_';
nameAtlas = [];

mkdir([PATHOUT,'results_cEEGrid\',SubjectName,'\'])
save([PATHOUT,'results_cEEGrid\',SubjectName,'\results_',nameVar,Grid,'.mat'],'SourceDist','ResultsC','SourceAngle','SourceSym');

SourcesCol(1,t-4).Sym = SourceSym.ImageGridAmp;
end
% 
% SourcesDiff.Mean = zeros(length(SourcesCol(1).Sym),1);
% SourcesDiff.STD = zeros(length(SourcesCol(1).Sym),1);
% 
% for s = 1:length(SourcesCol(1).Sym)
% SourcesDiff(s,1).Mean = mean([SourcesCol(1).Sym(s,1) SourcesCol(2).Sym(s,1) SourcesCol(3).Sym(s,1) SourcesCol(4).Sym(s,1) SourcesCol(5).Sym(s,1)]);
% SourcesDiff(s,1).STD = std([SourcesCol(1).Sym(s,1) SourcesCol(2).Sym(s,1) SourcesCol(3).Sym(s,1) SourcesCol(4).Sym(s,1) SourcesCol(5).Sym(s,1)]);
% end
% 
% SourceX = Source;
% SourceX.ImageGridAmp = [SourcesDiff.STD]';
% SourceX.Comment= 'DiffMap5'; %set the name referring to the chosen grid & atlas
% ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
% db_add(ProtocolInfo.iStudy, SourceX, []);

