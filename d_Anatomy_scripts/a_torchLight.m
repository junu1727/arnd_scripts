%% 

MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ind_Anat'; %name of your current protocol in brainstorm
Subject = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\S*']);

listHM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\Subject*\raw\headmodel*']);
listAnat = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\Subject*\tess_cortex_pial_low*']);
listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\Subject*\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\Subject*\raw\channel*']);

HM = load([listHM(1).folder,'\',listHM(1).name]);
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
anat = load([listAnat(1).folder,'\',listAnat(1).name]);
Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in a_PrepareSources.m...
Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1);

channel = load([listChannel(1).folder,'\',listChannel(1).name]);
channels = 1:128%129:148;
iChannels = 1:length(channels);

counter = 1:length(Subject);
for s = 1:length(counter)
listSens = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\Subject0',num2str(counter(s)),'\raw\results*']);
SensMap(s,1) = load([listSens(1).folder,'\',listSens(1).name]);
SensMap(s,2) = load([listSens(1).folder,'\',listSens(2).name]);
SensMap(s,3) = load([listSens(1).folder,'\',listSens(3).name]);
end

figure
subplot(1,2,1)
hist(SensMap(1, 1).ImageGridAmp,100)
title('Subject01 [cEEGrid]')
xlim([0 130])
ylim([0 2200])
subplot(1,2,2)
hist(SensMap(2, 1).ImageGridAmp,100)
title('Subject02 [cEEGrid]')
xlim([0 130])
ylim([0 2200])

figure
subplot(1,2,1)
hist(SensMap(1, 2).ImageGridAmp,100)
title('Subject01 [fullCap]')
xlim([0 150])
ylim([0 1300])
subplot(1,2,2)
hist(SensMap(2, 2).ImageGridAmp,100)
title('Subject02 [fullCap]')
xlim([0 150])
ylim([0 1300])

figure
subplot(1,2,1)
hist(SensMap(1, 3).ImageGridAmp,100)
title('Subject01 [differenceMap]')
xlim([0 100])
ylim([0 800])
subplot(1,2,2)
hist(SensMap(2, 3).ImageGridAmp,100)
title('Subject02 [differenceMap]')
xlim([0 100])
ylim([0 800])


[~,maxLoc] = max(SensMap(1,1).ImageGridAmp); %find maximum cEEGrid value to seed there
distLoc = sum((test.Vertices   - test.Vertices(maxLoc,:)) .^ 2, 2);
[minDist minLoc] = mink(distLoc,50); %the minimum dist of the seed is the seed itself. It is therefore already included in the 50
minSource = SensMap(1,1);
minSource.ImageGridAmp = zeros(length(SensMap(1,1).ImageGridAmp),1);
minSource.Comment = 'torchLightTest';

torchLight = linspace(1,0,length(minLoc))';

for i = 1:length(torchLight)
minSource.ImageGridAmp(minLoc(i,1)) = torchLight(i,1);
end

TorchSource = Source;
%torchLight = linspace(1,0,51)'; % 50 neareast neighbours, since the last value will be 0
for  k= 1:size(minSource.ImageGridAmp,1) %loop through every point on the brain mesh
    
    IMAGEGRIDAMP=Source.ImageGridAmp; %reset the matrix to the original 0-matrix every time
    
    distLoc = sum((anat.Vertices   - anat.Vertices(k,:)) .^ 2, 2);
    [~, minLoc] = mink(distLoc,length(torchLight)); %the minimum dist of the seed is the seed itself. It is therefore already included in the 50
    
    for i = 1:length(torchLight)
        IMAGEGRIDAMP(minLoc(i),1) = torchLight(i,1);
    end
    
    F = zeros(length(channels), 1); % simulation matrix. CM is the channel model
    F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
    
    collect=[];
    index=1;
    
    for ch=1:length(channels)-1
        for b=ch+1:length(channels)
            collect(index)=abs((F(iChannels(ch),1))- (F(iChannels(b),1))); %calc every combination of electrodes
            index=index+1;
        end
    end
    
    TorchSource.ImageGridAmp(k,1) = max(collect); %get the maximum electrode, aka the one with the highest signal after referencing
    
    if mod(k,1000)==0
        disp('Almost there')
    end
    
end

%finding the neighbors can be done with VertCon in anat. It finds
%neighouring vertices of a position. 

TorchSource.ImageGridAmp = TorchSource.ImageGridAmp / 10;
TorchSource.Comment = 'Torch_Sensitivity_fullCap';
ProtocolInfo = bst_get('ProtocolInfo');
 db_add(ProtocolInfo.iStudy, TorchSource, []);

diff = TorchSource;
diff.Comment = 'DifferenceMap';
for s = 1:length(diff.ImageGridAmp)
    diff.ImageGridAmp (s,1)= 100 -(test.ImageGridAmp(s) / TorchSource.ImageGridAmp(s) *100);
end
diff.ImageGridAmp(diff.ImageGridAmp<0)= -0.01; %change all negative values to -0.01 to have a uniform display in the bst-figure

 db_add(ProtocolInfo.iStudy, test_noT, []);
