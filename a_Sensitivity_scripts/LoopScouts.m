%% Simulate activity from scouts. The scouts can be from the standard bst 
% atlases or be user-generated. You can choose to seed activity in all
% scouts consecutively or a subselection. This presupposes that the indices
% of the scouts are known as they must be entered in a GUI.
clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's03_temp'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
SCOUTFILE = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_left_06_50.mat';
nameScout = 'left_06';
brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = FullCap
fullOrROI = '0'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '1'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
atlas = '0'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux 
UserAtlas = '2'; % 1 if there is an additional, user generated atlas to choose from. If 2, use ONLY that atlas. If 0, don't use the UserAtlas
refStandard = [30 31];
refNew = 144; %if 0, it will be assumed the data is already referenced on a common electrode
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);

if UserAtlas == '1' || UserAtlas == '2' % the loop is only neccesarry when going trough the hand-picked scouts. Therefore, if any other atlas is chosen, make the loop p = 1
    loadScout = load(SCOUTFILE); %definition of the scouts from an atlas. Only together with atlas = 'X'
    counter = 1;
    ScoutLabel = {loadScout.Scouts.Label};
else
    counter = 1;
end

    for s = 1:length(cEEGrid)

% Load and prepare the sourceModel

        ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI
        
        if s == 1
            ref = refNew; %index of the reference electrode for cEEGrid (in CM, E16)
            nameResult = 'SubjectName';
            if EarChannel == '1'
                channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
                nameVar = 'Left';
            elseif EarChannel == '2'
                channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
                nameVar = 'Right';
            else EarChannel == '0'
                channels=indexChan;
                nameVar = 'Double';
            end
            
        else s == 2
            ref = refStandard; %index of the reference electrode for 64ChanCap (in CM, E053)
            nameResult = 'fullCap';
            channels=1:indexChan(1)-1;
            nameVar = 'Full';
        end
        
        rightChannels = indexChan(length(indexChan)/2+1:end);
        
        listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results*']); %lists of the variables created in PrepareSource_cEEGrid.mat
        listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
        listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);
        
        Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
        CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
        iChannels = 1:length(channels); %number of channels according to your channel file
        iChannelsRight = 1:length(rightChannels); %number of channels according to your channel file
        HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
        HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
        
        Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
        Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
        
        Source.Time = 1;
        Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
        SourceDist = Source; % source variable to work in
        SourceSimple = Source;
        IMAGEGRIDAMP=Source.ImageGridAmp; %This is the source/scout. It is currently only zeros
        
        %calculate the sensitivity for a source (ROI from atlas) for the cEEGrids or fullCap
        
        sFiles = [];
        sFiles = {...
            [listSource(1).folder,'\',listSource(end).name]}; %always take the latest file
        
        % Start a new report
        bst_report('Start', sFiles);
        sProcess = bst_process('GetInputStruct', sFiles);
        [sSubject,iSubject] = bst_get('Subject', sProcess.SubjectFile); %gets the subject ID

        % Load the atlases

        if atlas == '0' && UserAtlas ~= 2 %currently, 3 different atlases are available
            
            % Process: Scouts time series: [148 scouts]
            sFilesDestrieux = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
                'timewindow',     [], ...
                'scouts',         {'Destrieux', {'G_Ins_lg_and_S_cent_ins L', 'G_Ins_lg_and_S_cent_ins R', 'G_and_S_cingul-Ant L', 'G_and_S_cingul-Ant R', 'G_and_S_cingul-Mid-Ant L', 'G_and_S_cingul-Mid-Ant R', 'G_and_S_cingul-Mid-Post L', 'G_and_S_cingul-Mid-Post R', 'G_and_S_frontomargin L', 'G_and_S_frontomargin R', 'G_and_S_occipital_inf L', 'G_and_S_occipital_inf R', 'G_and_S_paracentral L', 'G_and_S_paracentral R', 'G_and_S_subcentral L', 'G_and_S_subcentral R', 'G_and_S_transv_frontopol L', 'G_and_S_transv_frontopol R', 'G_cingul-Post-dorsal L', 'G_cingul-Post-dorsal R', 'G_cingul-Post-ventral L', 'G_cingul-Post-ventral R', 'G_cuneus L', 'G_cuneus R', 'G_front_inf-Opercular L', 'G_front_inf-Opercular R', 'G_front_inf-Orbital L', 'G_front_inf-Orbital R', 'G_front_inf-Triangul L', 'G_front_inf-Triangul R', 'G_front_middle L', 'G_front_middle R', 'G_front_sup L', 'G_front_sup R', 'G_insular_short L', 'G_insular_short R', 'G_oc-temp_lat-fusifor L', 'G_oc-temp_lat-fusifor R', 'G_oc-temp_med-Lingual L', 'G_oc-temp_med-Lingual R', 'G_oc-temp_med-Parahip L', 'G_oc-temp_med-Parahip R', 'G_occipital_middle L', 'G_occipital_middle R', 'G_occipital_sup L', 'G_occipital_sup R', 'G_orbital L', 'G_orbital R', 'G_pariet_inf-Angular L', 'G_pariet_inf-Angular R', 'G_pariet_inf-Supramar L', 'G_pariet_inf-Supramar R', 'G_parietal_sup L', 'G_parietal_sup R', 'G_postcentral L', 'G_postcentral R', 'G_precentral L', 'G_precentral R', 'G_precuneus L', 'G_precuneus R', 'G_rectus L', 'G_rectus R', 'G_subcallosal L', 'G_subcallosal R', 'G_temp_sup-G_T_transv L', 'G_temp_sup-G_T_transv R', 'G_temp_sup-Lateral L', 'G_temp_sup-Lateral R', 'G_temp_sup-Plan_polar L', 'G_temp_sup-Plan_polar R', 'G_temp_sup-Plan_tempo L', 'G_temp_sup-Plan_tempo R', 'G_temporal_inf L', 'G_temporal_inf R', 'G_temporal_middle L', 'G_temporal_middle R', 'Lat_Fis-ant-Horizont L', 'Lat_Fis-ant-Horizont R', 'Lat_Fis-ant-Vertical L', 'Lat_Fis-ant-Vertical R', 'Lat_Fis-post L', 'Lat_Fis-post R', 'Pole_occipital L', 'Pole_occipital R', 'Pole_temporal L', 'Pole_temporal R', 'S_calcarine L', 'S_calcarine R', 'S_central L', 'S_central R', 'S_cingul-Marginalis L', 'S_cingul-Marginalis R', 'S_circular_insula_ant L', 'S_circular_insula_ant R', 'S_circular_insula_inf L', 'S_circular_insula_inf R', 'S_circular_insula_sup L', 'S_circular_insula_sup R', 'S_collat_transv_ant L', 'S_collat_transv_ant R', 'S_collat_transv_post L', 'S_collat_transv_post R', 'S_front_inf L', 'S_front_inf R', 'S_front_middle L', 'S_front_middle R', 'S_front_sup L', 'S_front_sup R', 'S_interm_prim-Jensen L', 'S_interm_prim-Jensen R', 'S_intrapariet_and_P_trans L', 'S_intrapariet_and_P_trans R', 'S_oc-temp_lat L', 'S_oc-temp_lat R', 'S_oc-temp_med_and_Lingual L', 'S_oc-temp_med_and_Lingual R', 'S_oc_middle_and_Lunatus L', 'S_oc_middle_and_Lunatus R', 'S_oc_sup_and_transversal L', 'S_oc_sup_and_transversal R', 'S_occipital_ant L', 'S_occipital_ant R', 'S_orbital-H_Shaped L', 'S_orbital-H_Shaped R', 'S_orbital_lateral L', 'S_orbital_lateral R', 'S_orbital_med-olfact L', 'S_orbital_med-olfact R', 'S_parieto_occipital L', 'S_parieto_occipital R', 'S_pericallosal L', 'S_pericallosal R', 'S_postcentral L', 'S_postcentral R', 'S_precentral-inf-part L', 'S_precentral-inf-part R', 'S_precentral-sup-part L', 'S_precentral-sup-part R', 'S_suborbital L', 'S_suborbital R', 'S_subparietal L', 'S_subparietal R', 'S_temporal_inf L', 'S_temporal_inf R', 'S_temporal_sup L', 'S_temporal_sup R', 'S_temporal_transverse L', 'S_temporal_transverse R'}}, ...
                'scoutfunc',      1, ...  % Mean
                'isflip',         1, ...
                'isnorm',         0, ...
                'concatenate',    1, ...
                'save',           1, ...
                'addrowcomment',  1, ...
                'addfilecomment', 1);
            
            nameAtlas = 'Destrieux';
        elseif atlas == '1'
            % Process: Scouts time series: [24 scouts]
            sFilesBrodmann = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
                'timewindow',     [], ...
                'scouts',         {'Brodmann-thresh', {'BA1 L', 'BA1 R', 'BA2 L', 'BA2 R', 'BA3a L', 'BA3a R', 'BA3b L', 'BA3b R', 'BA44 L', 'BA44 R', 'BA45 L', 'BA45 R', 'BA4a L', 'BA4a R', 'BA4p L', 'BA4p R', 'BA6 L', 'BA6 R', 'MT L', 'MT R', 'V1 L', 'V1 R', 'V2 L', 'V2 R'}}, ...
                'scoutfunc',      1, ...  % Mean
                'isflip',         1, ...
                'isnorm',         0, ...
                'concatenate',    1, ...
                'save',           1, ...
                'addrowcomment',  1, ...
                'addfilecomment', 1);
            
            nameAtlas = 'Brodmann-thresh';
            
        else atlas == '2'
            % Process: Scouts time series: [68 scouts]
            sFilesDesikanKilliany = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
                'timewindow',     [1, 1], ...
                'scouts',         {'Desikan-Killiany', {'bankssts L', 'bankssts R', 'caudalanteriorcingulate L', 'caudalanteriorcingulate R', 'caudalmiddlefrontal L', 'caudalmiddlefrontal R', 'cuneus L', 'cuneus R', 'entorhinal L', 'entorhinal R', 'frontalpole L', 'frontalpole R', 'fusiform L', 'fusiform R', 'inferiorparietal L', 'inferiorparietal R', 'inferiortemporal L', 'inferiortemporal R', 'insula L', 'insula R', 'isthmuscingulate L', 'isthmuscingulate R', 'lateraloccipital L', 'lateraloccipital R', 'lateralorbitofrontal L', 'lateralorbitofrontal R', 'lingual L', 'lingual R', 'medialorbitofrontal L', 'medialorbitofrontal R', 'middletemporal L', 'middletemporal R', 'paracentral L', 'paracentral R', 'parahippocampal L', 'parahippocampal R', 'parsopercularis L', 'parsopercularis R', 'parsorbitalis L', 'parsorbitalis R', 'parstriangularis L', 'parstriangularis R', 'pericalcarine L', 'pericalcarine R', 'postcentral L', 'postcentral R', 'posteriorcingulate L', 'posteriorcingulate R', 'precentral L', 'precentral R', 'precuneus L', 'precuneus R', 'rostralanteriorcingulate L', 'rostralanteriorcingulate R', 'rostralmiddlefrontal L', 'rostralmiddlefrontal R', 'superiorfrontal L', 'superiorfrontal R', 'superiorparietal L', 'superiorparietal R', 'superiortemporal L', 'superiortemporal R', 'supramarginal L', 'supramarginal R', 'temporalpole L', 'temporalpole R', 'transversetemporal L', 'transversetemporal R'}}, ...
                'scoutfunc',      1, ...  % Mean
                'isflip',         1, ...
                'isnorm',         0, ...
                'concatenate',    1, ...
                'save',           1, ...
                'addrowcomment',  1, ...
                'addfilecomment', 1);
            
            nameAtlas = 'Desikan-Killiany';
        end
        
        if UserAtlas == '1'  || UserAtlas == '2'
                   % Process: Scouts time series 
           sFilesUserScout = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
               'timewindow',     [], ...
               'scouts',         {nameScout, ScoutLabel}, ...
               'scoutfunc',      1, ...  % Mean
               'isflip',         1, ...
               'isnorm',         0, ...
               'concatenate',    1, ...
               'save',           1, ...
               'addrowcomment',  1, ...
               'addfilecomment', 1);
           
           nameAtlasUser = 'UserScout';
           
           ReportFile = bst_report('Save', sFiles);
           bst_report('Open', ReportFile);
           
           listScout1 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*\matrix_scout*']);
           AtlasROI1 = load([listScout1(end).folder,'\',listScout1(end).name]); %always load newest scouts
        end
        
        listScout2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*\matrix_scout*']);
        AtlasROI2 = load([listScout2(1).folder,'\',listScout2(1).name]);
        
% Determine the ROI
        if s == 1
            if UserAtlas ~= '2'
            prompt = {'Enter desired ROI:'}; %the number of the desired ROI must be looked up in AtlasROI.Atlas.Scouts
            dlgtitle = nameAtlas;
            dims = [1 50];
            g = inputdlg(prompt,dlgtitle,dims);
            ROI = str2num(g{1,1}); %turn the entered strings into an array of numbers
            end
            if UserAtlas == '1' || UserAtlas == '2'
                prompt = {'Enter desired ROI:'}; %the number of the desired ROI must be looked up in AtlasROI.Atlas.Scouts
                dlgtitle = nameAtlasUser;
                dims = [1 50];
                q = inputdlg(prompt,dlgtitle,dims);
                ROIUser = str2num(q{1,1}); %turn the entered strings into an array of numbers                
                if q{1,1} == 'x' %if x is selected, use every Region of the scout, not a subselection
                    q = 1:length(AtlasROI1.Atlas.Scouts);
                    ROIUser = q; %turn the entered strings into an array of numbers
                end
            end
            
        else s == 2
            t=1;
            
        end
        
        if UserAtlas == '1' || UserAtlas == '2'
            TakeScout = AtlasROI1.Atlas.Scouts(ROIUser(1)).Vertices;            
            if length(ROIUser) > 1 %if more than one region was chosen, add the additional vertices to TakeScout
                for f = 2:length(ROIUser)
                    TakeScout(end+1:end+length(AtlasROI1.Atlas.Scouts(ROIUser(f)).Vertices)) = AtlasROI1.Atlas.Scouts(ROIUser(f)).Vertices; %vertices of the current scout
                end
            end
        else
            TakeScout = AtlasROI2.Atlas.Scouts(ROI(1)).Vertices; %store the index of all vertices in the ROI in TakeScout            
            if length(ROI) > 1 %if more than one region was chosen, add the additional vertices to TakeScout
                for f = 2:length(ROI)
                    TakeScout(end+1:end+length(AtlasROI2.Atlas.Scouts(ROI(f)).Vertices)) = AtlasROI2.Atlas.Scouts(ROI(f)).Vertices; %vertices of the current scout
                end
            end
        end
        
        if UserAtlas == '1' || UserAtlas == '2' % to keep the naming from here on out consistent, make ROI = ROIUser
            ROI = ROIUser;
        end
% Calculate the best electrode pair       
        
        IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
        IMAGEGRIDAMP(TakeScout,1)= 1; % set activity of one vertex to 1, the others stay 0
        SourceSimple.ImageGridAmp = IMAGEGRIDAMP;        
        SourceSimple.Comment= [nameResult,nameVar,'GridUNIFORM, ',nameAtlas,' ',num2str(ROI)]; %set the name referring to the chosen grid & atlas
        
        SourceSimple.ImageGridAmp = abs(SourceSimple.ImageGridAmp); %take only absolute values
        [a,b] = maxk(SourceSimple.ImageGridAmp(:,1),100); %find the 100 highest values
        c = find(a > 250); %find unrealisticly high values
        SourceSimple.ImageGridAmp(b(c),1)=0; % delete them
        
        F = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        if refNew == 0
            G = 0;
        else
        G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
        G = mean(G); %only nessecary for the fullCap. Here, we take the average from the two electrodes closest to the mastoid. Does not hurt for 1 electrode, mean of 1 stays 1
        %IChannels is just a list from 1 to the number of used channels.
        %channels is the index of the specified channels in gain
        end
        
        collect=[];
        index=1;
        
        for ch=1:length(channels)-1
            for b=ch+1:length(channels)
                collect(index) = abs((F(iChannels(ch),1)- G)- (F(iChannels(b),1)-G)); %calc every combination of electrodes
                checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
                checkPair(index,2) = {CM.Channel(channels(ch)).Name};
                checkPair(index,3) = {CM.Channel(channels(b)).Name};
                SelectedValues(index,1) = F(iChannels(ch),1)- G;
                SelectedValues(index,2) = F(iChannels(b),1)- G;
                index=index+1;
            end
        end
        
        F = zeros(length(rightChannels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannelsRight,:) = HM.Gain(rightChannels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        if refNew == 0
            G = 0;
        else
        G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
        G = mean(G); %only nessecary for the fullCap. Here, we take the average from the two electrodes closest to the mastoid. Does not hurt for 1 electrode, mean of 1 stays 1
        %IChannels is just a list from 1 to the number of used channels.
        %channels is the index of the specified channels in gain
        end
        
        if s == 1
            collect_right=[];
            index=1;
            for ch=1:length(rightChannels)-1
                for b=ch+1:length(rightChannels)
                    collect_right(index) = abs((F(iChannelsRight(ch),1)- G)- (F(iChannelsRight(b),1)-G)); %calc every combination of electrodes
                    checkPair_right(index,1) = {[CM.Channel(rightChannels(ch)).Name,' - ',CM.Channel(rightChannels(b)).Name]};
                    checkPair_right(index,2) = {CM.Channel(rightChannels(ch)).Name};
                    checkPair_right(index,3) = {CM.Channel(rightChannels(b)).Name};
                    SelectedValues_right(index,1) = F(iChannelsRight(ch),1)- G;
                    SelectedValues_right(index,2) = F(iChannelsRight(b),1)- G;
                    index=index+1;
                end
            end
            
% Store the results in a variable

            SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC.maxDiff =  max(collect);
            ResultsC.CheckPair = checkPair;
            ResultsC.CheckPair(:,4) = {'0'};
            ResultsC.CheckPair(collect==max(collect),4) = {'1'};
            ResultsC.Pair = checkPair(collect==max(collect),1);
            ResultsC.SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC.SelectedValues = SelectedValues;
            ResultsC.SelectedValues(:,3) = 0;
            ResultsC.SelectedValues(collect==max(collect),3) = 1;
            
            SelectedChannels_right = [checkPair_right(collect_right==max(collect_right),2) checkPair_right(collect_right==max(collect_right),3)];
            ResultsCRight.maxDiff =  max(collect_right);
            ResultsCRight.CheckPair = checkPair_right;
            ResultsCRight.CheckPair(:,4) = {'0'};
            ResultsCRight.CheckPair(collect_right==max(collect_right),4) = {'1'};
            ResultsCRight.Pair = checkPair_right(collect_right==max(collect_right),1);
            ResultsCRight.SelectedChannels = [checkPair_right(collect_right==max(collect_right),2) checkPair_right(collect_right==max(collect_right),3)];
            ResultsCRight.SelectedValues = SelectedValues_right;
            ResultsCRight.SelectedValues(:,3) = 0;
            ResultsCRight.SelectedValues(collect==max(collect_right),3) = 1;
        else s == 2
            ResultsF.maxDiff =  max(collect);
            ResultsF.CheckPair = checkPair;
            ResultsF.CheckPair(:,4) = {'0'};
            ResultsF.CheckPair(collect==max(collect),4) = {'1'};
            ResultsF.Pair = checkPair(collect==max(collect),1);
            ResultsF.SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsF.SelectedValues = SelectedValues;
            ResultsF.SelectedValues(:,3) = 0;
            ResultsF.SelectedValues(collect==max(collect),3) = 1;
        end
        
        ProtocolInfo = bst_get('ProtocolInfo');
        db_add(ProtocolInfo.iStudy, SourceSimple, []);
        listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_20*']);
        newDataFile = bst_simulation([listResults(end).folder,'\',listResults(end).name]);
        
        listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_simulation*']);
        
        if s == 2
            export_matlab([listSimulation(end).folder,'\',listSimulation(end).name],'simulation');
            simulation.ChannelFlag(channels) = -1; %determine which channels are treated as bad. This way, they will not be part of the plotting
            ProtocolInfo = bst_get('ProtocolInfo');
            db_add(ProtocolInfo.iStudy, simulation, []);
            
            listDataSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_1*']);
            
            if ~exist([PATHOUT,'figures\archive\'])
                mkdir([PATHOUT,'figures\archive\'])
            end
            
            leftFig = view_topography([listDataSimulation(end).folder,'\',listDataSimulation(end).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
            %bst_figures('SetSelectedRows', SelectedChannels); %display the selected channels
            figure_3d('ViewSensors', gcf, 0, 0);
            figure_3d('SetStandardView', gcf, {'left'});
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'left.png']);
            
            rightFig = view_topography([listDataSimulation(end).folder,'\',listDataSimulation(end).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
            %bst_figures('SetSelectedRows', SelectedChannels); %display the selected channels
            figure_3d('ViewSensors', gcf, 0, 0);
            figure_3d('SetStandardView', gcf, {'right'});
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'right.png']);
            
            rightFigOnly = view_topography([listDataSimulation(end).folder,'\',listDataSimulation(end).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
            %bst_figures('SetSelectedRows', SelectedChannels_right); %display the selected channels
            figure_3d('ViewSensors', gcf, 0, 0);
            figure_3d('SetStandardView', gcf, {'right'});
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'rightOnly.png']);
            
            figTopo = view_topography([listSimulation(end).folder,'\',listSimulation(end).name], 'EEG', '2DSensorCap'); %view the averaged signal of the subject
            %bst_figures('SetSelectedRows', SelectedChannels);
            figure_3d('ViewSensors', gcf, 0, 0); %specify that electrode-locations are shown, but their names are not
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'topo.png']);
            %view_topography('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s04_comp\raw84Chan\results_201015_1707.mat', 'EEG', '3DElectrodes-Cortex'); %view the averaged signal of the subject
            %view_connect('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s04_comp\raw84Chan\results_201015_1707.mat', 'Image');

            script_view_sources([SubjectName,'\raw\',listResults(1).name], 'cortex');
            figure_3d('SetStandardView', gcf, {'left'});
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'seedleft.png']);
            script_view_sources([SubjectName,'\raw\',listResults(1).name], 'cortex');
            figure_3d('SetStandardView', gcf, {'right'}); 
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'seedright.png']);
            script_view_sources([SubjectName,'\raw\',listResults(1).name], 'cortex');
            figure_3d('SetStandardView', gcf, {'bottom'});
            set(gcf, 'Position', get(0, 'ScreenSize'));
            saveas(gcf,[PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'seedbottom.png']);
        end
    end
    
    figDrop = figure;
    combo_Diff = [ResultsF.maxDiff ResultsC.maxDiff ResultsCRight.maxDiff];
    PerceChange = 100 - (100 / ResultsF.maxDiff) * ResultsC.maxDiff;
    PerceChangeOnly = 100 - (100 / ResultsF.maxDiff) * ResultsCRight.maxDiff;
    bar([1 2 3],combo_Diff)
    xticks([1 2 3])
    xticklabels({'FullCap','cEEGrid','cEEGridRight'})
    ylim([0 ResultsF.maxDiff+500])
    text(1:3,combo_Diff,num2str(combo_Diff'),'vert','bottom','horiz','center');
    title(['Signal Difference. Signal-loss from F to C = ',num2str(PerceChange),'%. From F to CRight = ',num2str(PerceChangeOnly),'%'],'FontSize',10);
    set(gcf, 'Position', get(0, 'ScreenSize'));
    saveas(gcf,[PATHOUT,'figures\',nameAtlas,num2str(ROI),'drop.png']);
    if ~exist([PATHOUT,'results_Diff\'])
        mkdir([PATHOUT,'results_Diff\'])
    end
    
    if UserAtlas == '1' || UserAtlas == '2'
        if ~exist([PATHOUT,'results_Diff\UserAtlas\'])
            mkdir([PATHOUT,'results_Diff\UserAtlas\'])
        end
        save([PATHOUT,'results_Diff\UserAtlas\Diff_',nameVar,'_',nameAtlas,'_',num2str(ROI)],'ResultsC','ResultsF','ResultsCRight');
    else
        save([PATHOUT,'results_Diff\Diff_',nameVar,'_',nameAtlas,'_',num2str(ROI)],'ResultsC','ResultsF','ResultsCRight');
    end
        
    close all
        
    figure
    subplot(3,2,1)
    figLeft=imread([PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'left.png']);
    imshow(figLeft);
    subplot(3,2,2)
    figRight=imread([PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'right.png']);
    imshow(figRight);
    subplot(3,2,3)
    figSeed1=imread([PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'seedleft.png']);
    imshow(figSeed1);
    subplot(3,2,4)
    figSeed2=imread([PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'seedright.png']);
    imshow(figSeed2);
    subplot(3,2,5)
    figSeed3=imread([PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'seedbottom.png']);
    imshow(figSeed3);
    subplot(3,2,6)
    figTopo=imread([PATHOUT,'figures\archive\',nameAtlas,num2str(ROI),'topo.png']);
    imshow(figTopo);
    sgtitle(['Signal-loss from FullCap to cEEGrid = ',num2str(PerceChange),'%. Loss to only right cEEGrid = ',num2str(PerceChangeOnly)],'FontSize',10);
    set(gcf, 'Position', get(0, 'ScreenSize'));

    
    if UserAtlas == '0'
        if ~exist([PATHOUT,'figures\collection\',SubjectName,'\'])
            mkdir([PATHOUT,'figures\collection\',SubjectName,'\'])
        end
        saveas(gcf,[PATHOUT,'figures\collection\',SubjectName,'\',nameAtlas,num2str(ROI),'.png']);
    elseif UserAtlas == '1'
        if ~exist([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
            mkdir([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
        end
        saveas(gcf,[PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\',nameAtlas,num2str(ROI),'UserScout',num2str(ROIUser),'.png']);
    else UserAtlas == '2'
        if ~exist([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
            mkdir([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
        end
        saveas(gcf,[PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\UserScout',num2str(ROIUser),'.png']);
    end
    
    close all
     
    % clear GUI, go back to default before starting the script. If not needed, comment this section out
    listDelete =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\matrix_scout*']);
    listDelete2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_1*']); %this needs to be smoother, in 2020 this won't work anymore
    listDelete3 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_simulation*']);
    listDelete4 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_1*']);
    
    for w = 1:length(listDelete)
        isDeleted = file_delete( [listDelete(w).folder,'\',listDelete(w).name], 1, -1);
    end
    for w = 1:length(listDelete2)
        isDeleted = file_delete( [listDelete2(w).folder,'\',listDelete2(w).name], 1, -1);
    end
    for w = 1:length(listDelete3)
        isDeleted = file_delete( [listDelete3(w).folder,'\',listDelete3(w).name], 1, -1);
    end
    for w = 2:length(listDelete4)
        isDeleted = file_delete( [listDelete4(w).folder,'\',listDelete4(w).name], 1, -1);
    end
    
    db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI
