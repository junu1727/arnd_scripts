%% This script creates the plots from figure 2 (source properties)
% In this script, we simulate and plot dipoles with different 
% orientations, positions and distances relative to the cEEGrid. This
% scripts finds the source grid point cloesest to the geometric middle of
% the electrodes (cEEGrid) and seeds activity. The orientation of that
% dipole is set to be orthogonal to the hypothetical plane of the cEEGrid.
% The other two orientations that are simulated are orthogonal to the first
% one. Second, a dipole with constant orientation is placed into the brain
% volume. It is then moved in the dorsal and the anterior direction,
% respectively. For distance, the dipole closest to the cEEGrid is moved
% into the brain volume, further away from the electrodes. All three
% simulations are plotted. 
% Some of the settings are restricted to the cEEGrid!!!

clear;
close all;
clc

%% Settings
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's16_volume'; %the subject must be the one with the VOLUME HEADMODEL, not the surface one
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\PhD\Experimente\cEEGrid_Sensitivity\figures_sim\'; %store the figures here
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
%brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = FullCap
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load the channel file and the dipole file
listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);
leftChannels = indexChan(1:length(indexChan)/2);

listSource    = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel   = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']); 
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in a_PrepareSources...
CM     = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
HM     = load([listHeadmodel(end).folder,'\',listHeadmodel(end).name]); %...and the headmodel
% [gainSort, ~] = sort(HM.Gain,'descend'); %safety check. All points should have an approximately equal course. Sometimes, one source point has highly elevated values for all electrodes
% figure
% plot(gainSort)
%% Caclulate the normal of the cEEGrid plane + the two orthogonals
%find the vertices on the scalp mesh that are closest to the cEEGrids

for h = 1:length(leftChannels) %calculate the center of the cEEGrid from the individual elec positions
    CenterLeft(h,1:3) = CM.Channel(leftChannels(h)).Loc;
end
CenterLeft = mean(CenterLeft);

listScalp = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\@default_subject\*remesh*']); %contains the remeshed scalp. with the higher tesselation, we can get vertices that are closer to the cEEGrids
remeshScalp = in_tess_bst([listScalp(end).folder,'\',listScalp(end).name]); %This function calls the scalp mesh and adds the missing fields (VertNormals etc.)

d = 1;
for s = leftChannels %store the locations of the electrodes in a new variable
    Ceelocsl(d,1:3) = CM.Channel(s).Loc;
    d=d+1;
end

[l ,distl] = dsearchn(remeshScalp.Vertices,Ceelocsl); %calc the vertices that are closest to the cEEGrid locations on the left...
[cen ,distcen] = dsearchn(remeshScalp.Vertices,CenterLeft); %vertex cen closest to the center of the cEEGrid

Cnearcen = remeshScalp.Vertices(cen,1:3); %store the new center of the cEEGrid

NormCl = mean(remeshScalp.VertNormals([l; cen],1:3)); %calc the normal vector of the plane from the center plus the cEEGrid positions
N = [NormCl; null(NormCl(:).')']; %calc the two orthogonals to the normal vector of the plane

%% Define point on the sourceGrid that is closest to the cEEGrid center (from those on the x/z-coordinates that are closest to the center)
%from there, calculate points  needed for the simulation of depth/position
GridLocsXZ = HM.GridLoc(:,[1 3]); %store the x and z coordinates of the source grid from the mri volume in a new variable
[left, distl] = dsearchn(GridLocsXZ,Cnearcen([1 3])); %find the grid points that have grid locations closest to the center of the cEEGrid on the x and z axis
NearestXZ = find(HM.GridLoc(:,1)== HM.GridLoc(left,1) & HM.GridLoc(:,3)==HM.GridLoc(left,3)); %since the grid points are equally distributed, there will be several points with the same xz coordinates

fac = 0.015; %this variable determines the distance between the points (in meter)
%find the index of the grid location with the smallest distance to the center of the cEEGrid
closest  = HM.GridLoc(NearestXZ(end),:)'; %due to the built of the matrix, the last of the found values is the left-most value (and therefore closest to the cEEGrid)
%closest is the point from where different orientations are simulated
closest(3)  = closest(3)+0.005; % +5mm is added, because for the nearest, there is not enough space for the other points
closestyminus = [closest(1);closest(2)-fac;closest(3)]; %from the starting point (closest) go on the y axis by a distance of fac
closestyminus2 = [closest(1);closest(2)-fac*2;closest(3)]; %from the starting point (closest) go on the y axis by a distance of fac*2
%closest/closestyminus/closestyminus2 are the points simulating the increasing depth of the source
closestxplus = [closestyminus(1)+fac*3;closestyminus(2);closestyminus(3)]; %from the second of 3 points on the y axis, go in the x and z direction by fac*3
closestzplus = [closestyminus(1);closestyminus(2);closestyminus(3)+fac*3];
%closestx/zplus are the points simulating different source positions

%% put the new source locations and orientations into the dipole file and load it to brainstorm

listDip = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles*']);%load the dipole file from a_PrepareSources.m
export_matlab([listDip(end).folder,'\',listDip(end).name],'dip');
deleteDip = file_delete([listDip(end).folder,'\',listDip(end).name], 1, -1); %since it will be replaced anyway, delete it

n = 3; %number of orientations
for f = 1:n
    dip.Dipole(f).Loc     = closest (:,1); %first 3 dipoles have the same location and differ only in orientation
    dip.Dipole(f+n).Loc   = closestyminus(:,1); %second loc, 3 orientations
    dip.Dipole(f+n*2).Loc = closestyminus2(:,1); %same concept for the remaining points
    dip.Dipole(f+n*3).Loc = closestxplus(:,1);
    dip.Dipole(f+n*4).Loc = closestzplus(:,1);

    dip.Dipole(f).Amplitude     = N(:,f); %here, the orientations for each loc are defined
    dip.Dipole(f+n).Amplitude   = N(:,f);
    dip.Dipole(f+n*2).Amplitude = N(:,f);
    dip.Dipole(f+n*3).Amplitude = N(:,f);
    dip.Dipole(f+n*4).Amplitude = N(:,f);
    dip.Dipole(f+n*5).Amplitude = N(:,f);
end

x= f+n*4+1;
dip.Dipole(x:end) = []; %delete the remaining values from the predefined dipole file. Only the once specified above remain
dip.Time(x:end) = []; %same with the time points

HM.GridLoc = round(HM.GridLoc(:,:),4); % this line is necessary because matlab rounds in a funny way

vecTime = linspace(0,1,length(dip.Dipole));
for w = 1:length(dip.Dipole)
    dip.Dipole(w).Loc = round(dip.Dipole(w).Loc,4); % this line is necessary because matlab rounds in a funny way
    dip.Dipole(w).Time = vecTime(w);
    dip.Dipole(w).Index = 1;
    dip.Dipole(w).Origin = [0,0,0];
    dip.Dipole(w).Goodness = 1;
end

w=1;
for u = 1:3:length(dip.Dipole)
    [logigB(w,:),findHM(w,:)] = ismember(dip.Dipole(u).Loc',HM.GridLoc,'rows'); %this loop finds for each wanted dipole location the equivalent on the HM 
    w=w+1;
end
%% Preapre an empty SourceGrid

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things

Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
SourceSimple = Source;

sFiles = [];
sFiles = {...
    [listSource(1).folder,'\',listSource(1).name]};

% Start a new report, get subject infos
bst_report('Start', sFiles);
sProcess = bst_process('GetInputStruct', sFiles);
[sSubject,iSubject] = bst_get('Subject', sProcess.SubjectFile); %gets the subject ID

%% Some settings for the cEEGrid channels

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
nameVar = 'Left';
nameResult = 'cEEGrid'; %for naming the resulting images
iChannels = 1:length(channels); %number of channels according to your channel file

%% for every source, calculate the channel pair recording the highest amplitude. Store all information in a structure
for d = 1: length(dip.Dipole) %loop through the dipoles with their 3 orientations
    SourceSimple = Source; %reset the source variable in every iteration
    dip_bst = dip; %reset the dipole variable in every iteration
    dip_bst.Dipole = dip_bst.Dipole(d);
    dip_bst.Time = 0;
    
    ProtocolInfo = bst_get('ProtocolInfo');
    dip_bst.Comment= [nameResult,'_left_dip_',num2str(d)];
    db_add(ProtocolInfo.iStudy, dip_bst, []); %add the dipole to brainstorm to have the plot of the dipole orientation
         
    IMAGEGRIDAMP = SourceSimple.ImageGridAmp; %This is the source
    tline = [1;1;1;2;2;2;3;3;3;4;4;4;5;5;5]';
    IMAGEGRIDAMP(findHM(tline(d))*3-2:findHM(tline(d))*3,1) = dip_bst.Dipole.Amplitude; % find the position of a dipole (findHM) in the IMAGEGRIDAMP.
    %since per source there are 3 orientation, multiply the loc, which is
    %only one point, by 3. Replace these values with those ones of the 3
    %orientations from dip_bst.Dipole(r).Amplitude. The result is one
    %dipole source with one orientation
    
    SourceSimple.ImageGridAmp = IMAGEGRIDAMP;
    
    Fi = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
    Fi(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
            
    ProtocolInfo = bst_get('ProtocolInfo');
    SourceSimple.Comment = [nameResult,'_left_dip_',num2str(d)];
    db_add(ProtocolInfo.iStudy, SourceSimple, []);
    db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI
    
    collect=[];
    index=1;
    
    for ch=1:length(channels)-1
        for b=ch+1:length(channels)
            collect(index) = abs((Fi(iChannels(ch),1))- (Fi(iChannels(b),1))); %calc the amp of every combination of bipolar channels
            checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]}; %store the channel pair that recorded the respective amp
            checkPair(index,2) = {CM.Channel(channels(ch)).Name};
            checkPair(index,3) = {CM.Channel(channels(b)).Name};
            SelectedValues(index,1) = Fi(iChannels(ch),1); %store the amps
            SelectedValues(index,2) = Fi(iChannels(b),1); 
            index=index+1;
        end
    end
    FixPairVert = abs(SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),1) - SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),2)); %choose two electrode pairs that are in the horizontal and vertical
    FixPairHorz = abs(SelectedValues(strcmp(checkPair(:,1),'E07 - E10'),1) - SelectedValues(strcmp(checkPair(:,1),'E07 - E10'),2));
    %these two are the horizontal and vertical channels, their values are
    %stored separately
    
    %% Store the results in a variable
    
    SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    ResultsC(d).maxDiff =  max(collect);
    ResultsC(d).CheckPair = checkPair;
    ResultsC(d).CheckPair(:,4) = {'0'};
    ResultsC(d).CheckPair(collect==max(collect),4) = {'1'};
    ResultsC(d).Pair = checkPair(collect==max(collect),1);
    ResultsC(d).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    ResultsC(d).SelectedValues = SelectedValues;
    ResultsC(d).SelectedValues(:,3) = 0;
    ResultsC(d).SelectedValues(collect==max(collect),3) = 1;
    ResultsC(d).FixPairHorz = FixPairHorz;
    ResultsC(d).FixPairVert = FixPairVert;

    %% simulate the recordings based on the dipoles
    listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_MN*']);
    newDataFile = bst_simulation([listResults(end).folder,'\',listResults(end).name]);
    
    listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\*simulation.mat']);
    
%     leftFigTopo = view_topography([listSimulation(end).folder,'\',listSimulation(end).name], 'EEG', '3DSensorCap'); %view the averaged signal of the subject
%     figure_3d('ViewSensors', gcf, 0, 0);
%     figure_3d('SetStandardView', gcf, {'left'});
%     set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'off'); 
%     saveas(gcf,[PATHFIG,'left_dipole_',num2str(d),'_Cap.png']);
    
    export_matlab([listSimulation(end).folder,'\',listSimulation(end).name],'simulation');
    simulation.ChannelFlag(1:channels(1)-1) = -1; %determine which channels are treated as bad. This way, they will not be part of the plotting
    simulation.Comment = [nameResult,'_left_dip_',num2str(d)];
    ProtocolInfo = bst_get('ProtocolInfo');
    db_add(ProtocolInfo.iStudy, simulation, []);
    listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_2*']);

%     %cEEGrid elec-topo from the left side
%     leftFig = view_topography([listSimulation(end).folder,'\',listSimulation(end).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
%     figure_3d('ViewSensors', gcf, 0, 0);
%     figure_3d('SetStandardView', gcf, {'left'});
%     set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'off'); 
%     saveas(gcf,[PATHFIG,'left_dipole_',num2str(d),'_Sensors.png']);
    
    %% clean the workspace, delete all simulations, reset everything
    listDelete1 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_2*']);
    listDelete2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_simulation*']);
    listDelete3 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_2*']);

    for w = 1:length(listDelete1)
        isDeleted = file_delete( [listDelete1(w).folder,'\',listDelete1(w).name], 1, -1);
    end
    for w = 1:length(listDelete2)
        isDeleted = file_delete( [listDelete2(w).folder,'\',listDelete2(w).name], 1, -1);
    end
    for w = 2:length(listDelete3)
        isDeleted = file_delete( [listDelete3(w).folder,'\',listDelete3(w).name], 1, -1);
    end
    
    db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI
end

%% plot the graphs showing the amplitudes of the bipolar channels
%create a color scheme
yellow = [247 252 185] / 255;
darkGreen = [49 163 84] / 255;
colors = [yellow;darkGreen];

%% plot for orientation
for s = 1: length(ResultsC)
vc(1:2,s) = [ResultsC(s).FixPairVert ResultsC(s).FixPairHorz]; %retrieve the amplitudes of the bipolar channels per dipole
end

m = length(colors);

figure
subplot(1,3,1)
hold on
for k = 1:2
bc = bar(k,vc(k)); %plot a bar for both channels
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
ylabel('Amplitude')
xlabel('Norm')
set(gcf,'color','w'); 
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,2)
hold on
for k = 1:2
bc = bar(k,vc(k+2));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Anterior')
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,3)
hold on
for k = 1:2
bc = bar(k,vc(k+4));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Dorsal')
set(gcf, 'Position', get(0, 'ScreenSize'));
set(gca,'fontweight','bold','fontsize',12);
%saveas(gcf,[PATHFIG,'orientation.png']);

%% plot for distance

figure
subplot(1,3,1)
hold on
for k = 1:2
bc = bar(k,vc(k+4)); %5&6 are the indices of the first dipole, dorsal orientation
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
ylabel('Amplitude')
xlabel('Low')
set(gcf,'color','w'); 
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,2)
hold on
for k = 1:2
bc = bar(k,vc(k+10)); %11&12 are the indices of the 4th dipole, dorsal orientation
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Medium')
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,3)
hold on
for k = 1:2
bc = bar(k,vc(k+16)); %17&18 are the indices of the 5th dipole, dorsal orientation
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('High')
set(gcf, 'Position', get(0, 'ScreenSize'));
set(gca,'fontweight','bold','fontsize',12);
%saveas(gcf,[PATHFIG,'depth.png']);

%% plot for position

figure
subplot(1,3,1)
hold on
for k = 1:2
bc = bar(k,vc(k+8)); 
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(21)])
ylabel('Amplitude')
xlabel('Central')
set(gcf,'color','w'); 
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,2)
hold on
for k = 1:2
bc = bar(k,vc(k+20)); 
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(21)])
xlabel('Anterior')
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,3)
hold on
for k = 1:2
bc = bar(k,vc(k+26)); 
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(21)])
xlabel('Dorsal')
set(gcf, 'Position', get(0, 'ScreenSize'));
set(gca,'fontweight','bold','fontsize',12);
%saveas(gcf,[PATHFIG,'position.png']);
