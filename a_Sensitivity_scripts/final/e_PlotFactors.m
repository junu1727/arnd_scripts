%% In this script, we create the prerequisits for making a short video of 
% the three source properties from b_SourceProperties.m, being the 
% orientations, positions and distances relative to the cEEGrid. This
% scripts finds the source grid point cloesest to the geometric middle of
% the electrodes (cEEGrid) and seeds activity. The orientation of that
% dipole is set to be orthogonal to the hypothetical plane of the cEEGrid.
% The other two orientations that are simulated are orthogonal to the first
% one. Second, a dipole with constant orientation is placed into the brain
% volume. It is then moved in the dorsal and the anterior direction,
% respectively. For distance, the dipole closest to the cEEGrid is moved
% into the brain volume, further away from the electrodes. It can be 
% specified, which source property should be simulated. In bst, one can
% make a short video from the data manually. Some of the settings are still
% restricted to the cEEGrid and to the specific head model.

clear;
close all;
clc

%% Settings
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's18_check'; %the subject must be the one with the VOLUME HEADMODEL, not the surface one
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_cEEGrid\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\figures_sim\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
EarChannel = '1'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered)
simVid = '3'; % 1 to simulate orientation, 2 for position, 3 for depth
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Copy existing folder
listFolder = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results*']); %lists of the variables created in PrepareSource_cEEGrid.mat

% Input files
sFiles = {...
    [SubjectName,'/raw/',listFolder(1).name], ...
    [SubjectName,'/raw/',listFolder(2).name]};

% Start a new report
bst_report('Start', sFiles);

% Process: Duplicate folders: Add tag "_plots"
sFiles = bst_process('CallProcess', 'process_duplicate', sFiles, [], ...
    'target', 2, ...  % Duplicate folders
    'tag',    '_plots');

%% Load the channel file and the dipole file
listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);
rightChannels = indexChan(length(indexChan)/2+1:end);
leftChannels = indexChan(1:length(indexChan)/2);

listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
iChannelsRight = 1:length(rightChannels); %number of channels according to your channel file
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
%% Caclulate the normal of the cEEGrid plane + the two orthogonals
%find the vertices on the scalp mesh that are closest to the cEEGrids

for h = 1:length(leftChannels) %calculate the center of the cEEGrid from the individual elec positions
    CenterLeft(h,1:3) = CM.Channel(leftChannels(h)).Loc;
end
CenterLeft = mean(CenterLeft);

listScalp = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\@default_subject\*remesh*']); %contains the remeshed scalp. with the higher tesselation, we can get vertices that are closer to the cEEGrids
remeshScalp = in_tess_bst([listScalp(end).folder,'\',listScalp(end).name]); %This function calls the scalp mesh and adds the missing fields (VertNormals etc.)

d = 1;
for s = leftChannels %store the locations of the electrodes (left/right) in new variables
    Ceelocsl(d,1:3) = CM.Channel(s).Loc;
    Ceelocsr(d,1:3) = CM.Channel(s+10).Loc;
    d=d+1;
end

[l ,distl] = dsearchn(remeshScalp.Vertices,Ceelocsl); %calc the vertices that are closest to the cEEGrid locations on the left...
[r ,distr] = dsearchn(remeshScalp.Vertices,Ceelocsr); %...and the right side
[cen ,distcen] = dsearchn(remeshScalp.Vertices,CenterLeft); %vertex cen closest to the centre of the cEEGrid

Cnearcen = remeshScalp.Vertices(cen,1:3); %store the new center of the cEEGrid

NormCl = mean(remeshScalp.VertNormals([l; cen],1:3)); %calc the normal vector of the plane from the center plus the cEEGrid positions
N = [NormCl; null(NormCl(:).')']; %calc the two orthogonals to the normal vector of the plane

%% Define point on the sourceGrid that is closest to the cEEGrid center (from those on the x/z-coordinates that are closest to the center)
% Then, calc more points that have equal distances to each other in every direction

GridLocsXZ = HM.GridLoc(:,[1 3]); %store the x and z coordinates of the source grid from the mri volume in a new variable
[left, distl] = dsearchn(GridLocsXZ,Cnearcen([1 3])); %find the grid points that have grid locations closest to the center of the cEEGrid on the x and z axis
NearestXZ = find(HM.GridLoc(:,1)== HM.GridLoc(left,1) & HM.GridLoc(:,3)==HM.GridLoc(left,3)); %since the grid points are equally distributed, there will be several points with the same xz coordinates

fac = 0.015; %this variable determines the distance between the points (in meter)
%find the index of the grid location with the smallest distance to the center of the cEEGrid
closest  = HM.GridLoc(NearestXZ(end),:)'; %due to the built of the matrix, the last of the found values is the left-most value (and therefore closest to the cEEGrid)
closest(3)  = closest(3)+0.005; %due to the built of the matrix, the last of the found values is the left-most value (and therefore closest to the cEEGrid). +5mm is added, because for the nearest, the other points don't exist
closestyminus = [closest(1);closest(2)-fac;closest(3)]; %from the starting point (closest) go on the y axis by a distance of fac
closestyminus2 = [closest(1);closest(2)-fac*2;closest(3)]; %from the starting point (closest) go on the y axis by a distance of fac*2
closestxplus = [closestyminus(1)+fac*3;closestyminus(2);closestyminus(3)]; %from the second of 3 points on the y axis, go in the x and z direction by fac*3
closestzplus = [closestyminus(1);closestyminus(2);closestyminus(3)+fac*3];

%% store new locations and orientations of the dipoles
listDip = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\dipoles*']);
export_matlab([listDip(end).folder,'\',listDip(end).name],'dip'); %load the dipole file from PrepareSourceDipole.m

n = 3; %number of points on the y axis
for f = 1:n
    dip.Dipole(f).Loc     = closest (:,1); %first 3 dipoles have the same location and differ only in orientation
    dip.Dipole(f+n).Loc   = closestyminus(:,1); %second loc, 3 orientations
    dip.Dipole(f+n*2).Loc = closestxplus(:,1); %same concept for the remaining points
    dip.Dipole(f+n*3).Loc = closestzplus(:,1);
    
    dip.Dipole(f).Amplitude     = N(:,f); %here, the orientations for each loc are defined
    dip.Dipole(f+n).Amplitude   = N(:,f);
    dip.Dipole(f+n*2).Amplitude = N(:,f);
    dip.Dipole(f+n*3).Amplitude = N(:,f);
end

dip.Dipole = dip.Dipole(1:n*4);

vecTime = linspace(0,1,length(dip.Dipole));
for w = 1:length(dip.Dipole)
    dip.Dipole(w).Loc = round(dip.Dipole(w).Loc,4); % this line is necessary because matlab rounds in a funny way
    dip.Dipole(w).Time = vecTime(w);
    dip.Dipole(w).Index = 1;
    dip.Dipole(w).Origin = [0,0,0];
    dip.Dipole(w).Goodness = 1;
end

HM.GridLoc = round(HM.GridLoc(:,:),4); % this line is necessary because matlab rounds in a funny way

w=1;
for u = 1:3:length(dip.Dipole)
    [logigB(w,:),findHM(w,:)] = ismember(dip.Dipole(u).Loc',HM.GridLoc,'rows') %this loop finds for each wanted dipole location the equivalent on the HM 
    w=w+1;
end

%% Some settings for the cEEGrid channels

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

if EarChannel == '1'
    channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
    nameVar = 'Left';
elseif EarChannel == '2'
    channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
    nameVar = 'Right';
else EarChannel == '0'
    channels=indexChan;
    nameVar = 'Double';
end

nameResult = 'cEEGrid'; %for naming the resulting images
iChannels = 1:length(channels); %number of channels according to your channel file

smoothVec = linspace(0,90,50); %create a vector for 50 dipoles, with steps from 0 to 90 (for rotation of source direction of up to 90�)
timeVec = linspace(0,10,length(smoothVec)*2);

%load in the raw signal file and extend it to be long enough for 50 dipoles
%to be shown for 100 samples each (=5000 sample points)
listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
export_matlab([listRaw(end).folder,'\',listRaw(end).name],'rawSim');
rawSim.F = rawSim.F(:,1);
rawSim.F = repmat(rawSim.F,1,length(timeVec)*50);
rawSim.Time = linspace(0,10,length(smoothVec)* length(timeVec));
rawSim.ChannelFlag(1:end) = 1;
rawSim.Comment = 'SimDip';

ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, rawSim, 1);
%% get results from different orientations
if simVid == '1'
  
dip_bst = dip;
dip_bst.Dipole(1) = dip.Dipole(1);
dip_bst.Time = timeVec;

vec1 = dip.Dipole(1).Amplitude; %norm vec
vec2 = dip.Dipole(3).Amplitude; % z vec

for t = 1 : length(smoothVec)
    angle(t) = smoothVec(t)*pi/180; % angle of rotation in rad
    v(:,t) = vec1 + tan(angle(t))*vec2; % length of vec1 is 1
    v(:,t) = v(:,t)/norm(v(:,t)); % v is a unit vector 5 degrees from vec1
    dip_bst.Dipole(t) = dip_bst.Dipole(1);
    dip_bst.Dipole(t).Amplitude = v(:,t);
    dip_bst.Dipole(t).Time = timeVec(t);
end

vec1 = dip.Dipole(3).Amplitude; % z vec
vec2 = dip.Dipole(2).Amplitude; % x vec

for t = length(smoothVec)+1: length(smoothVec)*2
    angle(t) = smoothVec(t-length(smoothVec))*pi/180;   % angle of rotation in rad
    v(:,t) = vec1 + tan(angle(t))*vec2;   % length of vec1 is 1
    v(:,t) = v(:,t)/norm(v(:,t));                % v is an unit vector 5 degrees from vec1
    dip_bst.Dipole(t) = dip_bst.Dipole(1);
    dip_bst.Dipole(t).Amplitude = v(:,t);
    dip_bst.Dipole(t).Time = timeVec(t);
end

dipReady = dip_bst;

u=0;
for bu = 1:100
    for st = 1: 50
        dipReady.Dipole(:,st+u) = dip_bst.Dipole(bu); % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
    end
    u = u +50;
end

dipReady.Time = linspace(0,10,length(dip_bst.Dipole)*50);
for h = 1:length(dipReady.Time)
    dipReady.Dipole(h).Time = dipReady.Time(h); %adjust the time vector to 5000 sample points
end

IMAGEGRIDAMP = zeros(length(Source.ImageGridAmp),length(dipReady.Time));

for st = 1: length(dipReady.Time)
    IMAGEGRIDAMP(findHM(1)*3-2:findHM(1)*3,st) = dipReady.Dipole(st).Amplitude; % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
end

%% From the dipoles, create simulations of the resulting topographies

Source.ImageGridAmp = IMAGEGRIDAMP;
Source.Time = dipReady.Time;

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
Source.DataFile = [SubjectName,'/raw_plots*/',listRaw(end).name]; % attach the source to the raw signal
dipReady.DataFile = [SubjectName,'/raw_plots*/',listRaw(end).name]; % attach the dipole file to the raw signal
Source.ChannelFlag(strcmp({CM.Channel.Comment},'NewChannels')==0) = -1; %change all put the cEEGrid channels to bad so only the cEEGrids will be shown in the 3DElectrode plot
db_add(ProtocolInfo.iStudy, Source, 1); %load the source into bst
db_add(ProtocolInfo.iStudy, dipReady, 1); %the dipole file, too

listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\results_20*']);
Sim3DElec = bst_simulation([listResults(end).folder,'\',listResults(end).name]); %simulate from the new source for the 3DElectrodes (only cEEGrid)

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
export_matlab([listRaw(end).folder,'\',listRaw(end).name],'SensorCap');
SensorCap.ChannelFlag(1:end) = 1; %change the channels to good, this way we get the 3DSensorCap topoplot
SensorCap.Comment = '3DSensorCap_orient';

ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, SensorCap, 1);
%% Plot the results

listDipNew =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\dipoles_2*']);
listSim = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);

%dipole orientations
hFig = view_dipoles([listDipNew(end).folder,'\',listDipNew(end).name], 'cortex')
set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'on');

%cEEGrid elec-topo from the left side
leftFig = view_topography([listSim(end-1).folder,'\',listSim(end-1).name], 'EEG', '3DElectrodes'); 
figure_3d('SetStandardView', gcf, {'left'});

%Topoplots
figTopo3D = view_topography([listSim(end).folder,'\',listSim(end).name], 'EEG', '3DSensorCap'); 
figTopo2D = view_topography([listSim(end).folder,'\',listSim(end).name], 'EEG', '2DSensorCap'); 

%% get results from different positions
elseif simVid == '2'

fac = 0.045; %this variable determines the distance between the points (in meter)
gridres = 0:0.005:fac;
closeLine = zeros(3,length(gridres)*2);
w=1;
for th = 0:0.005:fac
closeLine(1:3,w) = [closestyminus(1) + th; closestyminus(2) ;closestyminus(3)]; 
closeLine(1:3,w+length(gridres)) = [closestyminus(1) ; closestyminus(2) ;closestyminus(3) + th];
w=w+1;
end
closeLine(:,1:length(closeLine)/2) = flip(closeLine(:,1:length(closeLine)/2),2); %flip matrix so the plot will later go from the last x to the first and from there to the last z

dip_loc = dip;
for ds = 1:length(closeLine)
dip_loc.Dipole(ds).Loc = closeLine(:,ds);
dip_loc.Dipole(ds).Loc = round(dip_loc.Dipole(ds).Loc,4); % this line is necessary because matlab rounds in a funny way
dip_loc.Dipole(ds).Amplitude = dip.Dipole(2).Amplitude; % always take the orientation in the x-direction
dip_loc.Dipole(ds).Goodness = 1;
dip_loc.Dipole(ds).Origin = [0,0,0];
dip_loc.Dipole(ds).Index = 1;
dip_loc.Dipole(ds).Errors = 0;
dip_loc.Dipole(ds).Khi2 = dip_loc.Dipole(1).Khi2;
dip_loc.Dipole(ds).DOF = dip.Dipole(1).DOF;
dip_loc.Dipole(ds).Perform = dip.Dipole(1).Perform;
end

HM.GridLoc = round(HM.GridLoc(:,:),4); % this line is necessary because matlab rounds in a funny way

findHM =[];
w=1;
for u = 1:length(dip_loc.Dipole)
    [logigB(w,:),findHM(w,:)] = ismember(dip_loc.Dipole(u).Loc',HM.GridLoc,'rows') %this loop finds for each wanted dipole location the equivalent on the HM 
    w=w+1;
end

dipReady = dip_loc;
dipReady.DataFile = Source.DataFile;

IMAGEGRIDAMP = zeros(length(Source.ImageGridAmp),length(dipReady.Time));

u=0;
for bu = 1:length(dip_loc.Dipole)
    for st = 1: 5000/length(dip_loc.Dipole)
        dipReady.Dipole(:,st+u) = dip_loc.Dipole(bu); % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
        IMAGEGRIDAMP(findHM(bu)*3-2:findHM(bu)*3,st+u) = dipReady.Dipole(bu).Amplitude;
    end
    u = u + 5000/length(dip_loc.Dipole);
end

dipReady.Time = linspace(0,10,length(dipReady.Dipole));
for h = 1:length(dipReady.Time)
    dipReady.Dipole(h).Time = dipReady.Time(h);
end

Source.ImageGridAmp = IMAGEGRIDAMP;
Source.Time = dipReady.Time;

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
Source.DataFile = [SubjectName,'/raw_plots/',listRaw(end).name]; % attach the source to the raw signal
dipReady.DataFile = [SubjectName,'/raw_plots/',listRaw(end).name]; % attach the dipole file to the raw signal
Source.ChannelFlag(strcmp({CM.Channel.Comment},'NewChannels')==0) = -1;
db_add(ProtocolInfo.iStudy, Source, 1); %load the source into bst
db_add(ProtocolInfo.iStudy, dipReady, 1); %the dipole file, too

listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\results_20*']);
Sim3DElec = bst_simulation([listResults(end).folder,'\',listResults(end).name]); %simulate from the new source for the 3DElectrodes (only cEEGrid)

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
export_matlab([listRaw(end).folder,'\',listRaw(end).name],'SensorCap');
SensorCap.ChannelFlag(1:end) = 1; %change the channels to good, this way we get the 3DSensorCap topoplot
SensorCap.Comment = '3DSensorCap_pos';

ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, SensorCap, 1);

%% Plot the results

listDipNew =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\dipoles_2*']);
listSim = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);

%dipole positions
hFig = view_dipoles([listDipNew(end).folder,'\',listDipNew(end).name], 'cortex')
set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'on');

%cEEGrid elec-topo from the left side
leftFig = view_topography([listSim(end-1).folder,'\',listSim(end-1).name], 'EEG', '3DElectrodes'); 
figure_3d('SetStandardView', gcf, {'left'});

%Topoplots
figTopo3D = view_topography([listSim(end).folder,'\',listSim(end).name], 'EEG', '3DSensorCap'); 
figTopo2D = view_topography([listSim(end).folder,'\',listSim(end).name], 'EEG', '2DSensorCap');

%% get results from different distances
else simVid == '3'
    
fac = 0.03;
gridres = 0:0.005:fac;
closeLine = zeros(3,length(gridres));
w=1;
for th = gridres
closeLine(1:3,w) = [closest(1) ;closest(2) - th ;closest(3)]; 
w=w+1;
end

dip_dist = dip;
dip_dist.Dipole = dip_dist.Dipole(1:length(gridres));
for ds = 1:length(closeLine)
dip_dist.Dipole(ds).Loc = closeLine(:,ds);
dip_dist.Dipole(ds).Loc = round(dip_dist.Dipole(ds).Loc,4);
dip_dist.Dipole(ds).Amplitude = dip.Dipole(3).Amplitude; % always take the orientation in the x-direction
dip_dist.Dipole(ds).Goodness = 1;
dip_dist.Dipole(ds).Origin = [0,0,0];
dip_dist.Dipole(ds).Index = 1;
dip_dist.Dipole(ds).Errors = 0;
dip_dist.Dipole(ds).Khi2 = dip_dist.Dipole(1).Khi2;
dip_dist.Dipole(ds).DOF = dip.Dipole(1).DOF;
dip_dist.Dipole(ds).Perform = dip.Dipole(1).Perform;
end

findHM =[];
w=1;
for u = 1:length(dip_dist.Dipole)
    [logigB(w,:),findHM(w,:)] = ismember(dip_dist.Dipole(u).Loc',HM.GridLoc,'rows') %this loop finds for each wanted dipole location the equivalent on the HM 
    w=w+1;
end

dipReady = dip_dist;
dipReady.DataFile = Source.DataFile;

IMAGEGRIDAMP = zeros(length(Source.ImageGridAmp),length(dipReady.Time));

u=0;
for bu = 1:length(dip_dist.Dipole)
    for st = 1: ceil(5000/length(dip_dist.Dipole))
        dipReady.Dipole(:,st+u) = dip_dist.Dipole(bu); % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
        IMAGEGRIDAMP(findHM(bu)*3-2:findHM(bu)*3,st+u) = dipReady.Dipole(bu).Amplitude;
    end
    u = u +  ceil(5000/length(dip_dist.Dipole));
end

if size(IMAGEGRIDAMP,2) > 5000
IMAGEGRIDAMP(:,5001:end) = [];
dipReady.Dipole(:,5001:end) = [];
end

dipReady.Time = linspace(0,10,length(smoothVec)* length(timeVec));
for h = 1:length(dipReady.Time)
    dipReady.Dipole(h).Time = dipReady.Time(h);
end

Source.ImageGridAmp = IMAGEGRIDAMP;
Source.Time = dipReady.Time;

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
Source.DataFile = [SubjectName,'/raw_plots/',listRaw(end).name]; % attach the source to the raw signal
dipReady.DataFile = [SubjectName,'/raw_plots/',listRaw(end).name]; % attach the dipole file to the raw signal
Source.ChannelFlag(strcmp({CM.Channel.Comment},'NewChannels')==0) = -1;
db_add(ProtocolInfo.iStudy, Source, 1); %load the source into bst
db_add(ProtocolInfo.iStudy, dipReady, 1); %the dipole file, too

listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\results_20*']);
Sim3DElec = bst_simulation([listResults(end).folder,'\',listResults(end).name]); %simulate from the new source for the 3DElectrodes (only cEEGrid)

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);
export_matlab([listRaw(end).folder,'\',listRaw(end).name],'SensorCap');
SensorCap.ChannelFlag(1:end) = 1; %change the channels to good, this way we get the 3DSensorCap topoplot
SensorCap.Comment = '3DSensorCap_depth';

ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, SensorCap, 1);

%% Plot the results

listDipNew =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\dipoles_2*']);
listSim = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots*\data_2*']);

%dipole positions
hFig = view_dipoles([listDipNew(end).folder,'\',listDipNew(end).name], 'cortex')
set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'on');

%cEEGrid elec-topo from the left side
leftFig = view_topography([listSim(end-1).folder,'\',listSim(end-1).name], 'EEG', '3DElectrodes'); 
figure_3d('SetStandardView', gcf, {'left'});

%Topoplots
figTopo3D = view_topography([listSim(end).folder,'\',listSim(end).name], 'EEG', '3DSensorCap'); 
figTopo2D = view_topography([listSim(end).folder,'\',listSim(end).name], 'EEG', '2DSensorCap'); 

end
%% clean the workspace in bst

% listDeleteAll = [MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw_plots'];
% isDeleted = file_delete( listDeleteAll, 0, 1);
% db_reload_studies(ProtocolInfo.iStudy) %reload to get rid of the deleted files in the GUI
