%% This script creates the plots from figure 3 (signal loss) and figure 4 (channel performance) 
%We use a scout file that we predefined in bst (manually) to simulate how much 
%signal loss we can expect with the cEEGrid compared to the cap when 
%measuring each patch. This script plots the percentage signal drop as
%bars, both for the cEEGrid and two bipolar channels (vertical/horizontal).
%Additionally, a plot is made in bst to show the patches, colorcoded by
%signal loss. 

clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's18_checkSurf'; %the subject must be the one with the SURFACE HEADMODEL, not the volume one
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\figures_sim\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
% cd([MAINPATH,'brainstorm3\']);
% brainstorm
SCOUTPATH = 'C:\Users\arnd\Desktop\Experiments\cEEGridSimulation\';
SCOUTFILE = 'scout_left_06_50.mat';
EarChannel = '0'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
UserAtlas = '1'; % 1 if there is a user generated atlas to choose from. If 0, don't use the UserAtlas
refPairHor = 129; % pair1 reference (look up in the channel file)
refPairVer = 131; % pair2 reference
recPairHor = 132; % pair1 recording electrode
recPairVer = 136; % pair2 recording electrode
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);%lists of the variables created in a_PrepareSources.m
listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_MN*']); 
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']); 
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\headmodel*']);

export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap'); %load channel file from bst into matlab
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1); %identify ear-EEG channel
Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations

loadScout = load([SCOUTPATH,SCOUTFILE]); %definition of the scouts from an atlas
ScoutLabel = {loadScout.Scouts.Label};

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp,1),1); %set source matrix to zero with only 1 time point
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
Source.Time = 1;

for s = 1:4 %loop through cEEGrid, cap and two bipolar channels
    
    if s == 1 %cEEGrid
        nameResult = SubjectName;
        if EarChannel == '1'
            channels = indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
            nameVar = 'Left';
        elseif EarChannel == '2'
            channels = indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
            nameVar = 'Right';
        else EarChannel == '0'
            channels=indexChan;
            nameVar = 'Double';
        end
        
    elseif s == 2 %full cap
        nameResult = 'fullCap';
        channels = 1:indexChan(1)-1;
        nameVar = 'Full';
        
    elseif s == 3 %hor pair
        ref = refPairHor; %index of the reference electrode for bipolar cEEGrid channel (horz)
        nameResult = 'Bipolar_horizontal';
        channels = recPairHor; %name of the recording electrode for bipolar cEEGrid channel (horz)
        nameVar = 'Pair';
    else s == 4 %vert pair
        ref = refPairVer; %index of the reference electrode for bipolar cEEGrid channel (vert)
        nameResult = 'Bipolar_vertical';
        channels = recPairVer; %name of the recording electrode for bipolar cEEGrid channel (horz)
        nameVar = 'Pair';
    end
    
    iChannels = 1:length(channels); %number of channels according to your channel file
    SourceSimple = Source; %reset source variable in every iteration
    
    for x = 1:length(loadScout.Scouts) %calc the amplitudes recorded from every bipolar channel for all scout patches
        
        IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
        IMAGEGRIDAMP(loadScout.Scouts(x).Vertices)= 1; % set activity of vertices to 1, the others stay 0
        
        SourceSimple.ImageGridAmp = IMAGEGRIDAMP;
        
        F = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        if s == 3 || s == 4
            G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for the bipolar channels
        end
        
        collect=[];
        index=1;
        if s == 1 || s == 2
            for ch=1:length(channels)-1
                for b=ch+1:length(channels)
                    collect(index) = abs((F(iChannels(ch),1)) - (F(iChannels(b),1))); %calc every combination of electrodes
                    checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
                    checkPair(index,2) = {CM.Channel(channels(ch)).Name};
                    checkPair(index,3) = {CM.Channel(channels(b)).Name};
                    SelectedValues(index,1) = F(iChannels(ch),1);
                    SelectedValues(index,2) = F(iChannels(b),1);
                    index=index+1;
                end
            end
            
        else %for the bipolar channels, we simply subtract one channel from the other, there are no other combinations
            collect = [];
            checkPair = {};
            SelectedValues = [];
            collect = abs(F - G); %calc every combination of electrodes
            checkPair(1,1) = {[CM.Channel(channels).Name,' - ',CM.Channel(ref).Name]};
            checkPair(1,2) = {CM.Channel(channels).Name};
            checkPair(1,3) = {CM.Channel(ref).Name};
            SelectedValues(1,1) = F;
            SelectedValues(1,2) = G;
        end
        
        if s == 1 %store the results in a structure
            SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC(x).maxDiffAbs =  max(collect);
            ResultsC(x).CheckPair = checkPair;
            ResultsC(x).CheckPair(:,4) = {'0'};
            ResultsC(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsC(x).Pair = checkPair(collect==max(collect),1);
            ResultsC(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC(x).SelectedValues = SelectedValues;
            ResultsC(x).SelectedValues(:,3) = 0;
            ResultsC(x).SelectedValues(collect==max(collect),3) = 1;
        elseif s == 2
            ResultsF(x).maxDiffAbs =  max(collect);
            ResultsF(x).CheckPair = checkPair;
            ResultsF(x).CheckPair(:,4) = {'0'};
            ResultsF(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsF(x).Pair = checkPair(collect==max(collect),1);
            ResultsF(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsF(x).SelectedValues = SelectedValues;
            ResultsF(x).SelectedValues(:,3) = 0;
            ResultsF(x).SelectedValues(collect==max(collect),3) = 1;
        elseif s == 3
            ResultsP1(x).maxDiffAbs =  max(collect);
            ResultsP1(x).CheckPair = checkPair;
            ResultsP1(x).CheckPair(:,4) = {'0'};
            ResultsP1(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsP1(x).Pair = checkPair(collect==max(collect),1);
            ResultsP1(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsP1(x).SelectedValues = SelectedValues;
        else s == 4
            ResultsP2(x).maxDiffAbs =  max(collect);
            ResultsP2(x).CheckPair = checkPair;
            ResultsP2(x).CheckPair(:,4) = {'0'};
            ResultsP2(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsP2(x).Pair = checkPair(collect==max(collect),1);
            ResultsP2(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsP2(x).SelectedValues = SelectedValues;
        end
    end
end

%% with the results, plot the percentage loss for the cEEGrid and the channel pairs (bars and brain)
for f = 1:length(ResultsC) %calculate the percentage change from the full cap to the cEEGrid and the bipolar channels
    PerceChangecEEGrid(f,1) = 100 -(ResultsC(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
    PerceChangeHor(f,1) = 100 -(ResultsP1(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
    PerceChangeVert(f,1) = 100 -(ResultsP2(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
end

%some descriptives
mean(PerceChangecEEGrid)
std(PerceChangecEEGrid)
max(PerceChangecEEGrid)
min(PerceChangecEEGrid)

mean(PerceChangeHor)
std(PerceChangeHor)
max(PerceChangeHor)
min(PerceChangeHor)

mean(PerceChangeVert)
std(PerceChangeVert)
max(PerceChangeVert)
min(PerceChangeVert)

sum(PerceChangeHor>90)
sum(PerceChangeHor>90)
sum(PerceChangeVert>90)

[out,idx] = sort(PerceChangecEEGrid,'descend'); %sort the signal loss for the cEEGrid in a descending order

for g = 1:length(idx) %sort the bipolar channels the same way, the ordering inherited from the cEEGrid
    outHor(g,1) = PerceChangeHor(idx(g));
    outVert(g,1) = PerceChangeVert(idx(g));
end

w = 1;
for f = 1:2:length(ResultsC)*2 %store the results for the channel pairs in a new, single variable
    PerceChangePairs(f,1) = outHor(w);
    PerceChangePairs(f+1,1) = outVert(w);
    w=w+1;
end

Blue = [0 0 255]/255; %create a colorbar, from white to blue for the descending signal loss of the cEEGrid
jump = linspace(1,Blue(1), 1000);
for w = 1:length(jump)
    Blue(w,1:2) = jump(w);
    Blue(w,3) = 1;
end
yellow = [247 252 185] / 255; %colors for the bipolar channels
darkGreen = [49 163 84] / 255;
colors = [Blue;darkGreen;yellow];

colorBar = out ./ 100; %the percentage change goes from 0-1, just like the color bar below (Blue)

w=1;
for t = 1:length(colorBar)
[c(w) index(w,1)] = min(abs(Blue(:,1)-colorBar(t))); %find the closest value for the signal loss on the color bar
w=w+1;
end

figure %plot the bars in one chart
hold on
for k = 1:length(PerceChangePairs)
    h=bar(k,PerceChangePairs(k,1));
    if   any(k == 1:2:length(PerceChangePairs))
        set(h,'FaceColor',colors(end-1,:));
    else any(k == 2:2:length(PerceChangePairs))
        set(h,'FaceColor',colors(end,:));
    end
end
xticks(1:2:length(PerceChangePairs))
xticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50'})
ylim([-5 100])
xlabel('Patches on the cortex surface', 'FontSize', 12,'FontWeight','bold')
hold off
set(gcf,'color','none');
set(gca,'fontweight','bold','fontsize',12,'color','none');
%saveas(gcf,[PATHFIG,'Percentage_cap_cEEGrid_pair.png']);

figure
hold on
w=1;
for k = 1:length(PerceChangecEEGrid)
    h=bar(k,out(k,1));
    set(h,'FaceColor',colors(index(w),:));
    w=w+1;
end
xticks(1:length(PerceChangecEEGrid))
xticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50'})
ylim([-5 100])
set(gca,'fontweight','bold','fontsize',12,'Color','w');
ylabel('Signal loss [%]', 'FontSize', 12,'FontWeight','bold')
hold off
set(gcf,'color','none');
set(gca,'color','none');

%save([PATHOUT,'Percentage_cap_cEEGrid_pair.mat'],'ResultsP1','ResultsP2','ResultsC','ResultsF','PerceChangecEEGrid','PerceChangePairs');

listColor = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']);
colorbrain = load([listColor(end).folder,'\',listColor(end).name]);
colorbrain.ImageGridAmp = zeros(size(colorbrain.ImageGridAmp,1),1);
for x = 1:length(PerceChangecEEGrid)
    colorbrain.ImageGridAmp(loadScout.Scouts(x).Vertices) = PerceChangecEEGrid(x)/100;
end

ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, colorbrain, []);

%% plot how many times a cEEGrid pair had the highest amplitude 

w=1;
for f = numel(find(PerceChangecEEGrid < mean(PerceChangecEEGrid)))+1:length(PerceChangecEEGrid) %for all signal losses below the average...    
  bestPairs(w,1) = ResultsC(idx(f)).Pair ;  
  w=w+1;
end

outMatrix=zeros(numel(indexChan)/2); %create a matrix with the number of channels of the cEEGrid

for  k=1:length(bestPairs)
    a=str2num((bestPairs{k}(2:3)));
    b=str2num((bestPairs{k}(end-1:end)));
    outMatrix(a,b)=outMatrix(a,b)+1;
end

outMatrix(~triu(outMatrix,1)) = 1000; %not optimal. For the final coloring, this must be adjusted (its currently done manually)
outMatrix(~triu(outMatrix,1)) = -0.5;
outMatrix(~triu(outMatrix)) = -1;
outMatrix(outMatrix > 999) = 0;

figure
imagesc(outMatrix);
xticks(1:10)
xticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
yticks(1:10)
yticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
set(gca,'fontweight','bold','fontsize',12);
xlabel('cEEGrid electrodes', 'FontSize', 12,'FontWeight','bold')
set(gcf,'color','w');
set(gcf,'color','w');
axis square
saveas(gcf,[PATHFIG,'Connections.png']);

