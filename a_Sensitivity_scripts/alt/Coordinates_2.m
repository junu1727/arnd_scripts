clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's06_cEEGrid128'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
ClusterSize = 5;
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%load the predefined clustering and only use those with a length of precisely 20
SurfaceClustering = load('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_Surface_clustering_5V.mat');
SurfaceClustering = SurfaceClustering.Scouts;
lengths = arrayfun(@(x) size(SurfaceClustering(x).Vertices,2), 1:numel(SurfaceClustering));
longest = lengths > ClusterSize;
longest = find(longest == 1);
for r = 1:length(longest)
    SurfaceClustering(longest(r)).Vertices(21:end) = [];
    lengths(longest(r)) = ClusterSize;
end
keepClusters = SurfaceClustering(lengths == ClusterSize);
%load the headmodel and channel file of the subject
listScalp = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*remesh*']);
export_matlab([listScalp(end).folder,'\',listScalp(end).name],'remeshScalp');
listCM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']);
listHM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*low_warped*']); %raw s.t. starts with R or r
CM = load([listCM(end).folder,'\',listCM(end).name]);
HM = load([listHM(end).folder,'\',listHM(end).name]);

%now, calc the mean curvature of every ROI
cur = tess_curvature(HM.Vertices, HM.VertConn, HM.VertNormals, 10, 1);
for s = 1:length(keepClusters)
    maxminCur(s,1) = max(cur(keepClusters(s).Vertices));
    maxminCur(s,2) = min(cur(keepClusters(s).Vertices));
    absCur(s,1) = mean([maxminCur(s,1) abs(maxminCur(s,2))]);
end
keepClusters = keepClusters(absCur<0.5); %take only ROIs with an average curvature below 0.5 (arbitrary)

%calc the geometric middle of the ceegrids for later distance to every ROI
s=1;
for f = 139:148
    meanR(s,1:3) = CM.Channel(f).Loc(1:3);
    s=s+1;
end
%geometric middle right
meanCoR(1) = mean(meanR(:,1));
meanCoR(2) = mean(meanR(:,2));
meanCoR(3) = mean(meanR(:,3));

s=1;
for f = 129:138
    meanL(s,1:3) = CM.Channel(f).Loc(1:3);
    s=s+1;
end
%geometric middle left
meanCoL(1) = mean(meanL(:,1));
meanCoL(2) = mean(meanL(:,2));
meanCoL(3) = mean(meanL(:,3));

%create the distance between these points
for s = 1: length([keepClusters.Seed])
    check(s,1) = norm(HM.Vertices([keepClusters(s).Seed],1:3) - meanCoR);
    check(s,2) = norm(HM.Vertices([keepClusters(s).Seed],1:3) - meanCoL);
end

%create distance for each coordinate individually (only left)
for s = 1: length([keepClusters.Seed])
    xcheck(s,1) = norm(HM.Vertices([keepClusters(s).Seed],1) - meanCoL(1));
    ycheck(s,1) = norm(HM.Vertices([keepClusters(s).Seed],2) - meanCoL(2));
    zcheck(s,1) = norm(HM.Vertices([keepClusters(s).Seed],3) - meanCoL(3));
end

%finally, take the cEEGrid that is closer to every respective ROI
for s = 1: length([keepClusters.Seed])
    checkEnd(s,1) = max(check(s,1:2));
end

%find the vertices on the scalp mesh that are closest to the cEEGrids
t = 1;
for s = 129:138
    Ceelocs(t,1:3) = CM.Channel(s).Loc;
    t=t+1;
end
[k dist] = dsearchn(remeshScalp.Vertices,Ceelocs); %vertices k closest to the cEEGrids
Cnear = remeshScalp.Vertices(k,1:3); %these are the new cEEgrid normal vectors
meanNormC = mean(remeshScalp.VertNormals(k,1:3)); %calc the normal vector

%find the normal vector for every ROI
for x = 1: length(keepClusters)
    HMVertnormals = HM.VertNormals([keepClusters(x).Vertices],1:3); %the normal vector of every ROI is the mean of every vertex within the ROI
    meanNormROI(x,1) = mean(HMVertnormals(:,1));
    meanNormROI(x,2) = mean(HMVertnormals(:,2));
    meanNormROI(x,3) = mean(HMVertnormals(:,3));
    HMVertnormals = [];
end

%take the normal vector of each ROI and find its angle to the normal vector of the cEEGrid plane
cEEGridAngle = meanNormC;
ClusterAngle = meanNormROI;
for y = 1:length(meanNormROI)
    CosTheta(y,1:3) = dot(cEEGridAngle,ClusterAngle(y,1:3))/(norm(cEEGridAngle)*norm(ClusterAngle(y,1:3)));
    ThetaInDegrees(y,1) = acosd(CosTheta(y,1));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cluster = 569;

figure
x = Cnear(:,1);
y = Cnear(:,2);
z = Cnear(:,3);
x0 = min(x) ; x1 = max(x) ; nx = 500 ;
y0 = min(y) ; y1 = max(y) ; ny = 500 ;
xx = linspace(x0,x1,nx) ;
yy = linspace(y0,y1,ny) ;
[X,Y] = meshgrid(xx,yy) ;
Z = griddata(x,y,z,X,Y)  ;
[U,V,W] = surfnorm(X,Y,Z);

a = Cnear(3,1:3); %cEEGrid point
b = meanNormC; % your normal vector
c = a+b; % end position of normal vector

%quiver3 syntax: quiver3(x,y,z,u,v,w)
quiver3(a(1), a(2), a(3), c(1), c(2), c(3));
hold on
plot3(x,y,z,'mo')
mesh(X,Y,Z)
axis equal;

%for cluster = 1:length(keepClusters)
u = HM.Vertices(keepClusters(1).Vertices,1);
v = HM.Vertices(keepClusters(1).Vertices,2);
w = HM.Vertices(keepClusters(1).Vertices,3);
u0 = min(u) ; u1 = max(u) ; nu = 500 ;
v0 = min(v) ; v1 = max(v) ; nv = 500 ;
uu = linspace(u0,u1,nu) ;
vv = linspace(v0,v1,nv) ;
[A,B] = meshgrid(uu,vv) ;
L = griddata(u,v,w,A,B)  ;
[P,Q,R] = surfnorm(A,B,L);

d = HM.Vertices(keepClusters(1).Vertices(1),1:3); %ROI point
e = meanNormROI(1,:); % normal vector
f = d+e; % end position of normal vector

quiver3(d(1), d(2), d(3), f(1), f(2), f(3));
hold on
%plot3(u,v,w,'mo')
mesh(A,B,L)
axis equal;
%end

%load the sensitivity map for the cEEGrids, put them in a structure as the
%dependant variable, together with all predictors
listSrc = dir('C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_cEEGrid\s06_cEEGrid128\results_Left*');
load([listSrc(1).folder,'\',listSrc(1).name])

for s = 1:length(keepClusters)
SrcStrength(s,:) = mean(SourceDist.ImageGridAmp(keepClusters(s).Vertices));
end

GLM.Angle = ThetaInDegrees;
GLM.XDist = xcheck;
GLM.YDist = ycheck;
GLM.ZDist = zcheck;
GLM.SrcStrength = SrcStrength;

xlswrite('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeft.xlsx','GLM');
writetable(struct2table(GLM), 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeft.xlsx')
save('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeft.mat','GLM');

figure
subplot(2,2,1)
scatter(GLM.SrcStrength,GLM.XDist)
lsline
[rho,pval] = corr(GLM.SrcStrength,GLM.XDist)
subplot(2,2,2)
scatter(GLM.SrcStrength,GLM.YDist)
lsline
[rho,pval] = corr(GLM.SrcStrength,GLM.YDist)
subplot(2,2,3)
scatter(GLM.SrcStrength,GLM.ZDist)
lsline
[rho,pval] = corr(GLM.SrcStrength,GLM.ZDist)
subplot(2,2,4)
scatter(GLM.SrcStrength,GLM.Angle)
lsline
[rho,pval] = corr(GLM.SrcStrength,GLM.Angle)

[R,P] = corrcoef([GLM.XDist GLM.YDist GLM.ZDist GLM.Angle])

figure
subplot(2,3,1)
hist(GLM.SrcStrength,50)
subplot(2,3,2)
hist(GLM.Angle,50)
subplot(2,3,3)
hist(GLM.XDist,50)
subplot(2,3,4)
hist(GLM.YDist,50)
subplot(2,3,5)
hist(GLM.ZDist,50)