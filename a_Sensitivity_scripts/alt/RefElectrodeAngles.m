%% Calc the angles between the ref (E16) and all bipolar electrode combinations
clear;
close all;
clc

%% Settings
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's11_dipole'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load the channel file and the dipole file

listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile

d=1;
for s = 129:148
locs(d,1:3) = CM.Channel(s).Loc(1:3)';
d=d+1;
end

channels = 1:20;
P0 = locs(16,:);
index = 1;
CMname = 129:148;

for ch=1:length(locs)
    for b=ch+1:length(locs)
P1 = locs(channels(ch),:);
P2 = locs(channels(b),:);
n1 = (P2 - P0) / norm(P2 - P0);  % Normalized vectors
n2 = (P1 - P0) / norm(P1 - P0);
CosTheta = dot(n1,n2)/(norm(n1)*norm(n2));
ThetaInDegrees = acosd(CosTheta);
collect(index).checkPair = {['Angle E16 with: ',CM.Channel(CMname(ch)).Name,' & ',CM.Channel(CMname(b)).Name]};
collect(index).Degree = ThetaInDegrees;    
index=index+1;
    end
end

collect(isnan([collect.Degree])) = [];

save('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\saveAngles.mat','collect');













