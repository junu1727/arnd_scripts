%% Simulate recordings. Results from PreapreSource_cEEGrid
clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's03_temp'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
%brainstorm
SCOUTPATH = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\';
SCOUTFILE = 'scout_left_06_50.mat';
cEEGrid = 4; % FullCap, cEEGrid, Pair 
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '1'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
atlas = '0'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux
UserAtlas = '2'; % 1 if there is an additional, user generated atlas to choose from. If 2, use ONLY that atlas. If 0, don't use the UserAtlas
refStandard = [30 31]; % cap reference
refC = 144; % cEEGrid reference
refPair1 = 129; % pair1 reference
refPair2 = 131; % pair2 reference
recPair1 = 132; % pair1 recording electrode
recPair2 = 136; % pair2 recording electrode
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);

listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)

if UserAtlas == '1' || UserAtlas == '2' % the loop is only neccesarry when going trough the hand-picked scouts. Therefore, if any other atlas is chosen, make the loop p = 1
    loadScout = load([SCOUTPATH,SCOUTFILE]); %definition of the scouts from an atlas. Only together with atlas = 'X'
    counter = 1;
    ScoutLabel = {loadScout.Scouts.Label};
else
    counter = 1;
end

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp,1),1); %set source matrix to zero with only 1 time point
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
Source.Time = 1;

for s = 1:cEEGrid
  
    if s == 1
        ref = refC; %index of the reference electrode for cEEGrid (in CM, E16)
        nameResult = SubjectName;
        if EarChannel == '1'
            channels = indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
            nameVar = 'Left';
        elseif EarChannel == '2'
            channels = indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
            nameVar = 'Right';
        else EarChannel == '0'
            channels=indexChan;
            nameVar = 'Double';
        end
        
    elseif s == 2
        ref = refStandard; %index of the reference electrode for 64ChanCap (in CM, E053)
        nameResult = 'fullCap';
        channels = 1:indexChan(1)-1;
        nameVar = 'Full';
        
    elseif s == 3
        ref = refPair1; %index of the reference electrode for bipolar cEEGrid channel (horz)
        nameResult = 'Bipolar_horizontal';
        channels = recPair1; 
        nameVar = 'Pair';
    else s == 4
        ref = refPair2; %index of the reference electrode for bipolar cEEGrid channel (vert)
        nameResult = 'Bipolar_vertical';
        channels = recPair2; 
        nameVar = 'Pair';
    end
    
    iChannels = 1:length(channels); %number of channels according to your channel file
    SourceSimple = Source;
    
    for x = 1:length(loadScout.Scouts)
        
        IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
        IMAGEGRIDAMP(loadScout.Scouts(x).Vertices)= 1; % set activity of vertices to 1, the others stay 0
        
        SourceSimple.ImageGridAmp = IMAGEGRIDAMP;
       
        F = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        G = [];
        G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
        G = mean(G); %only nessecary for the fullCap. Here, we take the average from the two electrodes closest to the mastoid. Does not hurt for 1 electrode, mean of 1 stays 1
  
        collect=[];
        index=1;
        if s == 1 || s == 2
            for ch=1:length(channels)-1
                for b=ch+1:length(channels)
                    collect(index) = abs((F(iChannels(ch),1)- G)- (F(iChannels(b),1)-G)); %calc every combination of electrodes
                    checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
                    checkPair(index,2) = {CM.Channel(channels(ch)).Name};
                    checkPair(index,3) = {CM.Channel(channels(b)).Name};
                    SelectedValues(index,1) = F(iChannels(ch),1)- G;
                    SelectedValues(index,2) = F(iChannels(b),1)- G;
                    index=index+1;
                end
            end
            
        else s == 3
            collect = [];
            checkPair = {};
            SelectedValues = [];
            collect = abs(F - G); %calc every combination of electrodes
            checkPair(1,1) = {[CM.Channel(channels).Name,' - ',CM.Channel(ref).Name]};
            checkPair(1,2) = {CM.Channel(channels).Name};
            checkPair(1,3) = {CM.Channel(ref).Name};
            SelectedValues(1,1) = F;
            SelectedValues(1,2) = G;
        end
        
        if s == 1
            SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC(x).maxDiffAbs =  max(collect);
            ResultsC(x).CheckPair = checkPair;
            ResultsC(x).CheckPair(:,4) = {'0'};
            ResultsC(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsC(x).Pair = checkPair(collect==max(collect),1);
            ResultsC(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC(x).SelectedValues = SelectedValues;
            ResultsC(x).SelectedValues(:,3) = 0;
            ResultsC(x).SelectedValues(collect==max(collect),3) = 1;
        elseif s == 2
            ResultsF(x).maxDiffAbs =  max(collect);
            ResultsF(x).CheckPair = checkPair;
            ResultsF(x).CheckPair(:,4) = {'0'};
            ResultsF(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsF(x).Pair = checkPair(collect==max(collect),1);
            ResultsF(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsF(x).SelectedValues = SelectedValues;
            ResultsF(x).SelectedValues(:,3) = 0;
            ResultsF(x).SelectedValues(collect==max(collect),3) = 1;
        elseif s == 3
            ResultsP1(x).maxDiffAbs =  max(collect);
            ResultsP1(x).CheckPair = checkPair;
            ResultsP1(x).CheckPair(:,4) = {'0'};
            ResultsP1(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsP1(x).Pair = checkPair(collect==max(collect),1);
            ResultsP1(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsP1(x).SelectedValues = SelectedValues;
        else s == 4
            ResultsP2(x).maxDiffAbs =  max(collect);
            ResultsP2(x).CheckPair = checkPair;
            ResultsP2(x).CheckPair(:,4) = {'0'};
            ResultsP2(x).CheckPair(collect==max(collect),4) = {'1'};
            ResultsP2(x).Pair = checkPair(collect==max(collect),1);
            ResultsP2(x).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsP2(x).SelectedValues = SelectedValues;
        end
    end
end 

for f = 1:length(ResultsC)
PerceChange1(f,1) = 100 -(ResultsC(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
PerceChange2(f,1) = 100 -(ResultsP1(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
PerceChange3(f,1) = 100 -(ResultsP2(f).maxDiffAbs / ResultsF(f).maxDiffAbs *100);
end

[out,idx] = sort(PerceChange1,'descend');

for g = 1:length(idx)
out2(g,1) = PerceChange2(idx(g));
out3(g,1) = PerceChange3(idx(g));
end

w = 1;
for f = 1:3:length(ResultsC)*3
PerceChange(f,1) = out(w);
PerceChange(f+1,1) = out2(w);
PerceChange(f+2,1) = out3(w);
w=w+1;
end

mean(PerceChange1)
std(PerceChange1)
max(PerceChange1)
min(PerceChange1)

mean(PerceChange2)
std(PerceChange2)
max(PerceChange2)
min(PerceChange2)

mean(PerceChange3)
std(PerceChange3)
max(PerceChange3)
min(PerceChange3)

sum(PerceChange2>90)
sum(PerceChange3>90)

colorBar = PerceChange ./ 100; %the percentage change goes from 0-1, just like the color bar below (Blue)

Blue        = [0 0 255]/255;
jump        = linspace(1,Blue(1), 1000);
for w = 1:length(jump)
Blue(w,1:2) = jump(w);
Blue(w,3) = 1;
end
yellow = [247 252 185] / 255;
darkGreen = [49 163 84] / 255;
colors = [Blue;darkGreen;yellow];

% BScolor = colors(index(1:2:end),:);
% newScout = loadScout.Scouts;
% for d = 1:length(BScolor)
% newScout(d).Color = BScolor(d,:);    
% end
% save([SCOUTPATH,'scoutCont.mat'],'newScout');
w=1;
for t = 1:3:length(colorBar)
[c(w) index(w,1)] = min(abs(Blue(:,1)-colorBar(t))); %find the closest value for the signal loss on the color bar
w=w+1;
end

figure
hold on
w=1;
for k = 1:length(PerceChange) 
    h=bar(k,PerceChange(k,1));
    if     any(k == 1:3:length(PerceChange))
    set(h,'FaceColor',colors(index(w),:));
    w=w+1;
    elseif any(k == 2:3:length(PerceChange))
    set(h,'FaceColor',colors(end-1,:));
    else   any(k == 3:3:length(PerceChange))
    set(h,'FaceColor',colors(end,:));
    end
end
xticks(1:3:length(PerceChange))
xticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50'})
ylim([-5 100])
ylabel('Signal loss [%]', 'FontSize', 12,'FontWeight','bold')
xlabel('Patches on the cortex surface', 'FontSize', 12,'FontWeight','bold')
hold off
set(gcf,'color','w');
set(gca,'fontweight','bold','fontsize',12);
saveas(gcf,[PATHFIG,'Percentage_cap_cEEGrid_pair.png']);

figure
hold on
w=1;
for k = 1:length(PerceChange1) 
    h=bar(k,out(k,1));
    set(h,'FaceColor',colors(index(w),:));
    w=w+1;
end
xticks(1:length(PerceChange1))
xticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50'})
ylim([-5 100])
set(gca,'fontweight','bold','fontsize',10,'Color','w');
ylabel('Signal loss [%]', 'FontSize', 12,'FontWeight','bold')
xlabel('Patches on the cortex surface', 'FontSize', 12,'FontWeight','bold')
hold off
set(gcf,'color','k');
set(gca,'color','k');


figure
hold on
w=1;
for k = 1:length(PerceChange1) 
    h=bar(k,out2(k,1));
    set(h,'FaceColor',colors(end-1,:));
end
xticks(1:length(PerceChange1))
xticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50'})
ylim([-5 100])
ylabel('Signal loss [%]', 'FontSize', 12,'FontWeight','bold')
xlabel('Patches on the cortex surface', 'FontSize', 12,'FontWeight','bold')
hold off
set(gcf,'color','w');
set(gca,'fontweight','bold','fontsize',12);

figure
hold on
w=1;
for k = 1:length(PerceChange1) 
    h=bar(k,out3(k,1));
    set(h,'FaceColor',colors(end,:));
end
xticks(1:length(PerceChange1))
xticklabels({'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50'})
ylim([-5 100])
ylabel('Signal loss [%]', 'FontSize', 12,'FontWeight','bold')
xlabel('Patches on the cortex surface', 'FontSize', 12,'FontWeight','bold')
hold off
set(gcf,'color','w');
set(gca,'fontweight','bold','fontsize',12);

save([PATHFIG,'Percentage_cap_cEEGrid_pair.mat'],'ResultsP1','ResultsP2','ResultsC','ResultsF','PerceChange');

% listColor = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']);
% colorbrain = load([listColor(end).folder,'\',listColor(end).name]);
% test.ImageGridAmp = zeros(size(test.ImageGridAmp,1),1);
% for x = 1:length(PerceChange1)
%     test.ImageGridAmp(loadScout.Scouts(x).Vertices) = PerceChange1(x)/100;
% end
% 
% % colorbrain2 = load([listColor(end).folder,'\',listColor(end).name]);
% % colorbrain2.ImageGridAmp = zeros(size(colorbrain.ImageGridAmp,1),1);
% % for x = 1:length(PerceChange1)
% %     colorbrain2.ImageGridAmp(loadScout.Scouts(x).Vertices) = ResultsF(x).maxDiffAbs;
% % end
% % 
% ProtocolInfo = bst_get('ProtocolInfo');
% db_add(ProtocolInfo.iStudy, test, []);
