%% plot the results of PlotFactors.m
clear all
close all
clc

PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_cEEGrid\'; %store the results here
load([PATHOUT,'LocNOrient.mat'],'ResultsDir','ResultsLoc','ResultsDist');

yellow = [247 252 185] / 255;
darkGreen = [49 163 84] / 255;

colors = [yellow;darkGreen];

% for s = 1: length(ResultsDist)
% vc(1,s) = [abs(ResultsDist(s).FixPair(1)-ResultsDist(s).FixPair(3))];
% end
% 
% green       = [0 0.6 0.3];
% yellow       = [1 1 0];
% colors = [green;yellow];
% 
% figure, hold on
% m = length(colors);
% 
% for k = 1:3
% i = mod(k-1,2);
% i = i+1;
% bc = bar(k,vc(1,k));
% set(bc,'FaceColor',colors(k,1:3))    
% end
% xticks(1:3)
% xticklabels({'Low','Medium','High'})
% ylim([0 ceil(max(max(vc(1,:))))])
% ylabel('[a.u.]')
% xlabel('Distance')
% title('Difference amp. of E03 - E08 (vertical) at different distances','FontSize',10);
% %set(gcf, 'Position', get(0, 'ScreenSize'));
% %saveas(gcf,'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\PPTdrop.png');
 
%% plot for distance
for s = 1: length(ResultsDist)
vc(1:2,s) = [abs(ResultsDist(s).FixPair(1)-ResultsDist(s).FixPair(3)) abs(ResultsDist(s).FixPair(2)-ResultsDist(s).FixPair(4))];
end

m = length(colors);

figure
subplot(1,3,1)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
ylabel('[a.u.]')
xlabel('Low')
set(gcf,'color','w');
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,2)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k+2));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Medium')
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,3)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k+4));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('High')
set(gcf, 'Position', get(0, 'ScreenSize'));
set(gca,'fontweight','bold','fontsize',12);
saveas(gcf,'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\PPTdrop.png');


%% plot for orientation
for s = 1: length(ResultsDir)
vc(1:2,s) = [abs(ResultsDir(s).FixPair(1)-ResultsDir(s).FixPair(3)) abs(ResultsDir(s).FixPair(2)-ResultsDir(s).FixPair(4))];
end

m = length(colors);

figure
subplot(1,3,1)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
ylabel('[a.u.]')
xlabel('Norm')
set(gcf,'color','w');
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,2)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k+4));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Anterior')
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,3)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k+2));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Dorsal')
set(gca,'fontweight','bold','fontsize',12);
set(gcf, 'Position', get(0, 'ScreenSize'));
saveas(gcf,'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\PPTdrop.png');

%% plot for position
for s = 1: length(ResultsLoc)
vc(1:2,s) = [abs(ResultsLoc(s).FixPair(1)-ResultsLoc(s).FixPair(3)) abs(ResultsLoc(s).FixPair(2)-ResultsLoc(s).FixPair(4))];
end

m = length(colors);

figure
subplot(1,3,1)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k+2));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Central')
ylabel('[a.u.]')
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,2)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Anterior')
set(gcf,'color','w');
set(gca,'fontweight','bold','fontsize',12);
hold off
subplot(1,3,3)
hold on
for k = 1:2
i = mod(k-1,2);
i = i+1;
bc = bar(k,vc(k+4));
set(bc,'FaceColor',colors(k,1:3))    
end
xticks(1:2)
xticklabels({'Vertical','Horizontal'})
ylim([0 ceil(max(max(vc)))])
xlabel('Dorsal')
set(gca,'fontweight','bold','fontsize',12);
set(gcf, 'Position', get(0, 'ScreenSize'));
saveas(gcf,'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\PPTdrop.png');












