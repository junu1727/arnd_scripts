%% Analyse results and plot the mean amplitude of fullCap and cEEGrid for different settings
clear;
close all;
clc

SubjectNameFull = 's01_fullCap';
SubjectNameCEEGrid = 's02_cEEGrid'; 
fullOrROI = 'ROI'; %see results of ROI or fullBrain
    
FullCap = dir(['C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_fullCap\',fullOrROI,'\',SubjectNameFull,'\results_Full*']);
for s = 1:length(FullCap)
load([FullCap(s).folder,'\',FullCap(s).name]);
FullCapROI_mean(s) = mean(SourceGrid);
FullCapROI_std(s) = std(SourceGrid);
FullCapROI_max(s) = max(SourceGrid);
end

cEEGrid = dir(['C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_cEEGrid\',fullOrROI,'\',SubjectNameCEEGrid,'\results*']);
for s = 1:length(cEEGrid)
load([cEEGrid(s).folder,'\',cEEGrid(s).name]);
cEEGridROI_mean(s) = mean(SourceGrid);
cEEGridROI_std(s) = std(SourceGrid);
cEEGridROI_max(s) = max(SourceGrid);
end
    
combo_mean = [FullCapROI_mean, cEEGridROI_mean(1), cEEGridROI_mean(2), cEEGridROI_mean(3)];

hb = bar([1 2 3 4],combo_mean);
hb(1).FaceColor = 'c';
xticks([1 2 3 4])
xticklabels({['FullCap',' max: ',num2str(FullCapROI_max(1))],['Double cEEGrid',' max: ',num2str(cEEGridROI_max(1))],...
             ['Left cEEGrid',' max: ',num2str(cEEGridROI_max(2))],['Right cEEGrid',' max: ',num2str(cEEGridROI_max(3))]})
title(['Mean amplitudes per setting: ',fullOrROI],'FontSize',24)
text(1:length(combo_mean),combo_mean,num2str(combo_mean'),'vert','bottom','horiz','center'); 
box off
ylabel('Amplitude [mikroVolt]')
hold on
%errorbar(combo_mean,combo_std ,'.') %not very telling
legend('mean Amplitude')
set(gcf, 'Position', get(0, 'Screensize')); 

% savefig(['O:\arm_testing\Experiments\EEG_brainstorm\data\arm_split\GndAvg\charts_GndAvg\All_settings',filesaveComp,'_amplitude.fig']);
