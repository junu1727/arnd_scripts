%% Simulate recordings. Results from PreapreSource_cEEGrid
clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's06_channel128'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_Diff\'; %store the results here
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = 64ChanCap (specify if the sensitivity of the fullCap or the cEEGrid should be calculated)
fullOrROI = '0'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '0'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
atlas = 'X'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux   X = Picks 
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for s = 1:length(cEEGrid)
    
    ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI
    
    if s == 1
        ref = 80; %index of the reference electrode for cEEGrid (in CM, E16)
        nameResult = 'cEEGrid';
        if EarChannel == '1'
            channels=[65 66 67 68 69 70 71 72 73 74]; %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
            nameVar = 'Left';
        elseif EarChannel == '2'
            channels=[75 76 77 78 79 80 81 82 83 84]; %...and ends on the right ear, lower end
            nameVar = 'Right';
        else EarChannel == '0'
            channels=[65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84];
            nameVar = 'Double';
        end
        
    else s == 2
        ref = [16 64]; %index of the reference electrode for 64ChanCap (in CM, E053)
        nameResult = 'fullCap';
        channels=[1:64];
    end
    
    listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
    listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
    listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);
    
    Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
    CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
    iChannels = 1:length(channels); %number of channels according to your channel file
    HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
    HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
    
    Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
    Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
    
    Source.Time = 1;
    Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
    SourceDist = Source; % source variable to work in
    SourceSimple = Source;
    IMAGEGRIDAMP=Source.ImageGridAmp; %This is the source/scout. It is currently only zeros
    
    %calculate the sensitivity for a source (ROI from atlas) for the cEEGrids or fullCap
    
    sFiles = [];
    sFiles = {...
        [listSource(1).folder,'\',listSource(1).name]};
    
    % Start a new report
    bst_report('Start', sFiles);
    sProcess = bst_process('GetInputStruct', sFiles);
    [sSubject,iSubject] = bst_get('Subject', sProcess.SubjectFile); %gets the subject ID
    
    if atlas == '0' %currently, 3 different atlases are available
        
        % Process: Scouts time series: [148 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Destrieux', {'G_Ins_lg_and_S_cent_ins L', 'G_Ins_lg_and_S_cent_ins R', 'G_and_S_cingul-Ant L', 'G_and_S_cingul-Ant R', 'G_and_S_cingul-Mid-Ant L', 'G_and_S_cingul-Mid-Ant R', 'G_and_S_cingul-Mid-Post L', 'G_and_S_cingul-Mid-Post R', 'G_and_S_frontomargin L', 'G_and_S_frontomargin R', 'G_and_S_occipital_inf L', 'G_and_S_occipital_inf R', 'G_and_S_paracentral L', 'G_and_S_paracentral R', 'G_and_S_subcentral L', 'G_and_S_subcentral R', 'G_and_S_transv_frontopol L', 'G_and_S_transv_frontopol R', 'G_cingul-Post-dorsal L', 'G_cingul-Post-dorsal R', 'G_cingul-Post-ventral L', 'G_cingul-Post-ventral R', 'G_cuneus L', 'G_cuneus R', 'G_front_inf-Opercular L', 'G_front_inf-Opercular R', 'G_front_inf-Orbital L', 'G_front_inf-Orbital R', 'G_front_inf-Triangul L', 'G_front_inf-Triangul R', 'G_front_middle L', 'G_front_middle R', 'G_front_sup L', 'G_front_sup R', 'G_insular_short L', 'G_insular_short R', 'G_oc-temp_lat-fusifor L', 'G_oc-temp_lat-fusifor R', 'G_oc-temp_med-Lingual L', 'G_oc-temp_med-Lingual R', 'G_oc-temp_med-Parahip L', 'G_oc-temp_med-Parahip R', 'G_occipital_middle L', 'G_occipital_middle R', 'G_occipital_sup L', 'G_occipital_sup R', 'G_orbital L', 'G_orbital R', 'G_pariet_inf-Angular L', 'G_pariet_inf-Angular R', 'G_pariet_inf-Supramar L', 'G_pariet_inf-Supramar R', 'G_parietal_sup L', 'G_parietal_sup R', 'G_postcentral L', 'G_postcentral R', 'G_precentral L', 'G_precentral R', 'G_precuneus L', 'G_precuneus R', 'G_rectus L', 'G_rectus R', 'G_subcallosal L', 'G_subcallosal R', 'G_temp_sup-G_T_transv L', 'G_temp_sup-G_T_transv R', 'G_temp_sup-Lateral L', 'G_temp_sup-Lateral R', 'G_temp_sup-Plan_polar L', 'G_temp_sup-Plan_polar R', 'G_temp_sup-Plan_tempo L', 'G_temp_sup-Plan_tempo R', 'G_temporal_inf L', 'G_temporal_inf R', 'G_temporal_middle L', 'G_temporal_middle R', 'Lat_Fis-ant-Horizont L', 'Lat_Fis-ant-Horizont R', 'Lat_Fis-ant-Vertical L', 'Lat_Fis-ant-Vertical R', 'Lat_Fis-post L', 'Lat_Fis-post R', 'Pole_occipital L', 'Pole_occipital R', 'Pole_temporal L', 'Pole_temporal R', 'S_calcarine L', 'S_calcarine R', 'S_central L', 'S_central R', 'S_cingul-Marginalis L', 'S_cingul-Marginalis R', 'S_circular_insula_ant L', 'S_circular_insula_ant R', 'S_circular_insula_inf L', 'S_circular_insula_inf R', 'S_circular_insula_sup L', 'S_circular_insula_sup R', 'S_collat_transv_ant L', 'S_collat_transv_ant R', 'S_collat_transv_post L', 'S_collat_transv_post R', 'S_front_inf L', 'S_front_inf R', 'S_front_middle L', 'S_front_middle R', 'S_front_sup L', 'S_front_sup R', 'S_interm_prim-Jensen L', 'S_interm_prim-Jensen R', 'S_intrapariet_and_P_trans L', 'S_intrapariet_and_P_trans R', 'S_oc-temp_lat L', 'S_oc-temp_lat R', 'S_oc-temp_med_and_Lingual L', 'S_oc-temp_med_and_Lingual R', 'S_oc_middle_and_Lunatus L', 'S_oc_middle_and_Lunatus R', 'S_oc_sup_and_transversal L', 'S_oc_sup_and_transversal R', 'S_occipital_ant L', 'S_occipital_ant R', 'S_orbital-H_Shaped L', 'S_orbital-H_Shaped R', 'S_orbital_lateral L', 'S_orbital_lateral R', 'S_orbital_med-olfact L', 'S_orbital_med-olfact R', 'S_parieto_occipital L', 'S_parieto_occipital R', 'S_pericallosal L', 'S_pericallosal R', 'S_postcentral L', 'S_postcentral R', 'S_precentral-inf-part L', 'S_precentral-inf-part R', 'S_precentral-sup-part L', 'S_precentral-sup-part R', 'S_suborbital L', 'S_suborbital R', 'S_subparietal L', 'S_subparietal R', 'S_temporal_inf L', 'S_temporal_inf R', 'S_temporal_sup L', 'S_temporal_sup R', 'S_temporal_transverse L', 'S_temporal_transverse R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Destrieux';
    elseif atlas == '1'
        % Process: Scouts time series: [24 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Brodmann-thresh', {'BA1 L', 'BA1 R', 'BA2 L', 'BA2 R', 'BA3a L', 'BA3a R', 'BA3b L', 'BA3b R', 'BA44 L', 'BA44 R', 'BA45 L', 'BA45 R', 'BA4a L', 'BA4a R', 'BA4p L', 'BA4p R', 'BA6 L', 'BA6 R', 'MT L', 'MT R', 'V1 L', 'V1 R', 'V2 L', 'V2 R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Brodmann-thresh';
        
    elseif atlas == '2'
        % Process: Scouts time series: [68 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [1, 1], ...
            'scouts',         {'Desikan-Killiany', {'bankssts L', 'bankssts R', 'caudalanteriorcingulate L', 'caudalanteriorcingulate R', 'caudalmiddlefrontal L', 'caudalmiddlefrontal R', 'cuneus L', 'cuneus R', 'entorhinal L', 'entorhinal R', 'frontalpole L', 'frontalpole R', 'fusiform L', 'fusiform R', 'inferiorparietal L', 'inferiorparietal R', 'inferiortemporal L', 'inferiortemporal R', 'insula L', 'insula R', 'isthmuscingulate L', 'isthmuscingulate R', 'lateraloccipital L', 'lateraloccipital R', 'lateralorbitofrontal L', 'lateralorbitofrontal R', 'lingual L', 'lingual R', 'medialorbitofrontal L', 'medialorbitofrontal R', 'middletemporal L', 'middletemporal R', 'paracentral L', 'paracentral R', 'parahippocampal L', 'parahippocampal R', 'parsopercularis L', 'parsopercularis R', 'parsorbitalis L', 'parsorbitalis R', 'parstriangularis L', 'parstriangularis R', 'pericalcarine L', 'pericalcarine R', 'postcentral L', 'postcentral R', 'posteriorcingulate L', 'posteriorcingulate R', 'precentral L', 'precentral R', 'precuneus L', 'precuneus R', 'rostralanteriorcingulate L', 'rostralanteriorcingulate R', 'rostralmiddlefrontal L', 'rostralmiddlefrontal R', 'superiorfrontal L', 'superiorfrontal R', 'superiorparietal L', 'superiorparietal R', 'superiortemporal L', 'superiortemporal R', 'supramarginal L', 'supramarginal R', 'temporalpole L', 'temporalpole R', 'transversetemporal L', 'transversetemporal R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Desikan-Killiany';
        
    else atlas == 'X'
       load('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scoutsDestrieux30Percent.mat');
        %self-picked from Destrieux, with overlap between scout and region
        %detectable with cEEGrids at x percent
            sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Destrieux', {'G_Ins_lg_and_S_cent_ins L', 'G_Ins_lg_and_S_cent_ins R', 'G_and_S_cingul-Ant L', 'G_and_S_cingul-Ant R', 'G_and_S_cingul-Mid-Ant L', 'G_and_S_cingul-Mid-Ant R', 'G_and_S_cingul-Mid-Post L', 'G_and_S_cingul-Mid-Post R', 'G_and_S_frontomargin L', 'G_and_S_frontomargin R', 'G_and_S_occipital_inf L', 'G_and_S_occipital_inf R', 'G_and_S_paracentral L', 'G_and_S_paracentral R', 'G_and_S_subcentral L', 'G_and_S_subcentral R', 'G_and_S_transv_frontopol L', 'G_and_S_transv_frontopol R', 'G_cingul-Post-dorsal L', 'G_cingul-Post-dorsal R', 'G_cingul-Post-ventral L', 'G_cingul-Post-ventral R', 'G_cuneus L', 'G_cuneus R', 'G_front_inf-Opercular L', 'G_front_inf-Opercular R', 'G_front_inf-Orbital L', 'G_front_inf-Orbital R', 'G_front_inf-Triangul L', 'G_front_inf-Triangul R', 'G_front_middle L', 'G_front_middle R', 'G_front_sup L', 'G_front_sup R', 'G_insular_short L', 'G_insular_short R', 'G_oc-temp_lat-fusifor L', 'G_oc-temp_lat-fusifor R', 'G_oc-temp_med-Lingual L', 'G_oc-temp_med-Lingual R', 'G_oc-temp_med-Parahip L', 'G_oc-temp_med-Parahip R', 'G_occipital_middle L', 'G_occipital_middle R', 'G_occipital_sup L', 'G_occipital_sup R', 'G_orbital L', 'G_orbital R', 'G_pariet_inf-Angular L', 'G_pariet_inf-Angular R', 'G_pariet_inf-Supramar L', 'G_pariet_inf-Supramar R', 'G_parietal_sup L', 'G_parietal_sup R', 'G_postcentral L', 'G_postcentral R', 'G_precentral L', 'G_precentral R', 'G_precuneus L', 'G_precuneus R', 'G_rectus L', 'G_rectus R', 'G_subcallosal L', 'G_subcallosal R', 'G_temp_sup-G_T_transv L', 'G_temp_sup-G_T_transv R', 'G_temp_sup-Lateral L', 'G_temp_sup-Lateral R', 'G_temp_sup-Plan_polar L', 'G_temp_sup-Plan_polar R', 'G_temp_sup-Plan_tempo L', 'G_temp_sup-Plan_tempo R', 'G_temporal_inf L', 'G_temporal_inf R', 'G_temporal_middle L', 'G_temporal_middle R', 'Lat_Fis-ant-Horizont L', 'Lat_Fis-ant-Horizont R', 'Lat_Fis-ant-Vertical L', 'Lat_Fis-ant-Vertical R', 'Lat_Fis-post L', 'Lat_Fis-post R', 'Pole_occipital L', 'Pole_occipital R', 'Pole_temporal L', 'Pole_temporal R', 'S_calcarine L', 'S_calcarine R', 'S_central L', 'S_central R', 'S_cingul-Marginalis L', 'S_cingul-Marginalis R', 'S_circular_insula_ant L', 'S_circular_insula_ant R', 'S_circular_insula_inf L', 'S_circular_insula_inf R', 'S_circular_insula_sup L', 'S_circular_insula_sup R', 'S_collat_transv_ant L', 'S_collat_transv_ant R', 'S_collat_transv_post L', 'S_collat_transv_post R', 'S_front_inf L', 'S_front_inf R', 'S_front_middle L', 'S_front_middle R', 'S_front_sup L', 'S_front_sup R', 'S_interm_prim-Jensen L', 'S_interm_prim-Jensen R', 'S_intrapariet_and_P_trans L', 'S_intrapariet_and_P_trans R', 'S_oc-temp_lat L', 'S_oc-temp_lat R', 'S_oc-temp_med_and_Lingual L', 'S_oc-temp_med_and_Lingual R', 'S_oc_middle_and_Lunatus L', 'S_oc_middle_and_Lunatus R', 'S_oc_sup_and_transversal L', 'S_oc_sup_and_transversal R', 'S_occipital_ant L', 'S_occipital_ant R', 'S_orbital-H_Shaped L', 'S_orbital-H_Shaped R', 'S_orbital_lateral L', 'S_orbital_lateral R', 'S_orbital_med-olfact L', 'S_orbital_med-olfact R', 'S_parieto_occipital L', 'S_parieto_occipital R', 'S_pericallosal L', 'S_pericallosal R', 'S_postcentral L', 'S_postcentral R', 'S_precentral-inf-part L', 'S_precentral-inf-part R', 'S_precentral-sup-part L', 'S_precentral-sup-part R', 'S_suborbital L', 'S_suborbital R', 'S_subparietal L', 'S_subparietal R', 'S_temporal_inf L', 'S_temporal_inf R', 'S_temporal_sup L', 'S_temporal_sup R', 'S_temporal_transverse L', 'S_temporal_transverse R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Destrieux';    
    end
    
    listScout = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*\matrix_scout*']);
    AtlasROI = load([listScout(end).folder,'\',listScout(end).name]); %always load newest scouts
    
    if s == 1
        prompt = {'Enter desired ROI:'}; %the number of the desired ROI must be looked up in AtlasROI.Atlas.Scouts
        dlgtitle = nameAtlas;
        dims = [1 50];
        g = inputdlg(prompt,dlgtitle,dims);
        ROI=str2num(g{1,1}); %turn the entered strings into an array of numbers
    end
    
    TakeScout = AtlasROI.Atlas.Scouts(ROI(1)).Vertices; %store the index of all vertices in the ROI in TakeScout
    
    if length(ROI) > 1 %if more than one region was chosen, add the additional vertices to TakeScout
        for f = 2:length(ROI)
            TakeScout(end+1:end+length(AtlasROI.Atlas.Scouts(ROI(f)).Vertices)) = AtlasROI.Atlas.Scouts(ROI(f)).Vertices; %vertices of the current scout
        end
    end
    
    %%%%
    IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
    IMAGEGRIDAMP(TakeScout,1)= 1; % set activity of one vertex to 1, the others stay 0
    SourceSimple.ImageGridAmp = IMAGEGRIDAMP;
    
    SourceSimple.Comment= [nameResult,nameVar,'GridUNIFORM, ROI = ',nameAtlas,' ',num2str(ROI)]; %set the name referring to the chosen grid & atlas
    
    SourceSimple.ImageGridAmp = abs(SourceSimple.ImageGridAmp); %take only absolute values
    [a,b] = maxk(SourceSimple.ImageGridAmp(:,1),100); %find the 100 highest values
    c = find(a > 250); %find unrealisticly high vlaues
    SourceSimple.ImageGridAmp(b(c),1)=0; % delete them
    
    ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
    db_add(ProtocolInfo.iStudy, SourceSimple, []);
    
    F = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
    F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
    G = zeros(1,1); %preallocate the gain of the ref
    G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
    G = mean(G);
    %IChannels is just a list from 1 to the number of used channels.
    %channels is the index of the specified channels in gain
    
    collect=[];
    index=1;
    
    for ch=1:length(channels)-1
        for b=ch+1:length(channels)
            collect(index)=(F(iChannels(ch),1)- G)- (F(iChannels(b),1)-G); %calc every combination of electrodes
            checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
            checkPair(index,2) = {CM.Channel(channels(ch)).Name};
            checkPair(index,3) = {CM.Channel(channels(b)).Name};
            SelectedValues(index,1) = F(iChannels(ch),1)- G;
            SelectedValues(index,2) = F(iChannels(b),1)- G;
            index=index+1;
        end
    end
    
    if s == 1
        SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    else s == 2
        SelectedChannels(end+1:end+2) = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    end
    
    if s == 1
        ResultsC.maxDiff =  max(collect);
        ResultsC.CheckPair = checkPair;
        ResultsC.CheckPair(:,4) = {'0'};
        ResultsC.CheckPair(collect==max(collect),4) = {'1'};
        ResultsC.Pair = checkPair(collect==max(collect),1);
        ResultsC.SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
        ResultsC.SelectedValues = SelectedValues;
        ResultsC.SelectedValues(:,3) = 0;
        ResultsC.SelectedValues(collect==max(collect),3) = 1;
    else s == 2
        ResultsF.maxDiff =  max(collect);
        ResultsF.CheckPair = checkPair;
        ResultsF.CheckPair(:,4) = {'0'};
        ResultsF.CheckPair(collect==max(collect),4) = {'1'};
        ResultsF.Pair = checkPair(collect==max(collect),1);
        ResultsF.SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
        ResultsF.SelectedValues = SelectedValues;
        ResultsF.SelectedValues(:,3) = 0;
        ResultsF.SelectedValues(collect==max(collect),3) = 1;
    end
    
    listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw84Chan\results_19*']);
    newDataFile = bst_simulation([listResults(end).folder,'\',listResults(end).name]);
    
    listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw84Chan\data_simulation*']);
    
    if s == 2
        export_matlab([listSimulation(end).folder,'\',listSimulation(end).name],'simulation');
        simulation.ChannelFlag(channels) = -1; %determine which channels are treated as bad. This way, they will not be part of the plotting
        ProtocolInfo = bst_get('ProtocolInfo');
        db_add(ProtocolInfo.iStudy, simulation, []);
        
        listDataSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw84Chan\data_1*']);
        
        view_topography([listDataSimulation(end).folder,'\',listDataSimulation(end).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
        bst_figures('SetSelectedRows', SelectedChannels); %display the selected channels
        figure_3d('ViewSensors', gcf, 1, 0);
        figure_3d('SetStandardView', gcf, {'left','right'});
        view_topography([listSimulation(end).folder,'\',listSimulation(end).name], 'EEG', '2DSensorCap'); %view the averaged signal of the subject
        bst_figures('SetSelectedRows', SelectedChannels);
        figure_3d('ViewSensors', gcf, 1, 1); %specify that electrode-locations are shown, but their names are not
        %view_topography('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s04_comp\raw84Chan\results_191015_1707.mat', 'EEG', '3DElectrodes-Cortex'); %view the averaged signal of the subject
        %view_connect('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s04_comp\raw84Chan\results_191015_1707.mat', 'Image');
    end
    
    %clearvars -except iSubject SubjectName atlas sProcess cEEGrid PATHOUT MAINPATH PROTOCOLNAME nameVar SelectedChannels fullOrROI EarChannel nameAtlas ROI ResultsC ResultsF;
    
end

figure
combo_Diff = [ResultsF.maxDiff ResultsC.maxDiff];
PerceChange = 100 - (100 / ResultsF.maxDiff) * ResultsC.maxDiff;
bar([1 2],combo_Diff)
xticks([1 2])
xticklabels({'FullCap','cEEGrid'})
text(1:2,combo_Diff,num2str(combo_Diff'),'vert','bottom','horiz','center');
title(['Signal Difference. Signal-loss from F to C = ',num2str(PerceChange),'%'],'FontSize',10);
set(gcf, 'Position',[450,35, 760, 410]);

save([PATHOUT,'Diff_',nameVar,'_',nameAtlas,'_',num2str(ROI)],'ResultsC','ResultsF');

disp('Press a key !')  % Press a key here.You can see the message 'Paused: Press any key' in        % the lower left corner of MATLAB window.
pause;
close all

%% take care of the files
listDelete =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw84Chan\matrix_scout*']);
listDelete2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw84Chan\results_1*']); %this needs to be smoother, in 2020 this won't work anymore
listDelete3 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw84Chan\data_simulation*']);

for w = 1:length(listDelete)
    isDeleted = file_delete( [listDelete(w).folder,'\',listDelete(w).name], 1, -1);
    isDeleted = file_delete( [listDelete2(w).folder,'\',listDelete2(w).name], 1, -1);
    isDeleted = file_delete( [listDelete3(w).folder,'\',listDelete3(w).name], 1, -1);
end
    isDeleted = file_delete( [listDataSimulation(end).folder,'\',listDataSimulation(end).name], 1, -1);

db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI