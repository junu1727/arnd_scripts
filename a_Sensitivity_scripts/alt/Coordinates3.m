clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's06_cEEGrid128'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
ClusterSize = 5;
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%load the predefined clustering and only use those with a length of precisely 20
SurfaceClustering = load('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_Surface_clustering_5V.mat');
SurfaceClustering = SurfaceClustering.Scouts;
lengths = arrayfun(@(x) size(SurfaceClustering(x).Vertices,2), 1:numel(SurfaceClustering));
longest = lengths > ClusterSize;
longest = find(longest == 1);
for r = 1:length(longest)
    SurfaceClustering(longest(r)).Vertices(21:end) = [];
    lengths(longest(r)) = ClusterSize;
end
keepClusters = SurfaceClustering(lengths == ClusterSize);
%load the headmodel and channel file of the subject
listScalp = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*remesh*']);
export_matlab([listScalp(end).folder,'\',listScalp(end).name],'remeshScalp');
listCM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']);
listHM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*low_warped*']); %raw s.t. starts with R or r
CM = load([listCM(end).folder,'\',listCM(end).name]);
HM = load([listHM(end).folder,'\',listHM(end).name]);

%load the sensitivity map for each cEEGrid electrode, put them in a structure as the
%dependant variable, together with all predictors
listSrc = dir('C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_cEEGrid\s06_cEEGrid128\*AllElec*');
load([listSrc(1).folder,'\',listSrc(1).name]);

for s = 1:length(keepClusters) %check the clusters for values that are unrealisticly high. The values are from AmpAllElecs (b&c)
    ak = any(keepClusters(s).Vertices == b(c)) == 1;
    if    any(ak)==1
        x(s) = 1;
    else
        x(s) = 0;
    end
end
keepClusters(x==1)=[];

%now, calc the mean curvature of every ROI
cur = tess_curvature(HM.Vertices, HM.VertConn, HM.VertNormals, 10, 1);
for s = 1:length(keepClusters)
    maxminCur(s,1) = max(cur(keepClusters(s).Vertices));
    maxminCur(s,2) = min(cur(keepClusters(s).Vertices));
    absCur(s,1) = mean([maxminCur(s,1) abs(maxminCur(s,2))]);
end
keepClusters = keepClusters(absCur<0.5); %take only ROIs with an average curvature below 0.5 (arbitrary)
    
cr = 1;
for t = 129:138 %create the distance from every electrode to every patch from the atlas
    
    LeftChan = CM.Channel(t).Loc;
    RightChan = CM.Channel(t+10).Loc;
    
    %create the distance between these points
    for st = 1: length([keepClusters.Seed])
        checkr(st,cr) = norm(HM.Vertices([keepClusters(st).Seed],1:3) - RightChan);
        checkl(st,cr) = norm(HM.Vertices([keepClusters(st).Seed],1:3) - LeftChan);
    end

    %create distance for each coordinate individually (both cEEGrids)
    for s = 1: length([keepClusters.Seed])
        xcheckl(s,cr) = norm(HM.Vertices([keepClusters(s).Seed],1) - LeftChan(1));
        ycheckl(s,cr) = norm(HM.Vertices([keepClusters(s).Seed],2) - LeftChan(2));
        zcheckl(s,cr) = norm(HM.Vertices([keepClusters(s).Seed],3) - LeftChan(3));
        xcheckr(s,cr) = norm(HM.Vertices([keepClusters(s).Seed],1) - RightChan(1));
        ycheckr(s,cr) = norm(HM.Vertices([keepClusters(s).Seed],2) - RightChan(2));
        zcheckr(s,cr) = norm(HM.Vertices([keepClusters(s).Seed],3) - RightChan(3));
    end
        cr = cr+1;

end

% %finally, take the cEEGrid that is closer to every respective ROI
% for s = 1: length([keepClusters.Seed])
%     checkEnd(s,1) = max(check(s,1:2));
% end

%find the vertices on the scalp mesh that are closest to the cEEGrids
d = 1;
for s = 129:138
    Ceelocsl(d,1:3) = CM.Channel(s).Loc;
    Ceelocsr(d,1:3) = CM.Channel(s+10).Loc;
    d=d+1;
end
    Ceelocs3 = CM.Channel(60).Loc'; %Fpz

[l distl] = dsearchn(remeshScalp.Vertices,Ceelocsl); %vertices l closest to the cEEGrids
[r distr] = dsearchn(remeshScalp.Vertices,Ceelocsr); %vertices r closest to the cEEGrids
[three dist3] = dsearchn(remeshScalp.Vertices,Ceelocs3); %vertices three closest to the cEEGrids

Cnearl = remeshScalp.Vertices(l,1:3); %these are the new cEEgrid normal vertices
Cnearr = remeshScalp.Vertices(r,1:3); %these are the new cEEgrid normal vertices
Cnear3 = remeshScalp.Vertices(three,1:3); %these are the new cEEgrid normal vertices

meanNormCl = mean(remeshScalp.VertNormals(l,1:3)); %calc the normal vector of the plane
NormCl = remeshScalp.VertNormals(l,1:3);%the normal vector for each electrode
w=1; %calc the orthogonals for every normal vector of every electrode
for s = 1:length(NormCl)
d = Cnearl(s,1:3); %cEEgrid point
e = NormCl(s,1:3); % normal vector of the cEEGrid plane
f = d+e; % end position of normal vector
M(:,w:w+2) = [f; null(f(:).')']; %matrix with the normal vectors of the cEEgrid electrodes plus the two orthogonal vectors for each
w=w+3;
end

%find the normal vector for every ROI
for x = 1: length(keepClusters)
    HMVertnormals = HM.VertNormals([keepClusters(x).Vertices],1:3); %the normal vector of every ROI is the mean of every vertex within the ROI
    meanNormROI(x,1) = mean(HMVertnormals(:,1));
    meanNormROI(x,2) = mean(HMVertnormals(:,2));
    meanNormROI(x,3) = mean(HMVertnormals(:,3));
    HMVertnormals = [];
end
 
% %take the normal vector of each ROI and find its angle to the normal vector
% %of every cEEGrid electrode
% cEEGridAngleL = elecNorml;
% cEEGridAngleR = elecNormr;
% cEEGridAngle3 = repmat(elecNorm3,10,1);

ClusterAngle = meanNormROI;
CosThetaL = zeros(length(meanNormROI)*length(Cnearl),size(M,1));
CosThetaR = zeros(length(meanNormROI)*length(Cnearl),size(M,1));
ThetaInDegreesL = zeros(length(meanNormROI)*length(Cnearl),size(M,1));
ThetaInDegreesR = zeros(length(meanNormROI)*length(Cnearr),size(M,1));
t=0;
for o = 1:3:length(M)
    for x = 1:size(M,1)
        for y = 1:length(meanNormROI)
            CosThetaL(y+t,x) = dot(M(x,o:o+2),ClusterAngle(y,:))/(norm(M(x,o:o+2))*norm(ClusterAngle(y,:)));
            ThetaInDegreesL(y+t,x) = acosd(CosThetaL(y+t,x));
%             CosThetaR(y+t,x) = dot(M(x,o:o+2),ClusterAngle(y,:))/(norm(M(x,o:o+2))*norm(ClusterAngle(y,:)));
%             ThetaInDegreesR(y+t,x) = acosd(CosThetaR(y+t,x));
        end
    end
            t=t+length(meanNormROI);
end

categ = 5:5:180;
T= ThetaInDegreesL;
categDegree = zeros(length(T),size(T,2));
for i = 1:size(T,2)
    for s = 1:length(T)
        if T(s,i) < categ(1) || T(s,i) > categ(end-1)
            categDegree(s,i)  = 1; elseif T(s,i) > categ(1) && T(s,i) <= categ(2) || T(s,i) > categ(end-2) && T(s,i) <= categ(end-1)
            categDegree(s,i)  = 2; elseif T(s,i) > categ(2) && T(s,i) <= categ(3) || T(s,i) > categ(end-3) && T(s,i) <= categ(end-2)
            categDegree(s,i)  = 3;elseif T(s,i) > categ(3) && T(s,i) <= categ(4) || T(s,i) > categ(end-4) && T(s,i) <= categ(end-3)
            categDegree(s,i)  = 4;elseif T(s,i) > categ(4) && T(s,i) <= categ(5) || T(s,i) > categ(end-5) && T(s,i) <= categ(end-4)
            categDegree(s,i)  = 5;elseif T(s,i) > categ(5) && T(s,i) <= categ(6) || T(s,i) > categ(end-6) && T(s,i) <= categ(end-5)
            categDegree(s,i)  = 6;elseif T(s,i) > categ(6) && T(s,i) <= categ(7) || T(s,i) > categ(end-7) && T(s,i) <= categ(end-6)
            categDegree(s,i)  = 7;elseif T(s,i) > categ(7) && T(s,i) <= categ(8) || T(s,i) > categ(end-8) && T(s,i) <= categ(end-7)
            categDegree(s,i)  = 8;elseif T(s,i) > categ(8) && T(s,i) <= categ(9) || T(s,i) > categ(end-9) && T(s,i) <= categ(end-8)
            categDegree(s,i)  = 9;elseif T(s,i) > categ(9) && T(s,i) <= categ(10) || T(s,i) > categ(end-10) && T(s,i) <= categ(end-9)
            categDegree(s,i)  = 10;elseif T(s,i) > categ(10) && T(s,i) <= categ(11) || T(s,i) > categ(end-11) && T(s,i) <= categ(end-10)
            categDegree(s,i)  = 11;elseif T(s,i) > categ(11) && T(s,i) <= categ(12) || T(s,i) > categ(end-12) && T(s,i) <= categ(end-11)
            categDegree(s,i)  = 12;elseif T(s,i) > categ(12) && T(s,i) <= categ(13) || T(s,i) > categ(end-13) && T(s,i) <= categ(end-12)
            categDegree(s,i)  = 13;elseif T(s,i) > categ(13) && T(s,i) <= categ(14) || T(s,i) > categ(end-14) && T(s,i) <= categ(end-13)
            categDegree(s,i)  = 14;elseif T(s,i) > categ(14) && T(s,i) <= categ(15) || T(s,i) > categ(end-15) && T(s,i) <= categ(end-14)
            categDegree(s,i)  = 15;elseif T(s,i) > categ(15) && T(s,i) <= categ(16) || T(s,i) > categ(end-16) && T(s,i) <= categ(end-15)
            categDegree(s,i)  = 16;elseif T(s,i) > categ(16) && T(s,i) <= categ(17) || T(s,i) > categ(end-17) && T(s,i) <= categ(end-16)
            categDegree(s,i)  = 17;elseif T(s,i) > categ(17) && T(s,i) <= categ(18) || T(s,i) > categ(end-18) && T(s,i) <= categ(end-17)
            categDegree(s,i)  = 18;
        end
    end
end
%ThetaInDegrees is now a matrix containing the angles between ROI(~3000) per 
%electrode(10) and all three normal vectors of every electrode (3)
%[(3000*10) 3]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate for every electrode the mean signal that is measured from each patch
for x = 1:length(Ceelocsl)
    for s = 1:length(keepClusters)
        SrcStrength(s,x) = mean(abs(SourceDist.ImageGridAmp(keepClusters(s).Vertices,x)));
    end
end

%write all predictors (including the dummy predictor for the electrodes) and
%the explanatory variable in one struct
GLM.AngleNorm = categDegree(:,1); %this is the angle from the norm vector to the cEEGrid plane
GLM.Angle2 = categDegree(:,2); %this one is orthogonal to the norm and goes in the posterior direction (~)
GLM.Angle3 = categDegree(:,3); %this one is orthogonal to the norm and goes in the dorsal direction (~)
GLM.XDist = reshape(xcheckl,[],1);
GLM.YDist = reshape(ycheckl,[],1);
GLM.ZDist = reshape(zcheckl,[],1);
GLM.SrcStrength = reshape(abs(log(SrcStrength)),[],1); %after log-transformation, the srcstrength looks relatively normally distributed
GLM.Elec = [1:length(GLM.SrcStrength)]';
g = 1;
v = length(xcheckl);
for d = 1:length(Ceelocsl)
GLM.Elec(g:g+v-1) = d;
g=g+v;
end

%save them in various formats for R (still testing)
xlswrite('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeftAllElec.xlsx','GLM');
writetable(struct2table(GLM), 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeftAllElec.xlsx')
save('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeftAllElec.mat','GLM');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[rho,pval] = corr(GLM.SrcStrength(1:2862,1),GLM.XDist(1:2862,1))
figure
scatter(GLM.XDist(1:2862,1), GLM.SrcStrength(1:2862,1))
title(['XDist. Corr = ',num2str(rho)])
xlabel('XDist')
ylabel('SourceStrength')
lsline

[rho,pval] = corr(GLM.SrcStrength(1:2862,1),GLM.YDist(1:2862,1))
figure
scatter(GLM.YDist(1:2862,1), GLM.SrcStrength(1:2862,1))
title(['YDist. Corr = ',num2str(rho)])
xlabel('YDist')
ylabel('SourceStrength')
lsline

[rho,pval] = corr(GLM.SrcStrength(1:2862,1),GLM.ZDist(1:2862,1))
figure
scatter(GLM.ZDist(1:2862,1), GLM.SrcStrength(1:2862,1))
title(['ZDist. Corr = ',num2str(rho)])
xlabel('ZDist')
ylabel('SourceStrength')
lsline

[rho,pval] = corr(GLM.SrcStrength(1:2862,1),GLM.AngleNorm(1:2862,1))
figure
scatter(GLM.AngleNorm(1:2862,1), GLM.SrcStrength(1:2862,1))
title(['AngleNorm. Corr = ',num2str(rho)])
xlabel('AngleNorm')
ylabel('SourceStrength')
lsline

[rho,pval] = corr(GLM.SrcStrength(1:2862,1),GLM.Angle2(1:2862,1))
figure
scatter(GLM.Angle2(1:2862,1), GLM.SrcStrength(1:2862,1))
title(['Angle2. Corr = ',num2str(rho)])
xlabel('Angle2')
ylabel('SourceStrength')
lsline

[rho,pval] = corr(GLM.SrcStrength(1:2862,1),GLM.Angle3(1:2862,1))
figure
scatter(GLM.Angle3(1:2862,1), GLM.SrcStrength(1:2862,1))
title(['Angle3. Corr = ',num2str(rho)])
xlabel('Angle3')
ylabel('SourceStrength')
lsline


[R,P] = corrcoef([GLM.XDist GLM.YDist GLM.ZDist GLM.AngleNorm GLM.Angle2 GLM.Angle3])
% 
figure
subplot(2,4,1)
hist(GLM.SrcStrength(:,1),50)
subplot(2,4,2)
hist(GLM.AngleNorm(2863:5724,1),18)
subplot(2,4,3)
hist(GLM.Angle2(2863:5724,1),18)
subplot(2,4,4)
hist(GLM.Angle3(2863:5724,1),18)
subplot(2,4,5)
hist(GLM.XDist,50)
subplot(2,4,6)
hist(GLM.YDist,50)
subplot(2,4,7)
hist(GLM.ZDist,50)

mean(GLM.XDist)
var(GLM.XDist)
mean(GLM.YDist)
var(GLM.YDist)
mean(GLM.ZDist)
var(GLM.ZDist)
mean(GLM.Angle)
var(GLM.Angle)
% 
% src.src = rescale(GLM.SrcStrength,0.01,0.99);
% writetable(struct2table(src), 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\Predictortest.xlsx')
% 

% cluster = 1;
% 
% figure
% x = Cnear(:,1);
% y = Cnear(:,2);
% z = Cnear(:,3);
% x0 = min(x) ; x1 = max(x) ; nx = 500 ;
% y0 = min(y) ; y1 = max(y) ; ny = 500 ;
% xx = linspace(x0,x1,nx) ;
% yy = linspace(y0,y1,ny) ;
% [X,Y] = meshgrid(xx,yy) ;
% Z = griddata(x,y,z,X,Y)  ;
% [U,V,W] = surfnorm(X,Y,Z);
% 
% a = Cnear(3,1:3); %cEEGrid point
% b = meanNormC; % your normal vector
% c = a+b; % end position of normal vector
% 
% %quiver3 syntax: quiver3(x,y,z,u,v,w)
% quiver3(a(1), a(2), a(3), c(1), c(2), c(3));
% hold on
% plot3(x,y,z,'mo')
% mesh(X,Y,Z)
% axis equal;
% 
%for cluster = 1:length(keepClusters)
u = HM.Vertices(keepClusters(cluster).Vertices,1);
v = HM.Vertices(keepClusters(cluster).Vertices,2);
w = HM.Vertices(keepClusters(cluster).Vertices,3);
u0 = min(u) ; u1 = max(u) ; nu = 500 ;
v0 = min(v) ; v1 = max(v) ; nv = 500 ;
uu = linspace(u0,u1,nu) ;
vv = linspace(v0,v1,nv) ;
[A,B] = meshgrid(uu,vv) ;
L = griddata(u,v,w,A,B)  ;
[P,Q,R] = surfnorm(A,B,L);

d = HM.Vertices(keepClusters(cluster).Vertices(1),1:3); %ROI point
e = meanNormROI(cluster,:); % normal vector
f = d+e; % end position of normal vector

figure
quiver3(d(1), d(2), d(3), f(1), f(2), f(3));
hold on
%plot3(u,v,w,'mo')
%mesh(A,B,L)
quiver3(d(1), d(2), d(3), g(1), g(2), g(3));
quiver3(d(1), d(2), d(3), h(1), h(2), h(3));
axis equal;
%end

% PerceChange(1:length(full.ImageGridAmp),1) = 100;
% PerceChange = PerceChange - (100 ./ full.ImageGridAmp) .* left.ImageGridAmp;
% PerceChange(isnan(PerceChange))=0;
% mean(PerceChange)
% std(PerceChange)
% min(PerceChange)
% max(PerceChange)
% PerceChangeX = full;
% PerceChangeX.ImageGridAmp = PerceChange;
% ProtocolInfo = bst_get('ProtocolInfo'); 
% db_add(ProtocolInfo.iStudy, PerceChangeX, []); 
