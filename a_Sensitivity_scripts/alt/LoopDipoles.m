%% Simulate recordings. Results from PreapreSource_cEEGrid
clear;
close all;
clc

%% Settings
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's04_dipole'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\figures_sim\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = FullCap
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '1'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
numLocs = 4;
numDir = 3;
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load the channel file and the dipole file
listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);
rightChannels = indexChan(length(indexChan)/2+1:end);
leftChannels = indexChan(1:length(indexChan)/2);

listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
iChannelsRight = 1:length(rightChannels); %number of channels according to your channel file
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
refNew = find(strcmp({CM.Channel.Name}, 'E16')==1); %E16 is the reference electrode of the cEEGrid
%% Caclulate the normal of the cEEGrid plane + the two orthogonals
%find the vertices on the scalp mesh that are closest to the cEEGrids

for h = 1:length(leftChannels) %calculate the center of the cEEGrid from the individual elec positions
    CenterLeft(h,1:3) = CM.Channel(leftChannels(h)).Loc;
end
CenterLeft = mean(CenterLeft);

listScalp = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*remesh*']); %contains the remeshed scalp. with the higher tesselation, we can get vertices that are closer to the cEEGrids
remeshScalp = in_tess_bst([listScalp(end).folder,'\',listScalp(end).name]); %This function calls the scalp mesh and adds the missing fields (VertNormals etc.)

d = 1;
for s = 129:138 %store the locations of the electrodes (left/right) in new variables
    Ceelocsl(d,1:3) = CM.Channel(s).Loc;
    Ceelocsr(d,1:3) = CM.Channel(s+10).Loc;
    d=d+1;
end

[l ,distl] = dsearchn(remeshScalp.Vertices,Ceelocsl); %calc the vertices that are closest to the cEEGrid locations on the left...
[r ,distr] = dsearchn(remeshScalp.Vertices,Ceelocsr); %...and the right side
[cen ,distcen] = dsearchn(remeshScalp.Vertices,CenterLeft); %vertex cen closest to the center of the cEEGrid

Cnearl = remeshScalp.Vertices(l,1:3); %store the new cEEGrid locations on the remesh in a new variable
Cnearr = remeshScalp.Vertices(r,1:3);
Cnearcen = remeshScalp.Vertices(cen,1:3); %also, store the new center of the cEEGrid

NormCl = mean(remeshScalp.VertNormals([l; cen],1:3)); %calc the normal vector of the plane from the center plus the cEEGrid positions
N = [NormCl; null(NormCl(:).')']; %calc the two orthogonals to the normal vector of the plane

%% Define point on the sourceGrid that is closest to the cEEGrid center (from those on the x/z-coordinates that are closest to the center)
% Then, calc more points that have equal distances to each other in every direction

GridLocsXZ = HM.GridLoc(:,[1 3]); %store the x and z coordinates of the source grid from the mri volume in a new variable
[left, distl] = dsearchn(GridLocsXZ,Cnearcen([1 3])); %find the grid points that have grid locations closest to the center of the cEEGrid on the x and z axis
NearestXZ = find(HM.GridLoc(:,1)== HM.GridLoc(left,1) & HM.GridLoc(:,3)==HM.GridLoc(left,3)); %since the grid points are equally distributed, there will be several points with the same xz coordinates

fac = 0.01; %this variable determines the distance between the points (in meter)
%find the index of the grid location with the smallest distance to the center of the cEEGrid
closest  = HM.GridLoc(NearestXZ(end),:)'; %due to the build of the matrix, the last of the found values is the left-most value (and therefore closest to the cEEGrid)
closestyminus = [closest(1);closest(2)-fac;closest(3)]; %from the starting point (closest) go on the y axis by a distance of fac
closestxminus = [closestyminus(1)-fac;closestyminus(2);closestyminus(3)]; %from the second of 3 points on the y axis, go in the x and z direction
closestzplus = [closestyminus(1);closestyminus(2);closestyminus(3)+fac];

%% put the new source location and orientation into the dipole file and load it to brainstorm
% note that this is only for plotting the resulting source. The computation
% for the topoplots is done below

listDip = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles*']);
export_matlab([listDip(end).folder,'\',listDip(end).name],'dip'); %load the dipole file from PrepareSourceDipole.m

n = 3; %number of points on the y axis
for f = 1:n
    dip.Dipole(f).Loc     = closest (:,1); %first 3 dipoles have the same location and differ only in orientation
    dip.Dipole(f+n).Loc   = closestyminus(:,1); %second loc, 3 orientations
    dip.Dipole(f+n*2).Loc = closestxminus(:,1); %same concept for the remaining points
    dip.Dipole(f+n*3).Loc = closestzplus(:,1);
    
    dip.Dipole(f).Amplitude     = N(:,f); %here, the orientations for each loc are defined
    dip.Dipole(f+n).Amplitude   = N(:,f);
    dip.Dipole(f+n*2).Amplitude = N(:,f);
    dip.Dipole(f+n*3).Amplitude = N(:,f);
end

% x= f+n*3+1;
% dip.Dipole(x:end) = []; %delete the remaining values from the predefined dipole file. Only the above specified remain
% dip.Time(x:end) = []; %same with the time points

HM.GridLoc = round(HM.GridLoc(:,:),4); % this line is necessary because matlab rounds in a funny way

vecTime = linspace(0,1,length(dip.Dipole));
for w = 1:length(dip.Dipole)
    dip.Dipole(w).Loc = round(dip.Dipole(w).Loc,4); % this line is necessary because matlab rounds in a funny way
    dip.Dipole(w).Time = vecTime(w);
    dip.Dipole(w).Index = 1;
    dip.Dipole(w).Origin = [0,0,0];
    dip.Dipole(w).Goodness = 1;
end
save([PATHOUT,'dipole.mat'],'dip');

w=1;
for u = 1:3:length(dip.Dipole)
    findHM(w) = find(HM.GridLoc(:,1)== dip.Dipole(u).Loc(1) & HM.GridLoc(:,2)== dip.Dipole(u).Loc(2) & HM.GridLoc(:,3)== dip.Dipole(u).Loc(3));
    w=w+1;
end

%% Preapre an empty SourceGrid

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things

Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
SourceSimple = Source;

sFiles = [];
sFiles = {...
    [listSource(1).folder,'\',listSource(1).name]};

% Start a new report, get subject infos
bst_report('Start', sFiles);
sProcess = bst_process('GetInputStruct', sFiles);
[sSubject,iSubject] = bst_get('Subject', sProcess.SubjectFile); %gets the subject ID

%% Some settings for the cEEGrid channels

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

if EarChannel == '1'
    channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
    nameVar = 'Left';
elseif EarChannel == '2'
    channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
    nameVar = 'Right';
else EarChannel == '0'
    channels=indexChan;
    nameVar = 'Double';
end

ref = refNew; %index of the reference electrode for cEEGrid (in CM, E16)
nameResult = 'cEEGrid'; %for naming the resulting images
iChannels = 1:length(channels); %number of channels according to your channel file

%% Calculate amps (and highest amp) for all electrode pairs
for d = 1: length(dip.Dipole) %loop through the dipoles with their 3 orientations
    
    if any(1:3:length(dip.Dipole)) == d %determine the orientation for naming the output variables
        orientation = 'norm';
    elseif any(2:3:length(dip.Dipole)) == d
        orientation = '2';
    else any(3:3:length(dip.Dipole)) == d
        orientation = '3';
    end
    
    dip_bst = dip;
    dip_bst.Dipole = dip_bst.Dipole(d);
    dip_bst.Time = 0;
    
    ProtocolInfo = bst_get('ProtocolInfo');
    dip_bst.Comment= [nameResult,'_left_orient_',orientation,'_dipole_',num2str(d)];
    db_add(ProtocolInfo.iStudy, dip_bst, []); %add the dipole to brainstorm to have the plot of the dipole orientation
        
    IMAGEGRIDAMP = SourceSimple.ImageGridAmp; %This is the source
    
    IMAGEGRIDAMP(findHM(d)*3-2:findHM(d)*3,1) = dip_bst.Dipole.Amplitude; % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
    %since per point there are 3 orientation, multiply the loc, which is
    %only one point, by 3. replace these values with those of one of the 3
    %orientations from dip_bst.Dipole(r).Amplitude. the result is one
    %dipole source with one orientation
    
    SourceSimple.ImageGridAmp = IMAGEGRIDAMP;
    
    Fi = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
    Fi(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
    
    G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
        
    ProtocolInfo = bst_get('ProtocolInfo');
    SourceSimple.Comment = [nameResult,'_left_orient_',orientation,'_dipole_',num2str(d)];
    db_add(ProtocolInfo.iStudy, SourceSimple, []);
    db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI
    %IChannels is just a list from 1 to the number of used channels.
    %channels is the index of the specified channels in gain
    
    collect=[];
    index=1;
    
    for ch=1:length(channels)-1
        for b=ch+1:length(channels)
            collect(index) = abs((Fi(iChannels(ch),1)- G)- (Fi(iChannels(b),1)-G)); %calc every combination of electrodes
            checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
            checkPair(index,2) = {CM.Channel(channels(ch)).Name};
            checkPair(index,3) = {CM.Channel(channels(b)).Name};
            SelectedValues(index,1) = Fi(iChannels(ch),1)- G;
            SelectedValues(index,2) = Fi(iChannels(b),1)- G; 
            index=index+1;
        end
    end
    FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
    FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:); 

    
    %% Store the results in a variable
    
    SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    ResultsC(d).maxDiff =  max(collect);
    ResultsC(d).CheckPair = checkPair;
    ResultsC(d).CheckPair(:,4) = {'0'};
    ResultsC(d).CheckPair(collect==max(collect),4) = {'1'};
    ResultsC(d).Pair = checkPair(collect==max(collect),1);
    ResultsC(d).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
    ResultsC(d).SelectedValues = SelectedValues;
    ResultsC(d).SelectedValues(:,3) = 0;
    ResultsC(d).SelectedValues(collect==max(collect),3) = 1;
    ResultsC(d).FixPair = FixPair;

    %% simulate the recordings based on the dipoles
    listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_20*']);
    newDataFile = bst_simulation([listResults(end).folder,'\',listResults(end).name]);
    
    listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_sim*']);
    export_matlab([listSimulation(end).folder,'\',listSimulation(end).name],'simulation');
    simulation.ChannelFlag(1:channels(1)-1) = -1; %determine which channels are treated as bad. This way, they will not be part of the plotting
    simulation.Comment = [nameResult,'_left_orient_',orientation,'_dipole_',num2str(d)];
    ProtocolInfo = bst_get('ProtocolInfo');
    db_add(ProtocolInfo.iStudy, simulation, []);
    listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_1*']);

    if ~exist([PATHOUT,'figures\archive\'])
        mkdir([PATHOUT,'figures\archive\'])
    end
    
    %% Now plot the results
    
    % to plot only the cEEGrid, mark channels as bad in the source file
    listBadChan = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_Dipoles*']);
    export_matlab([listBadChan(end).folder,'\',listBadChan(end).name],'badChan');
    badChan.ChannelFlag(1:channels(1)-1) = -1; %determine which channels are treated as bad. This way, they will not be part of the plotting
    ProtocolInfo = bst_get('ProtocolInfo');
    db_add(ProtocolInfo.iStudy, badChan, []);
    
    listDipNew =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles_1*']);
    listTess = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\tess_head_warped.mat']);
    
    %dipole orientation from three angles
    hFig = view_dipoles([listDipNew(end).folder,'\',listDipNew(end).name], 'cortex') %get sensors to be displayed!
    figure_3d('SetStandardView', gcf, {'left'});
    figure_3d('ViewSensors', gcf, 0, 0); %specify that electrode-locations are shown, but their names are not
    %panel_surface('AddSurface',hFig, [listTess(end).folder,'\',listTess(end).name])
    set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'on');
    saveas(gcf,[PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'_left.png']);
    figure_3d('SetStandardView', gcf, {'Top'});
    saveas(gcf,[PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'_top.png']);
    figure_3d('SetStandardView', gcf, {'Back'});
    saveas(gcf,[PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'_back.png']);
    
    %cEEGrid elec-topo from the left side
    leftFig = view_topography([listSimulation(end).folder,'\',listSimulation(end).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
    figure_3d('ViewSensors', gcf, 0, 0);
    figure_3d('SetStandardView', gcf, {'left'});
    set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'off');
    saveas(gcf,[PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'.png']);
    
    %Topoplot
    figTopo = view_topography([listSimulation(end).folder,'\',listSimulation(end).name], 'EEG', '2DSensorCap'); %view the averaged signal of the subject
    figure_3d('ViewSensors', gcf, 1, 0); %specify that electrode-locations are shown, but their names are not
    set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'off');
    saveas(gcf,[PATHFIG,'left_topo_orient_',orientation,'_dipole_',num2str(d),'.png']);
    
    figure
    subplot(3,2,1)
    figLeft=imread([PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'_left.png']);
    imshow(figLeft);
    subplot(3,2,2)
    figTop=imread([PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'_top.png']);
    imshow(figTop);
    subplot(3,2,3)
    figBack=imread([PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'_back.png']);
    imshow(figBack);
    subplot(3,2,4)
    figElec=imread([PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'.png']);
    imshow(figElec);
    subplot(3,2,5)
    figTopo=imread([PATHFIG,'left_topo_orient_',orientation,'_dipole_',num2str(d),'.png']);
    imshow(figTopo);
    sgtitle(['Topoplots for different angles'],'FontSize',10);
    set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'off');
    
    listFig = dir([PATHFIG,'left*.png']);
    for o = 1:length(listFig)
    delete([listFig(o).folder,'\',listFig(o).name]);
    end
    
    saveas(gcf,[PATHFIG,'combo_left_orient_',orientation,'_dipole_',num2str(d),'.png']);
    
    listDelete1 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles_*']); %this needs to be smoother, in 2020 this won't work anymore
    listDelete2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_1*']); %this needs to be smoother, in 2020 this won't work anymore
    listDelete3 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_simulation*']);
    listDelete4 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_1*']);
    
    for w = 1:length(listDelete1)-1
        isDeleted = file_delete( [listDelete1(w).folder,'\',listDelete1(w).name], 1, -1);
    end
    for w = 1:length(listDelete2)
        isDeleted = file_delete( [listDelete2(w).folder,'\',listDelete2(w).name], 1, -1);
    end
    for w = 1:length(listDelete3)
        isDeleted = file_delete( [listDelete3(w).folder,'\',listDelete3(w).name], 1, -1);
    end
    for w = 2:length(listDelete4)
        isDeleted = file_delete( [listDelete4(w).folder,'\',listDelete4(w).name], 1, -1);
    end
    
    db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI
end
%     figDrop = figure;
%     combo_Diff = [ResultsF.maxDiff ResultsC.maxDiff ResultsCRight.maxDiff];
%     PerceChange = 100 - (100 / ResultsF.maxDiff) * ResultsC.maxDiff;
%     PerceChangeOnly = 100 - (100 / ResultsF.maxDiff) * ResultsCRight.maxDiff;
%     bar([1 2 3],combo_Diff)
%     xticks([1 2 3])
%     xticklabels({'FullCap','cEEGrid','cEEGridRight'})
%     ylim([0 ResultsF.maxDiff+500])
%     text(1:3,combo_Diff,num2str(combo_Diff'),'vert','bottom','horiz','center');
%     title(['Signal Difference. Signal-loss from F to C = ',num2str(PerceChange),'%. From F to CRight = ',num2str(PerceChangeOnly),'%'],'FontSize',10);
%     set(gcf, 'Position', get(0, 'ScreenSize'));
%     saveas(gcf,[PATHOUT,'figures\',nameAtlas,num2str(ROI),'drop.png']);
%     if ~exist([PATHOUT,'results_Diff\'])
%         mkdir([PATHOUT,'results_Diff\'])
%     end
%
%     if UserAtlas == '1' || UserAtlas == '2'
%         if ~exist([PATHOUT,'results_Diff\UserAtlas\'])
%             mkdir([PATHOUT,'results_Diff\UserAtlas\'])
%         end
%         save([PATHOUT,'results_Diff\UserAtlas\Diff_',nameVar,'_',nameAtlas,'_',num2str(ROI)],'ResultsC','ResultsF','ResultsCRight');
%     else
%         save([PATHOUT,'results_Diff\Diff_',nameVar,'_',nameAtlas,'_',num2str(ROI)],'ResultsC','ResultsF','ResultsCRight');
%     end
%
%     close all

%
%
%     if UserAtlas == '0'
%         if ~exist([PATHOUT,'figures\collection\',SubjectName,'\'])
%             mkdir([PATHOUT,'figures\collection\',SubjectName,'\'])
%         end
%         saveas(gcf,[PATHOUT,'figures\collection\',SubjectName,'\',nameAtlas,num2str(ROI),'.png']);
%     elseif UserAtlas == '1'
%         if ~exist([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
%             mkdir([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
%         end
%         saveas(gcf,[PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\',nameAtlas,num2str(ROI),'UserScout',num2str(ROIUser),'.png']);
%     else UserAtlas == '2'
%         if ~exist([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
%             mkdir([PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\'])
%         end
%         saveas(gcf,[PATHOUT,'figures\collection\',SubjectName,'\UserAtlas\UserScout',num2str(ROIUser),'.png']);
%     end
%
%     close all
%
% clear GUI, go back to default before starting the script. If not needed, comment this section out

