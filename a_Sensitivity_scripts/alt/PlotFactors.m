%% Simulate recordings. Plot different factors influencing signal strength
clear;
close all;
clc

%% Settings
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's04_dipole'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_cEEGrid\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\figures_sim\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = FullCap
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '1'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load the channel file and the dipole file
listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);
rightChannels = indexChan(length(indexChan)/2+1:end);
leftChannels = indexChan(1:length(indexChan)/2);

listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
iChannelsRight = 1:length(rightChannels); %number of channels according to your channel file
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
refNew = find(strcmp({CM.Channel.Name}, 'E16')); %E16 is the reference electrode of the cEEGrid
%% Caclulate the normal of the cEEGrid plane + the two orthogonals
%find the vertices on the scalp mesh that are closest to the cEEGrids

for h = 1:length(leftChannels) %calculate the center of the cEEGrid from the individual elec positions
    CenterLeft(h,1:3) = CM.Channel(leftChannels(h)).Loc;
end
CenterLeft = mean(CenterLeft);

listScalp = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*remesh*']); %contains the remeshed scalp. with the higher tesselation, we can get vertices that are closer to the cEEGrids
remeshScalp = in_tess_bst([listScalp(end).folder,'\',listScalp(end).name]); %This function calls the scalp mesh and adds the missing fields (VertNormals etc.)

d = 1;
for s = 129:138 %store the locations of the electrodes (left/right) in new variables
    Ceelocsl(d,1:3) = CM.Channel(s).Loc;
    Ceelocsr(d,1:3) = CM.Channel(s+10).Loc;
    d=d+1;
end

[l ,distl] = dsearchn(remeshScalp.Vertices,Ceelocsl); %calc the vertices that are closest to the cEEGrid locations on the left...
[r ,distr] = dsearchn(remeshScalp.Vertices,Ceelocsr); %...and the right side
[cen ,distcen] = dsearchn(remeshScalp.Vertices,CenterLeft); %vertex cen closest to the center of the cEEGrid

Cnearl = remeshScalp.Vertices(l,1:3); %store the new cEEGrid locations on the remesh in a new variable
Cnearr = remeshScalp.Vertices(r,1:3);
Cnearcen = remeshScalp.Vertices(cen,1:3); %also, store the new center of the cEEGrid

NormCl = mean(remeshScalp.VertNormals([l; cen],1:3)); %calc the normal vector of the plane from the center plus the cEEGrid positions
N = [NormCl; null(NormCl(:).')']; %calc the two orthogonals to the normal vector of the plane

%% Define point on the sourceGrid that is closest to the cEEGrid center (from those on the x/z-coordinates that are closest to the center)
% Then, calc more points that have equal distances to each other in every direction

GridLocsXZ = HM.GridLoc(:,[1 3]); %store the x and z coordinates of the source grid from the mri volume in a new variable
[left, distl] = dsearchn(GridLocsXZ,Cnearcen([1 3])); %find the grid points that have grid locations closest to the center of the cEEGrid on the x and z axis
NearestXZ = find(HM.GridLoc(:,1)== HM.GridLoc(left,1) & HM.GridLoc(:,3)==HM.GridLoc(left,3)); %since the grid points are equally distributed, there will be several points with the same xz coordinates

fac = 0.01; %this variable determines the distance between the points (in meter)
%find the index of the grid location with the smallest distance to the center of the cEEGrid
closest  = HM.GridLoc(NearestXZ(end),:)'; %due to the build of the matrix, the last of the found values is the left-most value (and therefore closest to the cEEGrid)
closestyminus = [closest(1);closest(2)-fac;closest(3)]; %from the starting point (closest) go on the y axis by a distance of fac
closestxplus = [closestyminus(1)+fac;closestyminus(2);closestyminus(3)]; %from the second of 3 points on the y axis, go in the x and z direction
closestzplus = [closestyminus(1);closestyminus(2);closestyminus(3)+fac];

%% put the new source location and orientation into the dipole file and load it to brainstorm
% note that this is only for plotting the resulting source. The computation
% for the topoplots is done below

listDip = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles*']);
export_matlab([listDip(end).folder,'\',listDip(end).name],'dip'); %load the dipole file from PrepareSourceDipole.m

n = 3; %number of points on the y axis
for f = 1:n
    dip.Dipole(f).Loc     = closest (:,1); %first 3 dipoles have the same location and differ only in orientation
    dip.Dipole(f+n).Loc   = closestyminus(:,1); %second loc, 3 orientations
    dip.Dipole(f+n*2).Loc = closestxplus(:,1); %same concept for the remaining points
    dip.Dipole(f+n*3).Loc = closestzplus(:,1);
    
    dip.Dipole(f).Amplitude     = N(:,f); %here, the orientations for each loc are defined
    dip.Dipole(f+n).Amplitude   = N(:,f);
    dip.Dipole(f+n*2).Amplitude = N(:,f);
    dip.Dipole(f+n*3).Amplitude = N(:,f);
end

dip.Dipole = dip.Dipole(1:n*4);

vecTime = linspace(0,1,length(dip.Dipole));
for w = 1:length(dip.Dipole)
    dip.Dipole(w).Loc = round(dip.Dipole(w).Loc,4); % this line is necessary because matlab rounds in a funny way
    dip.Dipole(w).Time = vecTime(w);
    dip.Dipole(w).Index = 1;
    dip.Dipole(w).Origin = [0,0,0];
    dip.Dipole(w).Goodness = 1;
end

HM.GridLoc = round(HM.GridLoc(:,:),4); % this line is necessary because matlab rounds in a funny way

w=1;
for u = 1:3:length(dip.Dipole)
    findHM(w) = find(HM.GridLoc(:,1)== dip.Dipole(u).Loc(1) & HM.GridLoc(:,2)== dip.Dipole(u).Loc(2) & HM.GridLoc(:,3)== dip.Dipole(u).Loc(3));
    w=w+1;
end

%% Some settings for the cEEGrid channels

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

if EarChannel == '1'
    channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
    nameVar = 'Left';
elseif EarChannel == '2'
    channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
    nameVar = 'Right';
else EarChannel == '0'
    channels=indexChan;
    nameVar = 'Double';
end

nameResult = 'cEEGrid'; %for naming the resulting images
iChannels = 1:length(channels); %number of channels according to your channel file

%% Prepare all angles

smoothVec = linspace(0,90,50);
timeVec = linspace(0,10,length(smoothVec)*2);

dip_bst = dip;
dip_bst.Dipole(1) = dip.Dipole(1);
dip_bst.Time = timeVec;

vec1 = dip.Dipole(1).Amplitude; %norm vec
vec2 = dip.Dipole(3).Amplitude; % z vec

for t = 1 : length(smoothVec)
    angle(t) = smoothVec(t)*pi/180;   % angle of rotation in rad
    v(:,t) = vec1 + tan(angle(t))*vec2;   % length of vec1 is 1
    v(:,t) = v(:,t)/norm(v(:,t));                % v is an unit vector 5 degrees from vec1
    dip_bst.Dipole(t) = dip_bst.Dipole(1);
    dip_bst.Dipole(t).Amplitude = v(:,t);
    dip_bst.Dipole(t).Time = timeVec(t);
end

vec1 = dip.Dipole(3).Amplitude; % z vec
vec2 = dip.Dipole(2).Amplitude; % x vec

for t = length(smoothVec)+1: length(smoothVec)*2
    angle(t) = smoothVec(t-length(smoothVec))*pi/180;   % angle of rotation in rad
    v(:,t) = vec1 + tan(angle(t))*vec2;   % length of vec1 is 1
    v(:,t) = v(:,t)/norm(v(:,t));                % v is an unit vector 5 degrees from vec1
    dip_bst.Dipole(t) = dip_bst.Dipole(1);
    dip_bst.Dipole(t).Amplitude = v(:,t);
    dip_bst.Dipole(t).Time = timeVec(t);
end

dipReady = dip_bst;

u=0;
for bu = 1:100
    for st = 1: 50
        dipReady.Dipole(:,st+u) = dip_bst.Dipole(bu); % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
    end
    u = u +50;
end

dipReady.Time = linspace(0,10,length(dip_bst.Dipole)*50);
for h = 1:length(dipReady.Time)
    dipReady.Dipole(h).Time = dipReady.Time(h);
end

IMAGEGRIDAMP = zeros(length(Source.ImageGridAmp),length(dipReady.Time));

for st = 1: length(dipReady.Time)
    IMAGEGRIDAMP(findHM(1)*3-2:findHM(1)*3,st) = dipReady.Dipole(st).Amplitude; % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
end

Source.ImageGridAmp = IMAGEGRIDAMP;
Source.Time = dipReady.Time;

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_2*']);
export_matlab([listRaw(end).folder,'\',listRaw(end).name],'rawSim');
rawSim.F = rawSim.F(:,1);
rawSim.F = repmat(rawSim.F,1,length(dip_bst.Dipole)*50);
rawSim.Time = Source.Time;

ProtocolInfo = bst_get('ProtocolInfo');
db_add(ProtocolInfo.iStudy, rawSim, 1);

listRaw = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_2*']);
Source.DataFile = [SubjectName,'/raw/',listRaw(end).name]; % attach the source to the raw signal
dipReady.DataFile = [SubjectName,'/raw/',listRaw(end).name]; % attach the dipole file to the raw signal
Source.ChannelFlag(strcmp({CM.Channel.Comment},'NewChannels')==0) = -1;
db_add(ProtocolInfo.iStudy, Source, 1); %load the source into matlab
db_add(ProtocolInfo.iStudy, dipReady, 1); %the dipole file, too

listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_20*']);
Sim3DElec = bst_simulation([listResults(end).folder,'\',listResults(end).name]);

%% Plot the results

listDipNew =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles_2*']);
listTess = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\tess_head_warped.mat']);
listSim = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\*simulation*']);

%dipole orientation from three angles
hFig = view_dipoles([listDipNew(end).folder,'\',listDipNew(end).name], 'cortex')
set(gcf, 'Position', get(0, 'ScreenSize'),'Visible', 'on');

%cEEGrid elec-topo from the left side
leftFig = view_topography([listSim(1).folder,'\',listSim(1).name], 'EEG', '3DElectrodes'); %view the averaged signal of the subject
figure_3d('SetStandardView', gcf, {'left'});
%saveas(gcf,[PATHFIG,'left_orient_',orientation,'_dipole_',num2str(d),'.png']);

%Topoplots
figTopo3D = view_topography([listSim(1).folder,'\',listSim(1).name], 'EEG', '3DSensorCap'); %view the averaged signal of the subject
%saveas(gcf,[PATHFIG,'left_topo_orient_',orientation,'_dipole_',num2str(d),'.png']);
figTopo2D = view_topography([listSim(1).folder,'\',listSim(1).name], 'EEG', '2DSensorCap'); %view the averaged signal of the subject
%saveas(gcf,[PATHFIG,'left_topo_orient_',orientation,'_dipole_',num2str(d),'.png']);

%% calc the results

export_matlab(Sim3DElec,'sim');

F = sim.F; 
G = sim.F(refNew,:); 
collect=[];
index=1;
storeID = 1;
for ph = [1 length(F)/2 length(F)]
for ch=1:length(channels)-1
    for b=ch+1:length(channels)
        collect(index) = abs((F(channels(ch),ph)- G(ph))- (F(channels(b),ph)-G(ph))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
        checkPair(index,2) = {CM.Channel(channels(ch)).Name};
        checkPair(index,3) = {CM.Channel(channels(b)).Name};
        SelectedValues(index,1) = F(channels(ch),ph)- G(ph);
        SelectedValues(index,2) = F(channels(b),ph)- G(ph);
        index=index+1;
    end
end
FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:);

% Store the results in a variable
SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDir(storeID).maxDiff =  max(collect);
ResultsDir(storeID).CheckPair = checkPair;
ResultsDir(storeID).CheckPair(:,4) = {'0'};
ResultsDir(storeID).CheckPair(collect==max(collect),4) = {'1'};
ResultsDir(storeID).Pair = checkPair(collect==max(collect),1);
ResultsDir(storeID).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDir(storeID).SelectedValues = SelectedValues;
ResultsDir(storeID).SelectedValues(:,3) = 0;
ResultsDir(storeID).SelectedValues(collect==max(collect),3) = 1;
ResultsDir(storeID).FixPair = FixPair;
storeID = storeID + 1;
index=1;
end

%% get results from different positions
fac = 0.05; %this variable determines the distance between the points (in meter)
gridres = 0:0.005:fac;
closeLine = zeros(3,length(gridres)*2);
w=1;
for th = 0:0.005:fac
closeLine(1:3,w) = [closestyminus(1) + th;0.0275 ;closestyminus(3)]; 
closeLine(1:3,w+length(gridres)) = [closestyminus(1) ;0.0275 ;closestyminus(3) + th];
w=w+1;
end
closeLine(:,1:length(closeLine)/2) = flip(closeLine(:,1:length(closeLine)/2),2); %flip matrix so the plot will later go from the last x to the first and from there to the last z
dip_loc = dip;
for ds = 1:length(closeLine)
dip_loc.Dipole(ds).Loc = closeLine(:,ds);
dip_loc.Dipole(ds).Loc = round(dip_loc.Dipole(ds).Loc,4);
dip_loc.Dipole(ds).Loc = single(dip_loc.Dipole(ds).Loc);
dip_loc.Dipole(ds).Amplitude = dip.Dipole(2).Amplitude; % always take the orientation in the x-direction
dip_loc.Dipole(ds).Goodness = 1;
dip_loc.Dipole(ds).Origin = [0,0,0];
dip_loc.Dipole(ds).Index = 1;
dip_loc.Dipole(ds).Errors = 0;
dip_loc.Dipole(ds).Khi2 = dip_loc.Dipole(1).Khi2;
dip_loc.Dipole(ds).DOF = dip.Dipole(1).DOF;
dip_loc.Dipole(ds).Perform = dip.Dipole(1).Perform;
end
dip_loc.Dipole([1 22]) = [];

findHM =[];
w=1;
for u = 1:length(dip_loc.Dipole)
    findHM(w) = find(HM.GridLoc(:,1)== dip_loc.Dipole(u).Loc(1) & HM.GridLoc(:,2)== dip_loc.Dipole(u).Loc(2) & HM.GridLoc(:,3)== dip_loc.Dipole(u).Loc(3));
    w=w+1;
end

dipReady = dip_loc;
dipReady.DataFile = Source.DataFile;

IMAGEGRIDAMP = zeros(length(Source.ImageGridAmp),length(dipReady.Time));

u=0;
for bu = 1:length(dip_loc.Dipole)
    for st = 1: 5000/length(dip_loc.Dipole)
        dipReady.Dipole(:,st+u) = dip_loc.Dipole(bu); % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
        IMAGEGRIDAMP(findHM(bu)*3-2:findHM(bu)*3,st+u) = dipReady.Dipole(bu).Amplitude;
    end
    u = u + 5000/length(dip_loc.Dipole);
end

dipReady.Time = linspace(0,10,length(dip_bst.Dipole)*50);
for h = 1:length(dipReady.Time)
    dipReady.Dipole(h).Time = dipReady.Time(h);
end

Source.ImageGridAmp = IMAGEGRIDAMP;
Source.Time = dipReady.Time;

db_add(ProtocolInfo.iStudy, Source, 1); %load the source into matlab
db_add(ProtocolInfo.iStudy, dipReady, 1); %the dipole file, too

listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_20*']);
Sim3DElec = bst_simulation([listResults(end).folder,'\',listResults(end).name]);

listSim = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\*simulation*']);
export_matlab([listSim(end).folder,'\',listSim(end).name],'sim');

F = sim.F; 
G = sim.F(refNew,:); 
collect=[];
index=1;
storeID = 1;
for ph = [1 length(F)/2-1 length(F)]
for ch=1:length(channels)-1
    for b=ch+1:length(channels)
        collect(index) = abs((F(channels(ch),ph)- G(ph))- (F(channels(b),ph)-G(ph))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
        checkPair(index,2) = {CM.Channel(channels(ch)).Name};
        checkPair(index,3) = {CM.Channel(channels(b)).Name};
        SelectedValues(index,1) = F(channels(ch),ph)- G(ph);
        SelectedValues(index,2) = F(channels(b),ph)- G(ph);
        index=index+1;
    end
end
FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:);

% Store the results in a variable
SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsLoc(storeID).maxDiff =  max(collect);
ResultsLoc(storeID).CheckPair = checkPair;
ResultsLoc(storeID).CheckPair(:,4) = {'0'};
ResultsLoc(storeID).CheckPair(collect==max(collect),4) = {'1'};
ResultsLoc(storeID).Pair = checkPair(collect==max(collect),1);
ResultsLoc(storeID).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsLoc(storeID).SelectedValues = SelectedValues;
ResultsLoc(storeID).SelectedValues(:,3) = 0;
ResultsLoc(storeID).SelectedValues(collect==max(collect),3) = 1;
ResultsLoc(storeID).FixPair = FixPair;
storeID = storeID + 1;
index=1;
end

%% get results from different distances

gridres = 0:0.005:0.005*10-0.005;
closeLine = zeros(3,length(gridres));
w=1;
for th = 0:0.005:0.005*10-0.005
closeLine(1:3,w) = [closest(1) ;closest(2) - th ;closest(3)]; 
w=w+1;
end

dip_dist = dip;
dip_dist.Dipole = dip_dist.Dipole(1:length(gridres));
for ds = 1:length(closeLine)
dip_dist.Dipole(ds).Loc = closeLine(:,ds);
dip_dist.Dipole(ds).Loc = round(dip_dist.Dipole(ds).Loc,4);
dip_dist.Dipole(ds).Loc = single(dip_dist.Dipole(ds).Loc);
dip_dist.Dipole(ds).Amplitude = dip.Dipole(3).Amplitude; % always take the orientation in the x-direction
dip_dist.Dipole(ds).Goodness = 1;
dip_dist.Dipole(ds).Origin = [0,0,0];
dip_dist.Dipole(ds).Index = 1;
dip_dist.Dipole(ds).Errors = 0;
dip_dist.Dipole(ds).Khi2 = dip_dist.Dipole(1).Khi2;
dip_dist.Dipole(ds).DOF = dip.Dipole(1).DOF;
dip_dist.Dipole(ds).Perform = dip.Dipole(1).Perform;
end

w=1;
for u = 1:length(dip_dist.Dipole)
    findHM(w) = find(HM.GridLoc(:,1)== dip_dist.Dipole(u).Loc(1) & HM.GridLoc(:,2)== dip_dist.Dipole(u).Loc(2) & HM.GridLoc(:,3)== dip_dist.Dipole(u).Loc(3));
    w=w+1;
end

dipReady = dip_dist;
dipReady.DataFile = Source.DataFile;

IMAGEGRIDAMP = zeros(length(Source.ImageGridAmp),length(dipReady.Time));

u=0;
for bu = 1:10
    for st = 1: 500
        dipReady.Dipole(:,st+u) = dip_dist.Dipole(bu); % find the index of a dipole (findHM) in the IMAGEGRIDAMP.
        IMAGEGRIDAMP(findHM(bu)*3-2:findHM(bu)*3,st+u) = dipReady.Dipole(bu).Amplitude;
    end
    u = u + 500;
end

dipReady.Time = linspace(0,10,length(dip_bst.Dipole)*50);
for h = 1:length(dipReady.Time)
    dipReady.Dipole(h).Time = dipReady.Time(h);
end

Source.ImageGridAmp = IMAGEGRIDAMP;
Source.Time = dipReady.Time;

db_add(ProtocolInfo.iStudy, Source, 1); %load the source into matlab
db_add(ProtocolInfo.iStudy, dipReady, 1); %the dipole file, too

listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_20*']);
Sim3DElec = bst_simulation([listResults(end).folder,'\',listResults(end).name]);

export_matlab(Sim3DElec,'sim');

F = sim.F; 
G = sim.F(refNew,:); 
collect=[];
index=1;
storeID = 1;
for ph = [1 length(F)/2 length(F)]
for ch=1:length(channels)-1
    for b=ch+1:length(channels)
        collect(index) = abs((F(channels(ch),ph)- G(ph))- (F(channels(b),ph)-G(ph))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
        checkPair(index,2) = {CM.Channel(channels(ch)).Name};
        checkPair(index,3) = {CM.Channel(channels(b)).Name};
        SelectedValues(index,1) = F(channels(ch),ph)- G(ph);
        SelectedValues(index,2) = F(channels(b),ph)- G(ph);
        index=index+1;
    end
end
FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:);

% Store the results in a variable
SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDist(storeID).maxDiff =  max(collect);
ResultsDist(storeID).CheckPair = checkPair;
ResultsDist(storeID).CheckPair(:,4) = {'0'};
ResultsDist(storeID).CheckPair(collect==max(collect),4) = {'1'};
ResultsDist(storeID).Pair = checkPair(collect==max(collect),1);
ResultsDist(storeID).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDist(storeID).SelectedValues = SelectedValues;
ResultsDist(storeID).SelectedValues(:,3) = 0;
ResultsDist(storeID).SelectedValues(collect==max(collect),3) = 1;
ResultsDist(storeID).FixPair = FixPair;
storeID = storeID + 1;
index=1;
end

save([PATHOUT,'LocNOrient.mat'],'ResultsDir','ResultsLoc','ResultsDist');

prompt = {'Do you want to delete the bst-files? (y/n)'};
dlgtitle = 'Input';
answer = inputdlg(prompt)

if answer{1,1} == 'y'
listDelete1 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_1*']);
listDelete2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\*simulation*']);
listDelete3 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_1*']);
listDelete4 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\dipoles_1*']);

isDeleted = file_delete( [listDelete1(2).folder,'\',listDelete1(2).name], 1, -1);

for w = 1:length(listDelete2)
    isDeleted = file_delete( [listDelete2(w).folder,'\',listDelete2(w).name], 1, -1);
end

for w = 1:length(listDelete3)
isDeleted = file_delete( [listDelete3(w).folder,'\',listDelete3(w).name], 1, -1);
end

for w = 2:length(listDelete4)
isDeleted = file_delete( [listDelete4(w).folder,'\',listDelete4(w).name], 1, -1);
end

db_reload_studies(ProtocolInfo.iStudy) %reload to get rid of the deleted files in the GUI
end
