%% With the head model from PrepareSources.m, we want to create a 
% sensitivity map with activity from each point on the cortex mesh,
% consecutively. For each source, the electrode pair with the highest
% measured amplitude is found and put in a matrix. The recording device 
%(full cap, cEEGrid, left/right) can be specified. Further, it can be specified to only
% create a map for patches taken from an atlas (the predefined ones in bst)
% below, the reference electrodes can be specified

clear;
close all;
clc

SubjectName = 's07_compare'; %specify the name of the subject as will be used in the brainstorm GUI

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
cd([MAINPATH,'brainstorm3\']);
brainstorm
cEEGrid = '0'; % 1 = cEEGrid, 0 = FullChanCap (specify if the sensitivity of the fullCap or the cEEGrid should be calculated)
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '3'; %1 = left, 2 = right, 0 = both, 3 = fullCap; choose which side of sensors should be considered
atlas = '2'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux
refStandard = [30 31];
refNew = 144; %no ref
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%this script assumes that you have a symmetric electrode setup, so that
%half of the electrodes is on one side!

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);

if EarChannel == '3' && cEEGrid == '1'
    error('error: There is no fullCap setting for the cEEGrid. Change "Earchannel" or "cEEGrid"');
end

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI

if cEEGrid == '1'
    ref = refNew; %index of the reference electrode for cEEGrid (in CM, E16)
    nameResult = 'cEEGrid';
    if EarChannel == '1'
        channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
        nameVar = 'Left';
    elseif EarChannel == '2'
        channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
        nameVar = 'Right';
    else EarChannel == '0'
        channels=indexChan;
        nameVar = 'Double';
    end
    
else cEEGrid == '0'
    ref = refStandard; %index of the reference electrode for 64ChanCap (in CM, E053)
    nameResult = 'fullCap';
    if EarChannel == '1'
        channels=[15 50 42 12 59 64]; %channel indices  !!!!specific for the data from ana (Strophal paper)!!!!
        nameVar = 'Left';
    elseif EarChannel == '2'
        channels=[61 53 9 36 47 14];
        nameVar = 'Right';
    elseif EarChannel == '0'
        channels=[15 50 42 12 59 64 61 53 9 36 47 14];
        nameVar = 'Double';
    else EarChannel == '3'
        channels=1:indexChan(1)-1;
        nameVar = 'Full';
    end
end

listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
iChannels = 1:length(channels); %number of channels according to your channel file
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things

Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
SourceDist = Source; % source variable to work in
SourceSimple = Source;
IMAGEGRIDAMP=Source.ImageGridAmp; %This is the source/scout. It is currently only zeros

%calculate the sensitivity for a source (pointwise or for a cluster (ROI
%from atlas)) for the cEEGrids

if fullOrROI == '1' %for full mesh
    for  k= 1:size(Source.ImageGridAmp,1) %loop through every point on the brain mesh
        
        IMAGEGRIDAMP=Source.ImageGridAmp; %reset the matrix to the original 0-matrix every time
        IMAGEGRIDAMP(k,1)= 1; % set activity of one vertex to 1, the others stay 0
        F = zeros(length(channels), 1); % simulation matrix. CM is the channel model
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        if refNew == 0
            G = 0;
        else
        G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
        G = mean(G); %only nessecary for the fullCap. Here, we take the average from the two electrodes closest to the mastoid. Does not hurt for 1 electrode, mean of 1 stays 1
        %IChannels is just a list from 1 to the number of used channels.
        %channels is the index of the specified channels in gain
        end
        
        collect=[];
        index=1;
 
        for ch=1:length(channels)-1
            for b=ch+1:length(channels)
                collect(index)=abs((F(iChannels(ch),1)- G)- (F(iChannels(b),1)-G)); %calc every combination of electrodes
                index=index+1;
            end
        end
      
        SourceDist.ImageGridAmp(k,1) = max(collect); %get the maximum electrode, aka the one with the highest signal after referencing
        
        if mod(k,1000)==0
            disp('Almost there')
        end
        
    end
    SourceDist.Comment= [nameResult,nameVar,'GridFull']; %set the name referring to the chosen grid (left/right/double)
    
    SourceDist.ImageGridAmp = abs(SourceDist.ImageGridAmp);
    [a,b] = maxk(SourceDist.ImageGridAmp(:,1),100); %find the 100 highest values
    c = find(a > 200); %find unrealisticly high vlaues
    SourceDist.ImageGridAmp(b(c),1)=0; % delete them
    
    ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
    db_add(ProtocolInfo.iStudy, SourceDist, []); %this adds the new source projection to the current protocol
    
else fullOrROI == '0' %only for Region of Interest
    
    sFiles = [];
    sFiles = {...
        [listSource(1).folder,'\',listSource(1).name]};
    
    % Start a new report
    bst_report('Start', sFiles);
    
    if atlas == '0' %currently, 3 different atlases are available
        
        % Process: Scouts time series: [148 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Destrieux', {'G_Ins_lg_and_S_cent_ins L', 'G_Ins_lg_and_S_cent_ins R', 'G_and_S_cingul-Ant L', 'G_and_S_cingul-Ant R', 'G_and_S_cingul-Mid-Ant L', 'G_and_S_cingul-Mid-Ant R', 'G_and_S_cingul-Mid-Post L', 'G_and_S_cingul-Mid-Post R', 'G_and_S_frontomargin L', 'G_and_S_frontomargin R', 'G_and_S_occipital_inf L', 'G_and_S_occipital_inf R', 'G_and_S_paracentral L', 'G_and_S_paracentral R', 'G_and_S_subcentral L', 'G_and_S_subcentral R', 'G_and_S_transv_frontopol L', 'G_and_S_transv_frontopol R', 'G_cingul-Post-dorsal L', 'G_cingul-Post-dorsal R', 'G_cingul-Post-ventral L', 'G_cingul-Post-ventral R', 'G_cuneus L', 'G_cuneus R', 'G_front_inf-Opercular L', 'G_front_inf-Opercular R', 'G_front_inf-Orbital L', 'G_front_inf-Orbital R', 'G_front_inf-Triangul L', 'G_front_inf-Triangul R', 'G_front_middle L', 'G_front_middle R', 'G_front_sup L', 'G_front_sup R', 'G_insular_short L', 'G_insular_short R', 'G_oc-temp_lat-fusifor L', 'G_oc-temp_lat-fusifor R', 'G_oc-temp_med-Lingual L', 'G_oc-temp_med-Lingual R', 'G_oc-temp_med-Parahip L', 'G_oc-temp_med-Parahip R', 'G_occipital_middle L', 'G_occipital_middle R', 'G_occipital_sup L', 'G_occipital_sup R', 'G_orbital L', 'G_orbital R', 'G_pariet_inf-Angular L', 'G_pariet_inf-Angular R', 'G_pariet_inf-Supramar L', 'G_pariet_inf-Supramar R', 'G_parietal_sup L', 'G_parietal_sup R', 'G_postcentral L', 'G_postcentral R', 'G_precentral L', 'G_precentral R', 'G_precuneus L', 'G_precuneus R', 'G_rectus L', 'G_rectus R', 'G_subcallosal L', 'G_subcallosal R', 'G_temp_sup-G_T_transv L', 'G_temp_sup-G_T_transv R', 'G_temp_sup-Lateral L', 'G_temp_sup-Lateral R', 'G_temp_sup-Plan_polar L', 'G_temp_sup-Plan_polar R', 'G_temp_sup-Plan_tempo L', 'G_temp_sup-Plan_tempo R', 'G_temporal_inf L', 'G_temporal_inf R', 'G_temporal_middle L', 'G_temporal_middle R', 'Lat_Fis-ant-Horizont L', 'Lat_Fis-ant-Horizont R', 'Lat_Fis-ant-Vertical L', 'Lat_Fis-ant-Vertical R', 'Lat_Fis-post L', 'Lat_Fis-post R', 'Pole_occipital L', 'Pole_occipital R', 'Pole_temporal L', 'Pole_temporal R', 'S_calcarine L', 'S_calcarine R', 'S_central L', 'S_central R', 'S_cingul-Marginalis L', 'S_cingul-Marginalis R', 'S_circular_insula_ant L', 'S_circular_insula_ant R', 'S_circular_insula_inf L', 'S_circular_insula_inf R', 'S_circular_insula_sup L', 'S_circular_insula_sup R', 'S_collat_transv_ant L', 'S_collat_transv_ant R', 'S_collat_transv_post L', 'S_collat_transv_post R', 'S_front_inf L', 'S_front_inf R', 'S_front_middle L', 'S_front_middle R', 'S_front_sup L', 'S_front_sup R', 'S_interm_prim-Jensen L', 'S_interm_prim-Jensen R', 'S_intrapariet_and_P_trans L', 'S_intrapariet_and_P_trans R', 'S_oc-temp_lat L', 'S_oc-temp_lat R', 'S_oc-temp_med_and_Lingual L', 'S_oc-temp_med_and_Lingual R', 'S_oc_middle_and_Lunatus L', 'S_oc_middle_and_Lunatus R', 'S_oc_sup_and_transversal L', 'S_oc_sup_and_transversal R', 'S_occipital_ant L', 'S_occipital_ant R', 'S_orbital-H_Shaped L', 'S_orbital-H_Shaped R', 'S_orbital_lateral L', 'S_orbital_lateral R', 'S_orbital_med-olfact L', 'S_orbital_med-olfact R', 'S_parieto_occipital L', 'S_parieto_occipital R', 'S_pericallosal L', 'S_pericallosal R', 'S_postcentral L', 'S_postcentral R', 'S_precentral-inf-part L', 'S_precentral-inf-part R', 'S_precentral-sup-part L', 'S_precentral-sup-part R', 'S_suborbital L', 'S_suborbital R', 'S_subparietal L', 'S_subparietal R', 'S_temporal_inf L', 'S_temporal_inf R', 'S_temporal_sup L', 'S_temporal_sup R', 'S_temporal_transverse L', 'S_temporal_transverse R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Destrieux';
    elseif atlas == '1'
        % Process: Scouts time series: [24 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Brodmann-thresh', {'BA1 L', 'BA1 R', 'BA2 L', 'BA2 R', 'BA3a L', 'BA3a R', 'BA3b L', 'BA3b R', 'BA44 L', 'BA44 R', 'BA45 L', 'BA45 R', 'BA4a L', 'BA4a R', 'BA4p L', 'BA4p R', 'BA6 L', 'BA6 R', 'MT L', 'MT R', 'V1 L', 'V1 R', 'V2 L', 'V2 R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Brodmann-thresh';
        
    else atlas == '2'
        % Process: Scouts time series: [68 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [1, 1], ...
            'scouts',         {'Desikan-Killiany', {'bankssts L', 'bankssts R', 'caudalanteriorcingulate L', 'caudalanteriorcingulate R', 'caudalmiddlefrontal L', 'caudalmiddlefrontal R', 'cuneus L', 'cuneus R', 'entorhinal L', 'entorhinal R', 'frontalpole L', 'frontalpole R', 'fusiform L', 'fusiform R', 'inferiorparietal L', 'inferiorparietal R', 'inferiortemporal L', 'inferiortemporal R', 'insula L', 'insula R', 'isthmuscingulate L', 'isthmuscingulate R', 'lateraloccipital L', 'lateraloccipital R', 'lateralorbitofrontal L', 'lateralorbitofrontal R', 'lingual L', 'lingual R', 'medialorbitofrontal L', 'medialorbitofrontal R', 'middletemporal L', 'middletemporal R', 'paracentral L', 'paracentral R', 'parahippocampal L', 'parahippocampal R', 'parsopercularis L', 'parsopercularis R', 'parsorbitalis L', 'parsorbitalis R', 'parstriangularis L', 'parstriangularis R', 'pericalcarine L', 'pericalcarine R', 'postcentral L', 'postcentral R', 'posteriorcingulate L', 'posteriorcingulate R', 'precentral L', 'precentral R', 'precuneus L', 'precuneus R', 'rostralanteriorcingulate L', 'rostralanteriorcingulate R', 'rostralmiddlefrontal L', 'rostralmiddlefrontal R', 'superiorfrontal L', 'superiorfrontal R', 'superiorparietal L', 'superiorparietal R', 'superiortemporal L', 'superiortemporal R', 'supramarginal L', 'supramarginal R', 'temporalpole L', 'temporalpole R', 'transversetemporal L', 'transversetemporal R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Desikan-Killiany';
        
    end
    
    listScout = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*\matrix_scout*']);
    AtlasROI = load([listScout(end).folder,'\',listScout(end).name]); %always load newest scouts
    
    prompt = {'Enter desired ROI:'}; %the number of the desired ROI must be looked up in AtlasROI.Atlas.Scouts
    dlgtitle = nameAtlas;
    dims = [1 50];
    s = inputdlg(prompt,dlgtitle,dims);
    ROI=str2num(s{1,1}); %turn the entered strings into an array of numbers
        
    TakeScout = AtlasROI.Atlas.Scouts(ROI(1)).Vertices; %store the index of all vertices in the ROI in TakeScout
    
    if length(ROI) > 1 %if more than one region was chosen, add the additional vertices to TakeScout
        for f = 2:length(ROI)
            TakeScout(end+1:end+length(AtlasROI.Atlas.Scouts(ROI(f)).Vertices)) = AtlasROI.Atlas.Scouts(ROI(f)).Vertices; %vertices of the current scout
        end
    end
    
    for  k= 1:size(TakeScout,2) %loop through every point of the scout
        
        IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
        IMAGEGRIDAMP(TakeScout(k),1)= 1; % set activity of one vertex to 1, the others stay 0
        
        F = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        G = zeros(1,1); %preallocate the gain of the ref
        G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
        G = mean(G);
        %IChannels is just a list from 1 to the number of used channels.
        %channels is the index of the specified channels in gain
        
        collect=[];
        index=1;
        
        for ch=1:length(channels)-1
            for b=ch+1:length(channels)
                collect(index)=(F(iChannels(ch),1)- G)- (F(iChannels(b),1)-G); %calc every combination of electrodes
                index=index+1;
            end
        end
        
        SourceDist.ImageGridAmp(TakeScout(k),1) = max(collect);
    end
    
    SourceDist.Comment= [nameResult,nameVar,'Grid, ROI = ',nameAtlas,' ',num2str(ROI)]; %set the name referring to the chosen grid & atlas
    
    SourceDist.ImageGridAmp = abs(SourceDist.ImageGridAmp); %take only absolute values
    [a,b] = maxk(SourceDist.ImageGridAmp(:,1),100); %find the 100 highest values
    c = find(a > 250); %find unrealisticly high vlaues
    SourceDist.ImageGridAmp(b(c),1)=0; % delete them
    
    ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
    db_add(ProtocolInfo.iStudy, SourceDist, []);

end

SourceGrid = nonzeros(SourceDist.ImageGridAmp);
%this is the collection of all scout values, but additionally, non-zero accounts for values deleted in the previous step

meanSource_c = mean(SourceGrid); %mean amp of the cEEGrid
stdSource_c = std(SourceGrid); %standard deviation

%save all relevant variables
if fullOrROI == '0'
    if ~exist([PATHOUT,'results_',nameResult,'\',SubjectName,'\'])
        mkdir([PATHOUT,'results_',nameResult,'\',SubjectName,'\']);
    end
    save([PATHOUT,'results_',nameResult,'\',SubjectName,'\results_',nameVar,'Grid_ROI_',nameAtlas,' ',num2str(ROI),'.mat'],'SourceDist','SourceGrid','nameAtlas','ROI');
else fullOrROI == '1'
    if ~exist([PATHOUT,'results_',nameResult,'\',SubjectName,'\'])
        mkdir([PATHOUT,'results_',nameResult,'\',SubjectName,'\']);
    end
    save([PATHOUT,'results_',nameResult,'\',SubjectName,'\results_',nameVar,'Grid_fullBrain.mat'],'SourceDist','SourceGrid','a','b');
end

for s = 1:length(full.ImageGridAmp)
diff (s,1)= 100 -(cee.ImageGridAmp(s) / full.ImageGridAmp(s) *100);
end

scatter(diff,cee.ImageGridAmp)
lsline
xlabel('amplitude loss fullcap-cEEGrid [%]')
ylabel('amplitude cEEGrid')

scatter(cee.ImageGridAmp,full.ImageGridAmp)
lsline
xlabel('amplitude cEEGrid')
ylabel('amplitude fullcap')

figure
histogram(full.ImageGridAmp)

figure
histogram(cee.ImageGridAmp)

figure
histogram(diff)