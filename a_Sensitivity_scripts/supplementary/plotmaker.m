


PATHFIG = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\';
load([PATHFIG,'Percentage_cap_cEEGrid_pair.mat']);

w = 1;
for f = 1:3:length(ResultsC)*3
xChange(w,1) = abs(PerceChange(f+1,1) - PerceChange(f+2,1));
w=w+1;
end
[maxChange val]= max(xChange)

outMatrix=zeros(10)

for  k=1:50
    a=str2num((ResultsC(k).Pair{1}(2:3)))

    b=str2num((ResultsC(k).Pair{1}(end-1:end)))
    outMatrix(a,b)=outMatrix(a,b)+1;
end

outMatrix(~triu(outMatrix,1)) = 1000
outMatrix(~triu(outMatrix,1)) = -0.5
outMatrix(~triu(outMatrix)) = -1
outMatrix(outMatrix > 999) = 0

figure
imagesc(outMatrix);
xticks(1:10)
xticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
yticks(1:10)
yticklabels({'L1','L2','L3','L4','L4a','L4b','L5','L6','L7','L8'})
set(gca,'fontweight','bold','fontsize',12);
xlabel('cEEGrid electrodes', 'FontSize', 12,'FontWeight','bold')
set(gcf,'color','w');
set(gcf,'color','w');
axis square



