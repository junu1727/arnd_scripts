%% create plots from the predefined UserScout

clear
close all;
clc

cyan        = [0.2 0.8 0.8];
brown       = [0.2 0 0];
orange      = [1 0.5 0];
blue        = [0 0.5 1];
green       = [0 0.6 0.3];
red         = [1 0.2 0.2];
yellow      = [1 1 0];
black       = [0 0 0];
white       = [1 1 1];
pink        = [1 0.5 0.5];
purple      = [.5 0 .5];
lightgreen  = [0.5 1 0.5];
colors = [cyan; brown; orange; blue; green; red; yellow; black; white; pink; purple; lightgreen];

UserScout = load('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_Destrieux_24.mat');

t=1;
for s = 1:2:length(UserScout.Scouts)
UserScout.Scouts(s).Color = colors(t,1:3);
UserScout.Scouts(s+1).Color = colors(t,1:3);
t=t+1;
end
UserScouts = UserScout.Scouts;

dropText = {'1/2','25/26','35/36','37/38','51/52','67/68','73/74','75/76','99/100','113/114','143/144','145/146'};
listResults = dir('C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_Diff\Diff_Double*');

for s = 1: length(listResults)
plotres(1,s) = load([listResults(s).folder,'\',listResults(s).name]);
PerceChange(1,s) = 100 - (100 / plotres(1,s).ResultsF.maxDiff) *  plotres(1,s).ResultsC.maxDiff;
PerceChangeOnly(1,s) = 100 - (100 / plotres(1,s).ResultsF.maxDiff) *  plotres(1,s).ResultsCRight.maxDiff;
end

[out,idx] = sort(PerceChange,'ascend');
[outRight,idxRight] = sort(PerceChangeOnly,'ascend');

t=1;
for s = 1:length(listResults)
color(s,1:3) = colors(idx(s),1:3);
end

figure, hold on
m = length(color);

for k = 1:length(out)
i = mod(k-1,m);
i = i+1;
bc = bar(k,out(k));
set(bc,'FaceColor',color(k,1:3))    
end
xticks(1:length(PerceChange))
xticklabels(dropText(idx))
ylim([0 105])
text(1:length(PerceChange),out,num2str(out'),'vert','bottom','horiz','center');
title('Signal-loss from FullCap(128Chan) to cEEgrid in %','FontSize',10);
set(gcf, 'Position', get(0, 'ScreenSize'));
saveas(gcf,'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\PPTdrop.png');
close all

figure, hold on
m = length(color);

for k = 1:length(outRight)
i = mod(k-1,m);
i = i+1;
bc = bar(k,outRight(k));
set(bc,'FaceColor',color(k,1:3))    
end
xticks(1:length(PerceChangeOnly))
xticklabels(dropText(idxRight))
ylim([0 105])
text(1:length(PerceChangeOnly),outRight,num2str(outRight'),'vert','bottom','horiz','center');
title('Signal-loss from FullCap(128Chan) to cEEgrid in %','FontSize',10);
set(gcf, 'Position', get(0, 'ScreenSize'));
saveas(gcf,'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\figures\PPTdropRight.png');
