%% Simulate recordings. Results from PreapreSource_cEEGrid
clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's06_cEEGrid128'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
SCOUTFILE = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_Surface_clustering_5V.mat';
nameScout = 'Surface clustering: 1_04';
brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = FullCap
fullOrROI = '0'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '1'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
atlas = '0'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux 
UserAtlas = '2'; % 1 if there is an additional, user generated atlas to choose from. If 2, use ONLY that atlas. If 0, don't use the UserAtlas
refStandard = [30 31];
refNew = 144; %if 0, it will be assumed the data is already referenced on a common electrode
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
predictors=load('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\PredictorsLeftAllElec.mat');

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);

    loadScout = load(SCOUTFILE); %definition of the scouts from an atlas. Only together with atlas = 'X'
    counter = 1;
    ScoutLabel = {loadScout.Scouts.Label};



        ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI
        
            ref = refNew; %index of the reference electrode for cEEGrid (in CM, E16)
            nameResult = 'SubjectName';
            if EarChannel == '1'
                channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
                nameVar = 'Left';
            elseif EarChannel == '2'
                channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
                nameVar = 'Right';
            else EarChannel == '0'
                channels=indexChan;
                nameVar = 'Double';
            end
        
        rightChannels = indexChan(length(indexChan)/2+1:end);
        
        listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
        listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
        listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);
        
        Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
        CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
        iChannels = 1:length(channels); %number of channels according to your channel file
        iChannelsRight = 1:length(rightChannels); %number of channels according to your channel file
        HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
        HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
        
        Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
        Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
        
        Source.Time = 1;
        Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
        SourceDist = Source; % source variable to work in
        SourceSimple = Source;
        IMAGEGRIDAMP=Source.ImageGridAmp; %This is the source/scout. It is currently only zeros
        
    
        for t = 1:length(keepClusters)
        SourceSimple.ImageGridAmp(keepClusters(t).Vertices)=ThetaInDegrees(t);  %same possible for SrcStrength
        end
        
        ProtocolInfo = bst_get('ProtocolInfo');
        db_add(ProtocolInfo.iStudy, SourceSimple, []);
       sqrStrength = sqrt(predictors.GLM.SrcStrength);