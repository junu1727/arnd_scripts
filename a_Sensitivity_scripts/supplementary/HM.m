% Script generated by Brainstorm (02-Dec-2019)

% Input files
sFiles = {...
    's14_hm/raw/data_200129_1351.mat'};

% Start a new report
bst_report('Start', sFiles);

% Process: Compute head model
sFiles = bst_process('CallProcess', 'process_headmodel', sFiles, [], ...
    'Comment',     '', ...
    'sourcespace', 2, ...  % MRI volume
    'volumegrid',  struct(...
    'Method',        'isotropic', ...
    'nLayers',       17, ...
    'Reduction',     3, ...
    'nVerticesInit', 1000, ...
    'Resolution',    0.005, ...
    'FileName',      []), ...
    'meg',         1, ...  %
    'eeg',         3, ...  % OpenMEEG BEM
    'ecog',        1, ...  %
    'seeg',        1, ...  %
    'openmeeg',    struct(...
    'BemSelect',    [1, 1, 1], ...
    'BemCond',      [1, 0.0125, 1], ...
    'BemNames',     {{'Scalp', 'Skull', 'Brain'}}, ...
    'BemFiles',     {{}}, ...
    'isAdjoint',    0, ...
    'isAdaptative', 1, ...
    'isSplit',      0, ...
    'SplitLength',  4000));

% Save and display report
ReportFile = bst_report('Save', sFiles);
bst_report('Open', ReportFile);
% bst_report('Export', ReportFile, ExportDir);

