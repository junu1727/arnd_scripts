%% Simulate recordings. Results from PreapreSource_cEEGrid
clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's03_temp'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
SCOUTFILE = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_left_06_50.mat';
nameScout = 'left_06';
brainstorm
cEEGrid = {'1', '0'}; % 1 = cEEGrid, 0 = FullCap
fullOrROI = '0'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '0'; %1 = left, 2 = right, 0 = both (choose which side of sensors should be considered. For fullCap, it is always all electrodes)
atlas = '0'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux
UserAtlas = '2'; % 1 if there is an additional, user generated atlas to choose from. If 2, use ONLY that atlas. If 0, don't use the UserAtlas
refStandard = [30 31];
refNew = 144; %if 0, it will be assumed the data is already referenced on a common electrode
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

listNewChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\channel*']);
export_matlab([listNewChannel(end).folder,'\',listNewChannel(end).name],'ReadyCap');
indexChan = find(strcmp({ReadyCap.Channel.Comment}, 'NewChannels')==1);

if UserAtlas == '1' || UserAtlas == '2' % the loop is only neccesarry when going trough the hand-picked scouts. Therefore, if any other atlas is chosen, make the loop p = 1
    loadScout = load(SCOUTFILE); %definition of the scouts from an atlas. Only together with atlas = 'X'
    counter = 1;
    ScoutLabel = {loadScout.Scouts.Label};
else
    counter = 1;
end

for s = 1
    for j = 1: length(loadScout.Scouts)
        % Load and prepare the sourceModel
        
        ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol. ALWAYS click the nedded protocol AND subject in the GUI
        
        if s == 1
            ref = refNew; %index of the reference electrode for cEEGrid (in CM, E16)
            nameResult = 'SubjectName';
            if EarChannel == '1'
                channels=indexChan(1:length(indexChan)/2); %the consecutive numbering of the cEEGrid starts at the left ear, upper end...
                nameVar = 'Left';
            elseif EarChannel == '2'
                channels=indexChan(length(indexChan)/2+1:end); %...and ends on the right ear, lower end
                nameVar = 'Right';
            else EarChannel == '0'
                channels=indexChan;
                nameVar = 'Double';
            end
            
        else s == 2
            ref = refStandard; %index of the reference electrode for 64ChanCap (in CM, E053)
            nameResult = 'fullCap';
            channels=1:indexChan(1)-1;
            nameVar = 'Full';
        end
        
        rightChannels = indexChan(length(indexChan)/2+1:end);
        
        listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\results_MN*']); %lists of the variables created in PrepareSource_cEEGrid.mat
        listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']); %raw s.t. starts with R or r
        listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\headmodel*']);
        
        Source = load([listSource(1).folder,'\',listSource(1).name]); %load the source template that was created in PrepareSource_cEEGrid...
        CM = load([listChannel(end).folder,'\',listChannel(end).name]); %...and the channelfile
        iChannels = 1:length(channels); %number of channels according to your channel file
        iChannelsRight = 1:length(rightChannels); %number of channels according to your channel file
        HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]); %...and the headmodel
        HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)
        
        Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
        Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things
        
        Source.Time = 1;
        Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
        SourceDist = Source; % source variable to work in
        SourceSimple = Source;
        IMAGEGRIDAMP=Source.ImageGridAmp; %This is the source/scout. It is currently only zeros
        
        %calculate the sensitivity for a source (ROI from atlas) for the cEEGrids or fullCap
        
        sFiles = [];
        sFiles = {...
            [listSource(1).folder,'\',listSource(1).name]};
        
        % Start a new report
        bst_report('Start', sFiles);
        sProcess = bst_process('GetInputStruct', sFiles);
        [sSubject,iSubject] = bst_get('Subject', sProcess.SubjectFile); %gets the subject ID
        
        % Load the atlases
        
            % Process: Scouts time series
            sFilesUserScout = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
                'timewindow',     [], ...
                'scouts',         {nameScout, ScoutLabel}, ...
                'scoutfunc',      1, ...  % Mean
                'isflip',         1, ...
                'isnorm',         0, ...
                'concatenate',    1, ...
                'save',           1, ...
                'addrowcomment',  1, ...
                'addfilecomment', 1);
            
            nameAtlas = 'Clustering';
            
            listScout1 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*\matrix_scout*']);
            AtlasROI1 = load([listScout1(end).folder,'\',listScout1(end).name]); %always load newest scouts
  
        
        listScout2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*\matrix_scout*']);
        AtlasROI2 = load([listScout2(1).folder,'\',listScout2(1).name]); 
        
        % Determine the ROI
        TakeScout = loadScout.Scouts(j).Vertices  ; %turn the entered strings into an array of numbers
           
        % Calculate the best electrode pair
        
        IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
        IMAGEGRIDAMP(TakeScout,1)= 1; % set activity of one vertex to 1, the others stay 0
        SourceSimple.ImageGridAmp = IMAGEGRIDAMP;
        SourceSimple.Comment= [nameResult,nameVar,'GridUNIFORM, ',nameAtlas]; %set the name referring to the chosen grid & atlas
        
        SourceSimple.ImageGridAmp = abs(SourceSimple.ImageGridAmp); %take only absolute values
        [a,b] = maxk(SourceSimple.ImageGridAmp(:,1),100); %find the 100 highest values
        c = find(a > 250); %find unrealisticly high values
        SourceSimple.ImageGridAmp(b(c),1)=0; % delete them
        
        F = zeros(length(channels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannels,:) = HM.Gain(channels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        if refNew == 0
            G = 0;
        else
            G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
            G = mean(G); %only nessecary for the fullCap. Here, we take the average from the two electrodes closest to the mastoid. Does not hurt for 1 electrode, mean of 1 stays 1
            %IChannels is just a list from 1 to the number of used channels.
            %channels is the index of the specified channels in gain
        end
        
        collect=[];
        index=1;
        
        for ch=1:length(channels)-1
            for b=ch+1:length(channels)
                collect(index) = abs((F(iChannels(ch),1)- G)- (F(iChannels(b),1)-G)); %calc every combination of electrodes
                checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
                checkPair(index,2) = {CM.Channel(channels(ch)).Name};
                checkPair(index,3) = {CM.Channel(channels(b)).Name};
                SelectedValues(index,1) = F(iChannels(ch),1)- G;
                SelectedValues(index,2) = F(iChannels(b),1)- G;
                index=index+1;
            end
        end
        
        F = zeros(length(rightChannels), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannelsRight,:) = HM.Gain(rightChannels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        if refNew == 0
            G = 0;
        else
            G = HM.Gain(ref,:) * IMAGEGRIDAMP; %calc the gain of the reference electrode for fullCap or cEEGrid
            G = mean(G); %only nessecary for the fullCap. Here, we take the average from the two electrodes closest to the mastoid. Does not hurt for 1 electrode, mean of 1 stays 1
            %IChannels is just a list from 1 to the number of used channels.
            %channels is the index of the specified channels in gain
        end
        
       
            collect_right=[];
            index=1;
            for ch=1:length(rightChannels)-1
                for b=ch+1:length(rightChannels)
                    collect_right(index) = abs((F(iChannelsRight(ch),1)- G)- (F(iChannelsRight(b),1)-G)); %calc every combination of electrodes
                    checkPair_right(index,1) = {[CM.Channel(rightChannels(ch)).Name,' - ',CM.Channel(rightChannels(b)).Name]};
                    checkPair_right(index,2) = {CM.Channel(rightChannels(ch)).Name};
                    checkPair_right(index,3) = {CM.Channel(rightChannels(b)).Name};
                    SelectedValues_right(index,1) = F(iChannelsRight(ch),1)- G;
                    SelectedValues_right(index,2) = F(iChannelsRight(b),1)- G;
                    index=index+1;
                end
            end
            
            % Store the results in a variable
            
            SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsC(j).maxDiff =  max(collect);
            ResultsC(j).CheckPair = checkPair;
            ResultsC(j).CheckPair(:,4) = {'0'};
            ResultsC(j).CheckPair(collect==max(collect),4) = {'1'};
            ResultsC(j).Pair = checkPair(collect==max(collect),1);
            ResultsC(j).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            
            SelectedChannels_right = [checkPair_right(collect_right==max(collect_right),2) checkPair_right(collect_right==max(collect_right),3)];
            ResultsCRight(j).maxDiff =  max(collect_right);
            ResultsCRight(j).CheckPair = checkPair_right;
            ResultsCRight(j).CheckPair(:,4) = {'0'};
            ResultsCRight(j).CheckPair(collect_right==max(collect_right),4) = {'1'};
            ResultsCRight(j).Pair = checkPair_right(collect_right==max(collect_right),1);
            ResultsCRight(j).SelectedChannels = [checkPair_right(collect_right==max(collect_right),2) checkPair_right(collect_right==max(collect_right),3)];

        ProtocolInfo = bst_get('ProtocolInfo');
        db_add(ProtocolInfo.iStudy, SourceSimple, []);
        listResults = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_20*']);
        newDataFile = bst_simulation([listResults(end).folder,'\',listResults(end).name]);
        
        listSimulation = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_simulation*']);
        
        
        listDelete =  dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\matrix_scout*']);
        listDelete2 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\results_2*']); %this needs to be smoother, in 2020 this won't work anymore
        listDelete3 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_simulation*']);
        listDelete4 = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\data_1*']);
        
        for w = 1:length(listDelete)
            isDeleted = file_delete( [listDelete(w).folder,'\',listDelete(w).name], 1, -1);
        end
        for w = 1:length(listDelete2)
            isDeleted = file_delete( [listDelete2(w).folder,'\',listDelete2(w).name], 1, -1);
        end
        for w = 1:length(listDelete3)
            isDeleted = file_delete( [listDelete3(w).folder,'\',listDelete3(w).name], 1, -1);
        end
        for w = 2:length(listDelete4)
            isDeleted = file_delete( [listDelete4(w).folder,'\',listDelete4(w).name], 1, -1);
        end
        
        db_reload_studies(sProcess.iStudy) %reload to get rid of the deleted files in the GUI
        
    end
end

if ~exist([PATHOUT,'results_Diff\UserAtlas\'])
    mkdir([PATHOUT,'results_Diff\UserAtlas\'])
end
save([PATHOUT,'results_Diff\UserAtlas\Diff_',nameVar,'_',nameAtlas,'_MEGA'],'ResultsC','ResultsCRight');
