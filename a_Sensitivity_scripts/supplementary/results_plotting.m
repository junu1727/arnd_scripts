%% calc the results

export_matlab(Sim3DElec,'sim');

F = sim.F; 
collect=[];
index=1;
storeID = 1;
for ph = [1 length(F)/2 length(F)]
for ch=1:length(channels)-1
    for b=ch+1:length(channels)
        collect(index) = abs((F(channels(ch),ph))- (F(channels(b),ph))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
        checkPair(index,2) = {CM.Channel(channels(ch)).Name};
        checkPair(index,3) = {CM.Channel(channels(b)).Name};
        SelectedValues(index,1) = F(channels(ch),ph);
        SelectedValues(index,2) = F(channels(b),ph);
        index=index+1;
    end
end
FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:);

% Store the results in a variable
SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDir(storeID).maxDiff =  max(collect);
ResultsDir(storeID).CheckPair = checkPair;
ResultsDir(storeID).CheckPair(:,4) = {'0'};
ResultsDir(storeID).CheckPair(collect==max(collect),4) = {'1'};
ResultsDir(storeID).Pair = checkPair(collect==max(collect),1);
ResultsDir(storeID).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDir(storeID).SelectedValues = SelectedValues;
ResultsDir(storeID).SelectedValues(:,3) = 0;
ResultsDir(storeID).SelectedValues(collect==max(collect),3) = 1;
ResultsDir(storeID).FixPair = FixPair;
storeID = storeID + 1;
index=1;
end

listSim = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\raw\*simulation*']);
export_matlab([listSim(end).folder,'\',listSim(end).name],'sim');

F = sim.F; 
collect=[];
index=1;
storeID = 1;
for ph = [1 length(F)/2-1 length(F)]
for ch=1:length(channels)-1
    for b=ch+1:length(channels)
        collect(index) = abs((F(channels(ch),ph))- (F(channels(b),ph))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
        checkPair(index,2) = {CM.Channel(channels(ch)).Name};
        checkPair(index,3) = {CM.Channel(channels(b)).Name};
        SelectedValues(index,1) = F(channels(ch),ph);
        SelectedValues(index,2) = F(channels(b),ph);
        index=index+1;
    end
end
FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:);

% Store the results in a variable
SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsLoc(storeID).maxDiff =  max(collect);
ResultsLoc(storeID).CheckPair = checkPair;
ResultsLoc(storeID).CheckPair(:,4) = {'0'};
ResultsLoc(storeID).CheckPair(collect==max(collect),4) = {'1'};
ResultsLoc(storeID).Pair = checkPair(collect==max(collect),1);
ResultsLoc(storeID).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsLoc(storeID).SelectedValues = SelectedValues;
ResultsLoc(storeID).SelectedValues(:,3) = 0;
ResultsLoc(storeID).SelectedValues(collect==max(collect),3) = 1;
ResultsLoc(storeID).FixPair = FixPair;
storeID = storeID + 1;
index=1;
end

export_matlab(Sim3DElec,'sim');

F = sim.F; 
collect=[];
index=1;
storeID = 1;
for ph = [1 length(F)/2 length(F)]
for ch=1:length(channels)-1
    for b=ch+1:length(channels)
        collect(index) = abs((F(channels(ch),ph))- (F(channels(b),ph))); %calc every combination of electrodes
        checkPair(index,1) = {[CM.Channel(channels(ch)).Name,' - ',CM.Channel(channels(b)).Name]};
        checkPair(index,2) = {CM.Channel(channels(ch)).Name};
        checkPair(index,3) = {CM.Channel(channels(b)).Name};
        SelectedValues(index,1) = F(channels(ch),ph);
        SelectedValues(index,2) = F(channels(b),ph);
        index=index+1;
    end
end
FixPair(1,:) = SelectedValues(strcmp(checkPair(:,1),'E03 - E08'),:); %choose two electrode pairs that are in the horizontal and vertical
FixPair(2,:) = SelectedValues(strcmp(checkPair(:,1),'E01 - E04'),:);

% Store the results in a variable
SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDist(storeID).maxDiff =  max(collect);
ResultsDist(storeID).CheckPair = checkPair;
ResultsDist(storeID).CheckPair(:,4) = {'0'};
ResultsDist(storeID).CheckPair(collect==max(collect),4) = {'1'};
ResultsDist(storeID).Pair = checkPair(collect==max(collect),1);
ResultsDist(storeID).SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
ResultsDist(storeID).SelectedValues = SelectedValues;
ResultsDist(storeID).SelectedValues(:,3) = 0;
ResultsDist(storeID).SelectedValues(collect==max(collect),3) = 1;
ResultsDist(storeID).FixPair = FixPair;
storeID = storeID + 1;
index=1;
end

save([PATHOUT,'LocNOrient.mat'],'ResultsDir','ResultsLoc','ResultsDist');
