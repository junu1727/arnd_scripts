%% Let's assume an 8 times 8 cm grid
x = 0:0.1:9;
y = 0:1:8;
[X,Y] = meshgrid(x,y);
resultGrid=ones(size(X));
D=ones(size(X));
% the dipoles can be located throughout the grid
L2  =  [X(1,10) Y(1,10)];
L7  =  [X(1,80) Y(1,80)]
 
% we assume at each point a source with strength 1
sour=-100;
 
for k=1:numel(X)
    G2=[X(k) Y(k)]
    d1=sqrt(sum((L2' - G2') .^ 2));
    d2=sqrt(sum((L7' - G2') .^ 2));
    resultGrid(k)=-sour.*(1./d1.^2)-sour.*(1./d2.^2);
    D(k)=d1;
end
%resultGrid(abs(resultGrid)>10)=NaN
figure
subplot(2,1,1)
surf(X,Y,(resultGrid))
xlim([4 6])
zlim([0 20])
 
subplot(2,1,2)
plot(resultGrid(:,30))
hold all
plot(resultGrid(:,35))
plot(resultGrid(:,40))
plot(y,10*1./y.^2)


%% dist
g = 1;
for f = [3 6 9]
 X = [dip.Dipole(f).Loc';Ceelocsl(3,1:3)];
 distVer(g,1) =  sqrt(sum((dip.Dipole(f).Loc' - Ceelocsl(3,1:3)) .^ 2)); %pdist(X,'euclidean');
 X = [dip.Dipole(f).Loc';Ceelocsl(8,1:3)]; % line = dipole * column = electrode
 distVer(g,2) = sqrt(sum((dip.Dipole(f).Loc' - Ceelocsl(8,1:3)) .^ 2)); %pdist(X,'euclidean');
 g=g+1;
end
 
g = 1;
for f = [3 6 9]
 X = [dip.Dipole(f).Loc';Ceelocsl(7,1:3)];
 distHor(g,1) = pdist(X,'euclidean');
 X = [dip.Dipole(f).Loc';Ceelocsl(10,1:3)];
 distHor(g,2) = pdist(X,'euclidean');
 g=g+1;
end

g=1;
for f = 1:length(Ceelocsl)
 X = [dip.Dipole(9).Loc';Ceelocsl(f,1:3)];
 distAll(g,1) = pdist(X,'euclidean');
 g=g+1;
end

%pdist([dip.Dipole(3).Loc';dip.Dipole(6).Loc';dip.Dipole(9).Loc'],'euclidean');

y = [distVer(1:3,1)'; distVer(1:3,2)';distHor(1:3,1)';distHor(1:3,2)'];
figure 
b = bar(y)
title('distances electrodes to 3 sources')
xticks([1 2 3 4])
xticklabels({'E03','E08','E07','E10'})

y = [abs(distVer(1,1)-distVer(2,1)) abs(distVer(2,1)-distVer(3,1));abs(distVer(1,2)-distVer(2,2)) abs(distVer(2,2)-distVer(3,2))];
figure 
b = bar(y)
title('difference distances (1-2, 2-3)')
xticks([1 2])
xticklabels({'E03','E08'})
%% val

figure 
plot(distVer(1:3,2)',Fi(8,1:3))


y = [Fi(3,1:3); Fi(8,1:3); Fi(7,1:3);Fi(10,1:3)];
figure 
b = bar(y);
title('amp per electrode for 3 sources')
xticks([1 2 3 4])
xticklabels({'E03','E08','E07','E10'})

y = [abs(Fi(3,1)-Fi(3,2)) abs(Fi(3,2)-Fi(3,3));Fi(8,1)-Fi(8,2) Fi(8,2)-Fi(8,3)];
figure 
b = bar(y);
title('difference amps (1-2, 2-3)')
xticks([1 2])
xticklabels({'E03','E08'})

y = [Fi(3,1:3); Fi(8,1:3); Fi(1,1:3);Fi(4,1:3)];
figure 
b = bar(y)

y = [abs(Fi(3,1:3)- Fi(8,1:3)); abs(Fi(1,1:3)-Fi(4,1:3))];
figure 
b = bar(y);


AmpE03 = abs(Fi(3,1:3))
DistE03 = distVer(:,1)'

figure 
plot(absDist,absAmp)

AmpE08 = abs(Fi(8,1:3))
DistE08 = distVer(:,2)'

y = [abs(Fi(3,1)-Fi(8,1)) abs(Fi(7,1)-Fi(10,1));abs(Fi(3,2)-Fi(8,2)) abs(Fi(7,2)-Fi(10,2));abs(Fi(3,3)-Fi(8,3)) abs(Fi(7,3)-Fi(10,3)) ];
figure 
b = bar(y);
title('potential differences for 3 dipoles; both pairs')

%% val

figure 
plot(distVer(1:3,2)',Fi(8,1:3))


y = [x1(22,1) x2(22,1) x3(22,1); x1(22,2) x2(22,2) x3(22,2); x1(42,1) x2(42,1) x3(42,1); x1(42,2) x2(42,2) x3(42,2);];
figure 
b = bar(y);
title('amp per electrode for 3 sources')
xticks([1 2 3 4])
xticklabels({'E03','E08','E07','E10'})

y = [abs(Fi(3,1)-Fi(3,2)) abs(Fi(3,2)-Fi(3,3));Fi(8,1)-Fi(8,2) Fi(8,2)-Fi(8,3)];
figure 
b = bar(y);
title('difference amps (1-2, 2-3)')
xticks([1 2])
xticklabels({'E03','E08'})

y = [Fi(3,1:3); Fi(8,1:3); Fi(1,1:3);Fi(4,1:3)];
figure 
b = bar(y)

y = [abs(Fi(3,1:3)- Fi(8,1:3)); abs(Fi(1,1:3)-Fi(4,1:3))];
figure 
b = bar(y);


AmpE03 = abs(Fi(3,1:3))
DistE03 = distVer(:,1)'

figure 
plot(absDist,absAmp)

AmpE08 = abs(Fi(8,1:3))
DistE08 = distVer(:,2)'

y = [abs(Fi(3,1)-Fi(8,1)) abs(Fi(7,1)-Fi(10,1));abs(Fi(3,2)-Fi(8,2)) abs(Fi(7,2)-Fi(10,2));abs(Fi(3,3)-Fi(8,3)) abs(Fi(7,3)-Fi(10,3)) ];
figure 
b = bar(y);
title('potential differences for 3 dipoles; both pairs')

35.7*(1/(3/1.5)^2)
35.7*(1/(5.33 /4.31)^2)
35.7*(1/(6.53 /4.31)^2)
35.7*(1/(5.81 /4.68)^2)
35.7*(1/(7.01 /4.68)^2)


