clear;
close all;
clc

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubjectName = 's06_cEEGrid128'; %specify the name of the subject as will be used in the brainstorm GUI
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SurfaceClustering = load('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\scout_Surface_clustering__1_02_750.mat');
SurfaceClustering = SurfaceClustering.Scouts;
lengths = arrayfun(@(x) size(SurfaceClustering(x).Vertices,2), 1:numel(SurfaceClustering));
keepClusters = SurfaceClustering(lengths == 20);

listCM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*aw*\channel*']);
listHM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubjectName,'\*low_warped*']); %raw s.t. starts with R or r
CM = load([listCM(end).folder,'\',listCM(end).name]); 
HM = load([listHM(end).folder,'\',listHM(end).name]); 

s=1;
for f = 139:148
    meanR(s,1:3) = CM.Channel(f).Loc(1:3);
    s=s+1;
end

meanCoR(1) = mean(meanR(:,1));
meanCoR(2) = mean(meanR(:,2));
meanCoR(3) = mean(meanR(:,3));

HMVertices = HM.Vertices([keepClusters.Seed],1:3);

for s = 1: length([keepClusters.Seed])
check(s,1) = norm(HM.Vertices([keepClusters(s).Seed],1:3) - meanCoR);
end

s=1;
for f = 129:138
    meanL(s,1:3) = CM.Channel(f).Loc(1:3);
    s=s+1;
end

meanCoL(1) = mean(meanL(:,1));
meanCoL(2) = mean(meanL(:,2));
meanCoL(3) = mean(meanL(:,3));

HMVertices = HM.Vertices([keepClusters.Seed],1:3);

for s = 1: length([keepClusters.Seed])
check(s,2) = norm(HM.Vertices([keepClusters(s).Seed],1:3) - meanCoL);
end

for s = 1: length([keepClusters.Seed])
checkEnd(s,1) = max(check(s,1:2));
end

[c, cs] = tess_curvature(HM.Vertices, HM.VertConn, HM.VertNormals, 10, 1);


x = HM.Vertices([keepClusters(s).Vertices],1);
y = HM.Vertices([keepClusters(s).Vertices],2);
z = HM.Vertices([keepClusters(s).Vertices],3);

DM = [x, y, ones(size(z))]; 
% Design Matrix
B = DM\z;                                               
% Estimate Parameters
[X,Y] = meshgrid(linspace(min(x),max(x),50), linspace(min(y),max(y),50));
Z = B(1)*X + B(2)*Y + B(3)*ones(size(X));
R=5+2*(X-4)+4*(Y-2);
mesh(X,Y,R)
hold
quiver3(4,2,5,2,4,-1)
hold off
axis equal

[X,Y]=meshgrid(linspace(min(x),max(x),50), linspace(min(y),max(y),50));
R=0.05+0.02*(X-0.04)+0.04*(Y-0.02);
mesh(X,Y,R)
hold
quiver3(0.04,0.02,0.05,0.02,0.04,-0.01)
hold off
axis equal

%%%%%%%%%%%%%%%
meanNorm = mean(scalp.VertNormals([scoutEar.Vertices],1:3));

HMVertices = HM.VertNormals([SurfaceClustering(221).Vertices],1:3); % 170 165 3
HMMean(1,1) = mean(HMVertices(:,1));
HMMean(1,2) = mean(HMVertices(:,2));
HMMean(1,3) = mean(HMVertices(:,3));

cEEGridAngle = meanNorm;
ClusterAngle = HMMean;
CosTheta = dot(cEEGridAngle,ClusterAngle)/(norm(cEEGridAngle)*norm(ClusterAngle));
ThetaInDegrees = acosd(CosTheta)            
%%%%%%%%%%%%%%%       

[X,Y] = meshgrid(-10:1:10);
R=5+2*(X-4)+4*(Y-2);
t = mesh(X,Y,R);
hold
quiver3(4,2,5,2,4,-1)
hold off
axis equal

HMVertices = HM.Vertices([keepClusters(1).Vertices],1:3);

x = HMVertices(:,1);
y = HMVertices(:,2);
z = HMVertices(:,3);

scatter3(x,y,z)

HMVertices = HM.Vertices([SurfaceClustering(374).Vertices],1:3);

x = meanL(:,1);
y = meanL(:,2);
z = meanL(:,3);

scatter3(x,y,z)       
        
x = meanL(:,1);
y = meanL(:,2);
z = meanL(:,3);

DM = [x, y, ones(size(z))]; 
% Design Matrix
B = DM\z;                                               
% Estimate Parameters
[X,Y] = meshgrid(linspace(min(x),max(x),50), linspace(min(y),max(y),50));
Z = B(1)*X + B(2)*Y + B(3)*ones(size(X));
R=5+2*(X-4)+4*(Y-2);
mesh(X,Y,R)
hold on
quiver3(4,2,5,2,4,-1)

axis equal        
%%%%%%%%%%%%%%%%%%
x = meanL(:,1);
y = meanL(:,2);
z = meanL(:,3);
x0 = min(x) ; x1 = max(x) ; nx = 500 ;
y0 = min(y) ; y1 = max(y) ; ny = 500 ;
xx = linspace(x0,x1,nx) ;
yy = linspace(y0,y1,ny) ;
[X,Y] = meshgrid(xx,yy) ;
Z = griddata(x,y,z,X,Y)  ;
[U,V,W] = surfnorm(X,Y,Z);
plot3(x,y,z,'mo')
hold on
mesh(X,Y,Z)
quiver3(X,Y,Z,U,V,W)
axis equal        
%%%%%%%%%%%%%%%%%%   
 
surfnorm(Z)
addpath('C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\Skripte\');
[normVec,V,p] = affine_fit(meanL);
     
t = 1;
for s = 129:138
Ceelocs(t,1:3) = CM.Channel(s).Loc;
t=t+1;
end

[k dist] = dsearchn(remeshScalp.Vertices,Ceelocs);
 
 
 
 
 
 
 
 
 
 
 