%% Show that the resulting angles actually exist in the brain

clear;
close all;
clc

%% Settings
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubHM = 's06_cEEGrid128'; %name of the subject from whom the parcellation will be taken
SubDA = 's12_dipole'; %name of the subject from whom the dipoles will be taken
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\'; %store the results here
PATHFIG = 'C:\Users\arnd\Desktop\Martin\tempeltools\Arnd\figures_sim\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
listHM = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\anat\',SubHM,'\*low_warped*']); %raw s.t. starts with R or r
HM = load([listHM(end).folder,'\',listHM(end).name]);
load([PATHOUT,'dipole.mat'],'dip');

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VN = HM.VertNormals;
DA = [dip.Dipole(1:3).Amplitude];
w=1;
for j = 1: length(DA)
    for s = 1: length(VN)
        CosTheta(s,w) = dot(VN(s,:),DA(j,:))/(norm(VN(s,:))*norm(DA(j,:)));
        Degrees = acosd(CosTheta);
    end
    w=w+1;
end

minNorm = mink(Degrees(:,1),100);
min2 = mink(Degrees(:,2),100);
min3 = mink(Degrees(:,3),100);

[Face Vert]= tess_area(HM.Vertices, HM.Faces);
