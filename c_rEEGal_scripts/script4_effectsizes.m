%% script_4: effect sizes
%calculate effect sizes for each task/participant and save them in matrices
%find the top 10 channel combinations (max effect sizes in ERP time window)
%for each PP/each task

%some of the prepping for mass univariate toolbox


clear all; close all; clc;
%% paths
MAINPATH = 'O:\projects\alk_rEEGal\\';
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\');
addpath([EEGLABPATH,'eeglab2020_0']);
addpath([EEGLABPATH,'fieldtrip']);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
cd(MAINPATH);


%% general stuff
TASKS = {'a','b','c','d'};
SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56','serw12'};
%BOOT = 100; %100 seems to be too few iterations for mes function (gives Warning: number of bootstrap repetitions is not adequate - not bootstrapping; with

IGNORE = 1; %to ignore bad/bridged chans in calculations if wanted not sure if needed
CHAN = [1:31;1:31]';
ORDER = [1:20,30,21:29,31];


% for plotting
col1 = [0.9290 0.6940 0.1250]; %colours, idk I just kinda like these
col2 = [0.4940 0.1840 0.5560];
SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size


%%
ICA= 1;
if ICA == 1
    FILENAME = '_ICAcorrected';
    FN1 = '_preICA';
else
    FILENAME = '_noICA';
    FN1 = FILENAME;
end

load([MAINPATH, 'out\\badchans',FN1,'.mat'])


[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
%%
for t = 1:length(TASKS)
    
    %might need to adjust timepoints again
    if t == 1
        CONDS = {'attended', 'unattended'};
        TIMEWINDOW = [80,150];
    elseif t== 2 || t == 3
        CONDS = {'target', 'standard'};
    elseif t == 4
        CONDS = {'incongruous', 'congruous'};
        TIMEWINDOW = [300, 600];
    end
    
    if t == 2
        TIMEWINDOW = [115,225];
    elseif t == 3
        TIMEWINDOW = [300, 600];
    end
    
    PATHIN = [MAINPATH, 'out\\task_', TASKS{t},'\\epoched\\' ];
    PLOTS = [MAINPATH, 'plots\\task_', TASKS{t},'\\' ];
    
    if ~exist([PATHIN,'pre_bin\\'])
        mkdir([PATHIN,'pre_bin\\'])
    end
    PATHOUT = [PATHIN,'pre_bin\\'];
    
    comb = zeros(31);
    if isfile([PATHIN, TASKS{t}, '_effectsizes_all', FILENAME,'.mat'])
        load([PATHIN, TASKS{t}, '_effectsizes_all',FILENAME,'.mat'])
        a = length(effect_sizes_all);
    else
        effect_sizes_all = struct('SUBJ', {}, 'max10_eff', [],'effect_sizes',[], 'CHANNEL_COMBI', [],'max10_chan', [],'max10_ind',[],'data_cond1',[],...
            'data_cond2', [],'maxeff', []);
        a = 1;
    end
    for PP = 1:length(SUBJ)
        if isfile([PATHIN, SUBJ{PP},'_',TASKS{t}, '_',CONDS{1},FILENAME,'.set'])&sum(strcmp({effect_sizes_all.SUBJ},SUBJ{PP}))==0
            for i = 1:length(CONDS)
                %% load data
                EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t},'_',CONDS{i}, FILENAME, '.set'],'filepath',PATHIN);
                [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
            end
            
            
            %% effect size calculation bits
            
            %1: get bipolar channel combinations
            ind = length(ALLEEG);
            [cond1, channels] = mbl_bipolarchannelexpansion1(ALLEEG(ind-1),1); %target/attended/incongrous
            cond2 = mbl_bipolarchannelexpansion1(ALLEEG(ind),1); %standard/unattended/congrous
            
            c2 = cond2.data;  %target/attended/incongrous
            c1 = cond1.data; %standard/unattended/congrous
            
            
            %3: calculate  effect sizes
            effect_sizes = [];
            
            for c = 1:size(c1,1)
                co1 = squeeze(c1(c,:,:));  %target/attended/incongrous
                co2 = squeeze(c2(c,:,:));  %standard/unattended/congrous
                % effect_sizes{c} = mes(co2',co1','hedgesg','isDep',0, 'nBoot', BOOT); %from mes documentation: For almost all two-sample MES the order of the two samples to be compared matters. In general, X, the first input argument, is assumed to be the first or 'control' group -> so first input = standard/unattended/congrous
                effect_sizes{c} = mes(co2',co1','hedgesg','isDep',0); %from mes documentation: For almost all two-sample MES the order of the two samples to be compared matters. In general, X, the first input argument, is assumed to be the first or 'control' group -> so first input = standard/unattended/congrous
                disp([num2str(c),' of ',num2str(size(c1,1)),' done!'])
            end
            
            effect_size_matrix = [];
            for i=1:length(effect_sizes)
                effect_size_matrix(i,:) = effect_sizes{i}.hedgesg;
            end
            
            
            %effect sizes for all PP in one struct per task
            effect_sizes_all(a).SUBJ = SUBJ{PP};
            effect_sizes_all(a).effect_sizes = effect_size_matrix;
            chanind = vertcat(CHAN,channels);
            effect_sizes_all(a).CHANNEL_COMBI = {cond1.chanlocs.labels};
            
            
            %% find max 10 effect sizes per PP
            
            %in case bad/bridged channels are to be exluded ->
            %get list of channels to ignore (bridged/bad channels)
            if IGNORE == 1
                ignore =[];
                b = bridge(strcmp({bridge.SUBJ}, SUBJ{PP}) & strcmp({bridge.TASK}, TASKS{t})).LABELS;
                if ~isempty(b{1})
                    b = cellfun(@str2num,b);
                else
                    b = [];
                end
                ignore = unique([badchans(strcmp({badchans.SUBJ}, SUBJ{PP}) & strcmp({badchans.TASK}, TASKS{t})).LABELS, ...
                    b]);
                ignore(ignore>20) = ignore(ignore>20)-1; %in bad/bridged matrices the labels are saved but the bipolar channel function works with indices so everything >20 needs to be adjusted by -1
            end
            
            
            %% find top 10 channel combinations with largest effect sizes in ERP timewindow
            tmp = effect_sizes_all(a);
            time(1) = dsearchn(EEG.times', TIMEWINDOW(1));
            time(2) = dsearchn(EEG.times', TIMEWINDOW(2));
            effect_sizes_all(a).max10_eff = [];
            i = 1;
            while length(effect_sizes_all(a).max10_eff)<10
                [effect_sizes_all(a).max10_eff(i),ind] = max(max(abs(tmp.effect_sizes(:,time(1):time(2))),[],2)); %find max effect size in ERP timewindow
                effect_sizes_all(a).max10_chan{i} = tmp.CHANNEL_COMBI{ind}; %find which channel combi this corresponds to (important:not labels of channels (they are off after 20!)
                effect_sizes_all(a).max10_ind(i,:) = chanind(ind,:);
                chanind(ind,:) = [];
                tmp.effect_sizes(ind,:) = []; %delete max from matrix to find next maximum
                tmp.CHANNEL_COMBI(ind) = []; %delete channel combi of max effect size then find next maximum
                
                %exclude bad or bridged channels from max list -> delete
                %previously added entry for max10 channels if one/both of
                %the channels were either previously identified as bad or
                %bridged
                if sum(effect_sizes_all(a).max10_ind(i,1) == ignore)||sum(effect_sizes_all(a).max10_ind(i,2) == ignore)==0
                    i = i+1;
                end
            end
            
            %saves data from best channel combi in effect size struct also
            effect_sizes_all(a).data_cond1(:,:) = squeeze(cond1.data(strcmp({cond1.chanlocs.labels},effect_sizes_all(a).max10_chan(1)),:,:));
            effect_sizes_all(a).data_cond2(:,:) = squeeze(cond2.data(strcmp({cond2.chanlocs.labels},effect_sizes_all(a).max10_chan(1)),:,:));
            %save max effect sizes
            effect_sizes_all(a).maxeff(:,:) = effect_sizes_all(a).effect_sizes(strcmp({cond2.chanlocs.labels},effect_sizes_all(a).max10_chan(1)),:,:);
            
            
            %prepare some bits for MassUnivariate Toolbox
            cond1.setname = ([SUBJ{PP},'_',TASKS{t}, '_', CONDS{1}, '_best']);
            cond1.subject = (SUBJ{PP});
            cond1.data = cond1.data(strcmp({cond1.chanlocs.labels},effect_sizes_all(a).max10_chan(1)),:,:);
            cond1.nbchan = 1;
            cond1.chanlocs = cond1.chanlocs(strcmp({cond1.chanlocs.labels},effect_sizes_all(a).max10_chan(1)));
            cond1 = pop_editset(cond1, 'run', [], 'chanlocs', '[]','icaweights', '[]', 'icasphere', '[]', 'icachansind', '[]'); % cause when loading it again eeglab complains about sth
            cond1.chanlocs(1).labels = 'E01';
            
            cond2.setname = ([SUBJ{PP},'_',TASKS{t}, '_', CONDS{2}, '_best']);
            cond2.subject = (SUBJ{PP});
            cond2.data = cond2.data(strcmp({cond2.chanlocs.labels},effect_sizes_all(a).max10_chan(1)),:,:);
            cond2.nbchan = 1;
            cond2.chanlocs = cond2.chanlocs(strcmp({cond2.chanlocs.labels},effect_sizes_all(a).max10_chan(1)));
            cond2 = pop_editset(cond2, 'run', [],'chanlocs', '[]', 'icaweights', '[]', 'icasphere', '[]', 'icachansind', '[]'); % cause when loading it again eeglab complains about sth
            cond2.chanlocs(1).labels = 'E01';
            
            cond1 = pop_saveset(cond1, 'filename', [cond1.setname,FILENAME '.set'], 'filepath', PATHOUT);
            cond2 = pop_saveset(cond2, 'filename', [cond2.setname, FILENAME,'.set'], 'filepath', PATHOUT);
            a = a+1;
            
            times = EEG.times; %saves time info from last loaded EEG set to use for plotting in plotbestchans script
        end
        
    end
    
    save([PATHIN,TASKS{t},'_effectsizes_all',FILENAME,'.mat'], 'effect_sizes_all', 'channels', 'times'); %save effect sizes
end

cd([MAINPATH, 'scripts\\'])
open('script4b_plotbestchans.m')
open('script5_ERPmassunivariate.m')
