%% script_2:
% extract epochs for the two oddball tasks (b & c)
% artifact rejection using amplitude criterion & joint probability;
% saves data set for each condition for each task & participant
% plots ERPs for task b&c

clear all; close all; clc;

%% paths
MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
addpath([MAINPATH, 'eeglab2021.1\'])

cd(MAINPATH);

%% general stuff

TASKS = {'b','c'};
SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};
EVENTS = {'S 70', 'S 80'}; %event triggers
CONDS = {'Target', 'Standard'}; %labels for events

%epoching limits
EP_from = -.2; %epoch beginning
EP_to = .8; %epoch end
%baseline limit
BASE_from = -200;
%for articfact rejection
AMP = 100;
JT = 3;

%for plotting
PLOT = 0; %set to 1 if plots are wanted, set to 0 for no plots

col1 = [0.9290 0.6940 0.1250]; %colours, idk I just kinda like these
col2 = [0.4940 0.1840 0.5560];
SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size

ORDER = [1:3,32,4:9,33,10:12,34,13:15,35,16:17,36,37,18:20,38,21:22,39:40,23:25,41,26:28,42,29:31,43:44]; %to reorder data so the order on the plots matches that on the pad

ICA= 1;
if ICA == 1
    FILENAME = '_ICAcorrected';
else 
    FILENAME = '_noICA';
end

%% loop through tasks/participants
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for t = 1:length(TASKS)
        
        PATHIN = [MAINPATH, 'out\\task_', TASKS{t}, '\\'];
        if ~exist([MAINPATH, 'plots\\task_', TASKS{t}])
            mkdir([MAINPATH, 'plots\\task_', TASKS{t}])
        end
        if ~exist([PATHIN, 'epoched\\'])
            mkdir([PATHIN, 'epoched\\'])
        end
        PLOTS = [MAINPATH, 'plots\\task_', TASKS{t}, '\\'];
        PATHOUT = [PATHIN, 'epoched\\'];

    for PP = 1:length(SUBJ)
        
        %load data
        if isfile([PATHIN, SUBJ{PP},'_',TASKS{t},FILENAME, '.set'])
        EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t}, FILENAME,'.set'],'filepath',[PATHIN]);
        [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
        
        
        
        %first couple of standard tones -> change trigger from s180 to s80
        for i = 1:length(EEG.event)
            if strcmp({EEG.event(i).type}, 'S180')
                EEG.event(i).type = 'S 80';
            end
        end
        [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
        
        %% epoching
        ind = length(ALLEEG);
        for i = 1:length(EVENTS)
            EEG = pop_epoch( ALLEEG(ind), EVENTS(i), [EP_from EP_to], 'epochinfo', 'yes');
            %baseline removal
            EEG = pop_rmbase( EEG, [BASE_from 0]);
            
            %artifact rejection 
            %EEG = pop_eegthresh(EEG,1,[1:size(EEG.data,1)] ,-AMP,AMP,-0.2,0.799,0,1);
            EEG = pop_jointprob(EEG,1,[1:size(EEG.data,1)],JT, JT, 0, 1, 0,0);
            
            EEG.setname = ([SUBJ{PP},'_',TASKS{t},'_', CONDS{i}]);
            EEG = pop_saveset(EEG, 'filename', [EEG.setname,FILENAME, '.set'], 'filepath', PATHOUT);
            
            [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
        end
        
        eeglab redraw;
        
        
        %only runs next part if you want to plot it, otherwise just
        %epoching and saving datasets for each condition 
        if PLOT == 1

        %% ERPs
        ERP = [];
        ind = length(ALLEEG);
        ERP(1,:,:) = mean(ALLEEG(ind-1).data,3); %mean target trials
        ERP(2,:,:) = mean(ALLEEG(ind).data,3);   %mean standard trials
        ERP(3,:,:) = ERP(2,:,:)-ERP(1,:,:); %difference standard-target
        
        GFP(1,:) = std(mean(ALLEEG(ind-1).data,3));
        GFP(2,:) = std(mean(ALLEEG(ind).data,3));
        
        %ordering data by how the electrodes are organised on the pad ->
        ERP1 = ERP;
        ERP1(:,size(ERP1,2)+1:length(ORDER),:) = nan;
        ERP1 = ERP1(:,ORDER,:); %reorder stuff to match order of electrodes on the pads
        
        ERP_ALL(PP,:,:,:) = ERP1;
        %% Plotting
        
        %1: plot just using normal matlab code
        
        ax = ceil(max(abs(ERP(:)))); %for axis limits
         
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        set(0,'DefaultAxesFontSize',10)
        set(0, 'DefaultAxesLineWidth', 1)
        set(0,'DefaultAxesTitleFontWeight','normal');
        for i = 1:size(ERP1,2)
            subplot(11,4,i)
            plot(EEG.times, squeeze(ERP1(1,i,:)),'LineWidth', .7, 'color', col1); %plot target trials
            hold on
            plot(EEG.times,squeeze(ERP1(2,i,:)),'LineWidth', .7, 'color', col2); %plot standard trials
            plot(EEG.times,squeeze(ERP1(3,i,:)),'LineWidth', .7, 'color', 'k'); %difference
            
            ylim([-ax, ax]);
            yticks([-ax 0 ax])
            xlim([-200 800])
            box off;
            axis off;
        end
        axis on;
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        legend('target', 'standard', 'difference (standard - target)', 'Orientation', 'horizontal','Location', 'best', 'Box', 'off')
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{t},FILENAME,'.png'])
        close(gcf)
        
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        set(0,'DefaultAxesFontSize',10)
        set(0, 'DefaultAxesLineWidth', 1)
        set(0,'DefaultAxesTitleFontWeight','normal');
        subplot(2,1,1)
        plot(EEG.times, squeeze(ERP(1,:,:)), 'LineWidth', .5, 'color', 'k')
        hold on
        plot(EEG.times, GFP(1,:),'LineWidth', 2, 'color', col1)
        hold off
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        ylim([-ax, ax]);
        title(CONDS{1})
        subplot(2,1,2)
        plot(EEG.times, squeeze(ERP(2,:,:)), 'LineWidth', .5, 'color', 'k')
        hold on
        plot(EEG.times, GFP(2,:),'LineWidth', 2, 'color', col1)
        hold off
        title(CONDS{2})
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        ylim([-ax, ax]);
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{t},FILENAME,'_GFP.png'])
        close(gcf)
     
        STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

%% 2: or plot it with eeglab      
        [STUDY ALLEEG] = std_editset( STUDY, [], 'commands',{{'index',1,'load',[PATHOUT,SUBJ{PP},'_',TASKS{t},'_', CONDS{1}, '.set']},{'index',2,'load',[PATHOUT,SUBJ{PP},'_',TASKS{t},'_', CONDS{2}, '.set']},{'index',1,'subject','1'},{'index',2,'subject','1'},{'index',1,'condition',CONDS{1}},{'index',2,'condition',CONDS{2}}},'updatedat','on','rmclust','on');
        CURRENTSTUDY = PP;
        EEG = ALLEEG;
        CURRENTSET = 1:length(EEG);
        [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'savetrials','on','erp','on','spec','on','specparams',{'specmode','fft','logtrials','off'},'erpim','on','erpimparams',{'nlines',10,'smoothing',10});
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, 'multiplotER', 'erp' ,{ALLEEG(1).chanlocs.labels},[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{t},FILENAME,'2.png'])
        close(gcf)
        

        end
        STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

        end
    end
end
   
