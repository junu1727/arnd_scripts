%% script_0 find bridges & bad channels + part of preprocessing
%import, filter & change sampling rate for all tasks & participants
%find bridges and bad channels in all tasks
%interpolate bad chans
%save datasets (only those with fewer than 10 bridges)
%save as bridges/badchans matlab file and as .csv table
%plot overview of bridges/bad chans

%% paths

MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
addpath('C:\Users\arnd\Desktop\toolboxes_matlab\');
PATHIN = [MAINPATH, 'raw_data\\'];
channellocations = [MAINPATH, 'alk31.xyz'];
cd(MAINPATH);

PLOTS = [MAINPATH, 'plots\\'];

%% general stuff
TASKS = {'a','b','c','d','artifact'};

TASK_NAMES = {'Directional Hearing', 'Passive Oddball', 'Active Oddball', 'Sentence Congruity'};
SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};

ICA = 1; 
if ICA == 1
    FILENAME = '_preICA';
    FILT = [1, 30]; %filter limits -> for ICA.
else 
    FILENAME = '_noICA';
        FILT = [.3,30]; %if ica is skipped
end

SAMP = 100; %for resampling

SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size

%so I can just add newer subject without running the entire thing on
%everyone again
if isfile([MAINPATH,'out\\badchans',FILENAME,'.mat'])
    load([MAINPATH,'out\\badchans', FILENAME,'.mat'])
    b = length(badchans)+1;
else
    badchans = struct('SUBJ', [], 'TASK', [],'COUNT', [], 'LABELS',[] );
    b = 1;
end
%% loop through tasks/participants
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for t = 1:length(TASKS)
    if ~exist([MAINPATH,'out\\task_', TASKS{t},'\\'])
        mkdir([MAINPATH,'out\\task_', TASKS{t},'\\'])
    end
    PATHOUT = [MAINPATH,'out\\task_', TASKS{t},'\\']; %different folder for each task
    
    for PP = 1:length(SUBJ)
        %in case any tasks were left out and so I can just add newer subject without running the entire thing on
        %everyone again
        if isfile([PATHIN, SUBJ{PP},'_',TASKS{t}, '.vhdr'])& sum(strcmp({badchans.SUBJ},SUBJ{PP}) & strcmp({badchans.TASK},TASKS{t}))==0

            %% import data
            EEG = pop_fileio([PATHIN, SUBJ{PP},'_',TASKS{t}, '.vhdr'],'dataformat','auto');
            
            %add channel locations
            EEG = pop_chanedit(EEG, 'load',{channellocations,'filetype','autodetect'},'delete',21); %21 is still in the channel location file so needs to be deleted or channel number doesn't match
            EEG = pop_cEEGrid_rename( EEG, struct('ch_31',{struct('channelnames',{{...
            {'1'},{'2'},{'3'},{'4'},{'5'},{'6'},{'7'},{'8'},{'9'},{'10'},{'11'},{'12'},...
            {'13'},{'14'},{'15'},{'16'},{'17'},{'18'},{'19'},{'20'},{'22'},{'23'},{'24'},...
            {'25'},{'26'},{'27'},{'28'},{'29'},{'30'},{'21'},{'32'}}},'types',...
            {{'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''}})}), 0, '' );            
            [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off');
            EEG.setname = ([SUBJ{PP},'_',TASKS{t}]);
            EEG = pop_saveset(EEG, 'filename', [EEG.setname,'.set'], 'filepath', PATHOUT);
            
            %cut jaw bit from artifact set
            if strcmp(TASKS{t}, 'artifact')
                c = find(strcmp({EEG.event.type}, 'S  1'), 1,'first');
                d = find(strcmp({EEG.event.type}, 'S  2'), 1,'last');
                del = [EEG.event(c).latency-EEG.srate,EEG.event(d).latency-EEG.srate];
                EEG = pop_select( EEG, 'point',[del(1) del(2)] );
            end
            
            %filter
            EEG = pop_eegfilt( EEG, FILT(1), 0, [], [0], 0, 0, 'fir1', 0); %high pass
            EEG = pop_eegfilt( EEG, 0, FILT(2), [], [0], 0, 0, 'fir1', 0); %low pass
            
            %id bridge
            a =  eBridge(EEG, 'PlotMode', 0);
            bridge(b).SUBJ = SUBJ{PP};
            bridge(b).TASK = TASKS{t};
            bridge(b).COUNT = a.Bridged.Count;
            bridge(b).LABELS = a.Bridged.Labels;
            bridge(b).PAIRS = a.Bridged.Pairs;
            
            %change sampling rate
            EEG = pop_resample(EEG, SAMP);
            
            %id bad channels & interpolate them
            tmp = pop_clean_rawdata(EEG, 'FlatlineCriterion',5,'ChannelCriterion',0.8,'LineNoiseCriterion',4,'Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );
            
            badchans(b).SUBJ = SUBJ{PP};
            badchans(b).TASK = TASKS{t};
            if length(tmp.chaninfo.removedchans) == 0
                badchans(b).LABELS = nan;
            else
                badchans(b).LABELS =  [cellfun(@str2num,{tmp.chaninfo.removedchans.labels})];
            end
            badchans(b).COUNT = length(tmp.chaninfo.removedchans);
            b = b+1;
           
            %interpolate and save data sets only if bridges <10
            if bridge((strcmp({bridge.SUBJ}, SUBJ{PP})&strcmp({bridge.TASK},TASKS{t}))).COUNT<10
                %interpolate bad chans
                if badchans(strcmp({badchans.SUBJ}, SUBJ{PP})&strcmp({badchans.TASK},TASKS{t})).COUNT > 0
                    aa = unique([badchans(strcmp({badchans.SUBJ}, SUBJ{PP}) & strcmp({badchans.TASK}, TASKS{t})).LABELS,...
                         str2double(bridge(strcmp({badchans.SUBJ}, SUBJ{PP}) & strcmp({badchans.TASK}, TASKS{t})).LABELS)]); %cause interpolate works with indices rather than labels, so if it's > 21, it's always one off
                    aa(aa>20) = aa(aa>20)-1; %adjust indices
                    EEG = pop_interp(EEG, [aa], 'spherical');
                end
               
                   EEG.setname = ([SUBJ{PP},'_',TASKS{t}]);
                
                EEG = pop_saveset(EEG, 'filename', [EEG.setname,FILENAME, '.set'], 'filepath', PATHOUT);
                [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
                eeglab redraw;
            end
        end
    end
end

   str2double(bridge(strcmp({badchans.SUBJ}, SUBJ{PP}) & strcmp({badchans.TASK}, TASKS{t})).LABELS)



%% plotting bad chans/ bridged chans

%load random data set -> doesn't matter which one
EEG = pop_loadset('filename','nvjr34_artifact.set','filepath',PATHOUT);
EEG = pop_cEEGrid_rename( EEG, struct('ch_31',{struct('channelnames',{{{'1'},{'2'},{'3'},{'4'},{'5'},{'6'},{'7'},{'8'},{'9'},{'10'},{'11'},{'12'},{'13'},{'14'},{'15'},{'16'},{'17'},{'18'},{'19'},{'20'},{'22'},{'23'},{'24'},{'25'},{'26'},{'27'},{'28'},{'29'},{'30'},{'21'},{'31'}}},'types',{{'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''}})}), 0, '' );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off');
ord = 1:32;
ord(21) = 31;
ord(31) = [];
exclude = [bridge.COUNT]<10;
for ex = 1:2
figure;
set(gcf,'Position', [0,0, SZ(3), SZ(4)])
set(0,'DefaultAxesFontSize',10)
set(0, 'DefaultAxesLineWidth', 1)
set(0,'DefaultAxesTitleFontWeight','normal');
t1 = 1;
t2 = 2;
mean_bridged = [];
mean_bad = [];
if ex == 1
    br = bridge; 
    ba = badchans; 
else 
    br = bridge(exclude); 
    ba = badchans(exclude); 
end
for t = 1:length(TASKS)-1
   %bridged channels -> per task: how many (mean), which ones, how many
   %different ones, how often are the same channels involved in bridges
   mean_bridged(t).MEAN =  mean([br(strcmp({br.TASK},TASKS{t})).COUNT]);
   mean_bridged(t).TASK = TASKS{t};
   b = [br(strcmp({br.TASK},TASKS{t})).LABELS];
   b = cellfun(@str2num,b(~cellfun(@isempty, b)));
   mean_bridged(t).CHANS(1,:) = unique(b);
   mean_bridged(t).TOTAL = length(mean_bridged(t).CHANS);
   mean_bridged(t).CHANS(2,:) = countcats(categorical(b));
   
   %bad channels -> per task: how many are bad (mean), which ones, how many different ones and how often are the same channels bad
   mean_bad(t).MEAN =  mean([ba(strcmp({ba.TASK},TASKS{t})).COUNT]);
   mean_bad(t).TASK = TASKS{t};
   mean_bad(t).CHANS(1,:) = unique([ba(strcmp({ba.TASK},TASKS{t})).LABELS]);
   mean_bad(t).CHANS(:,isnan(mean_bad(t).CHANS(1,:))) = [];
   mean_bad(t).TOTAL = length(mean_bad(t).CHANS);
   mean_bad(t).CHANS(2,:) = countcats(categorical([ba(strcmp({ba.TASK},TASKS{t})).LABELS]));
   
   %plot occurence of bad channels
    subplot(4,2,t1);
    A = zeros(1,32);
    A(mean_bad(t).CHANS(1,:))= mean_bad(t).CHANS(2,:);
    A(21)=A(31);
    A(31) = [];
    bar(A)
    xticks(1:31)
    xticklabels(ord)
    title({['Bad Channels, ', TASK_NAMES{t}];['(Mean Nr. of Bad Channels: ', num2str(round(mean_bad(t).MEAN,2)), ')']})
   
    %plot occurence of bridged channels
    subplot(4,2,t2);
    A = zeros(1,32);
    A(mean_bridged(t).CHANS(1,:))= mean_bridged(t).CHANS(2,:);
    A(21)=A(31);
    A(31) = [];
    bar(A)
    xticks(1:31)
    xticklabels(ord)
    title({['Bridged Channels, ', TASK_NAMES{t}];['(Mean Nr. of Bridged Channels: ', num2str(round(mean_bridged(t).MEAN,2)), ')']})
    t1 = t1+2;
    t2 = t2+2;
end
    saveas(gcf,[PLOTS,'badchans', num2str(ex),FILENAME,'.png'])
    
for t = 1:length(TASKS)-1
    
   %plot occurence of bad channels
    A = zeros(1,32);
    A(mean_bad(t).CHANS(1,:))= mean_bad(t).CHANS(2,:);
    EEG.data = [];
    EEG.data(:,1:3) = repmat(A',1,3);
    EEG.data(21,:) =[];
    
    pop_cEEGrid_topoplot(EEG,[],[],1, 'side', 'layout_reegal_new2', 'marker', 'labels', 'colorbar', 'yes');
    caxis([0 20])
    colormap(jet(10))
    title({['Bad Channels, ', TASK_NAMES{t}];['(Mean Nr. of Bad Channels: ', num2str(round(mean_bad(t).MEAN,2)), ')']}, 'FontSize', 15)
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    set(0,'DefaultAxesFontSize',10)
    set(0, 'DefaultAxesLineWidth', 1)
    set(0,'DefaultAxesTitleFontWeight','normal');
    saveas(gcf,[PLOTS,'task_',TASKS{t},'_badchans', num2str(ex),FILENAME,'.png'])
    close(gcf)

    %plot occurence of bridged channels
    A = zeros(1,32);
    A(mean_bridged(t).CHANS(1,:))= mean_bridged(t).CHANS(2,:);
    EEG.data = [];
    EEG.data(:,1:3) = repmat(A',1,3);
    EEG.data(21,:) =[];
    
    pop_cEEGrid_topoplot(EEG,[],[],1, 'side', 'layout_reegal_new2', 'marker', 'labels', 'colorbar', 'yes');
    caxis([0 20])
    colormap(jet(10))
    
    title({['Bridged Channels,', TASK_NAMES{t}];['(Mean Nr. of Bridged Channels: ', num2str(round(mean_bridged(t).MEAN,2)), ')']}, 'FontSize', 15)
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    set(0,'DefaultAxesFontSize',10)
    set(0, 'DefaultAxesLineWidth', 1)
    set(0,'DefaultAxesTitleFontWeight','normal');
    saveas(gcf,[PLOTS,'task_',TASKS{t},'_bridges', num2str(ex),FILENAME, '.png'])
    close(gcf)
end    
end      

%sort variables with bridges & badchans by participant
[~,index] = sortrows({bridge.SUBJ}.'); bridge = bridge(index); clear index1
[~,index] = sortrows({badchans.SUBJ}.'); badchans = badchans(index); clear index1

%save badchans/bridges
save([MAINPATH, 'out\\badchans',FILENAME,'.mat'], 'badchans','bridge')

bridge = struct2table(bridge);
badchans = struct2table(badchans);

writetable(bridge,[MAINPATH, 'out\\bridge',FILENAME,'.csv'])
writetable(badchans,[MAINPATH, 'out\\badchans',FILENAME,'.csv'])


%opens next script
cd([MAINPATH, 'scripts'])
if ICA == 1
    open('script1_runICA.m')
else
    open('script2a_task_a')
    open('script2bc_task_bc')
    open('script2d_task_d')
end