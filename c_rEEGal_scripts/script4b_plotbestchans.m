clear all; close all; clc;
%% paths
MAINPATH = 'O:\projects\alk_rEEGal\\';
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\');
addpath([EEGLABPATH,'eeglab2020_0']);
addpath([EEGLABPATH,'fieldtrip']);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
cd(MAINPATH);
%% general stuff
TASKS = {'a','b','c','d'};
TASK_NAMES = {'Directional Hearing', 'Passive Oddball', 'Active Oddball', 'Sentence Congruity'};

ICA = 1;
if ICA == 1
    FILENAME = '_ICAcorrected';
else 
    FILENAME = '_noICA';
end
col1 = [0.9290 0.6940 0.1250]; %colours, idk I just kinda like these
col2 = [0.4940 0.1840 0.5560];
SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size
ORDER = [1:20,30,21:29,31];



for t = 1:length(TASKS)
    PATHIN = [MAINPATH, 'out\\task_', TASKS{t},'\\epoched\\' ];
    PLOTS = [MAINPATH, 'plots\\task_', TASKS{t},'\\' ];
    cd(PATHIN);

    load([TASKS{t},'_effectsizes_all',FILENAME,'.mat'])
    
     if t == 1
        CONDS = {'attended', 'unattended'};
    elseif t== 2 || t == 3
        CONDS = {'target', 'standard'};
    elseif t == 4
        CONDS = {'incongruous', 'congruous'};
    end
    
    %best channel
    ijw = [];
    ijw(:,1) = arrayfun(@(effect_sizes_all) effect_sizes_all.max10_ind(1,1),effect_sizes_all);
    ijw(:,2) = arrayfun(@(effect_sizes_all) effect_sizes_all.max10_ind(1,2),effect_sizes_all);
    ijw(:,3) = arrayfun(@(effect_sizes_all) effect_sizes_all.max10_eff(1),effect_sizes_all);
    ijw(ijw(:,1) == ijw(:,2),2) = 32;
    %if both indices in channel pair are the same -> original data had best effect size (so agianst reference);
    %replace one index in this pair with 32 since that's the index of the
    %reference in the layout.label struct which is used by the function to plot
    %connections
    
    %input:
    %nr of channels = 31
    %list of nodes w/ weigths ->
    % = matrix -> index electrode 1; index electrode 2; weight
    % -> should be able to get that from effect size script -> weight ->
    % convert effect sizes to values between 0 and 1
    
    %rest I still need to figure out
    
    nch = 32; %take care of this variable (nch must be according to matrix size you want to plot it) -> 32 because ref included as electrode
    %ijw = index1, index2, weight
    
    %'w_intact': Lines to plot weights remain intact as the third column of ijw
    %           'w_unity': Lines to plot weights span from the minimum weight to ONE
    %           'w_wn2wx': Lines to plot weights span from a minimum weight
    %           (wn) to maximum weigth (wx). Default values of limits are 1 and
    %           10. You can internally modify them as you wish figure;  -> I
    %           set it to scale between 1 & 5
    
    %           'wcb':  Weigth colorbar in a colormap BONE plotted in left side
    %           -> looks weird
    
    % things to edit -> colourmap possibly; colourbar; get size of labels to match the nodes;
    % node colour?
    figure;
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    set(0,'DefaultAxesFontSize',10)
    set(0, 'DefaultAxesLineWidth', 1)
    set(0,'DefaultAxesTitleFontWeight','normal');
    f_PlotEEG_BrainNetwork1(nch, ijw, 'w_wn2wx', 'wcb');  %function is in matlab_functions folder
    %save
    title(['Best Channel Combinations for ', TASK_NAMES{t}])
    saveas(gcf,[PLOTS, 'BESTCHANS_', TASKS{t},FILENAME,'.png'])
    close(gcf)
    
    
   % top 10 channel combis 
     comb = zeros(31);

    for ab = 1:length(effect_sizes_all)
        for ch = 1:length(effect_sizes_all(ab).max10_ind)
            comb(effect_sizes_all(ab).max10_ind(ch,1),effect_sizes_all(ab).max10_ind(ch,2)) = comb(effect_sizes_all(ab).max10_ind(ch,1),effect_sizes_all(ab).max10_ind(ch,2))+1;
        end
    end
    figure;
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    set(0,'DefaultAxesFontSize',10)
    set(0, 'DefaultAxesLineWidth', 1)
    set(0,'DefaultAxesTitleFontWeight','normal');
    imagesc(comb(ORDER,ORDER))%Order it so 31 at correct spot
    ax = gca;
    ax.YDir = 'normal';
    xticks(1:31)
    yticks(1:31)
    xticklabels(1:31)
    yticklabels(1:31)
    ylabel('channels');
    xlabel('channels');
    caxis([0 6]) %i don't think there were ever more than 5 times were it was the same so limits set to 6 should be fine but could delete this part
    colormap(jet(6))
    axis square
    colorbar
    title(['Occurences of Channel Combinations with highest Effect Sizes (Top 10), (',TASK_NAMES{t}, ')'])
    saveas(gcf,[PLOTS, 'TOP10CHANS_', TASKS{t},FILENAME,'.png'])
    close(gcf)
    
    for i = 1:length(effect_sizes_all)
        erp_best(i,1,:) = mean(effect_sizes_all(i).data_cond1,2);
        eff_best(i,:) = effect_sizes_all(i).maxeff;
        erp_best(i,2,:) = mean(effect_sizes_all(i).data_cond2,2);
    end 
    mean_best = squeeze(mean(erp_best));
    mean_eff_best = mean(eff_best);
    figure;
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    set(0,'DefaultAxesFontSize',10)
    set(0, 'DefaultAxesLineWidth', 1)
    set(0,'DefaultAxesTitleFontWeight','normal');
    plot(times, mean_eff_best,'LineWidth', .7, 'color','k')
    hold on
    plot(times, mean_best(1,:),'LineWidth', .7, 'color', col1)
    plot(times, mean_best(2,:),'LineWidth', .7, 'color', col2)
    title('ERP for Best Channel Combination')
    ylabel('amplitude [\muV]');
    xlabel('time [ms]');
    title(['Grand Average in over best Channel Combinations, ', TASK_NAMES{t}])
    legend('best effect size mean',CONDS{1}, CONDS{2})
    saveas(gcf,[PLOTS, 'TOPCHANS_ERP_', TASKS{t},FILENAME, '.png'])
    close(gcf)
    
    
%     comb = zeros(32);
%     for ab = 1:length(effect_sizes_all)
%         for ch = 1:length(effect_sizes_all(ab).max10_ind)
%             if effect_sizes_all(ab).max10_ind(ch,1) == effect_sizes_all(ab).max10_ind(ch,2)
%                 effect_sizes_all(ab).max10_ind(ch,2) = 32;
%             end
%             comb(effect_sizes_all(ab).max10_ind(ch,1),effect_sizes_all(ab).max10_ind(ch,2)) = comb(effect_sizes_all(ab).max10_ind(ch,1),effect_sizes_all(ab).max10_ind(ch,2))+1;
%         end
%     end
%     
%     elec =  [arrayfun(@num2str,1:31,'uni',false), 'Ref'];
%     schemaball(elec,comb*0.1)
%     title(TASKS{t})
%    
%     figure;
%     myColorMap = jet(length(comb));
%     circularGraph(comb,'Colormap',myColorMap,'Label',elec);
%     title(TASKS{t})
end