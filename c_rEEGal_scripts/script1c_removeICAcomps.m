%% remove ICA components previously tagged as bad components

clear all; close all; clc;

%% paths
MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
addpath([MAINPATH, 'eeglab2021.1\'])
PATHIN = [MAINPATH, 'out\\ICA\\out\\'];

cd(MAINPATH);

%% general stuff

TASKS = {'a','b','c','d'};
SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};
load([MAINPATH,'out\\badchans_preICA.mat'])
FILT = [.3,30];
SAMP = 100;

ICA_CORRECT = 1;
%% loop through tasks/participants
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for t = 1:length(TASKS)
    PATHOUT = [MAINPATH, 'out\\task_', TASKS{t}];

    for PP = 1:length(SUBJ)
        
        %load and remove bad ICA components
        if isfile([PATHIN, SUBJ{PP},'_',TASKS{t}, '_ICA.set'])
            EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t}, '_ICA.set'],'filepath',PATHIN);
            [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
            
            EEG = pop_subcomp( EEG, [unique(EEG.badcomps)], 0);
            
            EEG = pop_eegfilt( EEG, FILT(1), 0, [], [0], 0, 0, 'fir1', 0); %high pass
            EEG = pop_eegfilt( EEG, 0, FILT(2), [], [0], 0, 0, 'fir1', 0); %low pass
            EEG = pop_resample( EEG, SAMP);
            
            if badchans(strcmp({badchans.SUBJ}, SUBJ{PP})&strcmp({badchans.TASK},TASKS{t})).COUNT > 0
                bad = [badchans(strcmp({badchans.SUBJ}, SUBJ{PP}) & strcmp({badchans.TASK}, TASKS{t})).LABELS];
                bad(bad>20) = bad(bad>20)-1;
                EEG = pop_interp(EEG, [bad], 'spherical');
            end
            
            EEG.setname = ([SUBJ{PP},'_',TASKS{t}]);
         
            EEG = pop_saveset(EEG, 'filename', [EEG.setname, '_ICAcorrected.set'], 'filepath', PATHOUT);
            [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
        end
    end
end
eeglab redraw;

cd([MAINPATH, 'scripts'])
    open('script2a_task_a')
    open('script2bc_task_bc')
    open('script2d_task_d')