%% script_3:
% extract epochs for the two oddball tasks (b & c)
% artifact rejection using amplitude criterion & joint probability;
% saves data set for each condition for each task & participant
% plots ERPs for task a


clear all; close all; clc;

%% paths
MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
addpath([MAINPATH, 'eeglab2021.1\'])
TASKS = {'a'};

PATHIN = [MAINPATH, 'out\\task_', TASKS{1}, '\\'];
        if ~exist([MAINPATH, 'plots\\task_', TASKS{1}])
            mkdir([MAINPATH, 'plots\\task_', TASKS{1}])
        end
        if ~exist([PATHIN, 'epoched\\'])
            mkdir([PATHIN, 'epoched\\'])
            mkdir([PATHIN, 'epoched\\onset\\']) %for the ones that only include the stimulus onset but not the rest of the tones
        end
        PLOTS = [MAINPATH, '\\plots\\task_', TASKS{1},'\\'];
        PATHOUT = [PATHIN, 'epoched\\'];
cd(MAINPATH);


%% general stuff


SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};
EVENTS = {'S  1', 'S  2'}; %event triggers
CONDS = {'attended', 'unattended'}; %labels for events


%epoching limits
EP_from = -.2; %epoch beginning
EP_to = .8; %epoch end
%baseline limit
BASE_from = -200;
%for articfact rejection
AMP = 100;
JT = 3;


%for plotting
PLOT = 0;% set to 1 if plots are wanted, set to 0 if no plots wanted

col1 = [0.9290 0.6940 0.1250]; %colours, idk I just kinda like these
col2 = [0.4940 0.1840 0.5560];
SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size

ORDER = [1:3,32,4:9,33,10:12,34,13:15,35,16:17,36,37,18:20,38,21:22,39:40,23:25,41,26:28,42,29:31,43:44]; %to reorder data so the order on the plots matches that on the pad

ICA= 1;
if ICA == 1
    FILENAME = '_ICAcorrected';
else 
    FILENAME = '_noICA';
end
%%

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

        
ab = 1; 
%% loop through participants

for PP = 1:length(SUBJ)
    
    
    %% load data
    if isfile([PATHIN,SUBJ{PP},'_',TASKS{1},FILENAME, '.set'])
        EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{1},FILENAME, '.set'],'filepath',PATHIN);
        [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
        
         
    %save how many answers participants got right      
    answers(ab).SUBJ = SUBJ{PP};
    answers(ab).correct = (sum(strcmp({EEG.event.type}, 'S 13'))+sum(strcmp({EEG.event.type}, 'S 14'))+sum(strcmp({EEG.event.type}, 'S 15'))...
                          -sum(strcmp({EEG.event(1:find(strcmp({EEG.event.type}, 'S  7'),1, 'last')).type}, 'S  9')))/...
                          (sum(strcmp({EEG.event.type}, 'S  9'))-sum(strcmp({EEG.event(1:find(strcmp({EEG.event.type}, 'S  7'),1, 'last')).type}, 'S  9'))); %minus the 10 practice trials;
    ab = ab+1;
    
    %% just first tone onset:
    
    EEG = pop_epoch( EEG, {'S  9'}, [EP_from EP_to], 'epochinfo', 'yes');
    EEG = pop_rmbase( EEG, [BASE_from 0] ,[]);
    %EEG = pop_eegthresh(EEG,1,[1:size(EEG.data,1)] ,-AMP,AMP,-0.2,0.799,0,1);
    EEG = pop_jointprob(EEG,1,[1:size(EEG.data,1)],JT, JT, 0, 1, 0,0);
    EEG.setname = ([SUBJ{PP},'_',TASKS{1}, '_onset']);
    [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
    EEG = pop_saveset(EEG, 'filename', [EEG.setname,FILENAME, '.set'], 'filepath', [PATHOUT,'onset\\']);
    eeglab redraw
        
    %only runs next part if you want to plot it, otherwise just
    %epoching and saving datasets for each condition
    if PLOT == 1
        % plot this part
        ind = length(ALLEEG);
        ERP = mean(ALLEEG(ind).data,3);
        GFP = std(mean(ALLEEG(ind).data,3));
        ax = ceil(max(abs(ERP(:))));
        %ordering data by how the electrodes are organised on the pad ->
        ERP1 = ERP;
        ERP1(size(ERP1,1)+1:length(ORDER),:) = nan;
        ERP1 = ERP1(ORDER,:); %reorder stuff to match order of electrodes on the pads
        
        
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        set(0,'DefaultAxesFontSize',10)
        set(0, 'DefaultAxesLineWidth', 1)
        set(0,'DefaultAxesTitleFontWeight','normal');
        for i = 1:size(ERP1,1)
            subplot(11,4,i)
            plot(EEG.times, ERP1(i,:),'LineWidth', .7, 'color', col1);
            ylim([-ax, ax]);
            yticks([-ax 0 ax])
            xlim([-200 800])
            box off;
            axis off;
        end
        axis on;
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        cd(PLOTS);
        saveas(gcf,[SUBJ{PP},'_',TASKS{1},FILENAME,'_onset.png'])
        close(gcf)
        
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        set(0,'DefaultAxesFontSize',10)
        set(0, 'DefaultAxesLineWidth', 1)
        set(0,'DefaultAxesTitleFontWeight','normal');
        plot(EEG.times, ERP, 'LineWidth', .5, 'color', 'k')
        hold on
        plot(EEG.times, GFP,'LineWidth', 2, 'color', col1)
        hold off
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        ylim([-ax, ax]);
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME,'_onsetGFP.png'])
        close(gcf)
        
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        pop_cEEGrid_multiplot(EEG,[],[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);
        saveas(gcf,[SUBJ{PP},'_', TASKS{1},FILENAME,'_onset2.png'])
        close(gcf)
    end
    %% attended vs unattended
    
    ind = length(ALLEEG);
    
    %for attended/unattended tones.
    right = (3*EEG.srate)/5; %right stream
    left = (3*EEG.srate)/4; %left stream
    
    %probably not the most efficient way to do this but whatever..
    %getting latencies for onsets of attended/unattended tones
    lat = [];
    for e = 1:length(EVENTS)
        l = 1;
        r = 1;
        for i = 1:length(ALLEEG(ind-1).event)-1
            a = ALLEEG(ind-1).event(i+1).latency;
            if strcmp(ALLEEG(ind-1).event(i).type, EVENTS{e}) && strcmp(ALLEEG(ind-1).event(i+1).type, 'S  9')
                if e == 1
                    lat.aleft(l:l+2) = a+left:left:a+(left*3); %left attended
                    lat.uaright(r:r+3) = a+right:right:a+(right*4); %right unattended
                else
                    lat.aright(r:r+3) = a+right:right:a+(right*4); %right attended
                    lat.ualeft(l:l+2) = a+left:left:a+(left*3); %left unattended
                end
                l = l+3;
                r = r+4;
            end
        end
    end
    lat.attended = [lat.aright, lat.aleft]; %all attended
    lat.unattended = [lat.uaright, lat.ualeft]; %all unattended
    
    %add attended/unattended to .event structure
    for e = 1:length(CONDS)
        tmp = ALLEEG(ind-1);
        a = length(tmp.event);
        for i = 1:length(lat.(CONDS{e}))
            tmp.event(a+i).type = CONDS{e};
            tmp.event(a+i).latency = lat.(CONDS{e})(i);
        end
        %added to the end so now need to be sorted by latency, so everything is in the right order
        [~,index] = sortrows([tmp.event.latency].');
        tmp.event = tmp.event(index); clear index
        
        %epoch around attended/unattended
        tmp = pop_epoch( tmp, CONDS(e), [EP_from EP_to], 'epochinfo', 'yes');
        tmp = pop_rmbase( tmp, [BASE_from 0] ,[]); 
        
        %artefact rejection
        %tmp = pop_eegthresh(tmp,1,[1:size(tmp.data,1)] ,-AMP,AMP,-0.2,0.799,0,1);
        tmp = pop_jointprob(tmp,1,[1:size(tmp.data,1)],JT, JT, 0, 1, 0,0);
        
        %save data sets for attended/unattended
        tmp.setname = ([SUBJ{PP},'_',TASKS{1},'_', CONDS{e}]);
        EEG = pop_saveset(tmp, 'filename', [tmp.setname,FILENAME, '.set'], 'filepath', [PATHOUT]);
        [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
    end
    eeglab redraw;
    
    
    %only runs next part if you want to plot it, otherwise just
    %epoching and saving datasets for each condition
    if PLOT == 1
        %plot attended/unattended
        ERP=[];
        GFP = [];
        ind = length(ALLEEG);
        ERP(1,:,:) = mean(ALLEEG(ind-1).data,3); %mean attended
        ERP(2,:,:) = mean(ALLEEG(ind).data,3);   %mean unattended
        ERP(3,:,:) = ERP(2,:,:)-ERP(1,:,:); %difference unattended-attended
        GFP(1,:) = std(mean(ALLEEG(ind-1).data,3));
        GFP(2,:) = std(mean(ALLEEG(ind).data,3));
        
        %ordering data by how the electrodes are organised on the pad ->
        ERP1 = ERP;
        ERP1(:,size(ERP1,2)+1:length(ORDER),:) = nan;
        ERP1 = ERP1(:,ORDER,:); %reorder stuff to match order of electrodes on the pads
        
        
        %% Plotting
        ax = ceil(max(abs(ERP(:)))); %for axis limits
        
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        set(0,'DefaultAxesFontSize',10)
        set(0, 'DefaultAxesLineWidth', 1)
        set(0,'DefaultAxesTitleFontWeight','normal');
        for i = 1:size(ERP1,2)
            subplot(11,4,i)
            plot(EEG.times, squeeze(ERP1(1,i,:)),'LineWidth', .7, 'color', col1); %plot attended trials
            hold on
            plot(EEG.times,squeeze(ERP1(2,i,:)),'LineWidth', .7, 'color', col2); %plot unattended trials
            plot(EEG.times,squeeze(ERP1(3,i,:)),'LineWidth', .7, 'color', 'k'); %difference
            
            ylim([-ax, ax]);
            yticks([-ax 0 ax])
            xlim([-200 800])
            box off;
            axis off;
        end
        axis on;
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        legend('attended', 'unattended', 'difference', 'Orientation', 'horizontal','Location', 'best', 'Box', 'off')
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME,'_attunatt.png'])
        close(gcf)
        
        
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        set(0,'DefaultAxesFontSize',10)
        set(0, 'DefaultAxesLineWidth', 1)
        set(0,'DefaultAxesTitleFontWeight','normal');
        subplot(2,1,1)
        plot(EEG.times, squeeze(ERP(1,:,:)), 'LineWidth', .5, 'color', 'k')
        hold on
        plot(EEG.times, GFP(1,:),'LineWidth', 2, 'color', col1)
        hold off
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        ylim([-ax, ax]);
        title(CONDS{1})
        subplot(2,1,2)
        plot(EEG.times, squeeze(ERP(2,:,:)), 'LineWidth', .5, 'color', 'k')
        hold on
        plot(EEG.times, GFP(2,:),'LineWidth', 2, 'color', col1)
        hold off
        title(CONDS{2})
        xticks([0 200 400 600 800])
        ylabel('amplitude [\muV]');
        xlabel('time [ms]');
        ylim([-ax, ax]);
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME,'_GFP.png'])
        close(gcf)
        
        STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

        
        [STUDY ALLEEG] = std_editset( STUDY, [], 'commands',{{'index',1,'load',[PATHOUT,SUBJ{PP},'_',TASKS{1},'_', CONDS{1}, '.set']},{'index',2,'load',[PATHOUT,SUBJ{PP},'_',TASKS{1},'_', CONDS{2}, '.set']},{'index',1,'subject','1'},{'index',2,'subject','1'},{'index',1,'condition',CONDS{1}},{'index',2,'condition',CONDS{2}}},'updatedat','on','rmclust','on');
        CURRENTSTUDY = PP;
        EEG = ALLEEG;
        CURRENTSET = 1:length(EEG);
        [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'savetrials','on','erp','on','spec','on','specparams',{'specmode','fft','logtrials','off'},'erpim','on','erpimparams',{'nlines',10,'smoothing',10});
        figure;
        set(gcf,'Position', [0,0, SZ(3), SZ(4)])
        [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, 'multiplotER', 'erp' ,{ALLEEG(1).chanlocs.labels},[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);
        saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME, '_attunatt2.png'])
        close(gcf)
    end
       STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

    end
end

%save correct answers as table
writetable(struct2table(answers),[PATHOUT, 'correctanswers.csv'])
