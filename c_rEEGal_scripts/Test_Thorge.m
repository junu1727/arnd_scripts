FILENAME = '_noICA';
TASKS = {'a','b','c','d'}; %ERP-tasks
TNAMES = {'Directional Hearing','Passive Oddball','Active Oddball','Sentence Congruity'};
NAMEPAIRS = {'_Standard','_Target'};
PATHIN = pwd;
SUBJ = {'jiew36';'iwbq34';'nvla90';'ncqp45'};

WinStart = [80 110 300 300]; %time windows of the ERPs (N100, MMN, P300, N400)
WinStop  = [150 220 600 600];
ITERATIONS = 1; %how many times will the trials used for the GED be taken randomly? For debugging, keep at 1
lo = 0; %how many channels will be removed? (if more than 15 are removed, the GED will contain only real numbers. Why, I do not know)

for s = 1:length(SUBJ)
    %load ICA cleaned dataset
    %subj{s} = strrep(list(s).name, '_alpha.set', '');
    subj =SUBJ{s};
    
    %standard
    fname = sprintf('%s_c%s%s.set',subj,NAMEPAIRS{1},FILENAME)
    EEG_s = pop_loadset('filename', fname, 'filepath',...
        PATHIN);
    
    %target
    fname = sprintf('%s_c%s%s.set',subj,NAMEPAIRS{2},FILENAME);
    EEG_t = pop_loadset('filename', fname, 'filepath',...
        PATHIN);
    
    %% initialize covariance matrices (one for each trial)
    hz = linspace(0,EEG.srate,EEG.pnts);
    [allCovS,allCovR] = deal( zeros(EEG.trials,EEG.nbchan,EEG.nbchan) );
    
    %% select the data like Arnd
    
    %take the number of trials from the condition with fewer trials
    if EEG_t.trials < EEG_s.trials
        TRIALS = EEG_t.trials;
        pickTrial = randperm(EEG_s.trials,TRIALS);
        EEG_s = pop_select(EEG_s,'trial',pickTrial);
    else
        TRIALS = EEG_s.trials;
        pickTrial = randperm(EEG_t.trials,TRIALS);
        EEG_t = pop_select(EEG_t,'trial',pickTrial);
    end
    
    START = find(EEG_t.times == WinStart(2));
    STOP =  find(EEG_t.times == WinStop(2));
    
    EEG_t.data = EEG_t.data(:,START:STOP,:);
    EEG_s.data = EEG_s.data(:,START:STOP,:);
    EEG_t.pnts = length(START:STOP);
    EEG_s.pnts = length(START:STOP);
    
    S = zeros(EEG_s.nbchan);
    R = zeros(EEG_s.nbchan);
    
    for t=1:size(EEG_t.data,3)
        %compute the S covariance matrix
        seg = EEG_t.data(:,:,t);
        seg = seg - mean(seg,2);
        allCovS(t,:,:) = seg*seg' / (size(seg,2)-1);
        S = S + (seg*seg'/(size(seg,2)-1));
    end
    S = S/t;
    for t=1:size(EEG_s.data,3)
        %compute the R covariance matrix
        segfa = EEG_s.data(:,:,t);
        segfa = seg - mean(segfa,2);
        allCovR(t,:,:) = seg*seg' / (size(seg,2)-1);
        R = R + (segfa*segfa'/(size(segfa,2)-1));
    end
    R = R/t;
    
    %regularization of covariance matrices
    gamma = 0.1;
    Rr = R * (1-gamma) + gamma*eye(size(R));
    Sr = S * (1-gamma) + gamma*eye(size(S));
    
    [W,L] = eig(Sr,Rr);
    [eigvals,sidx]=sort(diag(L),'descend');
    eigvecs = W(:,sidx);
    
    % clean R
    meanR = squeeze(mean(allCovR)); % average covariance
    dists = zeros(EEG_s.trials,1); % vector of distances to mean
    for segi=1:size(allCovR,1)
        r = allCovR(segi,:,:);
        % Euclidean distance
        dists(segi) = sqrt( sum((r(:)-meanR(:)).^2) );
    end
    % finally, average trial-covariances together, excluding outliers
    covR = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));
    %covR = squeeze(mean( allCovR ,1));
    
    % clean S
    meanS = squeeze(mean(allCovS)); % average covariance
    dists = zeros(EEG_t.trials,1); % vector of distances to mean
    for segi=1:size(allCovS,1)
        r = allCovS(segi,:,:);
        % Euclidean distance
        dists(segi) = sqrt( sum((r(:)-meanS(:)).^2) );
    end
    covS = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));
    %covS = squeeze(mean( allCovS ,1));
    
    %% now for the GED
    
    %testing area
    
    %NOTE: You can test PCA on these data by using only covS, or only covR,
    % in the eig() function.
    
    %     % eig and sort
    %     [evecs,evals] = eig(covS,covR); % here is where we are getting imaginary numbers for some reason
    %     [evals,sidx] = sort(diag(evals),'descend');
    %     evecs = evecs(:,sidx);
    %
    %     % apply some regularization
    %     gamma = 0.1
    %     covRr = covR * (1-gamma) + gamma*eye(size(covR));
    %     covSr = covS * (1-gamma) + gamma*eye(size(covS));
    %
    %     % eig and sort
    %     [evecs,evals] = eig(covSr,covRr); % here is where we are getting imaginary numbers for some reason
    %     [evals,sidx] = sort(diag(evals),'descend');
    %     evecs = evecs(:,sidx);
    
    %%% compute the component time series
    % for the multiplication, the data need to be reshaped into 2D
    data2D = reshape(EEG_t.data,EEG.nbchan,[]);
    
    compts = eigvecs(:,1)' * data2D;
    % and then reshaped back into trials
    compts = reshape(compts,EEG_t.pnts,EEG_t.trials);
end