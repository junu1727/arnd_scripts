%% script_1 ICA
%run ICA & add ICA weights to raw data, then save these files

clear all; close all; clc;
%% paths

MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
addpath([MAINPATH, 'eeglab2021.1\'])
cd(MAINPATH);

%% general stuff
TASKS = {'a','b','c','d', 'artifact'};

SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};

load([MAINPATH,'out\\badchans.mat']);

%% loop through tasks/participants
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for t = 1:length(TASKS)
    PATHIN = [MAINPATH,'out\\task_', TASKS{t},'\\']; %different folder for each task
    PATHOUT = [MAINPATH,'out\\ICA\\'];
        for PP = 1:length(SUBJ)
            
            %load data filtered at 1 & 30 Hz with bridges < 10, sampling rate at 100Hz and
             %interpolated bad channels
            if isfile([PATHIN, SUBJ{PP},'_',TASKS{t}, '_preICA.set'])& ~isfile([PATHOUT, SUBJ{PP},'_', TASKS{t}, '_ICA.set'])
           
             TMP = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t}, '_preICA.set'],'filepath',[PATHIN]);

            %% ICA
            TMP = eeg_regepochs(TMP);
            TMP = pop_jointprob(TMP,1,[1:size(TMP.data,1)],3, 3, 0, 1, 0,0);
            %run ICA but restrict number of ICs to that of good channels
            %(otherwise it gave weird results)
            TMP = pop_runica(TMP, 'icatype', 'runica', 'extended', 1, 'pca',length(TMP.chanlocs)-badchans(strcmp({badchans.SUBJ}, SUBJ{PP}) & strcmp({badchans.TASK}, TASKS{t})).COUNT);
            
            %raw data 
             EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t}, '.set'],'filepath',[PATHIN]);

            %cut jaw bit from artifact set
            if strcmp(TASKS{t}, 'artifact')
                 c = find(strcmp({EEG.event.type}, 'S  1'), 1,'first'); 
                d = find(strcmp({EEG.event.type}, 'S  2'), 1,'last');        
                del = [EEG.event(c).latency-EEG.srate,EEG.event(d).latency-EEG.srate];
                EEG = pop_select( EEG, 'point',[del(1) del(2)] );
            end
            
            %add ica weights to raw data
            EEG.icawinv = TMP.icawinv;
            EEG.icasphere = TMP.icasphere;
            EEG.icaweights = TMP.icaweights;
            EEG.icachansind = TMP.icachansind;
            [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
                        
            %% save data sets with ica weights
            EEG.setname = ([SUBJ{PP},'_',TASKS{t}]);
            EEG = pop_saveset(EEG, 'filename', [EEG.setname, '_ICA.set'], 'filepath', PATHOUT);
            [ALLEEG,EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
            eeglab redraw;
            
        end
    end
end


%open next script
cd([MAINPATH, 'scripts'])
open('script1b_tagICAcomps.m')
