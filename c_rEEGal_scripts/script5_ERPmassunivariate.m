%% Analysis to check for time windows where the difference between two conditions is statistically significant
%mostly just adapted to work with how I previously scripted things; some of
%the prepping already done in effect size script

clear all; close all; clc;
%% paths
MAINPATH = 'O:\projects\alk_rEEGal\\';
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\');
addpath([EEGLABPATH,'eeglab2020_0']);
addpath([EEGLABPATH,'fieldtrip']);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
cd(MAINPATH);


%% general stuff
TASKS = {'a','b','c','d'};
SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56','serw12'};

EP_from = -200; %epoch beginning
EP_to = 800; %epoch


%%
ICA= 0;
if ICA == 1
    FILENAME = '_ICAcorrected';
else 
    FILENAME = '_noICA';
end

%% 

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for t = 1:length(TASKS)
    PATHIN = [MAINPATH, 'data\\out\\task_', TASKS{t},'\\epoched\\pre_bin\\' ];
    %create a path for the output
    if ~exist([MAINPATH, 'data\\out\\task_', TASKS{t},'\\epoched\\post_bin\\'])
        mkdir([MAINPATH, 'data\\out\\task_', TASKS{t},'\\epoched\\post_bin\\'])
    end
    PATHOUT = [MAINPATH, 'data\\out\\task_', TASKS{t},'\\epoched\\post_bin\\' ];
    
    if t == 1
        CONDS = {'attended', 'unattended'};
    elseif t== 2 || t == 3
        CONDS = {'target', 'standard'};
    elseif t == 4
        CONDS = {'incongruous', 'congruous'};
    end
    for PP = 1:length(SUBJ)
        if isfile([PATHIN, SUBJ{PP},'_',TASKS{t}, '_',CONDS{1},'_best', FILENAME,'.set'])
            %% load data
            best_condition1 = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t},'_',CONDS{1} ,'_best', FILENAME,'.set'],'filepath',PATHIN);
            best_condition2 = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t},'_',CONDS{2},'_best', FILENAME,'.set'],'filepath',PATHIN); 
            
            %rest of prepping data sets for toolbox already done in effect
            for z = 1:size(best_condition1.data,3)
                best_condition1.event(z).type = strrep(best_condition1.event(z).type,' ','');
            end
            
            for z = 1:size(best_condition2.data,3) 
                best_condition2.event(z).type = strrep(best_condition2.event(z).type,' ','');
            end
            
            best_condition1 = pop_saveset(best_condition1, 'filename', [best_condition1.setname,FILENAME, '.set'], 'filepath', PATHIN);
            best_condition2 = pop_saveset(best_condition2, 'filename', [best_condition2.setname, FILENAME,'.set'], 'filepath', PATHIN);
                 
            bintext = [PATHIN,'bin_',TASKS{t}, '.txt'];
            p_condition1 = bin_info2EEG([PATHIN,best_condition1.setname,FILENAME,'.set'],bintext,[PATHOUT,best_condition1.setname, FILENAME],3);
            p_condition2 = bin_info2EEG([PATHIN,best_condition2.setname,FILENAME,'.set'],bintext,[PATHOUT,best_condition2.setname, FILENAME],3);
            
        end
        
    end
    
    
    cd(PATHOUT)
    %this one opens a GUI where you choose all saved files from one task.
    %Choose, for example, all files from task b, regardless of whether it is
    %the standard or deviant condition. The substructure best_condition1.subject
    %tells the toolbox that these belong to one subject.
    GND = sets2GND('gui','bsln',[EP_from 0],'verblevel',3);
    %Group 1 and 2 in GND are standard and target condition, respectively.
    %This one creates a new group, being the difference between standard and target condition
    GND = bin_dif(GND,1,2,'dev - sta');
    
    %Now, we perform the actual test.
    GND = tmaxGND(GND,3,'time_wind',[EP_from EP_to],'verblevel',3,'plot_raster','no','save_GND','no'); %whithin-subject test for differences between conditions
    grands{1,1} = GND.grands_t(:,:,3); % saves the t-values over time in a variable
    crit_v{1,1} = [GND.t_tests.crit_t(1) GND.t_tests.crit_t(2)]; %saves the critical t-values
    %plotting grands and crit_v as two lines should now give you the t-values over time plus
    %the critical threshold. Whereever the graph exceeds the threshold, there
    %is a statistically significant difference between the conditions.
end







