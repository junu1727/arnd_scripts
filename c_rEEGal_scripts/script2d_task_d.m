%% script_4:
% extract epochs for the two oddball tasks (b & c)
% artifact rejection using amplitude criterion & joint probability;
% saves data set for each condition for each task & participant
% plots ERPs for task a

clear all; close all; clc;

%% paths
MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
addpath([MAINPATH, 'eeglab2021.1\'])

TASKS = {'d'};

   PATHIN = [MAINPATH, 'out\\task_', TASKS{1}, '\\'];
        if ~exist([MAINPATH, 'plots\\task_', TASKS{1}])
            mkdir([MAINPATH, 'plots\\task_', TASKS{1}])
        end
        if ~exist([PATHIN, 'epoched\\'])
            mkdir([PATHIN, 'epoched\\'])
        end
        PLOTS = [MAINPATH, 'plots\\task_', TASKS{1}, '\\'];
        PATHOUT = [PATHIN, 'epoched\\'];
cd(MAINPATH);

%% general stuff

TASKS = {'d'};


SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};

EVENTS = {'S 11', 'S 12'}; %event triggers
CONDS = {'congruous', 'incongruous'}; %labels for events

%epoching limits
EP_from = -.2; %epoch beginning
EP_to = .8; %epoch end
%baseline limit
BASE_from = -200;
%for articfact rejection
AMP = 100;
JT = 3;


%for plotting
PLOT = 0; % set to 1 if plots are wanted, set to 0 if no plots wanted

col1 = [0.9290 0.6940 0.1250]; %colours
col2 = [0.4940 0.1840 0.5560];
SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size
ORDER = [1:3,32,4:9,33,10:12,34,13:15,35,16:17,36,37,18:20,38,21:22,39:40,23:25,41,26:28,42,29:31,43:44]; %to reorder data so the order on the plots matches that on the pad


%load audiostruc
load('audiostruc.mat')
sampms = 1/100*1000;


%%
ICA= 1;
if ICA == 1
    FILENAME = '_ICAcorrected';
else 
    FILENAME = '_noICA';
end
%%

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

     
ab = 1; 
%% loop through participants

for PP = 1:length(SUBJ)
    
    %% load data
    if isfile([PATHIN, SUBJ{PP},'_',TASKS{1},FILENAME, '.set'])
        EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{1}, FILENAME,'.set'],'filepath',PATHIN);
        [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
        
        
        

        %% get latencies of last words in sentences
        %latency at sentence trigger
        word.lat1 = [EEG.event(strcmp({EEG.event.type}, 'S  9')).latency];
        
        %get stim names from log file
        word.stim = [];
        lf = importPresentationLog([MAINPATH,'log\\', SUBJ{PP},'_',TASKS{1},'-N400.log']); % importPresentationLog function is in matlab_functions folder :) 
        word.stim = {lf(strcmp({lf.event_type}, 'Sound')).code};
        
        %get length of stim from audiostruc file
        word.stimlen = [];
        for i = 1:length(word.stim)
            word.stimlen(i) = audiostruc.length(strcmp(audiostruc.names,word.stim(i)));
        end
        
        %get latency of last word in stim
        word.lat2 = [];
        for i = 1:length(word.stim)
            word.lat2(i) = word.lat1(i)+(word.stimlen(i)*sampms); %?
        end
        
        tmp = EEG;
        c = find(strcmp({tmp.event.type}, 'S  9')); %get all 'S 9' event

        %adding latencies of word onset & changes S9 to  congruous/
        %inconcruous
           i = 1;
        while i<=length(c)&c(i)+1<length(tmp.event)
            tmp.event(c(i)).latency = word.lat2(i);
            if strcmp(tmp.event(c(i)+1).type, EVENTS{1})
                tmp.event(c(i)).type = CONDS{1};
            elseif  strcmp(tmp.event(c(i)+1).type, EVENTS{2})
                tmp.event(c(i)).type = CONDS{2};
            end
            i = i+1;
        end
        
         a = find(strcmp({EEG.event.type}, 'S  9'),3);
         tmp.event(1:a(2)-1) = []; %remove practice trials 
         
         % save how many correct answers participant had
          answers(ab).SUBJ = SUBJ{PP};
          answers(ab).correct = (sum(strcmp({tmp.event.type}, 'S 11'))+sum(strcmp({tmp.event.type}, 'S 12')))/(sum(strcmp({EEG.event.type}, 'S  9'))-2);%minus the two practice trials;
          ab = ab+1;
          
        %% epoching
        for i = 1:length(CONDS)
            EEG = pop_epoch( tmp, CONDS(i), [EP_from EP_to], 'epochinfo', 'yes');
            
            %baseline removal
            EEG = pop_rmbase( EEG, [BASE_from 0]); %change baseline to before sentence onset
            %artefact rejection
            %EEG = pop_eegthresh(EEG,1,[1:length(EEG.chanlocs)] ,-AMP,AMP,-0.2,0.799,0,1);
            EEG = pop_jointprob(EEG,1,[1:length(EEG.chanlocs)],JT, JT, 0, 1, 0,0);
            
            EEG.setname = ([SUBJ{PP},'_',TASKS{1}, '_', CONDS{i}]);
            [ALLEEG,EEG,CURRENTSET] = eeg_store(ALLEEG, EEG);
            EEG = pop_saveset(EEG, 'filename', [EEG.setname,FILENAME, '.set'], 'filepath', PATHOUT);
            
        end
        eeglab redraw;
        
        %%
        
        %only runs next part if you want to plot it, otherwise just
        %epoching and saving datasets for each condition
        if PLOT == 1

            ERP=[];
            ind = length(ALLEEG);
            ERP(1,:,:) = mean(ALLEEG(ind-1).data,3); %mean congruous
            ERP(2,:,:) = mean(ALLEEG(ind).data,3);   %mean incongruous
            ERP(3,:,:) = ERP(1,:,:)-ERP(2,:,:); %difference congruous-incongruous
            GFP(1,:) = std(mean(ALLEEG(ind-1).data,3));
            GFP(2,:) = std(mean(ALLEEG(ind).data,3));
            
            %ordering data by how the electrodes are organised on the pad ->
            ERP1 = ERP;
            ERP1(:,size(ERP1,2)+1:length(ORDER),:) = nan;
            ERP1 = ERP1(:,ORDER,:); %reorder stuff to match order of electrodes on the pads
            
            
            %% Plotting
            ax = ceil(max(abs(ERP(:)))); %for axis limits
            
            figure;
            set(gcf,'Position', [0,0, SZ(3), SZ(4)])
            set(0,'DefaultAxesFontSize',10)
            set(0, 'DefaultAxesLineWidth', 1)
            set(0,'DefaultAxesTitleFontWeight','normal');
            for i = 1:size(ERP1,2)
                subplot(11,4,i)
                plot(EEG.times, squeeze(ERP1(1,i,:)),'LineWidth', .7, 'color', col1); %plot congrous trials
                hold on
                plot(EEG.times,squeeze(ERP1(2,i,:)),'LineWidth', .7, 'color', col2); %plot incongrous trials
                plot(EEG.times,squeeze(ERP1(3,i,:)),'LineWidth', .7, 'color', 'k'); %difference
                
                ylim([-ax, ax]);
                yticks([-ax 0 ax])
                xlim([-200 800])
                box off;
                axis off;
                hold off;
            end
            axis on;
            xticks([0 200 400 600 800])
            ylabel('amplitude [\muV]');
            xlabel('time [ms]');
            legend('congrous', 'incongrous', 'difference', 'Orientation', 'horizontal','Location', 'best', 'Box', 'off')
            saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME,'.png'])
            close(gcf)
            
            figure;
            set(gcf,'Position', [0,0, SZ(3), SZ(4)])
            set(0,'DefaultAxesFontSize',10)
            set(0, 'DefaultAxesLineWidth', 1)
            set(0,'DefaultAxesTitleFontWeight','normal');
            subplot(2,1,1)
            plot(EEG.times, squeeze(ERP(1,:,:)), 'LineWidth', .5, 'color', 'k')
            hold on
            plot(EEG.times, GFP(1,:),'LineWidth', 2, 'color', col1)
            hold off
            xticks([0 200 400 600 800])
            ylabel('amplitude [\muV]');
            xlabel('time [ms]');
            ylim([-ax, ax]);
            title(CONDS{1})
            subplot(2,1,2)
            plot(EEG.times, squeeze(ERP(2,:,:)), 'LineWidth', .5, 'color', 'k')
            hold on
            plot(EEG.times, GFP(2,:),'LineWidth', 2, 'color', col1)
            hold off
            title(CONDS{2})
            xticks([0 200 400 600 800])
            ylabel('amplitude [\muV]');
            xlabel('time [ms]');
            ylim([-ax, ax]);
            saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME,'_GFP.png'])
            close(gcf)
           
            STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

            
            [STUDY ALLEEG] = std_editset( STUDY, [], 'commands',{{'index',1,'load',[PATHOUT,SUBJ{PP},'_',TASKS{1},'_', CONDS{1}, '.set']},{'index',2,'load',[PATHOUT,SUBJ{PP},'_',TASKS{1},'_', CONDS{2}, '.set']},{'index',1,'subject','1'},{'index',2,'subject','1'},{'index',1,'condition',CONDS{1}},{'index',2,'condition',CONDS{2}}},'updatedat','on','rmclust','on');
            CURRENTSTUDY = PP;
            EEG = ALLEEG;
            CURRENTSET = 1:length(EEG);
            [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'savetrials','on','erp','on','spec','on','specparams',{'specmode','fft','logtrials','off'},'erpim','on','erpimparams',{'nlines',10,'smoothing',10});
            figure;
            set(gcf,'Position', [0,0, SZ(3), SZ(4)])
            [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, 'multiplotER', 'erp' ,{ALLEEG(1).chanlocs.labels},[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);
            saveas(gcf,[PLOTS, SUBJ{PP},'_', TASKS{1},FILENAME,'2.png'])
            close(gcf)
        end
                STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

    end
end

writetable(struct2table(answers),[PATHOUT, 'correctanswers.csv'])
