clear all; close all; clc;

%% paths
MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
addpath([MAINPATH, 'eeglab2021.1\'])
PATHIN = [MAINPATH, '\\out\\ICA\\'];

cd(MAINPATH);

%% general stuff

TEMPLATE = {'iorw84_artifact'};
% Template data set:
%1 bad chan/ 0 bridged
%ICs:
% eyeblink 2
% eye movement 1
% heart beat 3
% jaw 5
IC =[1,2,3];
%IC =[1,2,3,5];

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

 if ~exist([PATHIN, 'out\\'])
        mkdir([PATHIN, 'out\\'])
 end
PATHOUT = [PATHIN, 'out\\'];
[STUDY, ALLEEG] = create_study_fv(PATHIN,PATHIN,['ICA'],'_ICA',1);
[EEG ALLEEG CURRENTSET] = eeg_retrieve(ALLEEG,1);
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'retrieve',[1:length(ALLEEG)] ,'study',1); CURRENTSTUDY = 1;
eeglab redraw;
for ii = 1:length(IC)
    [CORRMAP(ii),STUDY,ALLEEG]=cEEGrid_corrmap(STUDY,ALLEEG,find(strcmp({ALLEEG.setname},TEMPLATE)), IC(ii),'chanlocs','','th','auto',...
        'ics',2,'title','','clname','blink','badcomps','yes', 'resetclusters','off', 'side','layout_reegal_new1', 'plot', 'off');
    
    
    for i=1:length(CORRMAP(ii).output.sets{2})
        a=CORRMAP(ii).output.sets{2}(i);
        
        if isempty(ALLEEG(a).badcomps) % EEG.badcomponents is empty
            ALLEEG(a).badcomps(i)=CORRMAP(ii).output.ics{2}(i);
        else
            nbc=length(ALLEEG(a).badcomps); %number of bad components already stored
            ALLEEG(a).badcomps(nbc+1)=CORRMAP(ii).output.ics{2}(i);
        end
    end
    
    for jj=1:length(CORRMAP(ii).datasets.index)
        if ~isempty(ALLEEG(jj).badcomps)           
            ALLEEG(jj).badcomps(ALLEEG(jj).badcomps==0)=[];
        end
        
    end
end

    CURRENTSTUDY = 0;

for PP = 1:length(ALLEEG)
    EEG = ALLEEG(PP);
    EEG = pop_saveset(EEG, 'filename', [EEG.setname, '_ICA.set'], 'filepath', PATHOUT);
end

cd([MAINPATH, 'scripts'])
open('script1c_removeICAcomps.m')