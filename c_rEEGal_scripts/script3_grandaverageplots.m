%% script 3
%load epoched data for all Participants & plot grand averages


%% paths
MAINPATH = 'O:\projects\alk_rEEGal\\';
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\');
addpath([EEGLABPATH,'eeglab2020_0']);
addpath([EEGLABPATH,'fieldtrip']);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');
cd(MAINPATH);

%% general stuff

TASKS = {'a','b','c', 'd'};
TASK_NAMES = {'Directional Hearing', 'Passive Oddball', 'Active Oddball', 'Sentence Congruity'};

SUBJ = {'nfrj56','jiew36','iwbq34','nvla90','ofwo38','ncqp45','ieql23','nvla19','uowq46','vmka39','ncjw72','nfws02','jowf13',...
    'iorw84','ejla67','tziq71', 'hfop02','iopw69','jifd78', 'buif23', 'uiod02','nvjr34','mkbg56', 'serw12'};

ORDER = [1:3,32,4:9,33,10:12,34,13:15,35,16:17,36:37,18:20,38,30,21,39:40,22:24,41,25:27,42,28:29,31,43:44]; %to reorder data so the order on the plots matches that on the pad
col1 = [0.9290 0.6940 0.1250]; %colours, idk I just kinda like these
col2 = [0.4940 0.1840 0.5560];
SZ = get(0,'ScreenSize'); %Screensize, I use this for the figure size


[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

%%
ICA= 1;
if ICA == 1
    FILENAME = '_ICAcorrected';
else
    FILENAME = '_noICA';
end


for t =3%:length(TASKS)
    PATHIN = [MAINPATH,'out\\task_', TASKS{t},'\\epoched\\'];
    PLOTS = [MAINPATH, 'plots\\task_', TASKS{t},'\\'];
    
    %ERP timewindows -> might still want to adjust those :)
    if t == 1
        CONDS = {'attended', 'unattended'};
        TIMEWINDOW = [80,150];
    elseif t== 2 || t == 3
        CONDS = {'target', 'standard'};
    elseif t == 4
        CONDS = {'incongruous', 'congruous'};
        TIMEWINDOW = [300, 600];
    end
    
    if t == 2
        TIMEWINDOW = [115,225];
    elseif t == 3
        TIMEWINDOW = [300, 600];
    end
    
    ERP = [];
    for PP = 1:length(SUBJ)
        for c = 1:length(CONDS)
            if isfile([PATHIN, SUBJ{PP},'_',TASKS{t}, '_', CONDS{c},FILENAME,'.set'])
                EEG = pop_loadset('filename',[SUBJ{PP}, '_', TASKS{t}, '_', CONDS{c}, FILENAME,'.set'],'filepath',[PATHIN]);
                [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
                ERP(PP,c,:,:) = mean(EEG.data,3);
            end
        end
    end
    eeglab redraw
    ERP(ERP(:,1,1,1) == 0,:,:,:) = [];
    %ERP(:,3,:,:) = ERP(:,2,:,:) - ERP(:,1,:,:);
    
    grand_av = squeeze(mean(ERP));
    grand_av(:,size(grand_av,2)+1:length(ORDER),:) = nan;
    grand_av = grand_av(:,ORDER,:); %reorder stuff to match order of electrodes on the pads
    ax(1) = ceil(max(grand_av(:))); %for axis limits
    ax(2) = floor(min(grand_av(:))); %for axis limits
    time(1) = dsearchn(EEG.times', TIMEWINDOW(1));
    time(2) = dsearchn(EEG.times', TIMEWINDOW(2));
    tt = (time(1):time(2));
    
    figure;
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    set(0,'DefaultAxesFontSize',10)
    set(0, 'DefaultAxesLineWidth', 1)
    set(0,'DefaultAxesTitleFontWeight','normal');
    for i = 1:size(grand_av,2)
        subplot(11,4,i)
        plot(EEG.times, squeeze(grand_av(1,i,:)),'LineWidth', .7, 'color', col1); %plot target trials
        hold on
        plot(EEG.times,squeeze(grand_av(2,i,:)),'LineWidth', .7, 'color', col2); %plot standard trials
        %plot(EEG.times,squeeze(grand_av(3,i,:)),'LineWidth', .7, 'color', 'k'); %difference
        tt = (time(1):time(2));
        
        %next bit plots a line at max value in ERP timewindow with ms
        %and amp next to it -> however this doesn't work well for all
        %channels (e.g task c only works well for top channels but not
        %bottom ones since there polarity is flipped),
        %just wanted to be better able to see whether the peaks are at about the expected timepoints
        if ~isnan(grand_av(1,i,:))
            if t == 3
                tt = tt(grand_av(1,i,time(1):time(2)) == max(grand_av(1,i,time(1):time(2))));
                str = [num2str(round(max(grand_av(1,i,time(1):time(2))),2)),'\muV, ',num2str(EEG.times(tt)), 'ms'];
                
            else
                tt = tt(grand_av(1,i,time(1):time(2)) == min(grand_av(1,i,time(1):time(2))));
                str = [num2str(round(min(grand_av(1,i,time(1):time(2))),2)),'\muV, ',num2str(EEG.times(tt)), 'ms'];
            end
            rectangle('Position',[EEG.times(tt) -1.5 .5 5])
            text(EEG.times(tt)+1,ax(1)-.3,str,'FontSize',5)
        end
        ylim([ax(2), ax(1)]);
        yticks([ax(2) 0 ax(1)])
        xlim([-200 800])
        box off;
        axis off;
    end
    axis on;
    xticks([0 200 400 600 800])
    ylabel('amplitude [\muV]');
    xlabel('time [ms]');
    title(['Grand Average ERPs, ', TASK_NAMES{t}])
    legend(CONDS{1}, CONDS{2}, 'Orientation', 'horizontal','Location', 'best', 'Box', 'off')
    
    %     saveas(gcf,[PLOTS, 'grand_av_', TASKS{t},FILENAME,'.png'])
    %     close(gcf)
    
    STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];
    [STUDY, ALLEEG] = create_study_fv(PATHIN,PATHIN,FILENAME,0);
    [EEG ALLEEG CURRENTSET] = eeg_retrieve(ALLEEG,1);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'retrieve',[1:length(ALLEEG)] ,'study',1); CURRENTSTUDY = 1;
    
    a = 1;
    for i = 1:2:length(STUDY.datasetinfo)
        STUDY.datasetinfo(i).subject = num2str(a);
        STUDY.datasetinfo(i+1).subject = num2str(a);
        if contains(STUDY.datasetinfo(i).filename,[TASKS{t},'_', CONDS{1}])
            STUDY.datasetinfo(i).condition = CONDS{1};
            STUDY.datasetinfo(i+1).condition = CONDS{2};
        else
            STUDY.datasetinfo(i).condition = CONDS{2};
            STUDY.datasetinfo(i+1).condition = CONDS{1};
        end
        a = a+1;
    end
    [STUDY ALLEEG] = std_editset( STUDY, ALLEEG, 'updatedat','off','rmclust','off' );
    saveSTUDY = STUDY;
    saveALLEEG = ALLEEG;
    CURRENTSTUDY = 1; EEG = ALLEEG; CURRENTSET = [1:length(EEG)];
    eeglab redraw
    
    STUDY = std_makedesign(STUDY, ALLEEG, 1, 'name','STUDY.design 1','delfiles','off','defaultdesign','off','variable1','condition','values1',{CONDS{2},CONDS{1}},'vartype1','categorical','subjselect',STUDY.subject  );
    %     [STUDY EEG] = pop_savestudy( STUDY, EEG, 'savemode','resave');
    [STUDY, ALLEEG] = std_precomp(STUDY, ALLEEG, {},'savetrials','on','recompute','on','erp','on','spec','on','specparams',{'specmode','fft','logtrials','off'},'erpim','on','erpimparams',{'nlines',10,'smoothing',10});
    if t == 1
        STUDY.design.variable.value{1,1} = CONDS{2};
        STUDY.design.variable.value{1,2} = CONDS{1};
    end
    figure;
    set(gcf,'Position', [0,0, SZ(3), SZ(4)])
    [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, 'multiplotER', 'erp' ,{ALLEEG(1).chanlocs.labels},[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);
    title(['Grand Average ERPs, ', TASK_NAMES{t}])
    %     saveas(gcf,[PLOTS, 'grand_av_', TASKS{t},FILENAME,'2.png'])
    %     close(gcf)
    
    %     STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];
    
end

%% plot ERP-topographies
MEANEEG = [];
for i = 1:length(ALLEEG)
    MEANEEG(:,:,i) = mean(ALLEEG(i).data,3);
end
MEANEEG = mean(MEANEEG,3);
EEG = ALLEEG(4);
EEG.data = MEANEEG;
OUTEEG = pop_cEEGrid_topoplot(EEG, {EEG.chanlocs.labels},[],[350 ], 'side', 'layout_reegal_new1','marker', 'numbers','colorbar','yes');

%% plot effect sizes
% saveALLEEG = ALLEEG;
% saveSTUDY  = STUDY;
v = 1;
ALLES = ALLEEG;
for i = 1:length(ALLEEG)
    ALLES(i).data = effect_sizes_all(v).effect_sizes(1:31,:);
    ALLES(i).setname = strrep(ALLES(i).setname,'Standard','Target');
    if mod(i,2) == 0
    v = v+1;
    end
end
pop_ceegrid_advanced_plotting_std(STUDY, ALLES, 'multiplotER', 'erp' ,{ALLEEG(1).chanlocs.labels},[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);
% how do I get the Effect Sizes into the EEG-structure for plotting?

%% plot effect size topographies
load([PATHIN,'c','_effectsizes_all',FILENAME,'.mat'], 'effect_sizes_all', 'channels', 'times'); %save effect sizes

MEANES = [];
for i = 1:length(effect_sizes_all)
    MEANES(:,:,i) = effect_sizes_all(i).effect_sizes(1:31,:);
end
MEANES = abs(mean(MEANES,3));
EEG = ALLEEG(4);
EEG.data = MEANES;
pop_cEEGrid_topoplot(EEG, {EEG.chanlocs.labels},[],350, 'side', 'layout_reegal_new1','marker', 'numbers','colorbar','yes');
pop_ceegrid_advanced_plotting_std(STUDY, EEG, 'multiplotER', 'erp' ,{ALLEEG(1).chanlocs.labels},[], 'side', 'layout_reegal_new1', 'zoom', 'yes', 'linewidth', 0.7);

cd([MAINPATH, 'scripts\\'])
open('script4_effectsizes.m')