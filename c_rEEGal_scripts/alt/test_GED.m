clear all; close all; clc

%% set some paths/variables
special_N1 = 0;

%time window of expected ERP
WinStart = [80 110 300 300];
WinStop  = [150 220 600 600];

MAINPATH = '\\daten.uni-oldenburg.de\psychprojects$\Neuro\Anna Lena Knoll\data\\';
FILENAME = '_noICA';
EEGLABPATH = fullfile('C:\Users\arnd\Desktop\toolboxes_matlab\eeglab2020_0');
addpath(EEGLABPATH);
addpath('C:\Users\arnd\Desktop\Martin\tempeltools');

TASKS = {'a','b','c','d'};
TNAMES = {'Directional Hearing','Passive Oddball','Active Oddball','Sentence Congruity'};
NAMEPAIRS = {'_unattended','_attended';'_Standard','_Target';'_Standard','_Target';'_congruous','_incongruous'};

SUBJ = {'jiew36';'iwbq34';'nvla90';'ofwo38';'ncqp45';'ieql23';'nvla19';'uowq46';'vmka39';'ncjw72';...
    'iorw84';'ejla67';'tziq71';'hfop02';'iopw69';'jifd78';'buif23';'uiod02';'nvjr34';'serw12'};

%% get the highest effect size values and their indices from the bipolar channels
best_chan_max = cell(20,4);
for i = 1:length(TASKS)
    PATHIN_ES = [MAINPATH, 'out\\task_', TASKS{i},'\\epoched\\' ];
    load([PATHIN_ES,TASKS{i},'_effectsizes_all',FILENAME,'.mat'], 'effect_sizes_all', 'channels', 'times');
    
    
    %get the maximum effect size per subject
    max_es = [];
    max_es = [effect_sizes_all.max10_eff]'; %extract the max. 10 effect size values per subject
    max_es = reshape(max_es,10,length(max_es)/10); % %reshape to columns
    max_es = max_es(1,:)'; % only use the first column, those are the highest values per subject
    
    % get the index of the channel with the highest effect size
    for g = 1:length(effect_sizes_all)
        [~, ind{g,i}] = find(strcmp(effect_sizes_all(g).max10_chan(1),effect_sizes_all(g).CHANNEL_COMBI ));
        best_chan_max{g,i} = max_es(g); %contains all max. effect sizes per subject, per task
        best_chan_time{g,i} = smoothdata(effect_sizes_all(g).effect_sizes(ind{g,i},:),2,'movmean',4); %contains all effect sizes per subject, per task, over time
        %the max-data is from before smoothing! That means you won't find the
        %max_values in the time-data!
    end
    getlength(i) =  length(effect_sizes_all);
    
    for s = 1:length(SUBJ)
        if contains(SUBJ{s},{effect_sizes_all.SUBJ})
            countSBJ(s,i) = 1;
        else
            countSBJ(s,i) = 0;
        end
    end
    esNAMES{i} = {effect_sizes_all.SUBJ};
    
end

%% now get the GED components

ged_maps_time = cell(20,4);
ged_maps_max  = cell(20,4);

for i = 1:length(TASKS)
    
    PATHIN = ['O:\projects\alk_rEEGal\out\task_',TASKS{i},'\epoched\'];
    
    % load mat files containing EEG and channel locations
    Target_S   = dir([PATHIN,'*',NAMEPAIRS{i,2},'_noICA.set']);
    Standard_R = dir([PATHIN,'*',NAMEPAIRS{i,1},'_noICA.set']);
    
    %re-order the entries in the lists to match the order in the
    %effect_size_all-matrix
    readSBJ = [];
    for v =  1:length(Target_S)
        readSBJ{v} = Target_S(v).name(1:6);
    end
    for v =  1:length(Target_S)
        orderNum(v) = find(strcmp(esNAMES{1,i}{1,v},readSBJ));
    end
    
    Target_S   = Target_S  (orderNum);
    Standard_R = Standard_R(orderNum);
    
    for xd = 1:length(Target_S)
        
        EEG_S = pop_loadset('filename',Target_S(xd).name,'filepath',PATHIN);
        EEG_R = pop_loadset('filename',Standard_R(xd).name,'filepath',PATHIN);
        
        TRIALS_S = EEG_S.trials;
        TRIALS_R = EEG_R.trials;
        
        %     EEG_S = pop_select( EEG_S, 'channel',[1:10 18:31 ]);
        %     EEG_R = pop_select( EEG_R, 'channel',[1:10 18:31 ]);
        
        EEG_org_S = EEG_S;
        EEG_org_R = EEG_R;
        
        START = find(EEG_S.times == WinStart(i));
        STOP =  find(EEG_S.times == WinStop(i));
        
        EEG_S.data = EEG_S.data(:,START:STOP,:);
        EEG_R.data = EEG_R.data(:,START:STOP,:);
        EEG_S.pnts = length(START:STOP);
        EEG_R.pnts = length(START:STOP);
        
        % initialize covariance matrices (one for each trial)
        allCovS = zeros(TRIALS_S,EEG_S.nbchan,EEG_S.nbchan);
        allCovR = zeros(TRIALS_R,EEG_R.nbchan,EEG_R.nbchan);
        
        % loop over trials (data segments) and compute each covariance matrix
        for triali = 1:TRIALS_S
            tmpdat = EEG_S.data(:,:,triali);
            tmpdat = tmpdat-mean(tmpdat,2);
            allCovS(triali,:,:) = tmpdat*tmpdat' / EEG_S.pnts;
        end
        for triali = 1:TRIALS_R
            tmpdat = EEG_R.data(:,:,triali);
            tmpdat = tmpdat-mean(tmpdat,2);
            allCovR(triali,:,:) = tmpdat*tmpdat' / EEG_R.pnts;
        end
        
        % clean R
        meanR = squeeze(mean(allCovR));  % average covariance
        dists = zeros(TRIALS_R,1);
        for segi=1:size(allCovR,1)
            r = allCovR(segi,:,:);
            % Euclidean distance
            dists(segi) = sqrt( sum((r(:)-meanR(:)).^2) );
        end
        
        % finally, average trial-covariances together, excluding outliers
        covR = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));
        
        % clean S
        meanS = squeeze(mean(allCovS));  % average covariance
        dists = zeros(TRIALS_S,1);     % vector of distances to mean
        for segi=1:size(allCovS,1)
            r = allCovS(segi,:,:);
            % Euclidean distance
            dists(segi) = sqrt( sum((r(:)-meanS(:)).^2) );
        end
        
        covS = squeeze(mean( allCovS(zscore(dists)<3,:,:) ,1));
        
        % GED
        [W, L] = eig(covS, covR);
        if ~isreal(L)
            REALNUM(xd,i) = 1;
        else
            REALNUM(xd,i) = 0;
        end
        
        [eigvals, sidx] = sort(diag(L), 'descend');
        eigvecs = W(:,sidx);
        
        % Component activations (all trials, all latencies)
        data = EEG_org_S.data;
        data = reshape(data, size(data,1), size(data,2)*size(data,3));
        compact_tar = zeros(length(eigvecs),size(EEG_org_S.data,2), size(EEG_org_S.data,3));
        for e = 1:length(eigvecs)
            act = eigvecs(:,e)' * data;
            compact_tar(e,:,:) = reshape(act, size(EEG_org_S.data,2), size(EEG_org_S.data,3));
        end
        
        % Component activations (all trials, all latencies)
        data = EEG_org_R.data;
        data = reshape(data, size(data,1), size(data,2)*size(data,3));
        compact_sta = zeros(length(eigvecs),size(EEG_org_R.data,2), size(EEG_org_R.data,3));
        for e = 1:length(eigvecs)
            act = eigvecs(:,e)' * data;
            compact_sta(e,:,:) = reshape(act, size(EEG_org_R.data,2), size(EEG_org_R.data,3)); %component*samples*trials. 1 component map for each trial, there are as many components as there are channels
        end
        %calc effect sizes from components
        
        for c = 1:size(compact_sta,1)
            if special_N1 == 0
                sta = squeeze(compact_tar(c,:,:));
                dev = squeeze(compact_sta(c,:,:));
            else
                sta = squeeze(compact_tar(c,timeStart:timeEnd,:));
                dev = squeeze(compact_sta(c,bsWin,:)); %this is the baseline of all tone-mixes. It is used as a null-comparison
            end
            diff_all{c,1} = mes(sta',dev','hedgesg','isDep',0); %where the magic happens. Calc the effect size over time
        end
        
        if special_N1 == 0
            effect_size_matrix = zeros(size(diff_all,1),100);
        else
            effect_size_matrix = zeros(size(diff_all,1),20);
        end
        
        for x = 1:size(compact_tar,1)
            effect_size_matrix(x,:) = diff_all{x}.hedgesg;
        end
        
        effect_size_matrix  = smoothdata(effect_size_matrix,2,'movmean',4);
        
        co = 1;
        for c = 1:size(effect_size_matrix,1)
            if special_N1 == 0
                [maxAll(co).pks maxAll(co).locs] = max(abs(effect_size_matrix(c,START:STOP))); % find the peaks (largest, positive effect size)
            else
                [maxAll(co).pks maxAll(co).locs] = max(abs(effect_size_matrix(c,:))); % find the peaks (largest, positive effect size)
            end
            
            co = co + 1;
        end
        
        % get the n best maps and the largest value
        [ged_maps_max{xd,i}, best_loc] = maxk([maxAll.pks],1);
        ged_maps_time{xd,i} = effect_size_matrix(best_loc,:);
        
    end
end

%plot the effect sizes of the GEDs
figure
sgtitle('Effect sizes GED')
for c = 1:length(TASKS)
    subplot(2,2,c)
    title(TNAMES{c},'FontWeight','bold')
    hold on
    mean_GED = [];
    for f = 1:getlength(c)
        plot(EEG_org_R.times,abs(ged_maps_time{f,c}))
        mean_GED(f,:) = abs(ged_maps_time{f,c});
    end
    mean_GED_all(c,:) = mean(mean_GED,1);
    plot(EEG_org_R.times,mean_GED_all(c,:),'k','LineWidth',2)
    ylim([0 1]);
end


%plot the effect sizes of the best bipolar channels
figure
sgtitle('Effect sizes best bipolar channel')
for c = 1:length(TASKS)
    subplot(2,2,c)
    title(TNAMES{c},'FontWeight','bold')
    hold on
    mean_BIP = [];
    for f = 1:getlength(c)
        plot(EEG_org_R.times,abs(best_chan_time{f,c}))
        mean_BIP(f,:) = abs(ged_maps_time{f,c});
    end
    mean_BIP_all(c,:) = mean(mean_BIP,1);
    plot(EEG_org_R.times,mean_BIP_all(c,:),'k','LineWidth',2)
        ylim([0 1]);
end

!boxplot, um die max-Werte zu vergleichen
!!!!!!!Dazu muss auch nochmal getested werden, wie genau Anna Lena die Matrix berechnet hat. IM gRunde sollte ich einfach ihren Code kopieren und f�r die GEDs einsetzen

%% testing area

% figure
% imagesc(covR)
% figure
% imagesc(covS)






