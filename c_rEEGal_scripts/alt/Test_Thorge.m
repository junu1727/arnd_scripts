FILENAME = '_noICA';
TASKS = {'a','b','c','d'}; %ERP-tasks
TNAMES = {'Directional Hearing','Passive Oddball','Active Oddball','Sentence Congruity'};
NAMEPAIRS = {'_Standard','_Target'};
PATHIN = pwd;
SUBJ = {'jiew36';'iwbq34';'nvla90';'ncqp45'};

WinStart = [80 110 300 300]; %time windows of the ERPs (N100, MMN, P300, N400)
WinStop  = [150 220 600 600];
ITERATIONS = 1; %how many times will the trials used for the GED be taken randomly? For debugging, keep at 1
lo = 0; %how many channels will be removed? (if more than 15 are removed, the GED will contain only real numbers. Why, I do not know)

for s = 1:len
    %load ICA cleaned dataset
    %subj{s} = strrep(list(s).name, '_alpha.set', '');
    subj{s} =list(s).name;
    EEG = pop_loadset('filename', [subj{s}], 'filepath',...
        PATHIN);
   
    %% initialize covariance matrices (one for each trial)
    hz = linspace(0,EEG.srate,EEG.pnts);
    [allCovS,allCovR] = deal( zeros(EEG.trials,EEG.nbchan,EEG.nbchan) );
    
    % loop over trials (data segments) and compute each covariance matrix
    for triali=1:EEG.trials
        
        % cut out a segment
        tmpdat = alphafilt(:,:,triali);
        
        % mean-center
        tmpdat = tmpdat-mean(tmpdat,2);
        
        % add to S tensor
        allCovS(triali,:,:) = tmpdat*tmpdat' / EEG.pnts;
        
        % repeat for broadband data
        tmpdat = EEG.data(:,:,triali);
        tmpdat = tmpdat-mean(tmpdat,2);
        allCovR(triali,:,:) = tmpdat*tmpdat' / EEG.pnts;
    end
    
    %%% illustration of cleaning covariance matrices
    
    % clean R
    meanR = squeeze(mean(allCovR)); % average covariance
    dists = zeros(EEG.trials,1); % vector of distances to mean
    for segi=1:size(allCovR,1)
        r = allCovR(segi,:,:);
        % Euclidean distance
        dists(segi) = sqrt( sum((r(:)-meanR(:)).^2) );
    end
    % finally, average trial-covariances together, excluding outliers
    covR = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));
    %covR = squeeze(mean( allCovR ,1));
    
    
    % clean S
    meanS = squeeze(mean(allCovS)); % average covariance
    dists = zeros(EEG.trials,1); % vector of distances to mean
    for segi=1:size(allCovS,1)
        r = allCovS(segi,:,:);
        % Euclidean distance
        dists(segi) = sqrt( sum((r(:)-meanS(:)).^2) );
    end
    covS = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));
    %covS = squeeze(mean( allCovS ,1));
    
    %% now for the GED
    
    %%% NOTE: You can test PCA on these data by using only covS, or only covR,
    % in the eig() function.
    
    % eig and sort
    [evecs,evals] = eig(covS,covR); % here is where we are getting imaginary numbers for some reason
    [evals,sidx] = sort(diag(evals),'descend');
    evecs = evecs(:,sidx);
    
    %%% compute the component time series
    % for the multiplication, the data need to be reshaped into 2D
    data2D = reshape(EEG.data,EEG.nbchan,[]);
    
    compts = evecs(:,1)' * data2D;
    % and then reshaped back into trials
    compts = reshape(compts,EEG.pnts,EEG.trials);

end