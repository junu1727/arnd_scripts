for s = 1:len
%load ICA cleaned dataset
%subj{s} = strrep(list(s).name, '_alpha.set', '');
subj{s} =list(s).name;
EEG = pop_loadset('filename', [subj{s}], 'filepath',...
PATHIN);



% signal parameters in Hz
peakfreq = 10; % "alpha"
fwhm = 5; % full-width at half-maximum around the alpha peak
PRUNE = 3; % artifact rejection threshold in SD (for ICA only)
HP_erp = 0.1; % cut-off frequency high-pass filter [Hz]
LP_erp = 40; % cut-off frequency low-pass filter [Hz]
HP_ord = 500; % high-pass filter order depends on sampling rate
LP_ord = 100



% apply low and high pass filter
EEG = pop_firws(EEG, 'fcutoff', LP_erp, 'ftype', 'lowpass', 'wtype', ...
'hann', 'forder', LP_ord);
EEG = pop_firws(EEG, 'fcutoff', HP_erp, 'ftype', 'highpass', 'wtype', ...
'hann', 'forder', HP_ord);

% create dummy events and epoch data to these dummy events
%EEG = pop_select( EEG, 'channel',1:8);
EEG = pop_select( EEG, 'channel',1:17);
EEG = eeg_regepochs(EEG, 'recurrence', 1, 'eventtype', '999');
EEG = eeg_checkset(EEG, 'eventconsistency');

% remove epochs with artefacts to improve ICA training
EEG = pop_jointprob(EEG, 1, [1:size(EEG.data,1)], PRUNE, PRUNE, 0, 1, 0);

% filter the data around 10 Hz
alphafilt = filterFGx(EEG.data,EEG.srate,peakfreq,fwhm);



% initialize covariance matrices (one for each trial)
hz = linspace(0,EEG.srate,EEG.pnts);
[allCovS,allCovR] = deal( zeros(EEG.trials,EEG.nbchan,EEG.nbchan) );



% loop over trials (data segments) and compute each covariance matrix
for triali=1:EEG.trials

% cut out a segment
tmpdat = alphafilt(:,:,triali);

% mean-center
tmpdat = tmpdat-mean(tmpdat,2);

% add to S tensor
allCovS(triali,:,:) = tmpdat*tmpdat' / EEG.pnts;

% repeat for broadband data
tmpdat = EEG.data(:,:,triali);
tmpdat = tmpdat-mean(tmpdat,2);
allCovR(triali,:,:) = tmpdat*tmpdat' / EEG.pnts;
end



%%% illustration of cleaning covariance matrices



% clean R
meanR = squeeze(mean(allCovR)); % average covariance
dists = zeros(EEG.trials,1); % vector of distances to mean
for segi=1:size(allCovR,1)
r = allCovR(segi,:,:);
% Euclidean distance
dists(segi) = sqrt( sum((r(:)-meanR(:)).^2) );
end



% finally, average trial-covariances together, excluding outliers
%covR = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));



covR = squeeze(mean( allCovR ,1));



%%%%% Normally you'd repeat the above for S; ommitted here for simplicity




% clean S
meanS = squeeze(mean(allCovS)); % average covariance
dists = zeros(EEG.trials,1); % vector of distances to mean
for segi=1:size(allCovS,1)
r = allCovS(segi,:,:);
% Euclidean distance
dists(segi) = sqrt( sum((r(:)-meanS(:)).^2) );
end
covS = squeeze(mean( allCovS ,1));



%% now for the GED



%%% NOTE: You can test PCA on these data by using only covS, or only covR,
% in the eig() function.



% eig and sort
[evecs,evals] = eig(covS,covR);
[evals,sidx] = sort(diag(evals),'descend');
evecs = evecs(:,sidx);



%%% compute the component time series
% for the multiplication, the data need to be reshaped into 2D
data2D = reshape(EEG.data,EEG.nbchan,[]);





compts = evecs(:,1)' * data2D;
% and then reshaped back into trials
compts = reshape(compts,EEG.pnts,EEG.trials);



%% channelpower
channelpower = abs(fft(EEG.data,[],2)).^2;
channelpowerAve = squeeze(mean(channelpower,3));



%%% power spectrum
comppower = abs(fft(compts,[],1)).^2;
comppowerAve = squeeze(mean(comppower,2));



%%% component map
compmap = evecs(:,1)' * covS;
% flip map sign
[~,se] = max(abs( compmap ));
compmap = compmap * sign(compmap(se));



%% visualization



figure;plot(evals)



figure;



pos=[1 3 5]
for k=1:3
subplot(3,2,pos(k))




compts = evecs(:,k)' * data2D;
% and then reshaped back into trials
compts = reshape(compts,EEG.pnts,EEG.trials);



%%% power spectrum
comppower = abs(fft(compts,[],1)).^2;
comppowerAve = squeeze(mean(comppower,2));
plot(hz,comppowerAve(1:length(hz))/max(comppowerAve(1:length(hz))),'r','linew',3)
hold all
%plot(hz,channelpowerAve(1,1:length(hz))/max(channelpowerAve(1,1:length(hz))),'k','linew',1)
%plot(hz,channelpowerAve(3,1:length(hz))/max(channelpowerAve(3,1:length(hz))),'k','linew',1)
%plot(hz,channelpowerAve(6,1:length(hz))/max(channelpowerAve(6,1:length(hz))),'k','linew',1)
%plot(hz,channelpowerAve(11,1:length(hz))/max(channelpowerAve(11,1:length(hz))),'k','linew',1)
%plot(hz,channelpowerAve(13,1:length(hz))/max(channelpowerAve(13,1:length(hz))),'k','linew',1)
%plot(hz,channelpowerAve(16,1:length(hz))/max(channelpowerAve(16,1:length(hz))),'k','linew',1)





legend({'Source','Electrodes'},'box','off')
xlabel('Frequency (Hz)')
ylabel('Power (norm to max power)')
set(gca,'xlim',[0 40])



% font size for all axes
set(get(gcf,'children'),'fontsize',13)
subplot(3,2,pos(k)+1)



%%% component map
compmap = evecs(:,k)' * covS;
% flip map sign
[~,se] = max(abs( compmap ));
compmap = compmap * sign(compmap(se));
imagesc(compmap)




end



figure
hold all
for k=1:17
compts = evecs(:,k)' * data2D;
% and then reshaped back into trials
compts = reshape(compts,EEG.pnts,EEG.trials);



%%% power spectrum
comppower = abs(fft(compts,[],1)).^2;
comppowerAve = squeeze(mean(comppower,2));
plot(hz,comppowerAve(1:length(hz))/max(comppowerAve(1:length(hz))))
xlabel('Frequency (Hz)')
ylabel('Power (norm to max power)')
set(gca,'xlim',[0 40])
end
end