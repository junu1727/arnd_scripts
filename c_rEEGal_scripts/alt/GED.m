%%
% 
%  MATLAB code accompanying the paper:
%     A tutorial on generalized eigendecomposition for denoising, 
%     contrast enhancement, and dimension reduction in multichannel electrophysiology
% 
%   Mike X Cohen (mikexcohen@gmail.com)
%
% 
%  No MATLAB or 3rd party toolboxes are necessary.
%  The files emptyEEG_S.mat, filterFGx.m, and topoplotindie.m need to be in 
%    the MATLAB path or current directory.
% 
%%

% a clear MATLAB workspace is a clear mental workspace
close all; clear
addpath('O:\projects\alk_rEEGal\GED\');


%%
% load mat files containing EEG and channel locations
PATHIN = 'O:\projects\alk_rEEGal\out\task_c\epoched\';
Target_S = dir([PATHIN,'*Target_noICA.set']);
Standard_R = dir([PATHIN,'*Standard_noICA.set']);

for xd = 1:length(Target_S)

EEG_S = pop_loadset('filename',Target_S(1).name,'filepath',[PATHIN]);
EEG_R = pop_loadset('filename',Standard_R(1).name,'filepath',[PATHIN]);

%% ----------------------------- %%

% signal parameters in Hz
peakfreq = 10; % "alpha"
fwhm     =  5; % full-width at half-maximum around the alpha peak

%%% create frequency-domain Gaussian
hz = linspace(0,EEG_S.srate,EEG_S.pnts);
s  = fwhm*(2*pi-1)/(4*pi); % normalized width
x  = hz-peakfreq;          % shifted frequencies
fg = exp(-.5*(x/s).^2);    % gaussian

%% topoplot of alpha power

channelpower = abs(fft(EEG_S.data,[],2)).^2;
channelpowerAve = squeeze(mean(channelpower,3));

% vector of frequencies
hz = linspace(0,EEG_S.srate/2,floor(EEG_S.pnts/2)+1);

%% Create a covariance tensor (one covmat per trial)

% filter the data around 10 Hz
alphafilt = filterFGx(EEG_S.data,EEG_S.srate,10,4);

% initialize covariance matrices (one for each trial)
allCovS = zeros(EEG_S.trials,EEG_S.nbchan,EEG_S.nbchan);
allCovR = allCovS;
% Number of trials is different between conditions!!!!!!!!!!


% loop over trials (data segments) and compute each covariance matrix
for triali=1:EEG_S.trials
    
    % cut out a segment
    tmpdat = EEG_S.data(:,:,triali);
    
    % mean-center
    tmpdat = tmpdat-mean(tmpdat,2);
    
    % add to S tensor
    allCovS(triali,:,:) = tmpdat*tmpdat' / EEG_S.pnts;
    
    % repeat for broadband data
    tmpdat = EEG_R.data(:,:,triali);
    tmpdat = tmpdat-mean(tmpdat,2);
    allCovR(triali,:,:) = tmpdat*tmpdat' / EEG_R.pnts;
end
    
%%% illustration of cleaning covariance matrices

% clean R
meanR = squeeze(mean(allCovR));  % average covariance
dists = zeros(EEG_S.trials,1);     % vector of distances to mean !!!!!!!!!!!!!!!! S for the trials, not R
for segi=1:size(allCovR,1)
    r = allCovR(segi,:,:);
    % Euclidean distance
    dists(segi) = sqrt( sum((r(:)-meanR(:)).^2) );
end

% finally, average trial-covariances together, excluding outliers
covR = squeeze(mean( allCovR(zscore(dists)<3,:,:) ,1));

% clean S
meanS = squeeze(mean(allCovS));  % average covariance
dists = zeros(EEG_S.trials,1);     % vector of distances to mean
for segi=1:size(allCovS,1)
    r = allCovS(segi,:,:);
    % Euclidean distance
    dists(segi) = sqrt( sum((r(:)-meanS(:)).^2) );
end

covS = squeeze(mean( allCovS ,1));

%% now for the GED

%%% NOTE: You can test PCA on these data by using only covS, or only covR,
%         in the eig() function.

% eig and sort
[evecs,evals] = eig(covS,covR);
[evals,sidx]  = sort(diag(evals),'descend');
evecs = evecs(:,sidx);

%%% compute the component time series
% for the multiplication, the data need to be reshaped into 2D
data2D = reshape(EEG_S.data,EEG_S.nbchan,[]);
compts = evecs(:,1)' * data2D;
% and then reshaped back into trials
compts = reshape(compts,EEG_S.pnts,EEG_S.trials);

%%% power spectrum
comppower = abs(fft(compts,[],1)).^2;
comppowerAve = squeeze(mean(comppower,2));

%%% component map
compmap = evecs(:,1)' * covS;
% flip map sign
[~,se] = max(abs( compmap ));
compmap = compmap * sign(compmap(se));

%% visualization


figure(5), clf

subplot(241)
plot(evals,'ks-','markersize',10,'markerfacecolor','r')
axis square, box off
set(gca,'xlim',[0 20.5])
title('GED scree plot')
xlabel('Component number'), ylabel('Power ratio (\lambda)')
% Note that the max eigenvalue is <1, 
% because R has more overall energy than S.

subplot(243)
topoplotIndie(compmap,EEG_S.chanlocs,'numcontour',0);
title('Alpha component')
set(gca,'clim',[-1 1]*10)

subplot(244)
topoplotIndie(channelpowerAve(:,dsearchn(hz',10)),EEG_S.chanlocs,'numcontour',0);%,'electrodes','numbers');
title('Elecr. power (10 Hz)')
set(gca,'clim',[1e8 1.5e9])




subplot(212), cla, hold on
plot(hz,sourcepowerAve(1:length(hz))/max(sourcepowerAve(1:length(hz))),'m','linew',3)
plot(hz,comppowerAve(1:length(hz))/max(comppowerAve(1:length(hz))),'r','linew',3)
plot(hz,channelpowerAve(31,1:length(hz))/max(channelpowerAve(31,1:length(hz))),'k','linew',3)
legend({'Source','Component','Electrode 31'},'box','off')
xlabel('Frequency (Hz)')
ylabel('Power (norm to max power)')
set(gca,'xlim',[0 80])

% font size for all axes
set(get(gcf,'children'),'fontsize',13)

%% done.