OutputFile = import_sources(iStudy, SurfaceFile, SourceFiles, [], [], [], [], [])
view_scouts('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s05_scouts\raw84Chan\results_191018_1001.mat', 'SelectedScouts')

view_topography([listPlotSource(end).folder,'\',listPlotSource(end).name], 'EEG', '3DElectrodes-Cortex');

[hFig, iFig, isNewFig] = bst_figures('CreateFigure',     iDS, FigureId, CreateMode, Constrains)



bst_figures('ViewResults',       [listPlotSource(end).folder,'\',listPlotSource(end).name])


        %%%%%%%%%%%%%%%%%
        listPlotSource = dir('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s05_scouts\raw84Chan\results_1*');
        export_matlab([listPlotSource(end).folder,'\',listPlotSource(end).name],'plotSource');
        res = bst_get('ResultsFile', [listPlotSource(end).folder,'\',listPlotSource(end).name])
        in_bst_results([listPlotSource(end).folder,'\',listPlotSource(end).name])
        view_surface_data([], [listPlotSource(end).folder,'\',listPlotSource(end).name])
        [hFig, iDS, iFig] = view_sources(plotSource.SurfaceFile)
        bst_figures('ViewResults',      res.Result(3).FileName)
        [hFig, iDS, iFig] = view_surface(plotSource.HeadModelFile)
        [hFig, iDS, iFig] = view_mri_3d([listPlotSource(end).folder,'\',listPlotSource(end).name], [],[], 'NewFigure')
         sSrcPsdProj = bst_process('CallProcess', 'process_project_sources', [listPlotSource(end).folder,'\',listPlotSource(end).name], [], ...
    'headmodeltype', 'surface'); 
        %%%%%%%%%%%%%%%%%



            ResultsCRight.maxDiff =  max(collect);
            ResultsCRight.CheckPair = checkPair;
            ResultsCRight.CheckPair(:,4) = {'0'};
            ResultsCRight.CheckPair(collect==max(collect),4) = {'1'};
            ResultsCRight.Pair = checkPair(collect==max(collect),1);
            ResultsCRight.SelectedChannels = [checkPair(collect==max(collect),2) checkPair(collect==max(collect),3)];
            ResultsCRight.SelectedValues = SelectedValues;
            ResultsCRight.SelectedValues(:,3) = 0;
            ResultsCRight.SelectedValues(collect==max(collect),3) = 1;
            
            
            figure
            subplot(2,1,1)
            script_view_sources([SubjectName,'\raw148chan\',listResults(1).name], 'cortex');
            figure_3d('SetStandardView', gcf, {'bottom'});
            subplot(2,1,2)
            figure_3d('SetStandardView', gcf, {'left'});
            saveas(gcf,[PATHOUT,'figures\',nameAtlas,num2str(ROI),'seed2.png']);

            figure(1)
            script_view_sources([SubjectName,'\raw148chan\',listResults(1).name], 'cortex');
            figure_3d('SetStandardView', gcf, {'left'});
            ax1 = gca;
            figure(2)
            script_view_sources([SubjectName,'\raw148chan\',listResults(1).name], 'cortex');
            figure_3d('SetStandardView', gcf, {'right'});
            ax2 = gca;
            f3 = figure(3); % Copy Axes To Subplots
            ax1_copy = copyobj(ax1,f3);
            subplot(1,2,1,ax1_copy)
            ax2_copy = copyobj(ax2,f3);
            subplot(1,2,2,ax2_copy)
            saveas(gcf,[PATHOUT,'figures\',nameAtlas,num2str(ROI),'seed.png']);

