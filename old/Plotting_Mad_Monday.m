

[EEG ALLEEG CURRENTSET] = eeg_retrieve(ALLEEG,1);
EEG = eeg_checkset( EEG );
pop_topoplot(EEG, 1, [100 180 500] ,'ergi95_b_ica_cleaned resampled',[2 2] ,0,'electrodes','on');

figure; pop_timtopo(EEG, [-200  790], [80 160 500], 'map','electrodes','on');
figure
subplot(2,2,1)




Arnd
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Prepare Data%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%use EEGlab structure, load template data
%% ERP

%%Time specifications:
Fs = 100;                   % samples per second
dt = 1/Fs;                   % seconds per sample
StopTime = 1;             % seconds
t = (0:dt:StopTime-dt)';     % seconds
%%Sine wave:
Fc1 = 15;         % hertz
Fc2 = 5;        % hertz

AMPnoise = 1;

xwave1 = cos(2*pi*Fc1*t);
% hertz
xwave2 = cos(2*pi*Fc2*t);

for s = 1:EEG.nbchan
    EEG.data(s,:,1) = xwave1 + (AMPnoise* randn(1, length(xwave1)))';
    EEG.data(s,:,2) = xwave2 + (AMPnoise* randn(1, length(xwave2)))';
end

%% Histogram
x = 1 + randn(maxC,1);
y = randn(200,1);
maxC = 5000;
nbins1 = 50;
nbins2 = 10;

%% Boxplot
box1 = randi([0,10],20,1);
box2 = randi([0,10],20,1);
boxdata = [box1 box2];
zeros1 = ones(20,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot Data%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure

subplot(2,2,1)

plot(EEG.times,EEG.data(1,:,1),'LineWidth',2)
hold on
plot(EEG.times,EEG.data(2,:,1),'LineWidth',2)

ylim([min(min(EEG.data(:,:,1))) 20])
ylabel('Amp [�V]','FontSize', 12, 'FontWeight', 'bold')
xlabel('Time [ms]','FontSize', 12, 'FontWeight', 'bold')
title('Two ERPs','FontSize', 12, 'FontWeight', 'bold')
legend('ERP1','ERP2','FontSize', 12, 'FontWeight', 'bold','Location','northeast')
xticks([-200 0 200 400 600 800])
xticklabels({'-200','0','200','400','600','800'})

subplot(2,2,4)

h1 = histogram(x,nbins1);
hold on
h2 = histogram(y,nbins2);
ylabel('Cookies eaten per day','FontSize', 12, 'FontWeight', 'bold')
xlabel('Bins','FontSize', 12, 'FontWeight', 'bold')
title('Two Histograms','FontSize', 12, 'FontWeight', 'bold')
legend('Group1','Group2','FontSize', 12, 'FontWeight', 'bold','Location','northeast')

subplot(2,2,3)

b1 = boxplot(boxdata);
hold on
scatter (boxdata(:,1)',ones(length(boxdata),1)')
ylabel('Cookies eaten per day','FontSize', 12, 'FontWeight', 'bold')
xlabel('Groups','FontSize', 12, 'FontWeight', 'bold')
title('Two Boxplots','FontSize', 12, 'FontWeight', 'bold')
legend('Group1','Group2','FontSize', 12, 'FontWeight', 'bold','Location','northeast')

sgtitle('EEG super plot','FontSize', 20, 'FontWeight', 'bold')
set(gcf,'color','w');

print();

