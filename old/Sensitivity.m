%% Simulate recordings. Results from PreapreSource
clear;
close all;
clc

SubjectName = 's01_fullCap'; %use any name, but it needs to start with "s"

%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MAINPATH = 'C:\Users\arnd\Desktop\toolboxes_matlab\'; %path to brainstorm3
PATHOUT = 'C:\Users\arnd\Desktop\arm_testing\Experiments\cEEGridSimulation\results_fullCap\';
PROTOCOLNAME = 'Ana'; %name of your current protocol in brainstorm
cd([MAINPATH,'brainstorm3\']);
brainstorm
fullOrROI = '1'; % 1 = full, 0 = ROI to view all vertices or only specified regions of interest
EarChannel = '0'; %1 = left, 2 = right, 0 = both (ear), 3 = FullCap; choose which side of sensors should be considered
atlas = '2'; % 2 = Desikan-Killiany 1 = Brodmann   0 = Destrieux (when adding a new atlas, set "timewindow" to [])
%%%%%%%%%%%%SETTINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol

if EarChannel == '1'
    channels=[15 50 42 12 59 64]; %channel indices  !!!!specific for the data from ana (Strophal paper)!!!!
    nameVar = 'Left';
elseif EarChannel == '2'
    channels=[61 53 9 36 47 14];
    nameVar = 'Right';
elseif EarChannel == '0'
    channels=[15 50 42 12 59 64 61 53 9 36 47 14];
    nameVar = 'Double';
else EarChannel == '3'
    channels=[1:64];
    nameVar = 'Full';
end

listSource = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*_ep_ar\results_MN*']); %lists of the variables created in PrepareSource.mat
listChannel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*_ep_ar\channel*']);
listHeadmodel = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\*_ep_ar\headmodel*']);

Source = load([listSource(1).folder,'\',listSource(1).name]);
CM = load([listChannel(1).folder,'\',listChannel(1).name]);
iChannels = 1:length(CM.Channel); %channel number according to your channel file
HM = load([listHeadmodel(1).folder,'\',listHeadmodel(1).name]);
HM.Gain = bst_gain_orient(HM.Gain, HM.GridOrient); %modify the gain-matrix, get constrained orientations (1 coordinate instead of xyz)

Source.ImageGridAmp= zeros(size(Source.ImageGridAmp)); %set source matrix to zero
Source.DataFile=[]; % needed to avoid brainstorm confusion, otherwise it might try to reload things

Source.Time = 1;
Source.ImageGridAmp = Source.ImageGridAmp(:,1); %computationally less expensive, make sure to only have 1 col. More is unnecessary, the signal is always 1
SourceDist = Source;
IMAGEGRIDAMP=Source.ImageGridAmp; %This is the source/scout. It is currently only zeros

%calculate the sensitivity for a source (pointwise or for a cluster (ROI
%from atlas)) for the ear-electrodes

if fullOrROI == '1' %for full mesh
    for  k= 1:size(Source.ImageGridAmp,1) %loop through every point on the brain mesh
        
        IMAGEGRIDAMP=Source.ImageGridAmp; %reset the matrix to the original 0-matrix every time
        IMAGEGRIDAMP(k,1)= 1; % set activity of one vertex to 1, the others stay 0
        F = zeros(length(CM.Channel), 1); % simulation matrix. CM is the channel model
        F(iChannels,:) = HM.Gain(iChannels,:) * IMAGEGRIDAMP; %for every channel, calc the forward model of the simulated source with amp=1 (leadfield*activation)
        
        collect=[];
        index=1;
        
        for ch=1:length(channels)
            for b=ch+1:length(channels)
                collect(index)=F(channels(ch),1)-F(channels(b),1); %calc every combination of electrodes
                index=index+1;
            end
        end
        
        SourceDist.ImageGridAmp(k,1) = max(collect); %TODO provide more flexibility for getting different channel configurations.
        
        if mod(k,1000)==0
            disp('Almost there')
        end
        
    end
    SourceDist.Comment= ['Sensitivity',nameVar,'GridFull']; %set the name referring to the chosen grid (left/right/double)
    
    SourceDist.ImageGridAmp = abs(SourceDist.ImageGridAmp);
    [a,b] = maxk(SourceDist.ImageGridAmp(:,1),100); %find the 100 highest values
    c = find(a > 250); %find unrealisticly high vlaues
    SourceDist.ImageGridAmp(b(c),1)=0; % delete them
    
    ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
    db_add(ProtocolInfo.iStudy, SourceDist, []); %this adds the new source projection to the current protocol
    
    %Here, we will have automatic plots of SourceDist, it does not work yet
    %         %SelectedChannels = {'E01','E02','E03','E04','E05','E06','E07','E08','E09','E10','E11','E12','E13','E14','E15','E16','E17','E18','E19','E20'}; %specify which channels will be displaey in the plot
    %         SelectedChannels = {'E001','E002'}; %specify which channels will be displayed in the plot
    %
    %             bst_figures('SetSelectedRows', SelectedChannels); %display the selected channels
    %             sSubject = bst_get('Subject', 's01');
    %             CortexFile = sSubject.Surface(sSubject.iCortex).FileName;
    %             hFigSurf = view_surface(CortexFile);
    %             view_surface(SourceDist, [], [], hFigSurf);
    %             [hFig, iDS, iFig] = script_view_sources('C:\Users\arnd\Desktop\toolboxes_matlab\brainstorm_db\Ana\data\s01\s01_ep_ar\results_190930_1150.mat', 'cortex');
    %             [hFig, iDS, iFig] = view_surface_data(CortexFile, b, [], 'NewFigure');
    %             figure_3d('ViewSensors', gcf, 1, 0); %specify that electrode-locations are shown, but their names are not
    %
else fullOrROI == '0' %only for Region of Interest. For new atlas: Put source result in Process --> Extract --> Scouts Time series --> choose atlas & generate m.-script
    
    sFiles = [];
    sFiles = {...
        [listSource(1).folder,'\',listSource(1).name]};
    
    % Start a new report
    bst_report('Start', sFiles);
    
    if atlas == '0'
        % Process: Scouts time series: [148 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Destrieux', {'G_Ins_lg_and_S_cent_ins L', 'G_Ins_lg_and_S_cent_ins R', 'G_and_S_cingul-Ant L', 'G_and_S_cingul-Ant R', 'G_and_S_cingul-Mid-Ant L', 'G_and_S_cingul-Mid-Ant R', 'G_and_S_cingul-Mid-Post L', 'G_and_S_cingul-Mid-Post R', 'G_and_S_frontomargin L', 'G_and_S_frontomargin R', 'G_and_S_occipital_inf L', 'G_and_S_occipital_inf R', 'G_and_S_paracentral L', 'G_and_S_paracentral R', 'G_and_S_subcentral L', 'G_and_S_subcentral R', 'G_and_S_transv_frontopol L', 'G_and_S_transv_frontopol R', 'G_cingul-Post-dorsal L', 'G_cingul-Post-dorsal R', 'G_cingul-Post-ventral L', 'G_cingul-Post-ventral R', 'G_cuneus L', 'G_cuneus R', 'G_front_inf-Opercular L', 'G_front_inf-Opercular R', 'G_front_inf-Orbital L', 'G_front_inf-Orbital R', 'G_front_inf-Triangul L', 'G_front_inf-Triangul R', 'G_front_middle L', 'G_front_middle R', 'G_front_sup L', 'G_front_sup R', 'G_insular_short L', 'G_insular_short R', 'G_oc-temp_lat-fusifor L', 'G_oc-temp_lat-fusifor R', 'G_oc-temp_med-Lingual L', 'G_oc-temp_med-Lingual R', 'G_oc-temp_med-Parahip L', 'G_oc-temp_med-Parahip R', 'G_occipital_middle L', 'G_occipital_middle R', 'G_occipital_sup L', 'G_occipital_sup R', 'G_orbital L', 'G_orbital R', 'G_pariet_inf-Angular L', 'G_pariet_inf-Angular R', 'G_pariet_inf-Supramar L', 'G_pariet_inf-Supramar R', 'G_parietal_sup L', 'G_parietal_sup R', 'G_postcentral L', 'G_postcentral R', 'G_precentral L', 'G_precentral R', 'G_precuneus L', 'G_precuneus R', 'G_rectus L', 'G_rectus R', 'G_subcallosal L', 'G_subcallosal R', 'G_temp_sup-G_T_transv L', 'G_temp_sup-G_T_transv R', 'G_temp_sup-Lateral L', 'G_temp_sup-Lateral R', 'G_temp_sup-Plan_polar L', 'G_temp_sup-Plan_polar R', 'G_temp_sup-Plan_tempo L', 'G_temp_sup-Plan_tempo R', 'G_temporal_inf L', 'G_temporal_inf R', 'G_temporal_middle L', 'G_temporal_middle R', 'Lat_Fis-ant-Horizont L', 'Lat_Fis-ant-Horizont R', 'Lat_Fis-ant-Vertical L', 'Lat_Fis-ant-Vertical R', 'Lat_Fis-post L', 'Lat_Fis-post R', 'Pole_occipital L', 'Pole_occipital R', 'Pole_temporal L', 'Pole_temporal R', 'S_calcarine L', 'S_calcarine R', 'S_central L', 'S_central R', 'S_cingul-Marginalis L', 'S_cingul-Marginalis R', 'S_circular_insula_ant L', 'S_circular_insula_ant R', 'S_circular_insula_inf L', 'S_circular_insula_inf R', 'S_circular_insula_sup L', 'S_circular_insula_sup R', 'S_collat_transv_ant L', 'S_collat_transv_ant R', 'S_collat_transv_post L', 'S_collat_transv_post R', 'S_front_inf L', 'S_front_inf R', 'S_front_middle L', 'S_front_middle R', 'S_front_sup L', 'S_front_sup R', 'S_interm_prim-Jensen L', 'S_interm_prim-Jensen R', 'S_intrapariet_and_P_trans L', 'S_intrapariet_and_P_trans R', 'S_oc-temp_lat L', 'S_oc-temp_lat R', 'S_oc-temp_med_and_Lingual L', 'S_oc-temp_med_and_Lingual R', 'S_oc_middle_and_Lunatus L', 'S_oc_middle_and_Lunatus R', 'S_oc_sup_and_transversal L', 'S_oc_sup_and_transversal R', 'S_occipital_ant L', 'S_occipital_ant R', 'S_orbital-H_Shaped L', 'S_orbital-H_Shaped R', 'S_orbital_lateral L', 'S_orbital_lateral R', 'S_orbital_med-olfact L', 'S_orbital_med-olfact R', 'S_parieto_occipital L', 'S_parieto_occipital R', 'S_pericallosal L', 'S_pericallosal R', 'S_postcentral L', 'S_postcentral R', 'S_precentral-inf-part L', 'S_precentral-inf-part R', 'S_precentral-sup-part L', 'S_precentral-sup-part R', 'S_suborbital L', 'S_suborbital R', 'S_subparietal L', 'S_subparietal R', 'S_temporal_inf L', 'S_temporal_inf R', 'S_temporal_sup L', 'S_temporal_sup R', 'S_temporal_transverse L', 'S_temporal_transverse R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Destrieux';
        
    elseif atlas == '1'
        % Process: Scouts time series: [24 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Brodmann-thresh', {'BA1 L', 'BA1 R', 'BA2 L', 'BA2 R', 'BA3a L', 'BA3a R', 'BA3b L', 'BA3b R', 'BA44 L', 'BA44 R', 'BA45 L', 'BA45 R', 'BA4a L', 'BA4a R', 'BA4p L', 'BA4p R', 'BA6 L', 'BA6 R', 'MT L', 'MT R', 'V1 L', 'V1 R', 'V2 L', 'V2 R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Brodmann-thresh';
        
    else atlas == '2'
        % Process: Scouts time series: [68 scouts]
        sFiles = bst_process('CallProcess', 'process_extract_scout', sFiles, [], ...
            'timewindow',     [], ...
            'scouts',         {'Desikan-Killiany', {'bankssts L', 'bankssts R', 'caudalanteriorcingulate L', 'caudalanteriorcingulate R', 'caudalmiddlefrontal L', 'caudalmiddlefrontal R', 'cuneus L', 'cuneus R', 'entorhinal L', 'entorhinal R', 'frontalpole L', 'frontalpole R', 'fusiform L', 'fusiform R', 'inferiorparietal L', 'inferiorparietal R', 'inferiortemporal L', 'inferiortemporal R', 'insula L', 'insula R', 'isthmuscingulate L', 'isthmuscingulate R', 'lateraloccipital L', 'lateraloccipital R', 'lateralorbitofrontal L', 'lateralorbitofrontal R', 'lingual L', 'lingual R', 'medialorbitofrontal L', 'medialorbitofrontal R', 'middletemporal L', 'middletemporal R', 'paracentral L', 'paracentral R', 'parahippocampal L', 'parahippocampal R', 'parsopercularis L', 'parsopercularis R', 'parsorbitalis L', 'parsorbitalis R', 'parstriangularis L', 'parstriangularis R', 'pericalcarine L', 'pericalcarine R', 'postcentral L', 'postcentral R', 'posteriorcingulate L', 'posteriorcingulate R', 'precentral L', 'precentral R', 'precuneus L', 'precuneus R', 'rostralanteriorcingulate L', 'rostralanteriorcingulate R', 'rostralmiddlefrontal L', 'rostralmiddlefrontal R', 'superiorfrontal L', 'superiorfrontal R', 'superiorparietal L', 'superiorparietal R', 'superiortemporal L', 'superiortemporal R', 'supramarginal L', 'supramarginal R', 'temporalpole L', 'temporalpole R', 'transversetemporal L', 'transversetemporal R'}}, ...
            'scoutfunc',      1, ...  % Mean
            'isflip',         1, ...
            'isnorm',         0, ...
            'concatenate',    1, ...
            'save',           1, ...
            'addrowcomment',  1, ...
            'addfilecomment', 1);
        
        nameAtlas = 'Desikan-Killiany';
        
    end
    
    listScout = dir([MAINPATH,'brainstorm_db\',PROTOCOLNAME,'\data\',SubjectName,'\s*\matrix_scout*']);
    AtlasROI = load([listScout(end).folder,'\',listScout(end).name]); %always load newest scouts
    
    prompt = {'Enter desired ROI:'};
    dlgtitle = nameAtlas;
    dims = [1 50];
    s = inputdlg(prompt,dlgtitle,dims);
    ROI=str2num(s{1,1});
    
    iChannels = 1:length(CM.Channel); %matrix, length of the number of channels
    
    TakeScout = AtlasROI.Atlas.Scouts(ROI(1)).Vertices; %store the index of all vertices in the ROI in TakeScout
    
    if length(ROI) > 1 %if more than one region was chosen, add the additional vertices to TakeScout
        for f = 2:length(ROI)
            TakeScout(end+1:end+length(AtlasROI.Atlas.Scouts(ROI(f)).Vertices)) = AtlasROI.Atlas.Scouts(ROI(f)).Vertices; %vertices of the current scout
        end
    end
    
    for  k= 1:size(TakeScout,2) %loop through every point of the scout
        
        IMAGEGRIDAMP = Source.ImageGridAmp; %This is the source/scout
        IMAGEGRIDAMP(TakeScout(k),1)= 1; % set activity of one vertex to 1, the others stay 0
        
        F = zeros(length(CM.Channel), 1); % Simulation matrix size(Source.ImageGridAmp,2)
        F(iChannels,1) = HM.Gain(iChannels,:) * IMAGEGRIDAMP;
        
        collect=[];
        index=1;
        
        for ch=1:length(channels)
            for b=ch+1:length(channels)
                collect(index)=F(channels(ch),1)-F(channels(b),1);
                index=index+1;
            end
        end
        
        SourceDist.ImageGridAmp(TakeScout(k),1) = max(collect);
    end
    
    SourceDist.Comment= ['Sensitivity',nameVar,'Grid, ROI = ',nameAtlas,' ',num2str(ROI)]; %set the name referring to the chosen grid & atlas
    
    SourceDist.ImageGridAmp = abs(SourceDist.ImageGridAmp);
    [a,b] = maxk(SourceDist.ImageGridAmp(:,1),100); %find the 100 highest values
    c = find(a > 250); %find unrealisticly high vlaues
    SourceDist.ImageGridAmp(b(c),1)=0; % delete them
    
    ProtocolInfo = bst_get('ProtocolInfo'); %needed for number of the current protocol
    db_add(ProtocolInfo.iStudy, SourceDist, []);
    
end

SourceGrid = nonzeros(SourceDist.ImageGridAmp); 
%this is the collection of all scout values, but additionally, non-zero accounts for values deleted in the previous step

%save all relevant variables
if fullOrROI == '0'
    if ~exist([PATHOUT,'ROI\',SubjectName,'\'])
        mkdir([PATHOUT,'ROI\',SubjectName,'\']);
    end
    save([PATHOUT,'ROI\',SubjectName,'\results_',nameVar,'Grid_ROI_',nameAtlas,' ',num2str(ROI),'.mat'],'SourceDist','SourceGrid','nameAtlas','ROI'); %nameAtlas and ROI for naming purposes in the next script
else fullOrROI == '1'
    if ~exist([PATHOUT,'fullBrain\',SubjectName,'\'])
        mkdir([PATHOUT,'fullBrain\',SubjectName,'\']);
    end
    save([PATHOUT,'fullBrain\',SubjectName,'\results_',nameVar,'Grid_fullBrain.mat'],'SourceDist','SourceGrid');
end

%continue with PlotAmps.mat

